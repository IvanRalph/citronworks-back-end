<?php

namespace App\Container\Billing;

use App\Exceptions\BraintreeException;
use App\Model\Freelancer\FreelancerCompany;
use Exception;
use Carbon\Carbon;
use Braintree\Subscription as BraintreeSubscription;
use Illuminate\Support\Facades\Log;

class SubscriptionBuilder
{
    /**
     * The model that is subscribing Employer or RegistrationRequest.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * The name of the subscription.
     * subscription like basic, premium etc..
     *
     * @var string
     */
    protected $name;

    /**
     * The name of the plan being subscribed to.
     *
     * @var string
     */
    protected $plan;

    /**
     * The coupon code being applied to the customer.
     *
     * @var string|null
     */
    protected $coupon;

    /**
     * Create a new subscription builder instance.
     *
     * @param $model
     * @param  string  $name
     * @param  string  $plan
     * @param $coupon
     */
    public function __construct($model, $name, $plan, $coupon)
    {
        $this->name = $name;
        $this->plan = $plan;
        $this->model = $model;
        $this->coupon = $coupon;
    }

    /**
     * @param  null  $token
     * @param  array  $customerOptions
     * @param  array  $subscriptionOptions
     * @return mixed
     * @throws Exception
     */
    public function create($token, array $customerOptions = [], array $subscriptionOptions = [])
    {
        $payload = $this->getSubscriptionPayload(
            $this->getBraintreeCustomer($token, $customerOptions),
            $subscriptionOptions
        );

        if ($this->coupon) {
            $payload = $this->addCouponToPayload($payload);
        }
        //Charge Employer Company or RegistrationRequest Company
        $response = BraintreeSubscription::create($payload);

        if (! $response->success) {
            //Log the error when cannot create subscription
            Log::error('create subscription error', [
                'user' => $this->model,
                'message' => $response->message,
                'error' => $response->errors,
                'full response' => $response,
            ]);

            throw new BraintreeException(
                'Our payment processor was unable to process your credit card. Please double check your information and try again or reach out to your bank directly.'
            );
        }

        $model = $this->model instanceof FreelancerCompany;

        return $this->createSubscription($response, $model);
    }

    /**
     * Switching subscription from monthly or yearly
     * @param  array  $customerOptions
     * @param  array  $subscriptionOptions
     * @param  bool $toMonthly
     * @return mixed
     * @throws Exception
     */
    public function update(array $customerOptions = [], array $subscriptionOptions = [], $toMonthly)
    {
        $payload = $this->getSubscriptionPayload(
            $this->getBraintreeCustomer($customerOptions),
            $subscriptionOptions
        );

        $response = BraintreeSubscription::create($payload);

        if (! $response->success) {
            //Log error so that we can check the problem
            //when upgrading subscription
            //Log the error when cannot create subscription
            Log::error('create subscription error', [
                'user' => $this->model,
                'message' => $response->message,
                'error' => $response->errors,
                'full response' => $response,
            ]);

            throw new BraintreeException(
                'Our payment processor was unable to process your credit card. Please double check your information and try again or reach out to your bank directly.'
            );
        }

        $model = $this->model instanceof FreelancerCompany;

        //check if upgrading to monthly
        if ($toMonthly) {
            //NOTE no prorate
//            $this->$model->cancelNow();
//            return $this->model->currentSubscription()->update([
//                'subscription_name' => $response->subscription->planId,
//                'braintree_id'   => $response->subscription->id,
//                'braintree_plan' => $response->subscription->planId, //name of plan
//                'member_type' => $model ? 'freelancer_company' : 'employer',
//                'next_billing' => Carbon::parse($response->subscription->nextBillingDate),
//                'billing_interval' => Carbon::parse($response->subscription->billingPeriodStartDate),
//            ]);
            $this->model->currentSubscription()->update([
                'next_billing' => Carbon::parse($response->subscription->billingPeriodEndDate),
            ]);
        }

        return $this->createSubscription($response, $model);
    }

    /**
     * Add the coupon discount to the Braintree payload.
     * @param  array  $payload
     * @return array
     * @throws Exception
     */
    protected function addCouponToPayload(array $payload)
    {
        $coupon = Coupon::findCoupon($this->coupon);

        if (! isset($payload['discounts']['add'])) {
            $payload['discounts']['add'] = [];
        }

        $payload['discounts']['add'][] = [
            'inheritedFromId' => $coupon->id,
        ];

        return $payload;
    }

    /**
     * Get the base subscription payload for Braintree.
     *
     * @param  \Braintree\Customer  $customer
     * @param  array  $options
     * @return array
     * @throws \Exception
     */
    protected function getSubscriptionPayload($customer, array $options = [])
    {
        $plan = Plan::findPlan($this->plan);

        return array_merge([
            'planId' => $this->plan,
            'price' => number_format($plan->price, 2, '.', ''),
            'paymentMethodToken' => $this->model->paymentMethod()->token,
        ], $options);
    }

    protected function getBraintreeCustomer($token = null, array $options = [])
    {
        if (! optional($this->model->braintree)->braintree_id) {
            $customer = $this->model->createAsBraintreeCustomer($token, $options);
        } else {
            $customer = $this->model->asBraintreeCustomer();
        }

        return $customer;
    }

    /**
     * @param $response
     * @param $model
     * @return mixed
     */
    protected function createSubscription($response, $model)
    {
        return $this->model->subscriptions()->create([
            'subscription_name' => $this->name,
            'braintree_id'   => $response->subscription->id,
            'braintree_plan' => $this->plan,
            'member_type' => $model ? 'freelancer_company' : 'employer',
            'next_billing' => Carbon::parse($response->subscription->nextBillingDate),
            'billing_interval' => Carbon::parse($response->subscription->billingPeriodStartDate),
        ]);
    }
}
