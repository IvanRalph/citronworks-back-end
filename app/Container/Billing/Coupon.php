<?php

namespace App\Container\Billing;

use App\Exceptions\BraintreeException;
use Exception;
use Braintree\Discount as BraintreeDiscount;

class Coupon
{
    /**
     * Get the Braintree coupon that has the given ID.
     *
     * @param  string  $id
     * @return \Braintree\Plan
     * @throws \Exception
     */
    public static function findCoupon($id)
    {
        $coupons = BraintreeDiscount::all();

        foreach ($coupons as $coupon) {
            if ($coupon->id === $id) {
                return $coupon;
            }
        }

        throw new BraintreeException("We are unable to find coupon with ID [{$id}].");
    }
}
