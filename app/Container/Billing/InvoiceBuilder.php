<?php

namespace App\Container\Billing;

use App\Mail\ReceiptEmail;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Contracts\View\View as ViewContract;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class InvoiceBuilder
{
    protected $model;

    protected $transactions;

    protected $receipt;

    /**
     * InvoiceBuilder constructor.
     * @param $model
     * @param  array|object  $transactions
     */
    public function __construct($model, $transactions)
    {
        if (is_object($transactions)) {
            $this->transactions = $transactions;
        } else {
            $this->transactions = $transactions[0];
        }
        $this->options = new Options();
        $this->model = $model;
    }

    public function createInvoice()
    {
        $this->receipt = $this->model->invoice()->create([
             'amount' => $this->transactions->amount,
             'braintree_id' => $this->transactions->id,
             //            'description' => $this->subscroptionDetails(),
             'receipt_number' => $this->invoiceNumber(),
//             'receipt_date' => $this->transactions->createdAt,
         ]);
        Storage::put('invoices/'.$this->receipt->receipt_number.'.pdf', $this->pdf());
        Mail::to($this->model->member->email)->send(new ReceiptEmail($this->model->member, $this->receipt));
        return $this->receipt->receipt_number;
    }

    public function invoiceNumber()
    {
        $receipt = $this->model->invoice->last();
        if (! $receipt) {
            //start at 20020 as for Nick
            $nextInvoiceNumber = '20020';
        } else {
            $string = preg_replace("/[^0-9\.]/", '', $receipt->receipt_number);
            $nextInvoiceNumber = sprintf('%04d', $string+1);
        }

        return $nextInvoiceNumber;
    }

    /**
     *  Put details on the PDF
     * @return string
     */
    public function subscroptionDetails()
    {
        $month = Carbon::instance($this->transactions->createdAt)->getTranslatedMonthName();

        return 'Invoice for the month of ' . $month;
    }

    /**
     * Get the total amount that was paid (or will be paid).
     *
     * @return string
     */
    public function total() : string
    {
        return $this->formatAmount($this->rawTotal());
    }

    /**
     * Get a Carbon date for the invoice.
     *
     * @return \Carbon\Carbon
     */
    public function date() : Carbon
    {
        return Carbon::instance($this->transactions->createdAt);
    }

    /**
     * Get the total of the invoice (before discounts).
     *
     * @return string
     */
    public function subtotal() : string
    {
        return $this->formatAmount(
            max(0, $this->transactions->amount + $this->discountAmount())
        );
    }

    /**
     * Get the raw total amount that was paid (or will be paid).
     *
     * @return float
     */
    public function rawTotal() : float
    {
        return max(0, $this->transactions->amount);
    }

    /**
     * Format the given amount into a string based on the user's preferences.
     *
     * @param  int  $amount
     * @return string
     */
    protected function formatAmount($amount)
    {
        return '$' . number_format($amount, 2);
    }

    /**
     * Determine if the invoice has a discount.
     *
     * @return bool
     */
    public function hasDiscount()
    {
        return count($this->transactions->discounts) > 0;
    }

    /**
     * Get the discount amount.
     *
     * @return string
     */
    public function discount()
    {
        return $this->formatAmount($this->discountAmount());
    }

    /**
     * Get the raw discount amount.
     *
     * @return float
     */
    public function discountAmount()
    {
        $totalDiscount = 0;

        foreach ($this->transactions->discounts as $discount) {
            $totalDiscount += $discount->amount;
        }

        return (float) $totalDiscount;
    }

    /**
     * Get the coupon codes applied to the invoice.
     *
     * @return array
     */
    public function coupons()
    {
        $coupons = [];

        foreach ($this->transactions->discounts as $discount) {
            $coupons[] = $discount->name;
        }

        return $coupons;
    }

    /**
     * Get the View instance for the invoice.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\View\View
     */
    public function view() : ViewContract
    {
        $planName = ucfirst(explode('-', $this->model->braintree_plan)[0]);
        $dateFrom = $this->model->billing_interval->format('M j, Y');
        $dateTo = $this->model->next_billing->format('M j, Y');

        return View::make('invoice.receipt', array_merge(
            [
                'invoice' => $this->receipt,
                'receipt_number' => $this->invoiceNumber(),
                'owner' => $this->model->member,
                'member' => $this->model->member,
                'description' => $planName . ' Plan ' . $dateFrom . ' - ' . $dateTo,
                'transactions' => $this
            ]
        ));
    }

    /**
     * Capture the invoice as a PDF and return the raw bytes.
     *
     * @param  Options  $options
     * @return string
     */
    public function pdf()
    {
        //fix for not displaying the logo
        $this->options->setIsRemoteEnabled(true);

        $dompdf = new Dompdf;

        $dompdf->setOptions($this->options);

        $dompdf->loadHtml($this->view()->render());
        $dompdf->render();

        return $dompdf->output();
    }

    /**
     * Dynamically get values from the Braintree transaction.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->transactions->{$key};
    }
}
