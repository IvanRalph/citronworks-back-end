<?php

namespace App\Container\Billing\Traits;

use App\Container\Billing\Invoices;
use App\Container\Billing\SubscriptionBuilder;
use App\Exceptions\BraintreeException;
use App\Model\Billing\Braintree;
use App\Model\Billing\Invoice;
use App\Model\Billing\Subscription;
use App\Model\Employer\Employer;
use App\Model\Freelancer\Freelancer;
use Braintree\Result\Successful;
use Braintree\Transaction as BraintreeTransaction;
use Carbon\Carbon;
use Exception;
use Braintree\Customer;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Braintree\PaymentMethod;
use Braintree\PayPalAccount;
use Braintree\Customer as BraintreeCustomer;
use Braintree\Subscription as BraintreeSubscription;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

trait Billing
{
    protected $receipt;

    /**
     * Get all of the subscriptions for the model.
     *
     * @return MorphMany
     */
    public function subscriptions() : MorphMany
    {
        return
            $this
                ->morphMany(Subscription::class, 'subscription', 'member_type', 'member_id')
                ->orderBy('created_at', 'desc');
    }

    /**
     * Get the braintree relations
     * @return MorphOne
     */
    public function braintree() : MorphOne
    {
        return
            $this
                ->morphOne(Braintree::class, 'braintree', 'member_type', 'member_id');
    }

    /**
     * @return MorphMany
     */
    public function invoice() : MorphMany
    {
        return
            $this
                ->morphMany(Invoice::class, 'invoiceable');
    }

    /**
     * Make a "one off" charge on the customer for the given amount.
     *
     * @param  int  $amount
     * @param  array  $options
     * @return \Braintree\Result\Successful
     * @throws \Exception
     */
    public function charge($amount, array $options = []) : Successful
    {
        $response = BraintreeTransaction::sale(array_merge([
            'amount' => number_format($amount, 2, '.', ''),
            'paymentMethodToken' => $this->paymentMethod()->token,
            'options' => [
                'submitForSettlement' => true,
            ],
            'recurring' => true,
        ], $options));

        if (! $response->success) {
            throw new Exception('We are unable to process your subscription: ' . $response->message);
        }

        return $response;
    }

    public function newSubscription($subscription, $plan, $coupon)
    {
        return new SubscriptionBuilder($this, $subscription, $plan, $coupon);
    }

    /**
     * Determine if the model has a given subscription.
     *
     * @param  string  $subscription
     * @param  string|null  $plan
     * @return bool
     */
    public function subscribed($subscription, $plan = null)
    {
        $subscription = $this->subscription($subscription);

        if (is_null($subscription)) {
            return false;
        }

        if (is_null($plan)) {
            return $subscription->valid();
        }

        return $subscription->valid() &&
               $subscription->braintree_plan === $plan;
    }

    public function subscription($subscription = 'monthly')
    {
        return $this->subscriptions->sortByDesc(function ($value) {
            return $value->billing_interval->getTimestamp();
        })->first(function ($value) use ($subscription) {
            return $value->subscription_name === $subscription;
        });
    }

    public function plan($plan = 'Silver-monthly')
    {
        return $this->subscriptions->first(function ($value) use ($plan) {
            return $value->braintree_plan === $plan;
        });
    }

    public function getSubscription()
    {
        //TODO: temporary process, can be removed once 3 months free promo is done.
        $subscription = $this->subscriptions->first();

        //If user is on expired Special Promo plan
        if (!$subscription || ($subscription->promo() && !$subscription->onGracePeriod())) {
            return null;
        }

        return $subscription;

        //TODO: can be uncommented once 3 months free promo is done
//        return $this->subscriptions->first();
    }

    public function getBraintreeId($braintreeId)
    {
        return $this->subscriptions->sortByDesc(function ($value) {
            return $value->billing_interval->getTimestamp();
        })->first(function ($value) use ($braintreeId) {
            return $value->braintree_id === $braintreeId;
        });
    }

    /**
     * Update customer's credit card.
     *
     * @param  string  $token
     * @param  array  $options
     * @return void
     * @throws \Exception
     */
    public function updateCard($token, array $options = [])
    {
        $customer = $this->asBraintreeCustomer();

        $response = PaymentMethod::create(
            array_replace_recursive([
                'customerId' => $customer->id,
                'paymentMethodNonce' => $token,
                'options' => [
                    'makeDefault' => true,
                    'verifyCard' => true,
                ],
            ], $options)
        );

        if (! $response->success) {
            throw new BraintreeException(
                'Our payment processor was unable to process your credit card. Please double check your information and try again or reach out to your bank directly.'
            );
        }

        $paypalAccount = $response->paymentMethod instanceof PaypalAccount;

        $this->braintree->forceFill([
            'card_brand' => $paypalAccount ? null : $response->paymentMethod->cardType,
            'card_last_digits' => $paypalAccount ? null : $response->paymentMethod->last4,
        ])->save();

        $this->updateSubscriptionsToPaymentMethod(
            $response->paymentMethod->token
        );

        return $this->braintree;
    }

    /**
     * Update the payment method token for all of the model's subscriptions.
     *
     * @param  string  $token
     * @return void
     */
    protected function updateSubscriptionsToPaymentMethod($token)
    {
        foreach ($this->subscriptions as $subscription) {
            if ($subscription->active()) {
                BraintreeSubscription::update($subscription->braintree_id, [
                    'paymentMethodToken' => $token,
                ]);
            }
        }
    }

    /**
     * Get the default payment method for the customer.
     *
     * @return array
     * @throws \Braintree\Exception\NotFound
     */
    public function paymentMethod()
    {
        $customer = $this->asBraintreeCustomer();

        foreach ($customer->paymentMethods as $paymentMethod) {
            if ($paymentMethod->isDefault()) {
                return $paymentMethod;
            }
        }
    }

    /**
     * Determine if the model is actively subscribed to one of the given plans.
     *
     * @param  array|string  $plans
     * @param  string  $subscription
     * @return bool
     */
    public function subscribedToPlan($plans, $subscription = 'monthly')
    {
        $subscription = $this->subscription($subscription);

        if (! $subscription || ! $subscription->valid()) {
            return false;
        }

        foreach ((array) $plans as $plan) {
            if ($subscription->braintree_plan === $plan) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine if the entity is on the given plan.
     *
     * @param  string  $plan
     * @return bool
     */
    public function onPlan($plan)
    {
        return ! is_null($this->subscriptions->first(function ($value) use ($plan) {
            return $value->braintree_plan === $plan;
        }));
    }

    /**
     * Create a Braintree customer for the given model.
     *
     * @param $billingInformation
     * @param  string  $token
     * @param  array  $options
     * @return \Braintree\Customer
     * @throws \Braintree\Exception\NotFound
     */
    public function createAsBraintreeCustomer($token, array $options = []) : Customer
    {
        $companyType = $this instanceof  Employer;

        $response = BraintreeCustomer::create(
            array_replace_recursive([
                'company' => $companyType ? $this->company->company_name : $this->information->company_name,
                'website' => $companyType ? $this->company->company_url : $this->information->company_url,
                'firstName' => $this->first_name,
                'lastName' => $this->last_name,
                'email' => $this->email,
                'paymentMethodNonce' => $token,
                'creditCard' => [
                    'options' => [
                        'verifyCard' => true,
                    ],
                    'billingAddress' => [
                        'firstName' => $this->first_name,
                        'lastName' => $this->last_name,
                        //changes: $this->billingInformation->zip removed column in the billing information
                        'postalCode' => $this->company->billing->zip,
                        //state in the db
                        'region' => $this->company->billing->state,
                        'countryName' => $this->company->billing->country,
                        'extendedAddress' => $this->company->billing->address_1,
                        'streetAddress' => $this->company->billing->street_1,
                    ],
                ],
            ], $options)
        );

        if (! $response->success) {
            //Log useful error on the response
            Log::error('create braintree customer error', [
                'user' => $this,
                'message' => $response->message,
                'error' => $response->errors,
                'full response' => $response,
            ]);

            throw new BraintreeException(
                'Our payment processor was unable to process your credit card. Please double check your information and try again or reach out to your bank directly.'
            );
        }

        $this->braintree_id = $response->customer->id;

        $paymentMethod = $this->paymentMethod();

        if (!($this instanceof Freelancer)) {
            if ($this instanceof Employer) {
                $memberType = 'employer';
            } else {
                $memberType = 'freelancer_company';
            }
        } else {
            $memberType = 'freelancer';
        }

        $this->braintree()->create([
            'braintree_id' => $response->customer->id,
            'card_brand' => $paymentMethod->cardType,
            'card_last_digits' => $paymentMethod->last4,
            'is_active' => 'true',
            'member_type' => $memberType,
        ])->save();

        return $response->customer;
    }

    /**
     * Get the Braintree customer for the model.
     *
     * @return \Braintree\Customer
     * @throws \Braintree\Exception\NotFound
     */
    public function asBraintreeCustomer() : Customer
    {
        if ($this->braintree_id) {
            $braintreeID = $this->braintree_id;
        } else {
            $braintreeID = $this->braintree->braintree_id;
        }

        return BraintreeCustomer::find($braintreeID);
    }

    /**
     * Determine if the entity has a Braintree customer ID.
     *
     * @return bool
     */
    public function hasBraintreeId()
    {
        return ! is_null($this->braintree->braintree_id);
    }

    /**
     * Get the full name pass to card holder name
     * @return string
     */
    public function getFullNameAttribute() : string
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function invoices() : Collection
    {
        $invoices = [];
        if (!($this instanceof Freelancer)) {
            if ($this instanceof Employer) {
                $memberType = $this->company->company_name;
            } else {
                $memberType = $this->information->company_name;
            }
        } else {
            //freelancer
            $memberType = null;
        }

        $transactions = $this->invoice;
        // Here we will loop through the Braintree invoices and create our own custom Invoice
        // instance that gets more helper methods and is generally more convenient to work
        // work than the plain Braintree objects are. Then, we'll return the full array.
        foreach ($transactions as $transaction) {
            $invoice = new Invoices($this, $transaction);
            $invoices[] = [
                'id' => $invoice->receipt_id,
                'date' => $invoice->date()->format('Y-m-d'),
                'details' => $invoice->subscroptionDetails(),
                'braintree_id' => $invoice->braintree_id,
                'receipt_number' => $invoice->receipt_number,
                'company_name' => Str::kebab($memberType),
                'amount' => $invoice->total(),
            ];
        }

        return new Collection($invoices);
    }

    /**
     * Find an invoice by ID.
     *
     * @param  string  $id
     * @return Invoices|string
     */
    public function findInvoice($id)
    {
        $transaction = Invoice::with(['braintree'])->find($id);

        return new Invoices($this, $transaction);

//        $invoice = new \App\Container\Billing\InvoiceBuilder($this, $transaction);
//        $this->receipt = $invoice->createInvoice();

//        return $transaction;
    }

    /**
     * Create an invoice download response.
     *
     * @param  array  $data
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function download() : Response
    {
        $pdf = 'invoices/'.$this->receipt.'.pdf';

        return new Response(Storage::get($pdf), 200, [
            'Content-Description' => 'File Transfer',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Type' => 'application/pdf',
        ]);
    }

    /**
     * Get Current Subscription
     *
     * @return MorphOne
     */
    public function currentSubscription() : MorphOne
    {
        return
            $this
                ->morphOne(Subscription::class, 'billable');
    }

    /**
     * Get Card Details
     * @return mixed
     */
    public function getCardDetails()
    {
        return $this->braintree;
    }

    /**
     * Check if ther are other subscriptions
     * @return bool
     */
    public function has_subscriptions()
    {
//        $expired = $this->subscriptions->filter(function ($value) {
//            if (!Carbon::now()->lt(Carbon::instance($value->next_billing))) {
//                return $value;
//            }
//        })->first();

//        $expired->delete();
        if (count($this->subscriptions) > 1) {
            return true;
        }

        return false;
    }

    public function otherSubscription()
    {
        return $this->subscriptions->last();
    }
}
