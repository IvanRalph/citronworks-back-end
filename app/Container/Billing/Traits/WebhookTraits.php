<?php

namespace App\Container\Billing\Traits;

use App\Container\Billing\InvoiceBuilder;
use App\Model\Billing\Subscription;
use Symfony\Component\HttpFoundation\Response;

trait WebhookTraits
{
    /**
     * Handle a subscription cancellation notification from Braintree.
     *
     * @param  string  $subscriptionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function cancelSubscription($subscriptionId)
    {
        $subscription = $this->getSubscriptionById($subscriptionId);

        if ($subscription && (! $subscription->cancelled() || $subscription->onGracePeriod())) {
            $subscription->markAsCancelled();
        }

        return new Response('Webhook Handled', 200);
    }

    /**
     * Get the model for the given subscription ID.
     * @param $subscriptionId
     * @return Subscription|null
     */
    protected function getSubscriptionById($subscriptionId) : ?Subscription
    {
        return Subscription::where('braintree_id', $subscriptionId)->first();
    }

    protected function saveTransaction($subscriptionWebhook)
    {
        $subscription = $this->getSubscriptionById($subscriptionWebhook->id);
        $subscriber = $subscription;

        $invoice = new InvoiceBuilder($subscriber, $subscriptionWebhook->transactions);
        $invoice->createInvoice();

        return new Response('Webhook Handled', 200);
    }
}
