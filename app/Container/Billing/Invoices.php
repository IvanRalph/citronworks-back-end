<?php

namespace App\Container\Billing;

use App\Model\Billing\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class Invoices
{
    /**
     * @var $model
     */
    protected $model;

    /**
     * @var Invoice
     */
    protected $transaction;

    /**
     * Invoices constructor.
     * @param $model
     * @param  Invoice  $transaction
     */
    public function __construct($model, Invoice $transaction)
    {
        $this->model = $model;
        $this->transaction = $transaction;
    }

    public function subscroptionDetails()
    {
        $month = Carbon::instance($this->transaction->receipt_date)->getTranslatedMonthName();

        return 'Receipt for the month of ' . $month;
    }

    public function oneOffPayment()
    {
        return 'Add on payment';
    }

    /**
     * Get the total amount that was paid (or will be paid).
     *
     * @return string
     */
    public function total() : string
    {
        return $this->formatAmount($this->rawTotal());
    }

    /**
     * Get a Carbon date for the invoice.
     *
     * @return \Carbon\Carbon
     */
    public function date() : Carbon
    {
        return Carbon::instance($this->transaction->receipt_date);
    }

    /**
     * Get the raw total amount that was paid (or will be paid).
     *
     * @return float
     */
    public function rawTotal() : float
    {
        return max(0, $this->transaction->amount);
    }

    /**
     * Format the given amount into a string based on the user's preferences.
     *
     * @param  int  $amount
     * @return string
     */
    protected function formatAmount($amount)
    {
        return '$' . number_format($amount, 2);
    }

    /**
     * Create an invoice download response.
     *
     * @param  array  $data
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function download() : Response
    {
        $pdf = 'invoices/' . $this->receipt_number . '.pdf';

        return new Response(Storage::get($pdf), 200, [
            'Content-Description' => 'File Transfer',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Type' => 'application/pdf',
        ]);
    }

    /**
     * Dynamically get values from the Braintree transaction.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->transaction->{$key};
    }
}
