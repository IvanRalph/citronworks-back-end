<?php

namespace App\Container\Auth;

use \Illuminate\Support\Facades\Facade;

class TokenResponseFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'api-authentication';
    }
}
