<?php

namespace App\Container\Auth;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class OauthTokenRequest
{
    protected string $grantType;

    protected int $clientId;

    protected string $clientSecret;

    protected string $username;

    protected string $password;

    public function __construct($param)
    {
        $this->grantType = $param['grant_type'];

        $this->clientId = $param['client_id'];

        $this->username = $param['username'];

        $this->password = $param['password'];

        $this->clientSecret = $param['client_secret'];
    }

    protected function formParams()
    {
        return [
            'grant_type' => $this->grantType,
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'username' => $this->username,
            'password' => $this->password,
            'scope' => '',
        ];
    }

    public function returnClient()
    {
        try {
            $http = new Client();
            $response = $http->post(config('auth.passport.base_url') . '/oauth/token', [
                'form_params' => $this->formParams()
            ]);

            return json_decode((string)$response->getBody(), true);
        } catch (ClientException $e) {
            return ['errors' => ['message' => $e->getMessage(), 'code' => $e->getCode()]];
        }
    }
}
