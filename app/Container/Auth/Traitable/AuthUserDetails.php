<?php

namespace App\Container\Auth\Traitable;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Model\Employer\Employer;
use App\Model\Freelancer\Freelancer;
use App\Model\FreelancerCompany\FreelancerCompany;
use Illuminate\Database\QueryException;

trait AuthUserDetails
{
    /**
     * @param $username
     * @return mixed
     */
    public function employer($username)
    {
        try {
            $query =
                Employer::where('email', $username)->firstOrFail();

            return
                $query
                    ->load('company');
        } catch (QueryException $e) {
            return [];
        } catch (ModelNotFoundException $e) {
            return [];
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * @param $username
     * @return mixed
     */
    public function freelancer($username)
    {
        try {
            $query =
                Freelancer::where('email', $username)->firstOrFail();

            return
                $query
                    ->load('profile');
        } catch (QueryException $e) {
            return [];
        } catch (ModelNotFoundException $e) {
            return [];
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * @param $username
     * @return mixed
     */
    public function freelancerAgency($username)
    {
        try {
            $query =
                FreelancerCompany::where('email', $username)->firstOrFail();

            return
                $query
                    ->load('information');
        } catch (QueryException $e) {
            return [];
        } catch (ModelNotFoundException $e) {
            return [];
        } catch (Exception $e) {
            return [];
        }
    }
}
