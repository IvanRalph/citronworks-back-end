<?php

namespace App\Container\Auth;

use App\Container\Auth\Traitable\AuthUserDetails;

class TokenResponse
{
    use AuthUserDetails;

    /**
     * @var array
     */
    protected array $clientParameter;

    /**
     * TokenResponse constructor.
     * @param $param
     */
    public function __construct($param)
    {
        $this->clientParameter = $param;
    }

    /**
     * Check if condition has met will return the users details and generated token
     *
     * @return |null
     */
    public function returnClientProviderToken()
    {
        if (!empty($this->employer($this->clientParameter['username']))

            && $employer = $this->employer($this->clientParameter['username'])) {

            return $this->setResponse($this->setEmployerClientCredentials(), 'employer');

        } elseif (!empty($this->freelancer($this->clientParameter['username']))
            && $freelancer = $this->freelancer($this->clientParameter['username'])) {

            return $this->setResponse($this->setFreelancerClientCredentials(), 'freelancer');

        } elseif (!empty($this->freelancerAgency($this->clientParameter['username']))

            && $freelancerAgency = $this->freelancerAgency($this->clientParameter['username'])) {

            return $this->setResponse($this->setCompanyClientCredentials(), 'company');
        } else {

            return [];
        }
    }

    /**
     * @param array $mergedClientCredentials
     * @return mixed
     */
    protected function setClientToken(array $mergedClientCredentials)
    {
        $initializeClient = new OauthTokenRequest($mergedClientCredentials);

        return $initializeClient->returnClient();
    }

    /**
     * @param array $client
     * @param string $provider
     * @return mixed
     */
    protected function setResponse(array $client, string $provider)
    {
        $mergedClientCredentials = array_merge($this->clientParameter, $client);

        $model = $this->$provider($mergedClientCredentials['username']);
        $model->provider = $provider;
        $model->authorization = $this->setClientToken($mergedClientCredentials);

        return $model;
    }

    /**
     * @return array
     */
    protected function setFreelancerClientCredentials(): array
    {
        return [
            'client_id' => config('auth.passport.freelancer.client_id'),
            'client_secret' => config('auth.passport.freelancer.client_secret'),
            'grant_type' => config('auth.passport.freelancer.grant_type'),
        ];
    }

    /**
     * @return array
     */
    protected function setEmployerClientCredentials(): array
    {
        return [
            'client_id' => config('auth.passport.employer.client_id'),
            'client_secret' => config('auth.passport.employer.client_secret'),
            'grant_type' => config('auth.passport.employer.grant_type'),
        ];
    }

    /**
     * @return array
     */
    protected function setCompanyClientCredentials(): array
    {
        return [
            'client_id' => config('auth.company.employer.client_id'),
            'client_secret' => config('auth.company.employer.client_secret'),
            'grant_type' => config('auth.passport.company.grant_type'),
        ];
    }

    /**
     * @return array
     */
    protected function setMessageClientCredentials(): array
    {
        return [
            'client_id' => config('auth.message.employer.client_id'),
            'client_secret' => config('auth.message.employer.client_secret'),
            'grant_type' => config('auth.passport.message.grant_type'),
        ];
    }
}
