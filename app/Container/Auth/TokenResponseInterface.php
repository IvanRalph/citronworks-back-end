<?php

namespace App\Container\Auth;

use Psr\Http\Message\ServerRequestInterface;

interface TokenResponseInterface
{
    public function setTokenResponse(ServerRequestInterface $request);
}
