<?php

namespace App\Container\HubSpot\Form;

class FormIntegration
{
    protected $portalId;

    protected $formBaseUrl;

    protected $contactFormId;

    protected $input;

    protected bool $returnTransfer = false;

    protected bool $followLocation = false;

    protected array

 $setHeader = ['Content-Type: application/json'];

    protected $encoding = '';

    protected $maxredirs = 10;

    protected $timeout = 0;

    public function __construct($param)
    {
        $this->portalId = config('hubspot.portal_id');
        $this->formBaseUrl = config('hubspot.hub_base_form_url');
        $this->contactFormId = config('hubspot.contact_form_guid');
        $this->input = json_encode($param);
    }

    public function submitGeneralInquiry()
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $this->formBaseUrl . '/submit/' . $this->portalId . '/' . $this->contactFormId,
            CURLOPT_RETURNTRANSFER => $this->returnTransfer,
            CURLOPT_ENCODING => $this->encoding,
            CURLOPT_MAXREDIRS => $this->maxredirs,
            CURLOPT_TIMEOUT => $this->timeout,
            CURLOPT_FOLLOWLOCATION => $this->followLocation,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $this->input,
            CURLOPT_HTTPHEADER => $this->setHeader,
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return
            $response;
    }
}
