<?php

namespace App\Container\HubSpot\Form;

use \Illuminate\Support\Facades\Facade;

class FormIntegrationFacade extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'hubspot';
    }
}
