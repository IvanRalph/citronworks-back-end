<?php

namespace App\Container\Http;

class Ping
{
    /**
     * @var array
     */
    public $httpResponseHeaders = [];

    /**
     * @var boolean
     */
    public $pong = false;
    
    /**
     * @param string $url
     */
    public function __construct($url = null)
    {
        $this->httpResponseHeaders = $this->url($url);
        if (
            $this->httpResponseHeaders &&
            $this->httpResponseHeaders['http_code'] >= 200 &&
            $this->httpResponseHeaders['http_code'] < 300
        ) {
            $this->pong = true;
        } else {
            $this->pong = false;
        }
    }
    /**
     * Get HTTP Response Header
     *
     * @param string $url
     * @return void
     */
    public function url(string $url)
    {
        if ($url == null) {
            return false;
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        $httpRequest = curl_getinfo($ch);
        curl_close($ch);
        if ($httpRequest['http_code'] >= 200 && $httpRequest['http_code'] < 300) {
            return $httpRequest;
        }
        return [];
    }
}
