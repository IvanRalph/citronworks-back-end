<?php

namespace App\Container\Messaging;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

interface MessageableInterface
{
    public function participants() : MorphMany;

    public function conversations() : MorphToMany;

    public function setUniqueId();

    public function getMessengerIdAttribute() : string;

    public function getMemberTypeAttribute() : string;

    public function getIdAttribute();

    public function getTitleAttribute() : ?string;

    public function getNameAttribute() : string;
}
