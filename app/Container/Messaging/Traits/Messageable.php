<?php

namespace App\Container\Messaging\Traits;

use App\Model\Messaging\Conversation;
use App\Model\Messaging\Participant;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Str;

trait Messageable
{
    /**
     * @return MorphMany
     */
    public function participants() : MorphMany
    {
        return $this->morphMany(Participant::class, 'member', 'member_type', 'member_id', $this->primaryKey);
    }

    public function conversations() : MorphToMany
    {
        return $this->morphToMany(
            Conversation::class,
            'member',
            'participants',
            'member_id',
            'conversation_id',
            $this->getKeyName(),
            'conversation_id'
        )->using(Participant::class)
            ->withPivot('num_unread_messages');
    }

    /**
     * A normalized name attribute is needed in order to do Mail::to($user)
     *
     * @return string
     */
    public function getNameAttribute() : string
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function setUniqueId()
    {
        $this->keyType = 'string';
        $this->{$this->getKeyName()} = $this->messenger_id;
        $this->didUniqueWorkaround = true;

        return $this;
    }

    public function getMessengerIdAttribute() : string
    {
        return $this->member_type . '-' . $this->getKey();
    }

    public function getMemberTypeAttribute() : string
    {
        $class = static::class;

        if (Str::contains($class, 'Employer')) {
            return 'employer';
        }

        if (Str::contains($class, 'Freelancer')) {
            return 'freelancer';
        }

        abort('User of type ' . $class . ' is not yet supported in Messenger feature');
    }

    public function getIdAttribute()
    {
        // check if uniqueness workaround was done
        if ($this->didUniqueWorkaround) {
            return str_replace($this->member_type . '-', '', $this->getKey());
        }

        return $this->getKey();
    }

    public function getTitleAttribute() : ?string
    {
        if ($this->member_type == 'freelancer') {
            return $this->freelancerCompany->title;
        }

        if ($this->member_type == 'employer') {
            return $this->company->title;
        }

        return null;
    }

    public function addToConversation(Conversation $conversation)
    {
        $participants = new Participant([
            'member_id' => $this->getKey(),
            'member_type' => $this->getMorphClass(),
            'conversation_id' => $conversation->getKey(),
        ]);

        $this->participants()->save($participants);
    }

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'notifications.' . $this->member_type . '.' . $this->id;
    }
}
