<?php

namespace App\Container\SkillRelations;

use App\Model\SkillRelation\SkillRelation;

class SkillRelations implements SkillRelationsInterface
{
    protected $model;

    /**
     * Set Model function
     *
     * @param model
     * @return void
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function processSkillRelations()
    {
        $skill_count = $this->model->skill()->count();

        if ($skill_count > 1) {
            $skills = $this->model
                        ->skill()
                        ->get()
                        ->toArray();

            foreach ($skills as $idx => $skill) {
                $skill_arr = $skills;
                unset($skill_arr[$idx]);
                $child_skills = array_values($skill_arr);

                foreach ($child_skills as $key => $child_skill) {
                    $skill_relation = SkillRelation::firstOrCreate(
                        [
                            'skill_id' => $skill['skill_id'],
                            'related_skill_id' => $child_skill['skill_id'],
                        ],
                        [
                            'skill_id' => $skill['skill_id'],
                            'related_skill_id' => $child_skill['skill_id'],
                            'count' => 0,
                        ]
                    );

                    $skill_relation->count = $skill_relation->count + 1;
                    $skill_relation->save();
                }
            }
        }

        return null;
    }
}
