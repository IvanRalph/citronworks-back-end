<?php

namespace App\Container\SkillRelations;

interface SkillRelationsInterface
{
    public function setModel($model);

    public function processSkillRelations();
}
