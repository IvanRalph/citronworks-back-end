<?php

namespace App\Helpers;

class Constants
{
    const OPERATOR_AND = 'AND';

    const OPERATOR_OR = 'OR';

    const OPERATOR_LIKE = 'LIKE';

    const OPERATOR_WHERE = 'where';

    const OPERATOR_OR_WHERE = 'orWhere';
}
