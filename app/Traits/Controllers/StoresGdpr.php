<?php

namespace App\Traits\Controllers;

use App\Model\Gdpr\Gdpr;
use Illuminate\Database\Eloquent\Model;

trait StoresGdpr
{
    /**
     * @param $employer
     * @param $input
     * @return Gdpr|mixed
     */
    public function storeGdpr(Model $model)
    {
        return Gdpr::create([
            'source' => 'https://app.citronworks.com',
            'email' => $model->email,
            'member_id' => $model->getKey(),
            'policy_version' => 'v2.1.0',
            'user_type' => $model->member_type,
            'jurisdiction' => 'ca',
            'delete_request' => 'no',
            'policy_date' => now(),
        ]);
    }
}
