<?php

namespace App\Traits\Controllers;

trait BuildsSkillsRelations
{
    protected function skillsRelations($request, $model, $freelancer)
    {
        $freelancersSkills = $model->freelancerProfileSkills($freelancer->profile()->first());
        info($request->all());
        $skillId = $request->skill_id;

        if ($freelancersSkills->isNotEmpty()) {
            $f_skills_arr = collect($freelancersSkills)->map(function ($i, $k) {
                return $i['skill_id'];
            });

            $newSkillSet = [];
            $oldSkillSet = [];
            $skillArrCount = count($f_skills_arr);
            if ($skillArrCount >= 1) {
                // TODO: Store newly added skill
                foreach ($f_skills_arr as $i => $s) {
                    $newSkillSet[$i]['skill_id'] = (int) $skillId;
                    $newSkillSet[$i]['related_skill_id'] = $s;
                }
                // TODO: Store the skills
                for ($i = 0; $i < $skillArrCount; $i++) {
                    $oldSkillSet[$i]['skill_id'] = $f_skills_arr[$i];
                    $oldSkillSet[$i]['related_skill_id'] = (int) $skillId;
                }
                $wheels = array_merge($newSkillSet, $oldSkillSet);

                if (count($wheels)) {
                    foreach ($wheels as $sr) {
                        $model->createSkillRelation($sr['skill_id'], $sr['related_skill_id']);
                    }
                }
            }
        }
    }
}
