<?php

namespace App\Traits\Controllers;

use App\Mail\VerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

trait SendsVerificationEmail
{
    public function sendVerificationEmail(Model $model, string $type)
    {
        Mail::to($model->email)
            ->send(new VerifyEmail($model, $model->activation_token, $type));
    }
}
