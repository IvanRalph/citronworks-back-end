<?php

namespace App\Traits\Models;

trait ResyncsSearchIndex
{
    /**
     * Re-sync the Search index as a workaround because $touches
     * isn't working for some reason in BelongsToMany
     *
     * This may be removed when we switch to using queues in the future
     *
     * @return void
     */
    public function resyncSearchIndex() : void
    {
        if ($this->shouldBeSearchable()) {
            $this->searchable();
        }
    }
}
