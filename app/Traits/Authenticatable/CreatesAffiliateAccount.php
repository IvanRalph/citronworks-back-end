<?php

namespace App\Traits\Authenticatable;

use Facades\App\Services\PostAffiliateProService;

trait CreatesAffiliateAccount
{
    public static function bootCreatesAffiliateAccount()
    {
        static::created(function ($model) {
            PostAffiliateProService::createAffiliateAccount($model);
        });
    }
}
