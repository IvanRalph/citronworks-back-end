<?php

namespace App\Imports;

use App\Model\Category\Category;
use Illuminate\Support\Collection;

class CategoryImport
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            Category::create([
                'category_name' => $row[0],
            ]);
        }
    }
}
