<?php

namespace App\Exceptions;

use Exception;

class InvoiceException extends Exception
{
    public $message = 'Invoice not found';

    public $code = 422;
}
