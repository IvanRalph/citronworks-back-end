<?php

namespace App\Exceptions;

use App\Repositories\Eloquent\Api\EmployerDataManipulation;
use App\Repositories\Eloquent\Api\FreelancerDataManipulation;
use Exception;
use Illuminate\Support\Str;

class UnverifiedEmailException extends Exception
{
    public function __construct($user)
    {
        $this->sendEmail($user);
    }

    protected function sendEmail($user)
    {
        switch ($user['provider']) {
            case 'employer':
                $employer = new EmployerDataManipulation();
                $employerResult = $employer->findViaId($user['employer_id']);

                $employer->sendVerificationEmail($employerResult, $employerResult->activation_token);
                break;
            case 'freelancer':
                $freelancer = new FreelancerDataManipulation();
                $freelancerResult = $freelancer->findViaId($user['freelancer_id']);

                $freelancer->sendVerificationEmail($freelancerResult, $freelancerResult->activation_token);
        }
    }
}
