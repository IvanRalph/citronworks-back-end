<?php

namespace App\Services;

use App\Model\Messaging\Conversation;
use App\Model\Messaging\Participant;
use Illuminate\Support\Arr;
use Pusher\Pusher;

class PresenceService
{
    protected $pusher;

    public function __construct()
    {
        $config = config('broadcasting.connections.pusher');

        $this->pusher = new Pusher(
            $config['key'],
            $config['secret'],
            $config['app_id'],
            $config['options']
        );
    }

    public function request(string $path) : array
    {
        return json_decode($this->pusher->get($path)['body'], true);
    }

    public function idsPresentIn(Conversation $conversation) : array
    {
        return Arr::pluck(
            $this->request('/channels/presence-conversation.' . $conversation->conversation_id . '/users')['users'],
            'id'
        );
    }

    public function isParticipantPresent(Participant $participant) : bool
    {
        return $this->request('/channels/private-notifications.' . $participant->member_type . '.' . $participant->member_id)['occupied'];
    }
}
