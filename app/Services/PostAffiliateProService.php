<?php

namespace App\Services;

// unfortunately PostAffiliatePro's SDK is not in modern PHP (not namespaced)
include_once app_path('Libraries/PapApi.class.php');

use App\Mail\EmployerWelcomeEmail;
use App\Mail\FreelancerWelcomeEmail;
use App\Model\Freelancer\Freelancer;
use Exception;
use Gpf_Data_Filter;
use Gpf_Rpc_Array;
use Gpf_Rpc_FormRequest;
use Gpf_Rpc_GridRequest;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Pap_Api_Session;

class PostAffiliateProService
{
    protected $session;

    public function __construct()
    {
        $session = new Pap_Api_Session(config('affiliates.merchant.domain'));
        if (! @$session->login(config('affiliates.merchant.username'), config('affiliates.merchant.password'))) {
            Log::critical('Cannot Login to PostAffiliatePro: ' . $session->getMessage());
        } else {
            $this->session = $session;
        }
    }

    /**
     * Create Affiliate account from Authenticatable
     *
     * @param Illuminate\Foundation\Auth\User $user [description]
     *
     * @return void
     */
    public function createAffiliateAccount(User $user) : void
    {
        $session = $this->session;
        // was not able to login to Post Affiliate Pro; see error logs
        if (empty($session)) {
            return;
        }

        $isCompany = ! $user instanceof Freelancer;

        $request = new Gpf_Rpc_FormRequest('Pap_Merchants_User_AffiliateForm', 'add', $session);

        $temporaryPassword = Str::random(8);

        $request->setField('username', $user->email);
        $request->setField('firstname', $user->first_name);
        $request->setField('lastname', $user->last_name);
        $request->setField('rstatus', 'A');
        $request->setField('refid', Str::random(8));
        $request->setField('rpassword', $temporaryPassword);
        $request->setField('dontSendEmail', 'Y');

        try {
            $request->sendNow();
        } catch (Exception $e) {
            Log::critical('PostAffiliatePro API call error: ' . $e->getMessage());
        }

        $response = $request->getStdResponse();
        if ($response->success != 'Y') {
            Log::error('Unsuccessful request to PostAffiliatePro API to add affiliate: ' . $response->message);
        }

        // if employer, use employer welcome email
        if (isset($user->employer_id)) {
            $mail = new EmployerWelcomeEmail($user, $temporaryPassword);
        } elseif (isset($user->freelancer_id)) {
            $mail = new FreelancerWelcomeEmail($user, $temporaryPassword);
        }
        Mail::to($user->email)->send($mail);
    }

    /**
     * [topAffiliatesLast30Days description]
     *
     * @param string  $sortBy Accepted values: 'salesCount', 'clicksRaw'
     * @param integer $period Accepted values: 7, 30, 90
     * @param integer $limit  The number of rows to return
     *
     * @return array An array of affiliates with the ff. columns:
     *               'first_name', 'last_name', 'email', 'clicks', 'sales'
     */
    public function topAffiliates(string $sortBy = 'salesCount', int $period = 30, int $limit = 30) : array
    {
        $session = $this->session;
        // was not able to login to Post Affiliate Pro; see error logs
        if (empty($session)) {
            return [];
        }

        $request = new Gpf_Rpc_GridRequest('Pap_Merchants_User_TopAffiliatesGrid', 'getRows', $session);

        // declare the columns to be fetched
        $request->addParam('columns', new Gpf_Rpc_Array([
            ['id'], ['userid'], ['username'], ['firstname'], ['lastname'], ['clicksRaw'], ['salesCount'],
        ]));

        $request->setLimit(0, $limit);

        $rangeMap = [
            7 => Gpf_Data_Filter::RANGE_LAST_7_DAYS,
            30 => Gpf_Data_Filter::RANGE_LAST_30_DAYS,
            90 => Gpf_Data_Filter::RANGE_LAST_90_DAYS,
        ];

        // Filter stats for the past 30 days
        $request->addFilter(
            'statsdaterange',
            Gpf_Data_Filter::DATERANGE_IS,
            $rangeMap[$period]
        );

        // sorting based on sales count
        $request->setSorting('salesCount', false); // true = Ascending; false = Descending

        // send request
        try {
            $request->sendNow();
        } catch (Exception $e) {
            Log::critical('PostAffiliatePro API call error: ' . $e->getMessage());
        }

        // request was successful, get the grid result
        $grid = $request->getGrid();

        // get recordset from the grid
        $recordset = $grid->getRecordset();

        // iterate through the records
        $affiliates = [];
        foreach ($recordset as $rec) {
            $affiliates[] = [
                'first_name' => $rec->get('firstname'),
                'last_name' => $rec->get('lastname'),
                'email' => $rec->get('username'),
                'clicks' => $rec->get('clicksRaw'),
                'sales' => $rec->get('salesCount'),
            ];
        }

        return $affiliates;
    }
}
