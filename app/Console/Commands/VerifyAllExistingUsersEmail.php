<?php

namespace App\Console\Commands;

use App\Model\Employer\Employer;
use App\Model\Freelancer\Freelancer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class VerifyAllExistingUsersEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verify:users {user_type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populates email_verified_at for all existing employers and freelancers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        switch ($this->argument('user_type')) {
            case 'freelancer':
                $this->verifyFreelancers(new Freelancer());
                break;
            case 'employer':
                $this->verifyEmployers(new Employer());
                break;
            default:
                $this->error('Supported user types: employer, freelancer');
        }
    }

    protected function verifyFreelancers($freelancers) : void
    {
        foreach ($freelancers->whereNull('email_verified_at')->get() as $freelancer) {
            $freelancer->update([
                'email_verified_at' => Carbon::now(),
                'activation_token' => Str::random(8),

            ]);
        }
    }

    protected function verifyEmployers($employers) : void
    {
        foreach ($employers->whereNull('email_verified_at')->get() as $employer) {
            $employer->update([
                'email_verified_at' => Carbon::now(),
                'activation_token' => Str::random(8),

            ]);
        }
    }
}
