<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Contracts\Web\FreelancerInterface;
use App\Model\Skill\SkillFeeelancers;
use App\Model\Freelancer\Freelancer;

class AutomateProfileIdToSkillFreelancer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'automate:skill-freelancer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populates freelancer_profile_id in skill_freelancer table based from freelancer\'s profile';

    protected $freelancer;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->freelancer = new Freelancer;
    }

    /**
     * Apply Freelancer Profile ID to skill_freelancer table
     *
     * @param integer $freelancerId
     * @param Model $skillFreelancer
     * @return void
     */
    public function apply(int $freelancerId, Model $skillFreelancer)
    {
        $f =
            $this->freelancer
                ->find($freelancerId)
                ->profile()
                ->first('freelancer_profile_id');

        if ($f) {
            $f->toArray();
            $skillFreelancer->update($f->toArray());

            return true;
        }

        return false;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(FreelancerInterface $freelancer)
    {
        $f = $freelancer->showAll(SkillFeeelancers::class);
        if (!$f->isEmpty()) {
            foreach ($f as $d) {
                if ($d->freelancer_id) {
                    $this->apply($d->freelancer_id, $d);
                }
            }
        }
    }
}
