<?php

namespace App\Http\Requests\Job;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // authorization handled by middleware
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'keywords' => 'nullable|string',
            'job_type' => 'nullable|string|in:full-time,part-time,project,hourly',
            'price_min' => 'nullable|integer|min:0',
            'price_max' => 'nullable|integer|min:0',
            'per_page' => 'nullable|integer|min:0',
        ];
    }

    /**
     * Adds validation on min and max when its counterpart is present
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $ifBothPresent = fn ($input) => ! empty($input['price_max']) && ! empty($input['price_min']);
        $validator->sometimes('price_min', 'lte:price_max', $ifBothPresent);
        $validator->sometimes('price_max', 'gte:price_min', $ifBothPresent);
    }
}
