<?php

namespace App\Http\Requests\Job;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class SaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function getValidatorInstance()
    {
        $requestData = $this->all();
        $this->oldDesc = $this->input('description');
        $requestData['description'] = strip_tags($this->input('description'));
        $this->replace($requestData);

        return parent::getValidatorInstance();
    }

    public function validated()
    {
        $requestData = $this->validator->validated();
        $requestData['description'] = $this->oldDesc;

        return $requestData;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        // if (isset($this->skill[0]['skill_id'])) {
        //     $this->merge([
        //         'skill' => Arr::pluck($this->skill, 'skill_id'),
        //     ]);
        // }
        if ($this->skill) {
            $skills = [];
            $order = 0;
            foreach ($this->skill as $i => $s) {
                if (collect($s)->contains('proposal')) {
                    $skills['proposal'][] = $s;
                } else {
                    $skills['skill'][] = array_replace(collect($s)->only('skill_id')->toArray(), [
                       'skill_order' => $order++
                   ]);
                }
            }
            $this->merge([
                'skill' => $skills ?? [],
            ]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'description' => ['required', 'string', 'min:200', 'max:2000'],
            // 'category' => ['required'],
            'skill' => ['required_without:job_skill_proposals', 'array'],
            'price_max' => 'nullable|numeric|min:0|max:9999999.99',
            'price_min' => 'nullable|numeric|min:0|max:9999999.99',
            'job_type' => ['required', 'max:255', Rule::in(['project', 'full-time', 'part-time', 'hourly'])],
            'country_limit' => ['nullable'],
            'job_skill_proposals' => 'nullable'
        ];
    }

    /**
     * Adds validation on min and max when its counterpart is present
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $ifBothPresent = fn ($input) => ! empty($input['price_max']) && ! empty($input['price_min']);
        $validator->sometimes('price_min', 'lte:price_max', $ifBothPresent);
        $validator->sometimes('price_max', 'gte:price_min', $ifBothPresent);
    }
}
