<?php

namespace App\Http\Requests\Job;

use Illuminate\Foundation\Http\FormRequest;

class InviteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow inviting for a job the user owns
        return $this->user()->jobs()->pluck('job_id')->contains($this->job_id);
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'employer_id' => $this->user()->employer_id,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // job_id doesn't really need to be validated because
            // it's already validated in the authorize() method
            'job_id' => 'required|integer',
            // ensure that the freelancer exists in the database
            'freelancer_id' => 'required|integer|exists:freelancers',
            // employer_id was injected via prepareForValidation()
            'employer_id' => 'required|integer',
            // message came from the frontend
            'message' => 'required|string',
        ];
    }
}
