<?php

namespace App\Http\Requests\Job;

use Illuminate\Foundation\Http\FormRequest;

class Application extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => [
                'string', 'required', 'min:1', 'max:255',
            ], 'message' => [
                'string', 'required', 'min:1', 'max:500',
            ],
        ];
    }
}
