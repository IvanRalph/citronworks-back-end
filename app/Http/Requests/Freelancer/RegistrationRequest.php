<?php

namespace App\Http\Requests\Freelancer;

use App\Rules\LowerCase;
use App\Rules\UpperCase;
use App\Rules\Number;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $uniqueFreelancerEmail = $this->uniqueRule('freelancers', 'freelancer_id');

        return [
            'first_name' => [ 'required', 'string', 'max:255' ],
            'last_name' => [ 'required', 'string', 'max:255' ],
            'company.country' => [ 'required' ],
            'email' => [ 'required', 'string', 'email', 'max:255', $uniqueFreelancerEmail, 'unique:employers' ],
            'password' => [ 'required', 'min:8', 'max:20', 'confirmed', new Uppercase, new Number, new Lowercase ],
            // 'role' => [ 'required', 'max:255', Rule::in(['owner', 'admin', 'freelancer'])],
            'agreement' => [ 'accepted' ],
            'about_us' => [ 'nullable', 'string', 'max:255' ],
        ];
    }

    protected function uniqueRule(string $table, string $primaryKey)
    {
        $user = $this->user();

        $rule = Rule::unique($table);

        if (! empty($user)) {
            $rule->ignore($user->getKey(), $primaryKey);
        }

        return $rule;
    }
}
