<?php

namespace App\Http\Requests\Freelancer;

use App\Http\Requests\Abstraction\UpdateAbstractRequest;

final class UpdateRequest extends UpdateAbstractRequest
{
    /**
     * Filter th methods to allow.
     * @var array
     */
    protected $allowedMethod = [
        'updatePassword' => [
            'rules' => [
                'password' => ['required', 'min:8', 'max:20'],
                'confirm_password' => ['required', 'same:password'],
            ],
        ],
        'updatePrimary' => [
            'rules' => [
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'profile_photo' => ['required'],
                'role' => 'required|max:255|in:owner,admin,freelancer',
            ],
        ],
        'updateProfile' => [
            'rules' => [
                'title' => ['required', 'max:255'],
                'intro' => ['required'],
                'description' => ['string'],
                'category' => ['string', 'max:255'],
                'skills' => ['string', 'max:255'],
            ],
            'relation' => 'profile',
        ],
        'updateGdpr' => [
            'rules' => [
                'email' => ['required', 'email'],
                'source' => ['required'],
                'policy_version' => ['required'],
                'member_type' => ['required'],
                'jurisdiction' => ['required'],
                'delete_request' => ['required'],
                'policy_date' => ['required'],
                'delete_request_date' => ['required'],
            ],
            'relation' => 'gdpr',
        ],
    ];

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'role.in'  => 'Role must be either "owner", "admin", "freelancer"',
        ];
    }
}
