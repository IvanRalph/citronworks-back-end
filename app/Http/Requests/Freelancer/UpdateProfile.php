<?php

namespace App\Http\Requests\Freelancer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => [
                'string', 'min:1', 'max:255',
            ], 'last_name' => [
                'string', 'min:1', 'max:255',
            ], 'profile.title' => [
                'string', 'min:1', 'max:255',
            ], 'profile.intro' => [
                'string', 'min:1', 'max:255',
            ], 'profile.description' => [
                'string', 'min:1', 'max:500',
            ], 'profile.freelancer_type' => [
                Rule::in(['person', 'persona']),
            ], 'profile.category' => [
                'integer',
            ], 'profile.skills' => [
                'string', 'min:1', 'max:255',
            ], 'company.address_1' => [
                'min:1', 'max:255',
            ], 'company.address_2' => [
                'min:1', 'max:255',
            ], 'company.country' => [
                'min:1', 'max:100',
            ], 'company.zip' => [
                'min:1', 'max:15',
            ],
        ];
    }
}
