<?php

namespace App\Http\Requests\Freelancer;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address_1' => [ 'required', 'max:255' ],
            'address_2' => [ 'required', 'max:255' ],
            'country' => [ 'required', 'max:100' ],
            'zip' => [ 'required', 'max:15' ],
            'state' => [ 'required', 'max:15' ],
        ];
    }
}
