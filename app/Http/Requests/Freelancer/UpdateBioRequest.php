<?php

namespace App\Http\Requests\Freelancer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateBioRequest extends FormRequest
{
    protected $oldDesc;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function getValidatorInstance()
    {
        $requestData = $this->all();
        $this->oldDesc = $this->input('description');
        $requestData['description'] = strip_tags($this->input('description'));
        $this->replace($requestData);

        return parent::getValidatorInstance();
    }

    public function validated()
    {
        $requestData = $this->validator->validated();
        $requestData['description'] = $this->oldDesc;

        return $requestData;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => [ 'string', 'min:6', 'max:80', 'required' ],
            'intro' => [ 'string', 'min:30', 'max:130', 'required' ],
            'description' => [ 'string', 'min:200', 'max:2000', 'required' ],
            // 'freelancer_type' => [ Rule::in(['person', 'persona']) ],
            'category' => [ 'integer' ],
            'skills' => [ 'string', 'min:1', 'max:255' ],
            'full_time_ava' => 'required_without_all:part_time_ava,hourly_ava',
            'part_time_ava' => 'required_without_all:full_time_ava,hourly_ava',
            'hourly_ava' => 'required_without_all:part_time_ava,full_time_ava',
            'full_time_price' => 'required_if:full_time_ava,1|numeric|min:1|max:9999999.99|nullable',
            'part_time_price' => 'required_if:part_time_ava,1|numeric|min:1|max:9999999.99|nullable',
            'hourly_price' => 'required_if:hourly_ava,1|numeric|min:1|max:9999999.99|nullable',
        ];

        return $rules;
    }

    public function attributes()
    {
        return [
            'title' => 'Title',
            'intro' => 'Introduction',
            'description' => 'Bio',
        ];
    }
}
