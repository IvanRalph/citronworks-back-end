<?php

namespace App\Http\Requests\Billing;

use Illuminate\Foundation\Http\FormRequest;

class ValidatePaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->plan_type == 'Promo') {
            $rules = [];
        } else {
            $rules = [
                'email' => 'nullable|email',
                'token' => 'required|string',
                'subscription_type' => 'required|string',
                'billing_information.address_1' => 'required|string',
                'billing_information.address_2' => 'nullable|string',
                'billing_information.zip' => 'nullable|string',
                'billing_information.state' => 'nullable|string',
                'billing_information.city' => 'nullable|string',
                'billing_information.country' => 'required|string',
            ];
        }

        return $rules;

    }
}
