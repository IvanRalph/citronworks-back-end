<?php

namespace App\Http\Requests\FreelancerCompany;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => [
                'string', 'min:1', 'max:255',
            ], 'last_name' => [
                'string', 'min:1', 'max:255',
            ], 'billing_information.address_1' => [
                'min:1', 'max:255',
            ], 'billing_information.address_2' => [
                'min:1', 'max:255',
            ], 'billing_information.country' => [
                'min:1', 'max:100',
            ], 'billing_information.zip' => [
                'min:1', 'max:15',
            ], 'billing_information.state' => [
                'min:1', 'max:255',
            ], 'information.address_1' => [
                'min:1', 'max:255',
            ], 'information.address_2' => [
                'min:1', 'max:255',
            ], 'information.state' => [
                'min:1', 'max:255',
            ], 'information.country' => [
                'min:1', 'max:100',
            ], 'information.zip' => [
                'min:1', 'max:15',
            ], 'information.company_url' => [
                'url',
            ],
        ];
    }
}
