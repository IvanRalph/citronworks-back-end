<?php

namespace App\Http\Requests\FreelancerCompany;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class NextRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'email'], 'title' => ['string', 'max:255'], 'description' => ['string'], 'contractor_type' => [Rule::in(['person', 'persona'])], 'category' => ['string', 'max:255'], 'skills' => ['string', 'max:255'],
        ];
    }
}
