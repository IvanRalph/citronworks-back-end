<?php

namespace App\Http\Requests\FreelancerCompany;

use App\Rules\LowerCase;
use App\Rules\UpperCase;
use App\Rules\Number;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => [
                'required', 'string', 'max:255',
            ], 'last_name' => [
                'required', 'string', 'max:255',
            ], 'email' => [
                'required', 'string', 'email', 'max:255', 'unique:employers', 'unique:freelancer', 'unique:freelancer_company',
            ], 'password' => [
                'required', 'min:8', 'max:20', 'confirmed', new Uppercase, new Number, new Lowercase,
            ], 'role' => [
                'required', 'max:255', Rule::in(['owner', 'admin', 'read_only']),
            ],
        ];
    }
}
