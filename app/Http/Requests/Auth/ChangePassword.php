<?php

namespace App\Http\Requests\Auth;

use App\Rules\LowerCase;
use App\Rules\UpperCase;
use App\Rules\Number;
use Illuminate\Foundation\Http\FormRequest;

class ChangePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => [
                'required',
            ],
            'password' => [
                'required', 'min:8', 'max:20', 'confirmed', new Uppercase, new Number, new Lowercase,
            ],
        ];
    }
}
