<?php

namespace App\Http\Requests\Employer;

use Illuminate\Foundation\Http\FormRequest;

class CitronworkersStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'citronworkers_request.first_name' => [
                'required', 'string', 'max:255',
            ],
            'citronworkers_request.last_name' => [
                'string', 'max:255',
            ],
            'citronworkers_request.email' => [
                'required', 'email', 'max:255'
            ],
            'citronworkers_request.company' => [
                'max:255',
            ],
            'citronworkers_request.phone' => [
                'max:255',
            ],
            'citronworkers_request.company_website' => [
                'max:255',
            ],
            'citronworkers_request.referer' => [
                'string', 'max:255',
            ],
            'citronworkers_request.message' => [
                'required', 'string',
            ],
        ];
    }
}
