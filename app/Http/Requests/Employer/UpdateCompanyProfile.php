<?php

namespace App\Http\Requests\Employer;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\VerifyUrl;

class UpdateCompanyProfile extends FormRequest
{
    public function getValidatorInstance()
    {
        $requestData = $this->all();
        $this->oldDesc = $this->input('company.description');
        $requestData['company']['description'] = strip_tags($this->input('company.description'));
        $this->replace($requestData);

        return parent::getValidatorInstance();
    }

    public function validated()
    {
        $requestData = $this->validator->validated();
        $requestData['company']['description'] = $this->oldDesc;

        return $requestData;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company.description' => ['nullable', 'min:200','max:2000'],
            'company.address_1' => ['required'],
            // 'company.company_url' => [new VerifyUrl]
        ];
    }
}
