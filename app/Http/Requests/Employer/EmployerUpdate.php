<?php

namespace App\Http\Requests\Employer;

use Illuminate\Foundation\Http\FormRequest;

class EmployerUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required'], 'first_name' => ['string', 'max:255'], 'address_1' => ['string'], 'street_1' => ['string', 'max:255'], 'address_2' => ['string', 'max:255'], 'street_2' => ['string', 'max:255'], 'zip' => ['alpha_num', 'max:15'], 'state' => ['string', 'max:255'], 'country' => ['string', 'max:100'], 'title' => ['string', 'max:255'], 'intro' => ['string'], 'description' => ['string'], 'company_url' => ['url'], 'invitation' => ['array'],
        ];
    }
}
