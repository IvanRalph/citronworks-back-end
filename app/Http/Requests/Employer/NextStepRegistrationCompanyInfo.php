<?php

namespace App\Http\Requests\Employer;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\VerifyUrl;

class NextStepRegistrationCompanyInfo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'email'],
            'company_profile.company_name' => ['required', 'min:1', 'max:255', 'string'],
            'company_profile.address_1' => ['required', 'min:1', 'max:255', 'string'],
            'company_profile.address_2' => ['nullable', 'min:1', 'max:255', 'string'],
            'company_profile.zip' => ['required', 'min:1', 'max:15', 'string'],
            'company_profile.state' => ['nullable', 'min:1', 'max:255', 'string'],
            'company_profile.country' => ['required', 'min:1', 'max:100', 'string'],
            // 'company_profile.company_url' => ['nullable', 'min:1', 'max:255', 'regex:/^((?:https\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/', new VerifyUrl],
            'company_profile.company_url' => ['nullable'],
        ];
    }

    public function attributes()
    {
        return [
            'company_profile.company_url' => 'Company Url'
        ];
    }
}
