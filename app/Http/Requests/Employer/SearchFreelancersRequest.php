<?php

namespace App\Http\Requests\Employer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class SearchFreelancersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // authorization handled by middleware
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'keywords' => 'string',
            'skills' => 'array',
            'rates.full_time.enabled' => 'boolean',
            'rates.full_time.min' => 'nullable|integer|min:0',
            'rates.full_time.max' => 'nullable|integer|min:0',
            'rates.part_time.enabled' => 'boolean',
            'rates.part_time.min' => 'nullable|integer|min:0',
            'rates.part_time.max' => 'nullable|integer|min:0',
            'rates.hourly.enabled' => 'boolean',
            'rates.hourly.min' => 'nullable|integer|min:0',
            'rates.hourly.max' => 'nullable|integer|min:0',
        ];
    }

    /**
     * Adds validation on min and max when its counterpart is present
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $rateTypes = ['full_time', 'part_time', 'hourly'];
        foreach ($rateTypes as $rt) {
            $enabledPath = "rates.$rt.enabled";
            $minPath = "rates.$rt.min";
            $maxPath = "rates.$rt.max";
            $validator->sometimes(
                $minPath,
                "lte:$maxPath",
                fn ($input) => Arr::get($input, $enabledPath) && ! empty(Arr::get($input, $maxPath))
            );
            $validator->sometimes(
                $maxPath,
                "gte:$minPath",
                fn ($input) => Arr::get($input, $enabledPath) && ! empty(Arr::get($input, $minPath))
            );
        }
    }
}
