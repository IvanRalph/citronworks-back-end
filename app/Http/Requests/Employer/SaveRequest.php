<?php

namespace App\Http\Requests\Employer;

use App\Rules\LowerCase;
use App\Rules\UpperCase;
use App\Rules\Number;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class SaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $uniqueEmployerEmail = $this->uniqueRule('employers', 'employer_id');

        return [
            'first_name' => [ 'required', 'string', 'max:255' ],
            'last_name' => [ 'required', 'string', 'max:255' ],
            'email' => [ 'required', 'string', 'email', 'max:255', 'unique:freelancers', $uniqueEmployerEmail ],
            'password' => [ 'required', 'min:8', 'max:20', 'confirmed', new Uppercase, new Number, new Lowercase ],
            // 'role' => [ 'required', 'max:255', Rule::in(['owner', 'admin', 'read_only']) ],
            'company.country' => [ 'required' ],
            'agreement' => [ 'accepted' ],
        ];
    }

    protected function uniqueRule(string $table, string $primaryKey)
    {
        $user = $this->user();

        $rule = Rule::unique($table);

        if (! empty($user)) {
            $rule->ignore($user->getKey(), $primaryKey);
        }

        return $rule;
    }
}
