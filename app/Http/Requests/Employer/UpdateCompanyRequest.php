<?php

namespace App\Http\Requests\Employer;

use App\Rules\VerifyUrl;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateCompanyRequest extends FormRequest
{
    protected $originalDescription;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    public function getValidatorInstance()
    {
        $requestData = $this->all();
        $this->originalDescription = $this->input('description');
        $requestData['description'] = strip_tags($this->input('description'));
        $this->replace($requestData);

        return parent::getValidatorInstance();
    }

    public function validated()
    {
        $requestData = $this->validator->validated();
        $requestData['description'] = $this->originalDescription;

        return $requestData;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required|string|max:255',
            // 'company_url' => [new VerifyUrl],
            'company_url' => 'nullable|string',
            'description' => 'nullable|string|max:2000' . (empty($this->onboarding) ? '|min:200' : ''),
            'address_1' => 'required|string|max:255',
            'address_2' => 'nullable|string|max:255',
            'zip' => 'required|string|max:15',
            'city' => 'nullable|string|max:255',
            'state' => 'nullable|string|max:255',
            'country' => 'nullable|string|max:255',
        ];
    }
}
