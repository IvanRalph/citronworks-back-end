<?php

namespace App\Http\Requests\Employer;

use App\Http\Requests\Abstraction\UpdateAbstractRequest;

final class UpdateRequest extends UpdateAbstractRequest
{
    /**
     * Filter th methods to allow.
     * @var array
     */
    protected $allowedMethod = [
        'updatePassword' => [
            'rules' => [
                'password' => 'required',
                'confirm_password' => 'required',
            ],
        ],
        'updateDetails' => [
            'rules' => [
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'profile_photo' => ['required'],
            ],
        ],
        'updateCompany' => [
            'rules' => [
                'company_name' => ['required'],
                'address_1' => ['required'],
                'street_1' => ['required'],
                'address_2' => ['required'],
                'street_2' => ['required'],
                'zip' => ['required'],
                'state' => ['required'],
                'country' => ['required'],
                'logo' => ['required'],
                'title' => ['required'],
                'intro' => ['required'],
                'description' => ['required'],
                'company_url' => ['required'],
            ],
            'relation' => 'company',
        ],
        'updateGdpr' => [
            'rules' => [
                'email' => ['required', 'email'],
                'source' => ['required'],
                'policy_version' => ['required'],
                'member_type' => ['required'],
                'jurisdiction' => ['required'],
                'delete_request' => ['required'],
                'policy_date' => ['required'],
                'delete_request_date' => ['required'],
            ],
            'relation' => 'gdpr',
        ],
    ];
}
