<?php

namespace App\Http\Requests\Employer;

use Illuminate\Foundation\Http\FormRequest;

class NextStepRegistrationInvitation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'invitation.first_name.*' => ['required', 'min:1', 'max:255', 'string'],
            'invitation.last_name.*' => ['required', 'min:1', 'max:255', 'string'],
            'invitation.email.*' => ['required', 'min:1', 'max:255', 'email'],
        ];
    }
}
