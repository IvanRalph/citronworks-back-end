<?php

namespace App\Http\Requests\Abstraction;

use Illuminate\Foundation\Http\FormRequest;

abstract class UpdateAbstractRequest extends FormRequest
{
    protected $method;

    protected $methodExists = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->has('method')) {
            $this->method = $this->get('method');

            if (!array_key_exists($this->method, $this->allowedMethod)) {
                $this->methodExists = false;

                return [];
            }

            if ($this->method == 'updatePassword') {
                return $this->allowedMethod['updatePassword']['rules'];
            }

            return array_intersect_key($this->allowedMethod[$this->method]['rules'], $this->all());
        }

        $this->methodExists = false;

        return [];
    }

    /**
     * Check if method exists,
     * We will abort if none, since no data will be updated.
     *
     */
    public function methodExists()
    {
        if ($this->methodExists) {
            return true;
        }

        return false;
    }

    /**
     * This is to correctly point to relation.
     *
     * @param [type] $method
     */
    public function hasRelation()
    {
        if (array_key_exists('relation', $this->allowedMethod[$this->method])) {
            return $this->allowedMethod[$this->method]['relation'];
        }

        return false;
    }

    /**
     * Check if request has no error.
     * Continue to controller logic
     *
     * @param  [type] $request
     * @return [type]
     */
    public function throwErrorIf()
    {
        if (!$this->methodExists) {
            return response()->json([
                'method' => null,
                'message' => 'Bad Request, the requested method does not exists ....',
                'status' => 402,
            ], 402);
        }

        if ($this->methodExists) {
            if (empty($this->rules())) {
                return response()->json([
                    'method' => $this->method,
                    'message' => 'Bad Request, there is no fields to update, add fields to update.',
                    'status' => 402,
                ], 402);
            }
        }

        return false;
    }
}
