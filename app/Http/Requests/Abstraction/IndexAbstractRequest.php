<?php

namespace App\Http\Requests\Abstraction;

use Illuminate\Foundation\Http\FormRequest;

abstract class IndexAbstractRequest extends FormRequest
{
    protected $method;

    protected $methodExists = false;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->has('method')) {
            $this->methodExists = true;
            $this->method = $this->get('method');

            if (!array_key_exists($this->method, $this->allowedmethod)) {
                $this->methodExists = false;

                return [];
            }

            return $this->allowedmethod[$this->method]['rules'];
        }

        // this is for collection
        return $this->allowedmethod['collection'];
    }

    /**
     * Check if method exists,
     * We will abort if none, since no data will be updated.
     *
     */
    public function methodExists()
    {
        if ($this->methodExists) {
            return true;
        }

        return false;
    }

    /**
     * This is to correct arguments in finding a resources.
     *
     * @param [type] $method
     */
    public function where($method)
    {
        $where = $this->allowedmethod[$this->method]['where'];

        return [$where, $this->{$where}];
    }
}
