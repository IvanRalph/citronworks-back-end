<?php

namespace App\Http\Requests\Presentation;

use Illuminate\Foundation\Http\FormRequest;

class Invite extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => [
                'required', 'min:1', 'max:255',
            ],
            'last_name' => [
                'required', 'min:1', 'max:255',
            ],
            'email' => [
                'required', 'email', 'unique:employers', 'unique:freelancers', 'unique:invite.invite',
            ],
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'You are already subscribed to the list.',
        ];
    }
}
