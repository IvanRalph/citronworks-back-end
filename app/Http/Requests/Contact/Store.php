<?php

namespace App\Http\Requests\Contact;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact.first_name' => [
                'required', 'string',
            ],
            'contact.last_name' => [
                'required', 'string',
            ],
            'contact.email' => [
                'required', 'email',
            ],
            'contact.subject' => [
                'required', 'string',
            ],
            'contact.message' => [
                'required', 'string',
            ],
        ];
    }
}
