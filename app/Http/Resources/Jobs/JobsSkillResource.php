<?php

namespace App\Http\Resources\Jobs;

use Illuminate\Http\Resources\Json\JsonResource;

class JobsSkillResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'skill_id' => $this->skill_id,
            'skill_name' => $this->skill_name,
            'skill_order' => $this->whenPivotLoaded('skill_jobs', function () {
                return $this->pivot->skill_order;
            }),
        ];
    }
}
