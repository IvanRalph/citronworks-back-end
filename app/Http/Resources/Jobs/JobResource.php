<?php

namespace App\Http\Resources\Jobs;

use App\Http\Resources\ApplicationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class JobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);

        // on the freelancer view, if the job isn't marked as 'paid'
        // remove the employer relation from the response
        if ($this->resource->plan_status != 'paid') {
            unset($data['employer']);
        }

        // on the employer view, if applications are loaded,
        // blur the freelancer's name and email if employer is in free plan
        if (isset($data['applications'])) {
            $data['applications'] = ApplicationResource::collection($this->applications);
        }

        return $data;
    }
}
