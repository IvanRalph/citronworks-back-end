<?php

namespace App\Http\Resources\Invoice;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'date' => $this->date(),
            'details' => $this->subscroptionDetails(),
            'total' => $this->total(),
            'subscription_type' => $this->planId,
            'coupons' => $this->discounts,
        ];
    }
}
