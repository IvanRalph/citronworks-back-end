<?php

namespace App\Http\Resources\Traits;

trait StatusCodeTrait
{
    /**
     * Trait retain single responsibility for Resources
     *
     * @param int $value
     * @return $this
     */
    public function setHttpCode($value = 200)
    {
        $this->http_code = $value;

        return $this;
    }
}
