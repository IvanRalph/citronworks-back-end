<?php

namespace App\Http\Resources\Freelancer;

use Illuminate\Http\Resources\Json\JsonResource;

class FreelancerSkillResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'skill_id' => $this->skill_id,
            'skill_name' => $this->skill_name,
            'rating' => $this->whenPivotLoaded('skill_freelancers', function () {
                return $this->pivot->rating;
            }),
            'skill_order' => $this->whenPivotLoaded('skill_freelancers', function () {
                return $this->pivot->skill_order;
            }),
        ];
    }
}
