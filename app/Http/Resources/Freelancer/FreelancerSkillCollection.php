<?php

namespace App\Http\Resources\Freelancer;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Traits\StatusCodeTrait;

class FreelancerSkillCollection extends ResourceCollection
{
    use StatusCodeTrait;

    protected $http_code;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'http_code' => $this->http_code,
            'data' => FreelancerSkillResource::collection($this->collection),
        ];
    }
}
