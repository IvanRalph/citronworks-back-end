<?php

namespace App\Http\Resources\Employer;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class FreelancerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = parent::toArray($request);

        // if logged-in user is an employer on a free plan
        $user = Auth::user();
        if ($user->member_type === 'employer' && empty($user->getSubscription())) {
            // obfuscate personally identifiable information in the response
            foreach (['first_name', 'last_name', 'email', 'full_name'] as $fieldToObfuscate) {
                $resource[$fieldToObfuscate] = Str::random(strlen($resource[$fieldToObfuscate]));
            }
            // tell the frontend to blur obfuscated fields
            $resource['blurred'] = true;
        }

        return $resource;
    }
}
