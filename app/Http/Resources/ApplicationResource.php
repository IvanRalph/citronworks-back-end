<?php

namespace App\Http\Resources;

use App\Http\Resources\Employer\FreelancerResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ApplicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);
        // blur freelancer name and email if employer is in the free plan
        $data['member'] = new FreelancerResource($this->member);
        return $data;
    }
}
