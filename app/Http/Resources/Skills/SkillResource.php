<?php

namespace App\Http\Resources\Skills;

use App\Http\Resources\Category\CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SkillResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
            [
                'id' => $this->skill_id,
                'name' => $this->skill_name,
                'category' => new CategoryResource($this->whenLoaded('category')),
            ];
    }
}
