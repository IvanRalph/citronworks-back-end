<?php

namespace App\Http\Resources\Skills;

use App\Http\Resources\Category\CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SkillProposalResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
            [
                'skill_proposal_id' => $this->skill_proposal_id,
                'name' => $this->skill_name,
                'member_id' => $this->member_id,
                'member_type' => $this->member_type,
                'approved_by' => $this->approved_by,
                'approved' => $this->approved,
                'category' => new CategoryResource($this->whenLoaded('category')),
                'proposal_date' => $this->proposal_date ? $this->proposal_date->format('Y-m-d H:i:s') : null,
                'approved_date' => $this->approved_date ? $this->approved_date->format('Y-m-d H:i:s') : null,
            ];
    }
}
