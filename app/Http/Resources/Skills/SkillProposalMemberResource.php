<?php

namespace App\Http\Resources\Skills;

use Illuminate\Http\Resources\Json\JsonResource;

class SkillProposalMemberResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
            [
                'skill_id' => $this->skill_proposal_id,
                'skill_name' => $this->skill_name,
                'rating' => $this->rating,
                'approved' => $this->approved,
                //added proposal for identifying
                //between freelancer_skills and proposal_skills
                //proposal_proposal = name; skill = skill_name
                'proposal' => true,
            ];
    }
}
