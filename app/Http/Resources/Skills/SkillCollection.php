<?php

namespace App\Http\Resources\Skills;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Traits\StatusCodeTrait;

class SkillCollection extends ResourceCollection
{
    use StatusCodeTrait;

    protected $http_code;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'http_code' => $this->http_code,
            'data' => $this->collection,
        ];
    }
}
