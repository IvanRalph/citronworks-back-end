<?php

namespace App\Http\Resources\Category;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    const TYPE = 'Category';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        CategorySkillCollection::withoutWrapping();

        return [
            'id' => $this->category_id,
            'name' => $this->category_name,
            'type' => self::TYPE,
            'skills' => new CategorySkillCollection($this->whenLoaded('skills')),
        ];
    }
}
