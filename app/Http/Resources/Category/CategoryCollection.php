<?php

namespace App\Http\Resources\Category;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Traits\StatusCodeTrait;

class CategoryCollection extends ResourceCollection
{
    use StatusCodeTrait;

    protected $http_code;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'http_code' => $this->http_code,
            'data' => $this->collection,
        ];
    }
}
