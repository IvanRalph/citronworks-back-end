<?php

namespace App\Http\Resources\Billing;

use Illuminate\Http\Resources\Json\JsonResource;

class OtherSubscription extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->braintree_id,
            'plan' => ucfirst(strtok($this->braintree_plan, '-')),
            'subscription' => ucfirst($this->subscription_name),
            'to' => $this->next_billing->toFormattedDateString(),
            'from' => $this->billing_interval->toFormattedDateString(),
        ];
    }
}
