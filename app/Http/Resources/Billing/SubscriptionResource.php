<?php

namespace App\Http\Resources\Billing;

use Illuminate\Http\Resources\Json\JsonResource;

class SubscriptionResource extends JsonResource
{
    public static $wrap = '';

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //Return null if no subscription
        if (!isset($this->subscription_id)) {
            return null;
        }

        return [
            'id' => isset($this->braintree_id) ? $this->braintree_id : '',
            'plan' => ucfirst(strtok($this->braintree_plan, '-')),
            'subscription' => ucfirst($this->subscription_name),
            'to' => $this->next_billing->toFormattedDateString(),
            'from' => $this->billing_interval->toFormattedDateString(),
            'card' => new CardDetailsResource(auth()->user()->getCardDetails()),
            'has_other_subscription' => auth()->user()->has_subscriptions(),
            $this->mergeWhen(auth()->user()->has_subscriptions(), [
                'other_subscription' => new OtherSubscription(auth()->user()->otherSubscription()),
                'last_subscription_date' => $this->next_billing->toFormattedDateString(),
            ]),
            //check if subscription is active, paused, canceled
            'subscription_status' => $this->subscriptionStatus(),
            $this->mergeWhen($this->hasCoupons(), [
                'coupons' => $this->coupons()
            ])
        ];
    }
}
