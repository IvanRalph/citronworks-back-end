<?php

namespace App\Http\Controllers\Api\v1\Freelancer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Freelancer\UpdateAddressRequest;
use App\Http\Requests\Freelancer\UpdateBioRequest;
use App\Http\Requests\Freelancer\UpdateNameAndEmailRequest;
use App\Http\Resources\Employer\FreelancerResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MyProfileController extends Controller
{
    public function latestHiredNotice()
    {
        $latestUndismissedHire = Auth::user()->latestUndismissedHire;

        if (empty($latestUndismissedHire)) {
            return null;
        }

        return $latestUndismissedHire->load('employer', 'company');
    }

    public function closeHiredNotice()
    {
        $latestUndismissedHire = Auth::user()->latestUndismissedHire;

        if (empty($latestUndismissedHire)) {
            return null;
        }

        $latestUndismissedHire->show_hired_banner = false;
        $latestUndismissedHire->save();
        return 'ok';
    }

    public function show()
    {
        return new FreelancerResource(Auth::user()->load([
            'freelancerSkills.skill',
            'proposedSkills',
            'freelancerCompany',
            'profile',
            'hiresByMyCompany',
            'latestHire',
        ]));
    }

    public function showBio()
    {
        return Auth::user()->profile;
    }

    public function updateBio(UpdateBioRequest $request)
    {
        Auth::user()->profile->update($request->validated());
        return Auth::user()->profile;
    }

    public function showProfilePhoto()
    {
        return Auth::user()->only('profile_photo');
    }

    public function updateProfilePhoto(Request $request)
    {
        // validate
        $request->validate([
            'profile_photo' => 'required|image|max:12000',
        ]);

        // if valid, store the file and update the database
        if ($request->has('profile_photo') && $request->profile_photo->isValid()) {
            Auth::user()->update([
                'profile_photo' => $request->profile_photo->store('images'),
            ]);
        }

        return Auth::user()->only('profile_photo');
    }

    public function showAddress()
    {
        return Auth::user()->freelancerCompany;
    }

    public function updateAddress(UpdateAddressRequest $request)
    {
        Auth::user()->freelancerCompany->update($request->validated());
    }

    public function showNameAndEmail()
    {
        return Auth::user()->only('first_name', 'last_name', 'email');
    }

    public function updateNameAndEmail(UpdateNameAndEmailRequest $request)
    {
        Auth::user()->update($request->validated());
        return 'ok';
    }
}
