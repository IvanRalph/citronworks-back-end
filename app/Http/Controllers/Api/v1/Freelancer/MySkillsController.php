<?php

namespace App\Http\Controllers\Api\v1\Freelancer;

use App\Http\Controllers\Controller;
use App\Http\Resources\Freelancer\FreelancerSkillCollection;
use App\Model\Skill\Skill;
use App\Repositories\Contracts\Api\FreelancerInterface;
use App\Traits\Controllers\BuildsSkillsRelations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MySkillsController extends Controller
{
    use BuildsSkillsRelations;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(FreelancerInterface $repository)
    {
        // get the freelancer profile
        $profile = Auth::user()->profile;

        // fetch the skills related to the freelancer profile
        $skills = $repository->freelancerProfileSkills($profile);

        // respond!
        return response(FreelancerSkillCollection::make($skills)
            ->setHttpCode());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Skill\Skill    $skill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FreelancerInterface $repository, Skill $mySkill)
    {
        // get the freelancer and profile
        $freelancer = Auth::user();
        $profile = $freelancer->profile;

        // add skill_id to request
        $request->merge([
            'skill_id' => $mySkill->skill_id,
        ]);

        // build skills relations
        $this->skillsRelations($request, $repository, $freelancer);

        // get the request input
        $input = $request->only([
            'rating',
            'skill_order',
            'skill_id',
        ]);

        // update skills
        $skills = $repository->storeFreelancerProfileSkills($profile, $input);

        // build the response
        return FreelancerSkillCollection::make($skills)
            ->setHttpCode();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Skill\Skill    $skill
     * @return \Illuminate\Http\Response
     */
    public function destroy(FreelancerInterface $repository, Skill $mySkill)
    {
        // get the freelancer profile
        $profile = Auth::user()->profile;

        if ($profile->freelancers_skills()->count() <= 1) {
            return [
               'message' => 'You should have atleast one skill and the maximum is 15.',
               'http_code' => 500
           ];
        }

        // remove the skill from the freelancer profile
        $repository->deleteFreelancerProfileSkill($profile, $mySkill->skill_id);

        // respond!
        return [
            'http_code' => 200,
            'message' => 'Successfully deleted freelancer skill!',
        ];
    }
}
