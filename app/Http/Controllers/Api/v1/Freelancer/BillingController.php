<?php

namespace App\Http\Controllers\Api\v1\Freelancer;

use App\Http\Controllers\Controller;
use App\Http\Resources\Invoice\InvoicesCollection;
use App\Http\Resources\Invoice\InvoiceResource;

class BillingController extends Controller
{
    public function invoices()
    {
        return new InvoicesCollection(auth()->user()->invoices());
    }

    public function findInvoice($id)
    {
        return new InvoiceResource(auth()->user()->findInvoice($id));
    }

    public function downloadInvoice($id)
    {
        return auth()->user()->findInvoice($id)->download();
    }
}
