<?php

namespace App\Http\Controllers\Api\v1\Freelancer;

use App\Http\Controllers\Controller;
use App\Http\Resources\Skills\SkillProposalMemberCollection;
use App\Model\Skill\SkillProposal;
use App\Repositories\Contracts\Api\FreelancerInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MySkillProposalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(FreelancerInterface $repository)
    {
        // get the freelancer
        $freelancer = Auth::user();

        // fetch skill proposals of freelancer
        $skillProposals = $repository->onBoardSkillProposals($freelancer);

        // respond!
        return SkillProposalMemberCollection::make($skillProposals)
            ->setHttpCode();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, FreelancerInterface $repository)
    {
        // get the freelancer
        $freelancer = Auth::user();

        // get the request input
        $input = $request->only([
            'skill_name',
            'rating',
        ]);

        // store the skill proposal
        $skillProposals = $repository->onBoardSkillProposalsStore($freelancer, $input);

        // respond!
        return SkillProposalMemberCollection::make($skillProposals)
            ->setHttpCode();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Skill\SkillProposal $skillProposal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FreelancerInterface $repository, SkillProposal $mySkillProposal)
    {
        // get the freelancer
        $freelancer = Auth::user();

        // get the request input
        $input = $request->only([
            'skill_name',
            'rating',
        ]);

        // add the proposal ID to the input
        $input['$proposalId'] = $mySkillProposal->skill_proposal_id;

        // update skill proposal
        $skillProposals = $repository->onBoardSkillProposalsStore($freelancer, $input);

        // respond!
        return SkillProposalMemberCollection::make($skillProposals)
            ->setHttpCode();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Skill\SkillProposal $skillProposal
     * @return \Illuminate\Http\Response
     */
    public function destroy(FreelancerInterface $repository, SkillProposal $mySkillProposal)
    {
        // delete the skill proposal from the freelancer
        $repository->onBoardSkillProposalsDelete($mySkillProposal->skill_proposal_id);

        return [
            'http_code' => 200,
            'message' => 'Successfully deleted freelancer skill!',
        ];
    }
}
