<?php

namespace App\Http\Controllers\Api\v1\Freelancer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Job\Application;
use App\Http\Requests\Job\SearchRequest;
use App\Http\Resources\Jobs\JobResource;
use App\Mail\ApplyJobMail;
use App\Model\Attachment\Attachment;
use App\Model\Job\Job;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class JobController extends Controller
{
    /**
     * Show the list of jobs applied to.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function applied()
    {
        return JobResource::collection($this->listQuery()
            ->applied()
            ->paginate(10));
    }

    protected function listQuery() : Builder
    {
        return Job::with('skill', 'employer.company')->freelancerVisible();
    }

    /**
     * Show the list of jobs bookmarked.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function bookmarked()
    {
        return JobResource::collection($this->listQuery()
            ->bookmarked()
            ->paginate(10));
    }

    /**
     * Bookmark the job.
     *
     * @param Job $job
     *
     * @return Response
     */
    public function bookmark(Job $job)
    {
        // authorize bookmarking the job
        $this->authorize('bookmark', $job);

        // bookmark the job by the currently logged-in user
        $job->bookmark();

        // respond the job
        return $this->show($job);
    }

    public function unBookmark(Job $job)
    {
        // unbookmark the job by the currently logged-in user
        $job->unbookmark();

        // respond the job
        return $this->show($job);
    }

    /**
     * Apply for the job.
     *
     * @param Application $request
     * @param Job         $job
     *
     * @return Response
     */
    public function apply(Application $request, Job $job)
    {
        // authorize applying to the job
        $this->authorize('apply', $job);

        $application = $job->applications()->updateOrCreate(
            ['freelancer_id' => Auth::id()],
            $request->validated()
        );

        Mail::to($application->job->employer->email)
            ->send(new ApplyJobMail($application));

        return $application;
    }

    /**
     * Upload a file to the application for the job.
     *
     * @param Request $request
     * @param Job     $job
     *
     * @return Response
     */
    public function uploadFile(Request $request, Job $job, Attachment $upload)
    {
        // authorize applying to the job
        $this->authorize('apply', $job);

        $request->validate([
            'upload' => 'required|max:12228|mimes:jpeg,png,pdf',
        ]);
        $input = $request->only(['upload']);
        $freelancerId = Auth::id();
        $jobId = $job->id;

        if (! empty($input['upload']) && $input['upload']->isValid()) {
            $file = $input['upload']; // assign upload parameter.
            $originalExtension = $file->getClientOriginalExtension();
            $extension = '.' . $originalExtension;

            // Map to array every value.
            $filename = pathinfo($request->upload->getClientOriginalName(), PATHINFO_FILENAME);
            if ($upload->whereLabel($filename)->exists()) {
                $filename = $filename . '_' . ((int)$upload->all()->last()->attached_id + 1);
            }
            $attachment = $upload->create([
                'label' => $filename,
                'mime' => $file->getMimeType(),
                'size' => $file->getSize(),
                'extension' => $extension,
                // fix issue where some jpeg files have jpeg extension instead of jpg, and
                // jpg is the choice in the enum and not jpeg
                'type' => $originalExtension == 'jpeg' ? 'jpg' : $originalExtension,
            ]);

            // Move file to file folder and rename to id as filename.
            $file->move('files', $filename . $extension);
            //Adding new array variable
            $attachment = ['id' => $attachment->attached_id];
        } else {
            $attachment = ['id' => 0];
        }

        return $job->applications()->updateOrCreate(
            ['freelancer_id' => Auth::id()],
            compact('attachment')
        );
    }

    public function search(SearchRequest $request)
    {
        return JobResource::collection(tap(Job::searchFilter($request->validated())
            ->paginate($request->per_page ?: 10))->load('skill', 'employer.company'));
    }

    /**
     * Display the specified resource.
     *
     * @param Job $job
     *
     * @return Response
     */
    public function show(Job $job)
    {
        // authorize viewing the job
        $this->authorize('view', $job);

        // show job data
        $freelancerId = Auth::id();
        return new JobResource($job->load([
            'bookmarks' => fn ($query) => $query->where('member_id', $freelancerId),
            'application' => fn ($query) => $query->notEmpty()->where('freelancer_id', $freelancerId),
            'skill',
            'employer',
            'employer.company',
        ]));
    }
}
