<?php

namespace App\Http\Controllers\Api\v1\Freelancer;

use App\Http\Resources\Skills\SkillProposalMemberCollection;
use App\Http\Requests\Freelancer\FreelancerSkillUpdateRequest;
use App\Http\Resources\Freelancer\FreelancerSkillCollection;
use App\Repositories\Contracts\Api\FreelancerInterface;
use App\Container\SkillRelations\SkillRelationsInterface;
use App\Http\Requests\Freelancer\FreelancerSkillRequest;
use App\Http\Requests\Freelancer\UpdateAddress;
use App\Http\Requests\Freelancer\UpdateNameAndEmail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\Controllers\BuildsSkillsRelations;

class FreelancerController extends Controller
{
    use BuildsSkillsRelations;

    // /**
    //  * @return bool|mixed
    //  */
    public function location()
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://freegeoip.app/json/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'accept: application/json',
                'content-type: application/json',
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            $manage = json_decode($response, true);

            return $manage;
        }
    }

    // /**
    //  * @param UpdateAddress       $updateAddressRequest
    //  * @param FreelancerInterface $freelancer
    //  * @param $freeLancerId
    //  * @return mixed
    //  */
    // public function updateAddress(UpdateAddress $updateAddressRequest, FreelancerInterface $freelancer, $freeLancerId)
    // {
    //     $input = $updateAddressRequest->only(['company']);

    //     if (!empty($freelancer->findViaId($freeLancerId)
    //         && $findingContractor = $freelancer->findViaId($freeLancerId))) {
    //         return $freelancer->updateCompany($findingContractor, $input);
    //     }
    // }

    // /**
    //  * @param UpdateNameAndEmail  $request
    //  * @param FreelancerInterface $freelancer
    //  * @param $freeLancerId
    //  * @return mixed
    //  */
    // public function updateNameAndEmail(UpdateNameAndEmail $request, FreelancerInterface $freelancer, $freeLancerId)
    // {
    //     $input = $request->only(['first_name', 'last_name', 'email']);

    //     return $freelancer->update($freeLancerId, $input);
    // }

    // /**
    //  * @param Request             $request
    //  * @param FreelancerInterface $repository
    //  * @param $id
    //  * @return mixed
    //  */
    // public function freelancerSkills(Request $request, FreelancerInterface $repository, $id)
    // {
    //     if (!($freelancer = $repository->findViaId($id))) {
    //         return response([
    //             'http_code' => 404,
    //             'message' => 'Freelancer not found',
    //         ]);
    //     }
    //     $profile = $freelancer->profile()->first();
    //     $skills = $repository->freelancerProfileSkills($profile, $request->all());
    //     // $skills = $repository->freelancerSkills($freelancer, $request->all());

    //     return response(
    //         FreelancerSkillCollection::make($skills)->setHttpCode()
    //     );
    // }

    // /**
    //  * @param SkillRelationsInterface $skillRelations
    //  * @param FreelancerSkillRequest  $request
    //  * @param FreelancerInterface     $repository
    //  * @param $id
    //  * @return mixed
    //  */
    // public function createFreelancerSkills(SkillRelationsInterface $skillRelations, FreelancerSkillRequest $request, FreelancerInterface $repository, $id)
    // {
    //     $input = $request->only([
    //         'skill_id',
    //         'rating',
    //         'skill_order',
    //         'sort',
    //     ]);

    //     if (!($freelancer = $repository->findViaId($id))) {
    //         return response([
    //             'http_code' => 404,
    //             'message' => 'Freelancer not found',
    //         ]);
    //     }

    //     // added the handler for the skill limit
    //     if ($freelancer->skill()->count() == 15) {
    //         return response([
    //             'http_code' => 400,
    //             'message' => 'Freelancer skill limit reached',
    //         ]);
    //     }

    //     $skills = $repository->storeFreelancerSkills($freelancer, $input);

    //     /**
    //      * Skill Relations handler
    //      */
    //     try {
    //         $skillRelations->setModel($freelancer);
    //         $skillRelations->processSkillRelations();
    //     } catch (\Exception $error) {
    //         app('log')
    //             ->error('Skill Relation Error: ' . json_encode($error->getMessage() . ' - Skill List: ' . json_encode($skills)));
    //     }

    //     return response(
    //         FreelancerSkillCollection::make($skills)->setHttpCode()
    //     );
    // }

    // // // /**
    // // //  * Freelancer Onboard/SignUp Skill creation
    // // //  * @param FreelancerSkillUpdateRequest $request
    // // //  * @param FreelancerInterface          $repository
    // // //  * @param $email
    // // //  * @param $skill_id
    // // //  * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
    // // //  */
    // // // public function onboardSkillCreate(FreelancerSkillUpdateRequest $request, FreelancerInterface $repository, $email, $skill_id)
    // // // {
    // // //     $input = $request->only([
    // // //         'rating',
    // // //         'skill_order',
    // // //     ]);

    // // //     if (!($freelancer = $repository->findViaEmail($email))) {
    // // //         return response([
    // // //             'http_code' => 404,
    // // //             'message' => 'Freelancer not found',
    // // //         ]);
    // // //     }

    // // //     // added the handler for the skill limit
    // // //     if ($freelancer->freelancerSkills()->count() == 15 && $input['rating'] == 0) {
    // // //         return response([
    // // //             'http_code' => 400,
    // // //             'message' => 'Freelancer skill limit reached',
    // // //         ]);
    // // //     }

    // // //     $this->skillsRelations($request, $repository, $freelancer);

    // // //     $input['skill_id'] = $skill_id;
    // // //     // $skills = $repository->storeFreelancerSkills($freelancer, $input);
    // // //     $profile = $freelancer->profile()->first();
    // // //     $skills = $repository->storeFreelancerProfileSkills($profile, $input);

    // // //     return response(
    // // //         FreelancerSkillCollection::make($skills)->setHttpCode()
    // // //     );
    // // // }

    // // /**
    // //  * On Boarding skills api
    // //  *
    // //  * @param FreelancerInterface $repository
    // //  * @param $email
    // //  * @param $skill_id
    // //  * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
    // //  */
    // // public function onboardSkills(FreelancerInterface $repository, $email)
    // // {
    // //     if (!($freelancer = $repository->findViaEmail($email))) {
    // //         return response([
    // //             'http_code' => 404,
    // //             'message' => 'Freelancer not found',
    // //         ]);
    // //     }

    // //     // $skills = $repository->freelancerSkills($freelancer);
    // //     $profile = $freelancer->profile()->first();
    // //     $skills = $repository->freelancerProfileSkills($profile);

    // //     return response(
    // //         FreelancerSkillCollection::make($skills)->setHttpCode()
    // //     );
    // // }

    // /**
    //  * @param FreelancerInterface $repository
    //  * @param $email
    //  * @param $skill_id
    //  * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
    //  */
    // public function onboardSkillDelete(FreelancerInterface $repository, $email, $skill_id)
    // {
    //     if (!($freelancer = $repository->findViaEmail($email)->profile()->first())) {
    //         return response([
    //             'http_code' => 404,
    //             'message' => 'Freelancer not found',
    //         ]);
    //     }

    //     // $repository->deleteFreelancerSkill($freelancer, $skill_id);
    //     $repository->deleteFreelancerProfileSkill($freelancer, $skill_id);

    //     return response(
    //         [
    //             'http_code' => 200,
    //             'message' => 'Successfully deleted freelancer skill!',
    //         ]
    //     );
    // }

    // /**
    //  * @param FreelancerInterface $repository
    //  * @param $email
    //  * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
    //  */
    // public function onBoardSkillProposal(FreelancerInterface $repository, $email)
    // {
    //     if (!($freelancer = $repository->findViaEmail($email))) {
    //         return response([
    //             'http_code' => 404,
    //             'message' => 'Freelancer not found',
    //         ]);
    //     }

    //     $skill_proposals = $repository->onBoardSkillProposals($freelancer);

    //     return response(
    //         SkillProposalMemberCollection::make($skill_proposals)->setHttpCode()
    //     );
    // }

    // /**
    //  * @param Request             $request
    //  * @param FreelancerInterface $repository
    //  * @param $email
    //  * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
    //  */
    // public function onBoardSkillProposalStore(Request $request, FreelancerInterface $repository, $email)
    // {
    //     if (!($freelancer = $repository->findViaEmail($email))) {
    //         return response([
    //             'http_code' => 404,
    //             'message' => 'Freelancer not found',
    //         ]);
    //     }

    //     $input = $request->only([
    //         'skill_name',
    //         'rating',
    //     ]);

    //     $skill_proposals = $repository->onBoardSkillProposalsStore($freelancer, $input);

    //     return response(
    //         SkillProposalMemberCollection::make($skill_proposals)->setHttpCode()
    //     );
    // }

    // /**
    //  * @param Request             $request
    //  * @param FreelancerInterface $repository
    //  * @param $email
    //  * @param $id
    //  * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
    //  */
    // public function onBoardSkillProposalUpdate(Request $request, FreelancerInterface $repository, $email, $id)
    // {
    //     if (!($freelancer = $repository->findViaEmail($email))) {
    //         return response([
    //             'http_code' => 404,
    //             'message' => 'Freelancer not found',
    //         ]);
    //     }

    //     $input = $request->only([
    //         'skill_name',
    //         'rating',
    //     ]);

    //     $input['skill_proposal_id'] = $id;
    //     $skill_proposals = $repository->onBoardSkillProposalsStore($freelancer, $input);

    //     return response(
    //         SkillProposalMemberCollection::make($skill_proposals)->setHttpCode()
    //     );
    // }

    // /**
    //  * @param FreelancerInterface $repository
    //  * @param $email
    //  * @param $id
    //  * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
    //  */
    // public function onBoardSkillProposalDelete(FreelancerInterface $repository, $email, $id)
    // {
    //     if (!($freelancer = $repository->findViaEmail($email))) {
    //         return response([
    //             'http_code' => 404,
    //             'message' => 'Freelancer not found',
    //         ]);
    //     }

    //     $repository->onBoardSkillProposalsDelete($id);

    //     return response(
    //         [
    //             'http_code' => 200,
    //             'message' => 'Successfully deleted freelancer skill!',
    //         ]
    //     );
    // }

    // /**
    //  * @param FreelancerInterface $repository
    //  * @param $id
    //  * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
    //  */
    // public function freelancerSkillProposal(FreelancerInterface $repository, $id)
    // {
    //     if (!($freelancer = $repository->findViaId($id))) {
    //         return response([
    //             'http_code' => 404,
    //             'message' => 'Freelancer not found',
    //         ]);
    //     }

    //     $skill_proposals = $repository->onBoardSkillProposals($freelancer);

    //     return response(
    //         SkillProposalMemberCollection::make($skill_proposals)->setHttpCode()
    //     );
    // }
}
