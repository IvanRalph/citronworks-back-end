<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Repositories\Contracts\Api\FreelancerCompanyInterface;
use App\Repositories\Contracts\Api\FreelancerInterface;
use App\Repositories\Contracts\Api\EmployerInterface;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Auth\ChangePassword;
use App\Http\Controllers\Controller;

class ChangePasswordController extends Controller
{
    /**
     * @var FreelancerInterface
     */
    protected $getFreelancer;

    /**
     * @var FreelancerCompanyInterface
     */
    protected $getFreelancerCompany;

    /**
     * @var EmployerInterface
     */
    protected $getEmployer;

    /**
     * ChangePasswordController constructor.
     * @param FreelancerCompanyInterface $company
     * @param FreelancerInterface $freelancer
     * @param EmployerInterface $employer
     */
    public function __construct(FreelancerCompanyInterface $company, FreelancerInterface $freelancer, EmployerInterface $employer)
    {
        $this->getFreelancer = $freelancer;
        $this->getEmployer = $employer;
        $this->getFreelancerCompany = $company;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ChangePassword $request, string $provider, int $id)
    {
        $input =
            $request
                ->only([
                    'old_password',
                    'password',
                ]);

        try {
            $user =
                $this->_findUserViaProvider($provider, $id);

            return
                $this->_ifValidPasswordUpdate($provider, $user, $input);
        } catch (\Exception $e) {
            return
                response($this->_errorMessage(), 422);
        }
    }

    /**
     * Checking of password and update functionality.
     *
     * @param $provider
     * @param $resp
     * @param $input
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    private function _ifValidPasswordUpdate($provider, $resp, $input)
    {
        if (Hash::check($input['old_password'], $resp['password'])) {
            if ($input['old_password'] === $input['password']) {
                return
                    response($this->_oldPasswordUsedMessage(), 422);
            }

            $password = array_filter($input, function ($key) {
                return $key == 'password';
            }, ARRAY_FILTER_USE_KEY);

            $changed =
                $this->_updateUserAccount($resp, $provider, $password);

            return
                response($changed, 200);
        } else {
            return
                response($this->_wrongPasswordMessage(), 422);
        }
    }

    /**
     * Find Provider and check if user exist via id.
     *
     * @param $provider
     * @param $id
     * @return array|mixed
     */
    private function _findUserViaProvider($provider, $id)
    {
        switch ($provider) {
            case 'company':
                $resp = $this->getFreelancerCompany->findViaId($id);

                break;
            case 'freelancer':
                $resp = $this->getFreelancer->findViaId($id);

                break;
            case 'employer':

                $resp = $this->getEmployer->findViaId($id);

                break;
            default:
                $resp = [];
        }

        return
            $resp;
    }

    /**
     * Update password via provider if user exist.
     *
     * @param $id
     * @param $provider
     * @param $input
     * @return array|mixed
     */
    private function _updateUserAccount($id, $provider, $input)
    {
        switch ($provider) {
            case 'company':
                $resp = $this->getFreelancerCompany->update($id['freelancer_company_id'], $input);

                break;
            case 'freelancer':
                $resp = $this->getFreelancer->update($id['freelancer_id'], $input);

                break;
            case 'employer':
                $resp = $this->getEmployer->update($id['employer_id'], $input);

                break;
            default:
                $resp = [];
        }

        return
            $resp;
    }

    /**
     * Return message if user does not exist or throw error;
     *
     * @return array
     */
    private function _errorMessage()
    {
        return [
            'message' => 'The given data was invalid.',
            'errors' => [
                'account' => 'User does not exist.',
            ],
        ];
    }

    /**
     * Return message if the new password used the existing one.
     *
     * @return array
     */
    private function _oldPasswordUsedMessage()
    {
        return [
            'message' => 'The given data was invalid.',
            'errors' => [
                'check_password' => 'You cannot use same password as replacement for the new password..',
            ],
        ];
    }

    private function _wrongPasswordMessage()
    {
        return [
            'message' => 'The given data was invalid.',
            'errors' => [
                'check_password' => 'User\'s old password is incorrect.',
            ],
        ];
    }
}
