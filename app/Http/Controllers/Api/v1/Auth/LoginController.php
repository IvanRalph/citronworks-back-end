<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Exceptions\UnverifiedEmailException;
use App\Mail\EmployerAlertsEmail;
use App\Model\Employer\Employer;
use App\Model\Freelancer\Freelancer;
use App\Repositories\Eloquent\Api\EmployerDataManipulation;
use App\Repositories\Eloquent\Api\Subscription;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Mail;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    private function _validateCredentials($param)
    {
        $validator = app('validator')->make(
            $param,
            [
                'username' => [
                    'required',
                    'email',
                ],
                'password' => [
                    'required',
                ],
            ]
        );

        return $validator->fails();
    }

    /**
     * Return and generate all toke for authentication
     * Add master grant for freelancer.
     * Add master grant for employer.
     *
     * @param ServerRequestInterface $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $param = $request->only('username', 'password', 'from_signup');

        if ($this->_validateCredentials($param)) {
            return response($this->setErrorLoginResponse(), 422);
        };

        return $this->_normalGrant($param);
    }

    /**
     * Normal login authentication.
     *
     * @param $request
     * @param $param
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    protected function _normalGrant($param)
    {
        $initProvider = app('api-authentication', $param);

        $user = $initProvider->returnClientProviderToken();

        if (empty($user) || isset($user->authorization['errors'])) {
            return response($this->setErrorLoginResponse(), 422);
        }

        // Try and catch for email verification
        // Send alerts email
        // Subscription
        try {
            //If login is NOT from sign up
            if (!isset($param['from_signup'])) {
                $this->_isEmailVerified($user);
            } elseif ($this->_isOnBoardingEmployer($param, $user)) {
                $this->_sendAlertsEmail($user);
            }

            return response($user, 200);
        } catch (UnverifiedEmailException $e) {
            return response($this->setErrorEmailVerifiedResponse(), 422);
        } catch (OAuthServerException $e) {
            return response(['message' => $e->getMessage(), 'code' => $e->getCode()], 500);
        } catch (Exception $e) {
            return response(['message' => $e->getMessage(), 'code' => $e->getCode()], 500);
        }
    }

    /*
     * If login is from on boarding and user is an employer.
     */
    private function _isOnBoardingEmployer($param, $user)
    {
        return isset($param['from_signup']) && $user['provider'] == 'employer';
    }

    /**
     * @param $user
     * @throws UnverifiedEmailException
     */
    protected function _isEmailVerified($user) : void
    {
        if (!$user->email_verified_at) {
            throw new UnverifiedEmailException($user);
        }
    }

    /**
     * @param $user
     */
    protected function _sendAlertsEmail($user)
    {
        $employer = new EmployerDataManipulation();
        $employerResult = $employer->show($user)->load('subscription');

        Mail::to('alerts@citronworks.com')
            ->send(new EmployerAlertsEmail($employerResult));
    }

    public function verifyEmail($token)
    {
        if ($employer = Employer::where('activation_token', $token)->first()) {
            if ($employer->email_verified_at) {
                return response()->json([
                    'title' => 'Email Verification',
                    'message' => 'You have already validated this email, please log in.'
                ]);
            } else {
                $employer->email_verified_at = Carbon::now();
                $employer->save();
                return response()->json([
                    'title' => 'Email Verification',
                    'message' => 'Your email has been verified, you can now log in.',
                    'data' => $employer
                ]);
            }
        }

        if ($freelancer = Freelancer::where('activation_token', $token)->first()) {
            if ($freelancer->email_verified_at) {
                return response()->json([
                    'title' => 'Email Verification',
                    'message' => 'You have already validated this email, please log in.'
                ]);
            } else {
                $freelancer->email_verified_at = Carbon::now();
                $freelancer->save();
                return response()->json([
                    'title' => 'Email Verification',
                    'message' => 'Your email has been verified, you can now log in.',
                    'data' => $freelancer
                ]);
            }
        }

        return response()->json([
            'title' => 'Email Verification',
            'message' => 'This email validation link is invalid'
        ], 404);
    }

    /**
     * @return array
     */
    protected function setErrorLoginResponse(): array
    {
        return [ 'errors' => [ 'title' => 'Wrong Credentials' , 'message' => "These didn't match any of our records. It looks like you may have forgotten your credentials."]];
    }

    /**
     * @return array
     */
    protected function setErrorEmailVerifiedResponse(): array
    {
        return ['errors' => ['title' => 'You need to verify your email', 'message' => 'We have sent you a new verification email. Please check your spam folder if it\'s not in your inbox.']];
    }
}
