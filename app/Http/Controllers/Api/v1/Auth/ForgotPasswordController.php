<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Http\Controllers\Controller;
use App\Model\Employer\Employer;
use App\Model\Freelancer\Freelancer;
use App\Model\Freelancer\FreelancerCompany;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    protected $member;

    use SendsPasswordResetEmails;

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        //Check if employer|freelancer|freelancerCompany
        $this->resolverMember($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        if ($this->member === 'employer') {
            $response = $this->employerBroker()->sendResetLink(
                $this->credentials($request)
            );
        } else {
            $response = $this->freelancerBroker()->sendResetLink(
                $this->credentials($request)
            );
        }

        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse($request, $response)
            : $this->sendResetLinkFailedResponse($request, $response);
    }

    protected function sendResetLinkResponse(Request $request, $response)
    {
        return response([
            'success' => trans($response),
        ]);
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return response([
            trans($response),
        ], 422);
    }

    /**
     * @return mixed
     */
    protected function employerBroker()
    {
        return Password::broker('employer');
    }

    /**
     * @return mixed
     */
    protected function freelancerBroker()
    {
        return Password::broker('freelancer');
    }

    /**
     * @return mixed
     */
    protected function fCompanyBroker()
    {
        return Password::broker('company');
    }

    protected function resolverMember($request)
    {
        if (Employer::where('email', $request['email'])->first()) {
            return $this->member = 'employer';
        }
        if (Freelancer::where('email', $request['email'])->first()) {
            return $this->member = 'freelancer';
        }

        return null;
    }
}
