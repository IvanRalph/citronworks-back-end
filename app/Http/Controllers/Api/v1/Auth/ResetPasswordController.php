<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Http\Controllers\Controller;
use App\Model\Employer\Employer;
use App\Model\Freelancer\Freelancer;
use App\Rules\LowerCase;
use App\Rules\Number;
use App\Rules\UpperCase;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    protected $member;

    use ResetsPasswords;

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'memberType' => 'string',
            'password' => [
                'required',
                'min:8',
                'confirmed',
                'max:20',
                new Uppercase(),
                new Number(),
                new Lowercase(),
            ],
        ];
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $request->validate($this->rules(), $this->validationErrorMessages());

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $this->resolveMember($request);

        if ($this->member === 'employer') {
            $response = $this->employerBroker()->reset(
                $this->credentials($request),
                function ($user, $password) {
                    $this->resetPassword($user, $password);
                }
            );
        } elseif ($this->member === 'freelancer') {
            $response = $this->freelancerBroker()->reset(
                $this->credentials($request),
                function ($user, $password) {
                    $this->resetPassword($user, $password);
                }
            );
        }
        /*else {
            $response = $this->fCompanyBroker()->reset(
                $this->credentials($request),
                function ($user, $password) {
                    $this->resetPassword($user, $password);
                }
            );
        }*/

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
            ? $this->sendResetResponse($request, $response)
            : $this->sendResetFailedResponse($request, $response);
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return string
     */
    protected function sendResetResponse(Request $request, $response)
    {
        return  response([
            'success' => trans($response),
        ]);
    }

    protected function sendResetFailedResponse(Request $request, $response)
    {
        return response([
            'error' => trans($response),
        ], 422);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->update(['password' => $password]);

        event(new PasswordReset($user));
    }

    /**
     * @return mixed
     */
    protected function employerBroker()
    {
        return Password::broker('employer');
    }

    /**
     * @return mixed
     */
    protected function freelancerBroker()
    {
        return Password::broker('freelancer');
    }

    /**
     * @return mixed
     */
    protected function fCompanyBroker()
    {
        return Password::broker('company');
    }

    protected function resolveMember($request)
    {
//        if (Employer::where('email', $request['email'])->first()) {
//            return $this->member = 'employer';
//        }
//        if (Freelancer::where('email', $request['email'])->first()) {
//            return $this->member = 'freelancer';
//        }
        if ($request['memberType'] === 'freelancer') {
            return $this->member = 'freelancer';
        }
        if ($request['memberType'] === 'employer') {
            return $this->member = 'employer';
        }

        return null;
    }
}
