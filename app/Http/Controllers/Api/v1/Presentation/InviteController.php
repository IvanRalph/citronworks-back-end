<?php

namespace App\Http\Controllers\Api\v1\Presentation;

use App\Http\Requests\Presentation\Invite as InviteRequest;
use App\Mail\BetaWelcomeEmail;
use App\Model\PresentationInvite\Invite as InviteModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class InviteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InviteRequest $request)
    {
        $input = $request
            ->only(
                'first_name',
                'last_name',
                'email',
                'member_category',
                'company_name',
                'company_url',
                'phone'
            );
        $input['created_at'] = now();

        $invite =
                InviteModel::create($input);

        Mail::to($input['email'])
            ->send(new BetaWelcomeEmail($input['first_name'], str_replace('_', ' ', $input['member_category'])));

        return
            response($invite, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
