<?php

namespace App\Http\Controllers\Api\v1\FreelancerCompany;

use App\Http\Requests\FreelancerCompany\RegistrationRequest;
use App\Http\Requests\FreelancerCompany\NextRegistrationRequest;
use App\Http\Requests\FreelancerCompany\UpdateProfile;
use App\Model\FreelancerCompany\FreelancerCompany;
use App\Repositories\Contracts\Api\FreelancerCompanyInterface;
use App\Http\Controllers\Controller;

class FreelancerCompanyController extends Controller
{
    /**
     * Registration
     *
     * @param RegistrationRequest $request
     * @param FreelancerCompanyInterface $freelancerCompany
     * @return array
     */
    public function registration(RegistrationRequest $request, FreelancerCompanyInterface $freelancerCompany)
    {
        $input = $request
            ->only(
                [
                    'first_name', 'last_name', 'email', 'password', 'role',
                ]
            );

        try {
            $company = $freelancerCompany->store($input);
            $freelancerCompany->storeInformation($company);
            $freelancerCompany->storeGdpr($company, $input);
            $freelancerCompany->storeBilling($company);

            return
                $company
                    ->load(
                        'information',
                        'gdpr',
                        'billingInformation'
                    );
        } catch (\Exception $e) {
            return [
                'error' => $e->getCode(),
                'message' > $e->getMessage(),
            ];
        }
    }

    /**
     * Update information and details.
     *
     * @param NextRegistrationRequest $request
     * @param FreelancerCompanyInterface $freelancerCompany
     * @return mixed
     */
    public function nextRegistration(NextRegistrationRequest $request, FreelancerCompanyInterface $freelancerCompany)
    {
        $company = [];
        $input =
            $request->only([
                'email', 'company_profile', 'invitation', 'billing_information', 'information',
            ]);

        if (is_null($input['email'])) {
            $input['email'] = $request->query('email');
        }

        /**
         * Upload logo on images
         */
        if (isset($request->file('information')['logo'])
            && $request->file('information')['logo']->isValid()) {
            $path = $request->information['logo']->store('images');
            $input['information']['logo'] = $path;
        }

        /**
         * Find id employer exist
         */
        if (!empty($freelancerCompany->findFreelancerCompany($input))
            && $company = $freelancerCompany->findFreelancerCompany($input)) {
            /**
             * Update information
             */
            if (isset($input['company_profile'])) {
                $freelancerCompany
                    ->updateInformation($company, $input['company_profile']);
            }

            /**
             * Update Billing Information
             */
            if (isset($input['billing_information'])) {
                $freelancerCompany
                    ->updateBilling($company, $input['billing_information']);
            }

            /**
             * Upload logo
             */
            if (isset($input['information']['logo'])) {
                $freelancerCompany
                    ->updateLogo($company, $input['information']['logo']);
            }

            /**
             * Save invitation.
             */
            if (isset($input['invitation']) && count($input['invitation']) > 0) {
                $freelancerCompany
                    ->storeInvitation($company, $input['invitation']);
            }

            return
                $company
                    ->load(
                        'gdpr',
                        'information',
                        'billingInformation',
                        'invite'
                    );
        }

        return
            $company;
    }

    /**
     * Show details
     *
     * @param $email
     * @param FreelancerCompanyInterface $freelancerCompany
     * @return mixed
     */
    public function show($email, FreelancerCompanyInterface $freelancerCompany)
    {
        $return = FreelancerCompany::where('email', $email)->first();

        return
            $freelancerCompany
                ->show($return);
    }

    /**
     * Updating details
     *
     * @param UpdateProfile $request
     * @param FreelancerCompanyInterface $freelancerCompany
     * @param $freelancerCoId
     * @return mixed
     */
    public function updateProfile(UpdateProfile $request, FreelancerCompanyInterface $freelancerCompany, $freelancerCoId)
    {
        $input =
            $request
                ->only([
                    'first_name',
                    'last_name',
                    'information',
                    'billing_information',
                ]);
        if (!empty($freelancerCompany->update($freelancerCoId, $input)) &&
            $company = $freelancerCompany->update($freelancerCoId, $input)) {
            /**
             * Update information
             */
            if (isset($input['information'])) {
                $freelancerCompany
                    ->updateInformation($company, $input['information']);
            }

            /**
             * Update Billing Information
             */
            if (isset($input['billing_information'])) {
                $freelancerCompany
                    ->updateBilling($company, $input['billing_information']);
            }
        }

        return
            $company
                ->load(
                    'information',
                    'billingInformation',
                );
    }
}
