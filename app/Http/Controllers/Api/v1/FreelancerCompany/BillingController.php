<?php

namespace App\Http\Controllers\Api\v1\FreelancerCompany;

use App\Http\Controllers\Controller;
use App\Http\Requests\Billing\ValidateSubscriptionRequest;
use App\Http\Requests\Billing\ValidateTokenRequest;
use App\Http\Resources\Billing\CardDetailsResource;
use App\Http\Resources\Billing\SubscriptionResource;
use App\Http\Resources\Invoice\InvoicesCollection;
use App\Http\Resources\Invoice\InvoiceResource;
use App\Repositories\Contracts\Api\SubscriptionInterface;

class BillingController extends Controller
{
    public function invoices()
    {
        return new InvoicesCollection(auth()->user()->invoices());
    }

    public function findInvoice($id)
    {
        return new InvoiceResource(auth()->user()->findInvoice($id));
    }

    public function downloadInvoice($id)
    {
        return auth()->user()->findInvoice($id)->download();
    }

    public function updateCard(ValidateTokenRequest $request)
    {
        return new CardDetailsResource(auth()->user()->updateCard($request->validated()['token']));
    }

    public function getSubscription()
    {
        return new SubscriptionResource(auth()->user()->getSubscription());
    }

    /**
     * Upgrade subscription to Yearly
     * Or Downgrade from Monthly
     * @param $planId
     * @return SubscriptionResource
     * @throws \Exception
     */
    public function upgradeSubscription($planId)
    {
        //check if monthly or yearly
        if ($subscription = auth()->user()->subscription('monthly')) {
            if (strtok($subscription->braintree_plan, '-') === 'Silver') {
                auth()->user()->subscription('monthly')->swap('yearly', 'Silver-yearly');
            } else {
                auth()->user()->subscription('monthly')->swap('yearly', 'Gold-yearly');
            }
        } else {
            $subscription = auth()->user()->subscription('yearly');

            if (strtok($subscription->braintree_plan, '-') === 'Silver') {
                auth()->user()->subscription('yearly')->swap('monthly', 'Silver-monthly');
            } else {
                auth()->user()->subscription('yearly')->swap('monthly', 'Gold-monthly');
            }
        }

        return new SubscriptionResource(auth()->user()->getSubscription());
    }

    public function pauseSubscription(ValidateSubscriptionRequest $request, SubscriptionInterface $subscription)
    {
        $subscription->pauseSubscription($request->validated());

        return new SubscriptionResource(auth()->user()->getSubscription());
    }

    public function resumeSubscription(ValidateSubscriptionRequest $request, SubscriptionInterface $subscription)
    {
        $subscription->resumeSubscription($request->validated());

        return new SubscriptionResource(auth()->user()->getSubscription());
    }
}
