<?php

namespace App\Http\Controllers\Api\v1\Referrer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\Api\RefUrlInterface;

class ReferrerController extends Controller
{
    /**
     * Post Ref Token and UTM
     *
     * @param Request $request
     * @param RefUrlInterface $refUrl
     * @return void
     */
    public function index(
        Request $request,
        RefUrlInterface $refUrl
    ) {
        return
            $refUrl->insert(
                array_replace(
                    $request->all(),
                    [
                    'ip_address' => $request->ip(),
                    'utm' => json_encode($request->utm),
                ]
                )
            );
    }

    /**
     * Track down return user visit
     *
     * @param Request $request
     * @param RefUrlInterface $refUrl
     * @return void
     */
    public function return(
        Request $request,
        RefUrlInterface $refUrl
    ) {
        $ref =
            $refUrl
                ->show($request->ref['ref_url_id'], 0);

        $count = 0;
        switch ($ref->return_visit) {
            case null:
                $count = 1;

                break;
            default:
                $count = $ref->return_visit + 1;
        }

        $update =
            tap($ref)->update([
                'return_visit' => $count,
            ]);

        return
            response()->json($update);
    }
}
