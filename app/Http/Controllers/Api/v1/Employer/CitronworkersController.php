<?php

namespace App\Http\Controllers\Api\v1\Employer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Employer\CitronworkersStoreRequest;
use App\Model\Employer\Citronworkers;
use App\Mail\CitronworkersRequestEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CitronworkersController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Citronworkers();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CitronworkersStoreRequest $request)
    {
        $input = $request->input('citronworkers_request');

        $this->model->first_name =  $input['first_name'] ?? '';
        $this->model->last_name = $input['last_name'] ?? '';
        $this->model->email = $input['email'] ?? '';
        $this->model->referer = $input['referer'] ?? 'none';
        $this->model->company = $input['company'] ?? '';
        $this->model->phone = $input['phone'] ?? '';
        $this->model->company_website = $input['company_website']?? '';
        $this->model->message = $input['message'] ?? '';

        if (!$this->model->save()) {
            abort(422, 'Something went wrong saving citronworkers request.');
        }

        /**
         * Email notification
         */
        Mail::to(['nick@citronworks.com', 'leo@citronworks.com'])->send(new CitronworkersRequestEmail($this->model));

        return
            response($this->model, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
