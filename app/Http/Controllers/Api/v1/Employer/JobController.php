<?php

namespace App\Http\Controllers\Api\v1\Employer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Job\SaveRequest;
use App\Http\Resources\Jobs\JobResource;
use App\Mail\JobAlertsEmail;
use App\Model\Job\Job;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Skill\SkillProposal;
use Illuminate\Support\Facades\Mail;

class JobController extends Controller
{
    /**
     * Fetch jobs of the employer
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // get the jobs of the employer
        $query = Auth::user()->jobs()
            // include number of applications
            ->withCount([
                'applications' => fn ($subQuery) => $subQuery->notEmpty(),
            ])
            // order by most recently updated
            ->orderBy('updated_at', 'desc');

        // scope the query if requested
        $scope = $request->scope;
        if (! empty($scope)
            // white-list acceptable scopes for security purposes
            && in_array($scope, ['public', 'deactivated'])
        ) {
            // apply the query scope
            $query->$scope();
        }

        // show 10 per page
        return $query->paginate($request->perPage ?: 10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SaveRequest                                    $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(SaveRequest $request)
    {
        // authorize creating a job
        $this->authorize('create', Job::class);

        // create the job
        $job = Auth::user()->jobs()->create($request->validated());
        // sync skills with the job
        $job->skill()->sync($request->skill['skill'] ?? []);
        // Add suggested if present in the request skill request object
        if ($request->skill && isset($request->skill['proposal'])) {
            // (new SkillProposal)->createAll($request->skill['proposal'], 'employer');
            if ($request->skill['proposal']) {
                foreach ($request->skill['proposal'] as $m) {
                    $job->skill_proposal()->create(array_replace(collect($m)->only('skill_name')->toArray(), [
                        'member_type' => 'employer',
                        'member_id' => Auth::user()->getKey(),
                        'category_id' => 1,
                        'job_id' => $job->getKey()
                    ]));
                }
            }
        }

        // re-sync with Scout
        $job->resyncSearchIndex();

        Mail::to('alerts@citronworks.com')
            ->send(new JobAlertsEmail($job));

        // return the job
        return $this->show($job);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Job\Job                             $job
     * @return JobResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Job $job)
    {
        // authorize viewing the job
        $this->authorize('view', $job);

        // return the job
        return new JobResource($job->load([
            'skill',
            'applications' => fn (HasMany $query) => $query->notEmpty(),
            'applications.member.profile',
            'applications.member.freelancerSkills.skill',
            'job_skill_proposals'
        ]));
    }

    /**
     * Update the specified storage
     *
     * @param SaveRequest $request
     * @param Job         $job
     *
     * @return Response
     */
    public function update(SaveRequest $request, Job $job)
    {
        // authorize updating the job
        $this->authorize('update', $job);

        if ($request->skill && isset($request->skill['skill'])) {
            $skills = collect($request->skill['skill'])->map(fn ($i, $k) => ['skill_id' => $i['skill_id'], 'skill_order' => $i['skill_order']]);

            // sync skills with the job
            $job->skill()->sync($skills);
        }

        // Add suggested if present in the request skill request object
        if ($request->skill && isset($request->skill['proposal'])) {
            foreach ($request->skill['proposal'] as $m) {
                $job->skill_proposal()->create(array_replace(collect($m)->only('skill_name')->toArray(), [
                    'member_type' => 'employer',
                    'member_id' => Auth::user()->getKey(),
                    'category_id' => 1,
                    'job_id' => $job->getKey()
                ]));
            }
        }

        if ($request->skill && isset($request->job_skill_proposals)) {
            (new SkillProposal)->filterDelete($request->job_skill_proposals);
        }

        // update the job
        $job->update($request->validated());

        // return the job
        return $this->show($job);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Job\Job        $job
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {
        return $this->authorizeAndDo('delete', $job);
    }

    protected function authorizeAndDo(string $action, Job $job)
    {
        // authorize doing the action
        $this->authorize($action, $job);

        // perform the action
        $job->$action();

        // return the job in case frontend needs
        // a record of the deleted job
        return $this->show($job);
    }

    public function deactivate(Job $job)
    {
        return $this->authorizeAndDo('deactivate', $job);
    }

    public function reactivate(Job $job)
    {
        return $this->authorizeAndDo('reactivate', $job);
    }
}
