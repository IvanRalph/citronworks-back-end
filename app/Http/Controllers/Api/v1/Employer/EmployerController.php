<?php

namespace App\Http\Controllers\Api\v1\Employer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Employer\EmployerUpdate as EmployerUpdateRequest;
use App\Http\Requests\Employer\NextStepRegistrationInvitation;
use App\Http\Requests\Employer\UpdateCompanyProfile;
use App\Http\Requests\Employer\UpdateCompanyRequest;
use App\Repositories\Contracts\Api\EmployerInterface;
use App\Repositories\Eloquent\Api\EmployerDataManipulation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmployerController extends Controller
{
    /**
     * @param  NextStepRegistrationInvitation                                          $request
     * @param  EmployerInterface                                                       $employer
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function nextStepRegistrationInvitation(NextStepRegistrationInvitation $request, EmployerInterface $employer)
    {
        $findEmployer = [];
        $input = $request->only(['email', 'invitation']);

        if (! empty($employer->findAndEmployer($input))
            && $findEmployer = $employer->findAndEmployer($input)) {
            $invitation = $employer->invitation($findEmployer, $input['invitation']);

            return response(['invitation' => $invitation], 200);
        }

        return response(['invitation' => $findEmployer], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param $email
     * @param  EmployerInterface                                                       $employer
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function show()
    {
        return Auth::user()->load('company.billing');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  EmployerInterface         $employer
     * @param  int                       $employerId
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployerInterface $employer, $employerId)
    {
        //
    }

    /**
     * @param  Request           $request
     * @param  EmployerInterface $employer
     * @return void
     */
    public function updateEmployerProfile(
        EmployerUpdateRequest $request,
        EmployerInterface $employer
    ) {
        $input = $request->only(['first_name', 'last_name']);

        return $employer->update($request->employer_id, $input);
    }

    /**
     * @param  UpdateCompanyProfile $request
     * @param  EmployerInterface    $employer
     * @return void
     */
    public function updateCompanyProfile(
        UpdateCompanyProfile $request,
        EmployerInterface $employer
    ) {
        $employer
            ->updateCompanyByEmployer(
                $request->except(['billing', 'company']),
                collect($request->company)->except(['billing'])->toArray()
            );

        return $request->company;
    }

    public function showCompanyInfo()
    {
        return Auth::user()->company;
    }

    /**
     * Update company info.
     *
     * @param UpdateCompanyRequest $request
     * @param EmployerInterface $employer
     * @return Illuminate\Database\Eloquent\Model
     */
    public function updateCompanyInfo(
        UpdateCompanyRequest $request,
        EmployerInterface $employer
    ) : Model {
        return $employer->updateCompany($request->validated());
    }

    /**
     * Update company logo.
     *
     * @param Request $request
     * @param EmployerInterface $employer
     * @return Illuminate\Database\Eloquent\Model
     */
    public function updateCompanyLogo(
        Request $request,
        EmployerInterface $employer
    ) : Model {
        $request->validate(['logo' => 'required|mimes:jpg,png|max:2056']);

        return $employer->updateCompany([
            'logo' => $request->logo->store('images')
        ]);
    }

    public function showNameAndEmail(Request $request)
    {
        return Auth::user()->only(['first_name', 'last_name', 'email']);
    }

    public function updateNameAndEmail(Request $request)
    {
        return tap(Auth::user())->update($request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
        ]))->load('company.billing');
    }

    public function bookmarkedFreelancers()
    {
        $employer = new EmployerDataManipulation();
        return $employer->getBookmarkedFreelancers(Auth::user());
    }

    public function storeBookmarkedFreelancers(Request $request)
    {
        $employer = new EmployerDataManipulation();
        return $employer->storeBookmarkedFreelancer(Auth::user(), $request);
    }

    public function destroyBookmarkedFreelancers($id)
    {
        $employer = new EmployerDataManipulation();
        return $employer->deleteBookmarkedFreelancer(Auth::user(), $id);
    }
}
