<?php

namespace App\Http\Controllers\Api\v1\Employer;

use \TestAbc;
use App\Container\TestAbcInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\Employer\TestRequest;

class TestAbcController extends Controller
{
    public function update()
    {
        return 'update';
    }

    public function save()
    {
        return 'Saving';
    }

    /**
     * Testing interface service container.
     *
     * @param TestAbcInterface $testAbc
     * @return mixed
     */
    public function index(TestAbcInterface $testAbc)
    {
        return
            $testAbc->getName();
    }

    /**
     * Testing facade
     *
     */
    public function facade()
    {
        return
            TestAbc::getName();
    }

    /**
     * Testing store validation
     */
    public function store(TestRequest $request)
    {
        return
            $request->all();
    }
}
