<?php

namespace App\Http\Controllers\Api\v1\Employer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Employer\SearchFreelancersRequest;
use App\Http\Resources\Employer\FreelancerResource;
use App\Model\Freelancer\Freelancer;
use Auth;

class FreelancerController extends Controller
{
    public function search(SearchFreelancersRequest $request)
    {
        return FreelancerResource::collection(tap(Freelancer::searchFilter($request->validated())
            ->paginate(10))
            ->load('freelancerSkills.skill', 'freelancerCompany', 'profile'));
    }

    public function show(Freelancer $freelancer)
    {
        $freelancer->bookmark = Auth::user()->bookmarkedFreelancers()->where('freelancer_id', $freelancer->freelancer_id)->first();
        return new FreelancerResource($freelancer->load([
            'freelancerSkills.skill',
            'freelancerCompany',
            'profile',
            'hiresByMyCompany',
            'latestHire',
        ]));
    }

    public function hire(Freelancer $freelancer)
    {
        // hire the freelancer
        $freelancer->hire();

        // respond the freelancer
        return $this->show($freelancer);
    }
}
