<?php

namespace App\Http\Controllers\Api\v1\Employer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Billing\ValidateSubscriptionRequest;
use App\Http\Requests\Billing\ValidateTokenRequest;
use App\Http\Resources\Billing\CardDetailsResource;
use App\Http\Resources\Billing\SubscriptionResource;
use App\Http\Resources\Invoice\InvoicesCollection;
use App\Http\Resources\Invoice\InvoiceResource;
use App\Repositories\Contracts\Api\SubscriptionInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Model\Employer\Employer;

class BillingController extends Controller
{
    /**
     * @return InvoicesCollection
     */
    public function invoices(Request $request)
    {
        return
            new InvoicesCollection(
                optional(auth()
                    ->user()
                    ->subscriptions()
                    ->with(['invoice', 'member.hasCompany'])
                    ->first())->toArray()
            );
    }


    /**
     * @param $id
     * @return InvoiceResource
     */
    public function findInvoice($id)
    {
        return new InvoiceResource(auth()->user()->findInvoice($id));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function downloadInvoice($id, Request $request)
    {
        return auth()->user()->findInvoice($id)->download();
    }

    /**
     * @param  ValidateTokenRequest $request
     * @return CardDetailsResource
     */
    public function updateCard(ValidateTokenRequest $request)
    {
        return new CardDetailsResource(auth()->user()->updateCard($request->validated()['token']));
    }

    public function freeSubscriptions(SubscriptionInterface $subscription)
    {
        return response()->json([
            'count' => $subscription->count(['braintree_id' => ''])
        ]);
    }

    /**
     * @return SubscriptionResource
     */
    public function getSubscription()
    {
        return new SubscriptionResource(auth()->user()->getSubscription());
    }

    public function getSubscriptionName()
    {
        if (! empty($subscription = Auth::user()->getSubscription())) {
            return $subscription->subscription_name;
        }

        return 'free';
    }

    /**
     * Upgrade subscription to Yearly
     * Or Downgrade from Monthly
     * @param $planId
     * @return SubscriptionResource
     * @throws \Exception
     */
    public function upgradeSubscription($planId)
    {
        if (is_null(auth()->user()->getBraintreeId($planId))) {
            throw new \Exception('Plan does not exist');
        }

        //check if monthly or yearly
        if ($subscription = auth()->user()->subscription('monthly')->first()) {
            if (isset($subscription->braintree_plan) && strtok($subscription->braintree_plan, '-') === 'Silver') {
                $subscription->swap('yearly', 'Silver-yearly');
            } else {
                $subscription->swap('yearly', 'Gold-yearly');
            }
        } else {
            $subscription = auth()->user()->subscription('yearly')->first();

            if (strtok($subscription->braintree_plan, '-') === 'Silver') {
                $subscription->swap('monthly', 'Silver-monthly');
            } else {
                $subscription->swap('monthly', 'Gold-monthly');
            }
        }

        return new SubscriptionResource(auth()->user()->getSubscription());
    }

    public function pauseSubscription(ValidateSubscriptionRequest $request, SubscriptionInterface $subscription)
    {
        $subscription->pauseSubscription($request->validated());

        // need to use Eloquent update instead of mass-update in order to
        // trigger Algolia's ::updated() model-event listener
        auth()->user()->jobs->each->update([
            'plan_status' => 'free_unverified',
        ]);

        return new SubscriptionResource(auth()->user()->getSubscription());
    }

    public function resumeSubscription(ValidateSubscriptionRequest $request, SubscriptionInterface $subscription)
    {
        $subscription->resumeSubscription($request->validated());

        auth()->user()->jobs->each->update([
            'plan_status' => 'paid',
        ]);

        return new SubscriptionResource(auth()->user()->getSubscription());
    }
}
