<?php

namespace App\Http\Controllers\Api\v1\Employer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Job\InviteRequest;
use App\Mail\JobInviteMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class JobInviteController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InviteRequest $request)
    {
        $jobInvite = Auth::user()
            ->company
            ->jobInvites()
            ->create($request->validated());

        // send an email to the freelancer
        Mail::to($jobInvite->freelancer->email)
            ->send(new JobInviteMail($jobInvite));

        return $jobInvite;
    }
}
