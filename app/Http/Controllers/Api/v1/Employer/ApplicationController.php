<?php

namespace App\Http\Controllers\Api\v1\Employer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ApplicationResource;
use App\Model\Job\Application;
use  App\Model\Attachment\Attachment;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class ApplicationController extends Controller
{
    public function show(Application $application)
    {
        return new ApplicationResource($application->load(['member.hiresByMyCompany', 'member.profile']));
    }

    public function info(Request $request, Attachment $attachment)
    {
        return
            $attachment->find($request->attachmentid) ?? null;
    }
    
    /**
     * @param Request $request
     * @return void
     */
    public function attachment(Request $request, Attachment $attachment)
    {
        $data = json_decode($request->data);
        $attachment = $attachment->find($data->id);
        $filepath = public_path($data->path);
        $headers = [
            'Content-Disposition' => 'attachment; filename='.$attachment->label . $attachment->extension,
            'Content-type' => $attachment->mime
        ];
        try {
            return \Response::download($filepath, $attachment->label . $attachment->extension, $headers);
        } catch (FileNotFoundException $f) {
            // For old format
            return \Response::download(public_path('/files/'.$attachment->attached_id . $attachment->extension), $attachment->attached_id . $attachment->extension, $headers);
        }
    }
}
