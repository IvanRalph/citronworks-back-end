<?php

namespace App\Http\Controllers\Api\v1\Employer;

use App\Mail\ConciergeRequestEmail;
use App\Http\Requests\Employer\ConciergeRequest;
use App\Model\Employer\Concierge;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ConciergeRequestController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Concierge();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConciergeRequest $request)
    {
        $input = $request->only('concierge_request');

        $this->model->first_name = $input['concierge_request']['first_name'] ?? '';
        $this->model->last_name = $input['concierge_request']['last_name'] ?? '';
        $this->model->email = $input['concierge_request']['email'] ?? '';
        $this->model->referer = $input['concierge_request']['referer'] ?? 'none';
        $this->model->company = $input['concierge_request']['company'] ?? '';
        $this->model->phone = $input['concierge_request']['phone'] ?? '';
        $this->model->company_website = $input['concierge_request']['company_website']?? '';
        $this->model->message = $input['concierge_request']['message'] ?? '';

        if (!$this->model->save()) {
            abort(422, 'Something went wrong saving concierge request.');
        }

        /**
         * Email notification
         */
        Mail::to(['nick@citronworks.com', 'leo@citronworks.com'])->send(new ConciergeRequestEmail($this->model));

        return
            response($this->model, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
