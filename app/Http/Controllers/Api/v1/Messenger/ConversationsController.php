<?php

namespace App\Http\Controllers\Api\v1\Messenger;

use App\Http\Controllers\Controller;
use App\Model\Employer\Employer;
use App\Model\Freelancer\Freelancer;
use App\Model\Messaging\Conversation;
use App\Model\Messaging\Participant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConversationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Auth::user()
            ->conversations()
            ->has('lastMessage')
            ->with('lastMessage', 'currentParticipant', 'participants')
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // authorize creation of Conversation
        $this->authorize('create', Conversation::class);

        // validate the request
        $data = $request->validate([
            'member_id' => 'required|integer',
            'member_type' => 'required|in:employer,freelancer',
        ]);

        // create a map between member_type and the model class
        $memberTypeClasses = [
            'employer' => Employer::class,
            'freelancer' => Freelancer::class,
        ];

        // create list of participants
        $participants = [];

        // ensure member exists
        $participants[] = $memberTypeClasses[$data['member_type']]::findOrFail($data['member_id']);

        // of course, add current user as a participant
        $participants[] = Auth::user();

        $this->checkSubscription($participants);

        // fetch or create the conversation between participants
        return Conversation::createOrFetchConversation($participants);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Messaging\Conversation $conversation
     * @return \Illuminate\Http\Response
     */
    public function show(Conversation $conversation)
    {
        // ensure user is authorized to read the conversation
        $this->authorize('read', $conversation);

        // mark conversation as read
        $conversation->read();

        // load related models and send to user
        return $conversation->load('currentParticipant.member');
    }

    public function search(Request $request)
    {
        $search = $request->search;
        if (empty($search)) {
            return [];
        }

        return Participant::search($search)
            ->with(['tagFilters' => Auth::user()->messenger_id])
            ->get()
            ->unique('conversation_id')
            ->load([
                'conversation.lastMessage',
                'conversation.participants',
                'conversation.currentParticipant.member',
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request          $request
     * @param  \App\Model\Messaging\Conversation $conversation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conversation $conversation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Messaging\Conversation $conversation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conversation $conversation)
    {
        //
    }

    /**
     * Check current employer's subscription plan's messaging limit.
     *
     *
     */
    public function checkSubscription(array $participantMembers)
    {
        if (! empty(Conversation::fetchByParticipants($participantMembers))) {
            return true;
        }
        $authUser = Auth::user();
        $plan = $authUser->subscription->braintree_plan ?? null;
        $currentMonth = Carbon::today()->format('m');

        if ($plan === 'Silver-monthly') {
            $conversationsCount = Conversation::whereHas('participants', function ($participants) use ($authUser) {
                $participants->where('member_type', 'employer')->where('member_id', $authUser->id);
            })->whereMonth('created_at', $currentMonth)->count();
            if ($conversationsCount >= 50) {
                return abort(403, 'You have reached your monthly limit to contact freelancers.');
            }
        }
    }
}
