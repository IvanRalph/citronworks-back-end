<?php

namespace App\Http\Controllers\Api\v1\Messenger;

use App\Http\Controllers\Controller;
use App\Model\Messaging\Conversation;
use App\Model\Messaging\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Conversation $conversation)
    {
        return $conversation->messages()->paginate(30);
    }

    /**
     * Add a message to the conversation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Conversation $conversation)
    {
        // authorize sending messages
        $this->authorize('sendMessage', $conversation);

        // validate
        $request->validate([
            'message' => 'required|string|min:1|max:500',
        ]);

        // create a validated message and return it
        return $conversation->sendMessage($request->message)
            ->load('participant.member');
    }

    public function search(Request $request)
    {
        $search = $request->search;
        if (empty($search)) {
            return ['data' => []];
        }

        return tap(Message::search($search)
            ->with(['tagFilters' => Auth::user()->messenger_id])
            ->paginate(10))->load([
                'conversation.lastMessage',
                'conversation.participants',
                'conversation.currentParticipant.member',
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Messaging\Message $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request     $request
     * @param  \App\Model\Messaging\Message $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Messaging\Message $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }
}
