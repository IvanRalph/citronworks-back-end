<?php

namespace App\Http\Controllers\Api\v1\Messenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Broadcast;

class BroadcastController extends Controller
{
    public function auth(Request $request)
    {
        // this workaround solves the issue where two users from
        // different auth guards have the same primary key values
        $user = Auth::user()->setUniqueId();

        return Broadcast::auth($request->setUserResolver(function () use ($user) {
            return $user;
        }));
    }
}
