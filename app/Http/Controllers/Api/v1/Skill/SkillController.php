<?php

namespace App\Http\Controllers\Api\v1\Skill;

use App\Http\Requests\Skill\SkillRequest;
use App\Http\Requests\Skill\SearchRequest;
use App\Http\Requests\Skill\SkillProposalRequest;
use App\Http\Requests\Skill\SkillProposalUpdateRequest;
use App\Http\Resources\Skills\SkillCollection;
use App\Http\Resources\Skills\SkillResource;
use App\Http\Resources\Skills\SkillProposalCollection;
use App\Http\Resources\Skills\SkillProposalResource;
use App\Repositories\Contracts\Api\SkillInterface;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SkillController extends Controller
{
    /**
     *
     * @param SearchRequest $request
     * @param SkillInterface $skill
     */
    public function search(SearchRequest $request, SkillInterface $skill)
    {
        $searchValue = $request->input('q');

        try {
            $skill = $skill->search_skill($searchValue);

            $httpcode = 200;

            if ($skill->count() == 0) {
                $httpcode = 404;
            }

            return SkillCollection::make($skill)->setHttpCode($httpcode);
        } catch (\Exception $error) {
            $httpcode = 500;

            return response()->json([
                'http_code'     => $httpcode,
                'error'         => $error->getMessage(),
            ]);
        }
    }

    /**
     * @param SkillInterface $skill
     * @return SkillCollection|\Illuminate\Http\JsonResponse
     */
    public function index(SkillInterface $skill)
    {
        try {
            $skills = $skill->collection(['category'], 10);

            return SkillCollection::make($skills)->setHttpCode();
        } catch (\Exception $error) {
            $httpcode = 500;

            return response()->json([
                'http_code'     => $httpcode,
                'error'         => $error->getMessage(),
            ]);
        }
    }

    /**
     * @param SkillRequest $request
     * @param SkillInterface $skill
     * @return SkillResource|\Illuminate\Http\JsonResponse
     */
    public function store(SkillRequest $request, SkillInterface $skill)
    {
        try {
            $input = $request->only([
                'category_id',
                'skill_name',
            ]);

            $skill = $skill->store($input);
            $httpcode = 201;

            return SkillResource::make($skill)->additional(['http_code' => $httpcode]);
        } catch (\Exception $error) {
            $httpcode = 500;

            return response()->json([
                'http_code'     => $httpcode,
                'error'         => $error->getMessage(),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $skill_name
     * @param SkillInterface $skill
     * @return mixed
     */
    public function show(SkillInterface $skill, $skillId)
    {
        try {
            $skill = $skill->show($skillId, true, ['category']);
            $httpcode = 200;

            if ($skill->count() == 0) {
                $httpcode = 404;
            }

            return SkillResource::make($skill)->additional(['http_code' => $httpcode]);
        } catch (\Exception $error) {
            $httpcode = 500;

            return response()->json([
                'http_code'     => $httpcode,
                'error'         => $error->getMessage(),
            ]);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int $skillId
     */
    public function update(SkillRequest $request, SkillInterface $skill, $skillId)
    {
        try {
            $skill = $skill->update($request->all(), $skillId);
            $httpcode = 200;

            if (!$skill) {
                $httpcode = 404;
            }

            return SkillResource::make($skill)->additional(['http_code' => $httpcode]);
        } catch (\Exception $error) {
            $httpCode = 500;

            return response()->json([
                'http_code' => $httpCode,
                'error'     => $error->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(SkillInterface $skill, $skillId)
    {
        try {
            $targetSkill = $skill->show($skillId, false, []);
            $httpCode = 204;

            $skill->destroy($targetSkill);

            return response()->json([
                'http_code' => $httpCode,
                'message'   => 'Successfully deleted skill',
            ]);
        } catch (\Exception $error) {
            $httpCode = 500;

            return response()->json([
                'http_code' => $httpCode,
                'error'     => $error->getMessage(),
            ]);
        }
    }

    /**
     * @param Request $request
     * @param SkillInterface $repository
     * @return \Illuminate\Http\JsonResponse
     */
    public function proposals(Request $request, SkillInterface $repository)
    {
        try {
            $skill_proposals = $repository->proposals($request->toArray());

            return SkillProposalCollection::make($skill_proposals)->setHttpCode();
        } catch (\Exception $error) {
            $httpCode = 500;

            return response()->json([
                'http_code' => $httpCode,
                'error'     => $error->getMessage(),
            ]);
        }
    }

    /**
     * @param SkillInterface $repository
     * @param $id
     * @return $this|\Illuminate\Http\JsonResponse
     */
    public function findProposal(SkillInterface $repository, $id)
    {
        try {
            $skill_proposal = $repository->findProposal($id);

            return SkillProposalResource::make($skill_proposal)->additional(['http_code' => 200]);
        } catch (\Exception $error) {
            $httpCode = 500;

            return response()->json([
                'http_code' => $httpCode,
                'error'     => $error->getMessage(),
            ]);
        }
    }

    /**
     * @param SkillProposalRequest $request
     * @param SkillInterface $repository
     * @return $this|\Illuminate\Http\JsonResponse
     */
    public function createProposal(SkillProposalRequest $request, SkillInterface $repository)
    {
        try {
            $input = $request->only([
                'skill_name',
                'category_id',
                'member_id',
                'member_type',
            ]);

            $skill_proposal = $repository->skillProposalCreate($input);

            return SkillProposalResource::make($skill_proposal)->additional(['http_code' => 200]);
        } catch (\Exception $error) {
            $httpCode = 500;

            return response()->json([
                'http_code' => $httpCode,
                'error'     => $error->getMessage(),
            ]);
        }
    }

    /**
     * @param SkillProposalUpdateRequest $request
     * @param SkillInterface $repository
     * @param $id
     * @return $this|\Illuminate\Http\JsonResponse
     */
    public function updateProposal(SkillProposalUpdateRequest $request, SkillInterface $repository, $id)
    {
        try {
            $skill_proposal = $repository->skillProposalUpdate($request->all(), $id);

            return SkillProposalResource::make($skill_proposal)->additional(['http_code' => 200]);
        } catch (\Exception $error) {
            $httpCode = 500;

            return response()->json([
                'http_code' => $httpCode,
                'error'     => $error->getMessage(),
            ]);
        }
    }

    /**
     * @param SkillInterface $repository
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteProposal(SkillInterface $repository, $id)
    {
        try {
            $skill_proposal = $repository->findProposal($id);
            $httpCode = 204;

            $repository->destroyProposal($skill_proposal);

            return response()->json([
                'http_code' => $httpCode,
                'message'   => 'Successfully deleted skill',
            ]);
        } catch (\Exception $error) {
            $httpCode = 500;

            return response()->json([
                'http_code' => $httpCode,
                'error'     => $error->getMessage(),
            ]);
        }
    }

    public function approveProposal(Request $request, SkillInterface $repository, $id)
    {
        try {
            $input = $request->only([
                'approved_by',
            ]);

            $input['approved_date'] = Carbon::now()->toDateTimeString();

            $skill_proposal = $repository->skillProposalUpdate($input, $id);

            return SkillProposalResource::make($skill_proposal)->additional(['http_code' => 200]);
        } catch (\Exception $error) {
            $httpCode = 500;

            return response()->json([
                'http_code' => $httpCode,
                'error'     => $error->getMessage(),
            ]);
        }
    }
}
