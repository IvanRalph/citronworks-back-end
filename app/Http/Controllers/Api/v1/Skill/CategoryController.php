<?php

namespace App\Http\Controllers\Api\v1\Skill;

use App\Http\Requests\Category\CategoryRequest;
use App\Http\Requests\Category\SearchRequest;
use App\Http\Resources\Category\CategoryCollection;
use App\Http\Resources\Category\CategoryResource;
use App\Repositories\Contracts\Api\CategoryInterface;
use App\Imports\CategoryImport;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     *
     * @param  SearchRequest        $request
     * @param  CategoryInterface    $category
     */
    public function search(SearchRequest $request, CategoryInterface $category)
    {
        try {
            $category = $category->search($request->input('q'));
            $httpcode = 200;

            if ($category->count() == 0) {
                $httpcode = 404;
            }

            return CategoryCollection::make($category)->setHttpCode($httpcode);
        } catch (\Exception $error) {
            $httpcode = 500;

            return response()->json([
                'http_code'     => $httpcode,
                'error'         => $error->getMessage(),
            ]);
        }
    }

    /**
     * @param  CategoryInterface $category
     */
    public function index(CategoryInterface $category)
    {
        try {
            $categories = $category->collection(['skills'], 10);

            return CategoryCollection::make($categories)->setHttpCode();
        } catch (\Exception $error) {
            $httpcode = 500;

            return response()->json([
                'http_code'     => $httpcode,
                'error'         => $error->getMessage(),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $category_name
     * @param CategoryInterface $category
     * @return mixed
     */
    public function show(CategoryInterface $category, $categoryId)
    {
        try {
            $category = $category->show($categoryId, true, ['skills']);
            $httpcode = 200;

            if ($category->count() == 0) {
                $httpcode = 404;
            }

            return CategoryResource::make($category)->additional(['http_code' => $httpcode]);
        } catch (\Exception $error) {
            $httpcode = 500;

            return response()->json([
                'http_code'     => $httpcode,
                'error'         => $error->getMessage(),
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryRequest $request
     * @param CategoryInterface $category
     * @return \Illuminate\Http\JsonResponse | CategoryResource
     */
    public function store(CategoryRequest $request, CategoryInterface $category)
    {
        try {
            $input = $request->only([
                'category_name',
            ]);

            $category = $category->store($input);

            $httpcode = 201;

            return CategoryResource::make($category)->additional(['http_code' => $httpcode]);
        } catch (\Exception $error) {
            $httpcode = 500;

            return response()->json([
                'http_code'     => $httpcode,
                'error'         => $error->getMessage(),
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     * Methods:  updateCategory
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     */
    public function update(CategoryRequest $request, CategoryInterface $category, $categoryId)
    {
        try {
            $category = $category->update($request->all(), $categoryId);

            $httpcode = 200;

            if (!$category) {
                $httpcode = 404;
            }

            return CategoryResource::make($category)->additional(['http_code' => $httpcode]);
        } catch (\Exception $error) {
            $httpCode = 500;

            return response()->json([
                'http_code' => $httpCode,
                'error'     => $error->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $categoryId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(CategoryInterface $category, $categoryId)
    {
        try {
            $targetCategory = $category->show($categoryId, false, []);
            $httpCode = 204;

            $category->destroy($targetCategory);

            return response()->json([
                'http_code' => $httpCode,
                'message'   => 'Successfully deleted category',
            ]);
        } catch (\Exception $error) {
            $httpCode = 500;

            return response()->json([
                'http_code' => $httpCode,
                'error'     => $error->getMessage(),
            ]);
        }
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function import()
    {
        Excel::import(new CategoryImport, request()->file('file'));
    }

    /**
     * @param CategoryInterface $category
     * @return \Illuminate\Http\JsonResponse | CategoryCollection
     */
    public function popular(CategoryInterface $category)
    {
        try {
            $category = CategoryResource::collection($category->collection([], 10));
            $httpCode = 200;

            return CategoryCollection::make($category)->setHttpCode($httpCode);
        } catch (\Exception $error) {
            $httpCode = 500;

            return response()->json([
                'http_code' => $httpCode,
                'error'     => $error->getMessage(),
            ]);
        }
    }
}
