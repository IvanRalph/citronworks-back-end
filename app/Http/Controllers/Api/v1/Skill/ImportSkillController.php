<?php

namespace App\Http\Controllers\Api\v1\Skill;

use App\Http\Controllers\Controller;

class ImportSkillController extends Controller
{
    public function import()
    {
        Excel::import(new UsersImport, 'users.xlsx');
    }
}
