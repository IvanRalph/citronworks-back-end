<?php

namespace App\Http\Controllers\Api\v1\Billing;

use App\Http\Controllers\Controller;
use App\Http\Requests\Billing\ValidatePaymentRequest;
use App\Repositories\Contracts\Api\EmployerInterface;
use App\Repositories\Contracts\Api\FreelancerCompanyInterface;
use App\Repositories\Contracts\Api\SubscriptionInterface;
use Braintree_ClientToken;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class BillingController extends Controller
{
    /**
     * @var SubscriptionInterface
     */
    protected $subscription;

    /**
     * BillingController constructor.
     * @param SubscriptionInterface $subscription
     */
    public function __construct(SubscriptionInterface $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * @param  ValidatePaymentRequest                                                  $request
     * @param  EmployerInterface                                                       $employer
     * @param  FreelancerCompanyInterface                                              $freelancerCompany
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function subscribe(ValidatePaymentRequest $request, EmployerInterface $employer, FreelancerCompanyInterface $freelancerCompany)
    {
        $subscription = $request->only([
            'token',
            'coupon',
            'subscription_type',
            'plan_type',
        ]);

        // $request->validated()['billing_information']['zip'] = $employer->braintreeDetails($request->only(['email']))->addresses[0]->postalCode;

        // get subscriber via token instead of via email in post body
        $subscriber = Auth::user();

        if ($request->get('plan_type') == 'Promo') {
            return response($subscriber);
        }


        // get validated billing info
        $billingInformation = $request->billing_information;

        // employer-specific logic for subscriptions
        if ($request->company_type === 'employer') {
            // update subscriber's billing info
            $subscriber->company->billing->update($billingInformation);

            // need to use Eloquent update instead of mass-update in order to
            // trigger Algolia's ::updated() model-event listener
            $subscriber->jobs->each->update([
                'plan_status' => 'paid',
            ]);
        } else {
            // freelancer company specific logic
            $freelancerCompany->updateBilling($subscriber, $billingInformation);
        }

        if (App::environment('dev')) {
            $subscription['subscription_type'] = 'monthly';
            $subscription['plan_type'] = 'Test';
        }

        $subscriptionResponse = $this->subscription->subscribe($subscriber, $subscription);

        return response($subscriptionResponse, 200);
    }

    /**
     * Generate token from braintree server
     * Then return back to the client
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function token()
    {
        return response([
            'token' => Braintree_ClientToken::generate(),
        ]);
    }
}
