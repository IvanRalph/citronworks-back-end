<?php

namespace App\Http\Controllers\Api\v1\Billing;

use App\Container\Billing\Traits\WebhookTraits;
use App\Http\Controllers\Controller;
use Braintree\WebhookNotification;
use Braintree\WebhookTesting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class BillingWebhookController extends Controller
{
    use WebhookTraits;

    /**
     * Handle a Braintree webhook call.
    /**
     * @param  Request  $request
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function __invoke(Request $request)
    {
        try {
            $webhook = $this->parseBraintreeNotification($request);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        $method = 'handle' . Str::studly(str_replace('.', '_', $webhook->kind));

        if (method_exists($this, $method)) {
            return $this->{$method}($webhook);
        }

        return $this->missingMethod(['method' => $method]);
    }

    /**
     * Parse the given Braintree webhook notification request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Braintree\WebhookNotification
     */
    protected function parseBraintreeNotification($request)
    {
        return WebhookNotification::parse($request['bt_signature'], $request['bt_payload']);
    }

    /**
     * Handle a subscription cancellation notification from Braintree.
     *
     * @param  \Braintree\WebhookNotification  $webhook
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function handleSubscriptionCanceled($webhook)
    {
        return $this->cancelSubscription($webhook->subscription->id);
    }

    /**
     * Handle a subscription charge successfully notification from Braintree.
     * build an invoice/receipt and attach to subscriber
     * build pdf and attached to the subscriber
     *
     * @param  \Braintree\WebhookNotification  $webhook
     * @return Response
     */
    protected function handleSubscriptionChargedSuccessfully($webhook)
    {
        return $this->saveTransaction($webhook->subscription);
    }

    /**
     * Handle a transaction for one off payment decline notification from Braintree.
     *
     * @param  \Braintree\WebhookNotification  $webhook
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function handleTransactionSettlementDeclined($webhook)
    {
        Log::channel('stack')
            ->info('transaction decline', ['data' => $webhook]);
    }

    /**
     * Handle a transaction for one of payment decline notification from Braintree.
     *
     * @param  \Braintree\WebhookNotification  $webhook
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function handleTransactionSettled($webhook)
    {
        Log::channel('stack')
            ->info('transaction settled', ['data' => $webhook]);
    }

    /**
     * Handle a subscription expiration notification from Braintree.
     *
     * @param  \Braintree\WebhookNotification  $webhook
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function handleSubscriptionExpired($webhook)
    {
        return $this->cancelSubscription($webhook->subscription->id);
    }

    /**
     * Handle calls to missing methods on the controller.
     *
     * @param  array  $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function missingMethod(array $parameters = [])
    {
        return new Response;
    }

    public function handleCheck($webhook)
    {
        return response('', 200);
    }
}
