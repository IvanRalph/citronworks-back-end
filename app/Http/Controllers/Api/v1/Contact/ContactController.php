<?php

namespace App\Http\Controllers\Api\v1\Contact;

use App\Mail\ContactFormMail;
use App\Model\Contact\Contact;
use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Contact();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Contact\Store  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $input = $request->only('contact');

        $this->model->first_name = $input['contact']['first_name'] ?? '';
        $this->model->last_name = $input['contact']['last_name'] ?? '';
        $this->model->email = $input['contact']['email'] ?? '';
        $this->model->subject = $input['contact']['subject'] ?? '';
        $this->model->message = $input['contact']['message'] ?? '';

        if (isset($input['contact']['company_website'])) {
            $this->model->company_url = $input['contact']['company_website'] ?? '';
        }
        if (isset($input['contact']['phone'])) {
            $this->model->phone = $input['contact']['phone'] ?? '';
        }

        if (!$this->model->save()) {
            abort(422, 'Something went wrong saving contacts');
        }

        /**
         * Hubspot contact form integration
         */
        $this->_submitContactFormHubSpotIntegration($input);

        /**
         * Email notification
         */
        Mail::to('team@citronworks.com')->send(new ContactFormMail($this->model));

        return
            response([$this->model], 200);
    }

    /**
     * @param $input
     * @return mixed
     */
    private function _submitContactFormHubSpotIntegration($input)
    {
        $test = [
            'fields' => [
                [
                    'name' => 'firstname',
                    'value' => $input['contact']['first_name'],
                    'fieldType' => 'text',
                ],
                [
                    'name' => 'lastname',
                    'value' => $input['contact']['last_name'],
                    'fieldType' => 'text',
                ],
                [
                    'name' => 'email',
                    'value' => $input['contact']['email'],
                    'fieldType' => 'text',
                ],
                [
                    'name' => 'subject',
                    'value' => $input['contact']['subject'],
                    'fieldType' => 'text',
                ],
                [
                    'name' => 'message',
                    'value' => $input['contact']['message'],
                    'fieldType' => 'text',
                ],
            ],
        ];

        return
           app('hubspot', $test)->submitGeneralInquiry();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
