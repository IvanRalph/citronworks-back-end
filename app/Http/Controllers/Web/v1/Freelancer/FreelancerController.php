<?php

namespace App\Http\Controllers\Web\v1\Freelancer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\Web\FreelancerInterface;
use App\Repositories\Contracts\Web\SkillsInterface;

class FreelancerController extends Controller
{
    /**
     * Get Freelancer
     *
     * @param FreelancerInterface $freelancer
     * @param Request $request
     * @return void
     */
    public function downloadBySkills(
        Request $request,
        SkillsInterface $model,
        FreelancerInterface $freelancer
    ) {
        $skills =
            $model->in($request->data, 'skill_id') ?? null;
        
        $freelancers = $model->getFreelancersBySkills($skills->toArray(), [
            'profiles.freelancer',
            'profiles.freelancer_skills',
            'profiles.freelancer_skills.skill'
        ]);

        $d = $freelancers->map(fn ($d) => [
            $d->profiles->freelancer->freelancer_id,
            $d->profiles->freelancer->email,
            $d->profiles->freelancer->first_name,
            $d->profiles->freelancer->last_name,
            $d->profiles->freelancer_skills->map(fn ($f) => $f->skill->skill_name)->filter(fn ($v) => collect($request->data)->contains($v))->count()
        ]);

        return $freelancer->write($d, ['Search Skills: ' . implode(', ', $request->data)]);
    }

    /**
     * @param FreelancerEloquent $freelancer
     * @return collection
     */
    public function showFreelancers(
        FreelancerInterface $freelancer,
        Request $request
    ) {
        if ($request->name !== null) {
            // return $freelancer->findFreelancer($request, 20, ['skill', 'profile', 'profile.category', 'freelancer_company'])
            //         ?? null;
            return
                $freelancer
                    ->findFreelancer($request, 20)
                        ?? null;
        }

        // return $freelancer->paginate(20, ['skill', 'profile', 'profile.category', 'freelancer_company'])
        //     ?? null;
        return
            $freelancer
                ->paginate(20, ['profiles'])
                    ?? null;
    }

    /**
     * @param FreelancerInterface $freelancer
     * @param Request $request
     * @return void
     */
    public function delete(
        FreelancerInterface $freelancer,
        Request $request
    ) {
        $f = $freelancer->show($request->id, true);

        try {
            return $f->destroy() ?
                response()->json($f->obj) :
                null;
        } catch (\Exception $e) {
            return response()->json(['code' => 500, 'message' => 'There\'s seem to be an error.']);
        }
    }

    /**
     * @param Request $request
     * @param DocumentUploaderService $documentService
     * @param FreelancerInterface $freelancer
     * @return void
     */
    public function createPhoto(
        Request $request,
        FreelancerInterface $freelancer
    ) {
        $filename = $request->file->store('images');

        return response()->json(['data' => $filename])
                ?? null;
    }

    /**
     * @param Request $request
     * @param FreelancerInterface $freelancer
     * @return void
     */
    public function freelancerCompany(
        Request $request,
        FreelancerInterface $freelancer
    ) {
        return $freelancer->findCompany($request->name) ?? null;
    }

    /**
     * @param FreelancerInterface $freelancer
     * @param Request $request
     * @return void
     */
    public function freelancer(
        FreelancerInterface $freelancer,
        Request $request
    ) {
        $f =
            $freelancer->show($request->freelancer_id, 1);

        if (!$request->pwdEdit) {
            $req = $request->except('cpassword', 'pwdEdit', 'password', 'skill', 'freelancer_company', 'profiles');
            $f->update($req);
        }
        // else {
        //     $req = $request->except('cpassword', 'pwdEdit', 'skill', 'freelancer_company', 'profiles');
        //     $f->update($req);
        // }

        return response()->json($f);
    }

    public function patchFreelancerCompany(
        FreelancerInterface $freelancer,
        Request $request
    ) {
        $data = $request->only('freelancer_id', 'freelancer_company');
        $exceptKeys = [
            'company_name',
            'intro',
            'description'
        ];
        foreach ($exceptKeys as $r) {
            unset($data['freelancer_company'][$r]);
        }
        $f =
            $freelancer
                ->show($data['freelancer_id'], 0)
                ->freelancerCompany()
                ->update(array_replace($data['freelancer_company'], ['country' => $request->freelancer_company['country']]));

        return response()->json($f);
    }

    /**
     * @param FreelancerInterface $freelancer
     * @param Request $request
     * @return void
     */
    public function create(
        FreelancerInterface $freelancer,
        Request $request
    ) {
        // dd($request->all());
        $fc = $freelancer->storeFreelancerCompany($request->freelancer_company);

        $f = $freelancer->storeFreelancer(array_replace(
            $request->freelancer,
            [
                'freelancer_company_id' => $fc->freelancer_company_id,
            ]
        ));
        // if (count($request->freelancer_profile->skills)) {
        //     foreach($request->freelancer_profile->skills as $s) {
        //         $f->storeSkill($s);
        //     }
        // }

        if (count($request->freelancer_profile)) {
            foreach ($request->freelancer_profile as $p) {
                if (isset($p['id'])) {
                    unset($p['id']);
                }
                $freelancer
                    ->storeProfile(
                        $f->obj,
                        array_replace($p, ['category' => $p['category']])
                    );
                $f->storeSkill($p['skills']);
            }
        }

        return response()
                    ->json($freelancer
                        ->show($f->obj->freelancer_id, 0, ['skill', 'profile', 'profile.category', 'freelancer_company']))
            ?? null;
    }

    /**
     * @param Request $request
     * @param FreelancerInterface $freelancer
     * @return void
     */
    public function getInfo(
        Request $request,
        FreelancerInterface $freelancer
    ) {
        return
            $freelancer->show($request->id, 0, [
                'profiles',
                'freelancerCompany',
                'profiles.freelancer_skills',
                'profiles.freelancer_skills.skill',
            ])
                ?? null;
    }

    /***
     * @param Request $request
     * @param FreelancerInterface $freelancer
     * @return void
     */
    public function updateProfile(
        Request $request,
        FreelancerInterface $freelancer
    ) {
        $exceptKeys = [
            'freelancer_skills',
            'freelancer_type',
            'category',
            'full_time_price',
            'part_time_price',
            'hourly_price',
            'agreed_price',
            'full_time_ava',
            'part_time_ava',
            'hourly_ava'
        ];
        if (!collect($request->all())->isEmpty()) {
            foreach ($request->all() as $p) {
                $skills = $p['freelancer_skills'];
                foreach ($exceptKeys as $r) {
                    unset($p[$r]);
                }
                if (isset($p['isNew']) && $p['isNew']) {
                    unset($p['id']);
                    unset($p['isNew']);
                    $profile = $freelancer
                        ->show($p['freelancer_id'], 0, ['profile'])
                        ->profile()
                        ->create($p);
                    /**
                     * Skills
                     */
                    if (count($skills)) {
                        foreach ($skills as $s) {
                            if (isset($s['isNew']) && $s['isNew']) {
                                $freelancer->storeProfileSkill($profile->freelancer_profile_id, $s);
                                continue;
                            }
                        }
                    }
                    continue;
                }
                if (isset($p['isRemoved']) && $p['isRemoved']) {
                    $freelancer
                        ->show($p['freelancer_id'], 0, ['profile'])
                        ->profile()
                        ->whereFreelancerProfileId($p['freelancer_profile_id'])
                        ->delete();

                    continue;
                }
                $freelancer
                    ->show($p['freelancer_id'], 0, ['profile'])
                    ->profile()
                    ->whereFreelancerProfileId($p['freelancer_profile_id'])
                    ->update($p);

                /**
                 * Skills
                 */
                if (count($skills)) {
                    foreach ($skills as $s) {
                        if (isset($s['isNew']) && $s['isNew'] && isset($p['freelancer_profile_id'])) {
                            $freelancer->storeProfileSkill($p['freelancer_profile_id'], $s);
                            continue;
                        }
                        if (isset($s['isRemoved']) && $s['isRemoved']) {
                            $freelancer->removeProfileSkill($s['skill_freelancer_id']);
                            continue;
                        }
                        $freelancer->updateProfileSkill($s['skill_freelancer_id'], $s);
                    }
                }
            }
        }

        return response()
            ->json($freelancer
                ->show($request->freelancer_id, 0, [
                    'profiles',
                    'freelancerCompany',
                    'profiles.freelancer_skills',
                    'profiles.freelancer_skills.skill',
                ]));
    }
}
