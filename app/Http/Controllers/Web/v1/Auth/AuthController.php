<?php

namespace App\Http\Controllers\Web\v1\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        return view('layouts.login');
    }

    /**
     * @param Request $request
     * @return void
     */
    public function authenticate(Request $request)
    {
        if (Auth::attempt($request->only('email', 'password'), $request->remember_me)) {
            return response()->json($request->user());
        }

        return response()->json(false);
    }

    /**
     * @return void
     */
    public function verify()
    {
        if (!Auth::guard('web')->check()) {
            return response()->json(false);
        }

        return response()->json(true);
    }

    /**
     * @return void
     */
    public function logout()
    {
        Auth::logout();

        return response()->json(true);
    }

    /**
     * @param Request $request
     * @return void
     */
    public function createPhoto(
        Request $request
    ) {
        $filename = $request->file->store('images');

        return response()->json(['data' => $filename])
                ?? null;
    }

    /**
     * @param Request $request
     * @return void
     */
    public function updateProfile(
        Request $request
    ) {
        try {
            return tap($request->user())
                ->update($request->all())
                ?? null;
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @return void
     */
    public function updatePw(
        Request $request
    ) {
        try {
            return tap($request->user())
                    ->update($request->only('password'))
                ?? null;
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
