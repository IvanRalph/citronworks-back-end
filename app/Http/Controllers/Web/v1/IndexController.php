<?php

namespace App\Http\Controllers\Web\v1;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function app()
    {
        return view('layouts.app');
    }
}
