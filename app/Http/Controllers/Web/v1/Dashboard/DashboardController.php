<?php

namespace App\Http\Controllers\Web\v1\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\Web\StatisticsInterface;
use App\Model\Ref\RefUrl;

class DashboardController extends Controller
{
    /**
     * @param Request $request
     * @param StatisticsInterface $stats
     * @return json
     */
    public function _stats_freelancer_signups(
        Request $request,
        StatisticsInterface $stats
    ) {
        $req = 'last_30_days';
        if ($request->model) {
            $req = json_decode($request->model);
        }

        $data = $stats->showFromDate('created_at', $req->dropdown ?? $req, 1)->groupBy('m/d/Y');
        if (count($data)) {
            $arr = [];
            $arr['data'][] = [
                'data' => [],
                'name' => 'Freelancer Sign Up',
                'type' => 'line',
            ];
            foreach ($data as $i => $d) {
                $arr['labels'][] = $i;
                $arr['data'][0]['data'][] = count($d);
            }
        }

        return response()->json($arr);
    }

    /**
     * Get Total Signup
     *
     * @param StatisticsInterface $stats
     * @return void
     */
    public function _stats_freelancer_total_signup(StatisticsInterface $stats)
    {
        return $stats
                    ->show()->count()
                        ?? null;
    }

    /**
     * Get UTM collection
     *
     * @param Request $request
     * @param StatisticsInterface $stats
     * @return void
     */
    public function _stats_freelancer_utms(
        Request $request,
        StatisticsInterface $stats
    ) {
        $udata = $stats
                ->makeModel(new RefUrl)
                ->showFromDate('created_at', $request->model, 1)
                ->filterSource()->toArray();

        $utm = [];
        $c = 0;
        if ((array) count($udata)) {
            foreach ($udata as $tag => $data) {
                $utm[$c]['tag'] = $tag;
                if (count($data)) {
                    foreach ($data as $assoc => $d) {
                        $utm[$c]['datas'][$assoc] = $d;
                    }
                }
                $c++;
            }
        }

        return response()->json($utm);
    }

    /**
     * Get countries collection
     *
     * @param Request $request
     * @param StatisticsInterface $stats
     * @return void
     */
    public function _stats_countries(
        Request $request,
        StatisticsInterface $stats
    ) {
        $udata = $stats
                ->makeModel(new RefUrl)
                ->showFromDate('created_at', $request->model, 1)
                ->filterBy('utm_term')->toArray();

        $utm = [];
        $c = 0;
        if ((array) count($udata)) {
            foreach ($udata as $tag => $data) {
                $utm[$c]['tag'] = $tag;
                if (count($data)) {
                    foreach ($data as $assoc => $d) {
                        $utm[$c]['datas'][$assoc] = $d;
                    }
                }
                $c++;
            }
        }

        return response()->json($utm);
    }
}
