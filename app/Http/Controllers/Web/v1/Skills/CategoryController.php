<?php

namespace App\Http\Controllers\Web\v1\Skills;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\Web\CategoryInterface;

class CategoryController extends Controller
{
    /**
     * @param Request $request
     * @param CategoryInterface $category
     * @return void
     */
    public function show(
        Request $request,
        CategoryInterface $category
    ) {
        return $category->paginate(20, 'category_id', 'ASC') ?? null;
    }
    /**
     * @return void
     */
    public function showAll(
        Request $request,
        CategoryInterface $category
    ) {
        return
            $category
                ->all()
                    ?? null;
    }

    /**
     * @param Request $request
     * @param CategoryInterface $category
     * @return void
     */
    public function update(
        Request $request,
        CategoryInterface $category
    ) {
        return
            $category->update($request->all(), $request->category_id)
            ?? null;
    }

    /**
     * @param Request $request
     * @param CategoryInterface $category
     * @return void
     */
    public function destroy(
        Request $request,
        CategoryInterface $category
    ) {
        $d = $category->show($request->categoryId, 1);

        try {
            $d->destroy();

            return $d->obj ?? null;
        } catch (\Exception $e) {
            return response()->json(['code' => 500, 'message' => 'There\'s seem to be an error.']);
        }
    }

    public function post(
        Request $request,
        CategoryInterface $category
    ) {
        return response()->json($category->store($request->all())) ?? null;
    }
}
