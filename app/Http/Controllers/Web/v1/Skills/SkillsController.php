<?php

namespace App\Http\Controllers\Web\v1\Skills;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\Web\SkillsInterface;
use App\Repositories\Contracts\Web\CategoryInterface;
use App\Http\Resources\Skills\SkillResource;

class SkillsController extends Controller
{
    /**
     * @param SkillsInterface $skills
     * @return void
     */
    public function show(SkillsInterface $skills)
    {
        return $skills->paginate(20, ['category'])
            ?? null;
    }

    /**
     * @param Request $request
     * @param SkillsInterface $skills
     * @return void
     */
    public function findByName(
        Request $request,
        SkillsInterface $skills
    ) {
        return response()->json(['data' => $skills->findByName($request->name, ['category'])]);
    }

    /**
     * @param CategoryInterface $category
     * @return void
     */
    public function showCategory(CategoryInterface $category)
    {
        return $category->all();
    }

    /**
     * @param Request $request
     * @param SkillsInterface $skills
     * @return void
     */
    public function edit(
        Request $request,
        SkillsInterface $skills
    ) {
        return $skills->update($request->all(), $request->skill_id);
    }

    /**
     * @param Request $request
     * @param SkillsInterface $skills
     * @return void
     */
    public function delete(
        Request $request,
        SkillsInterface $skills
    ) {
        $toDestroy = $skills->show($request->skillid, 1);

        try {
            $toDestroy->destroy();

            return new SkillResource($toDestroy->obj);
        } catch (\Exception $e) {
            return response()->json(['code' => 500, 'message' => 'There\'s seem to be an error.']);
        }
    }

    /**
     * @param Request $request
     * @param SkillsInterface $skills
     * @return void
     */
    public function post(
        Request $request,
        SkillsInterface $skills
    ) {
        return $skills->post($request->all());
    }

    /**
     * Get all skills
     *
     * @param Request $request
     * @return void
     */
    public function all(
        Request $request,
        SkillsInterface $model
    ) {
        $skills =
            $model->in($request->all(), 'skill_id') ?? null;

        return $model->getFreelancersBySkills($skills->toArray(), [
            'profiles.freelancer',
            'profiles.freelancer_skills',
            'profiles.freelancer_skills.skill'
        ]);
    }
}
