<?php

namespace App\Http\Controllers\Web\v1\Skills;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\Web\SkillsInterface;
use App\Repositories\Contracts\Web\SkillsProposalInterface;
use Illuminate\Http\Request;

class SkillsProposalController extends Controller
{
    const LIMIT = 20;

    /**
     * @param SkillsInterface $repository
     * @return void
     */
    public function index(
        Request $request,
        SkillsInterface $repository
    ) {
        return
            $repository
                ->proposals(0)
                ->with(['freelancer', 'employer'])
                    ->paginate(self::LIMIT)
                        ?? null;
    }
    /**
     * @param Request $request
     * @param SkillsProposalInterface $repository
     * @return void
     */
    public function suggestedSkill(
        Request $request,
        SkillsProposalInterface $repository
    ) {
        $skillProposal = $repository->show($request->skill_proposal_id);
        return
            $skillProposal->updateToSkill($request->member_type, $request->all());
    }
    /**
     * @param Request $request
     * @param SkillsProposalInterface $repository
     * @return void
     */
    public function revertSuggestedSkill(
        Request $request,
        SkillsProposalInterface $repository
    ) {
        $skillProposal = $repository->show($request->skill_proposal_id);
        return
            $skillProposal->revert($request->only(['skill_name', 'category_id', 'freelancer_id', 'skill_proposal_id']));
    }
    /**
     * @param Request $request
     * @param SkillsProposalInterface $repository
     * @return void
     */
    public function delete(
        Request $request,
        SkillsProposalInterface $repository
    ) {
        $d =
            $repository->show($request->id)->delete();
        
        return response()->json($request->id);
    }
}
