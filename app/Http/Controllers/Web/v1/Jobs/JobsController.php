<?php

namespace App\Http\Controllers\Web\v1\Jobs;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\Web\JobsInterface;
use Illuminate\Http\Request;

class JobsController extends Controller
{
    protected $jobs;

    public function __construct(JobsInterface $jobs)
    {
        $this->jobs = $jobs;
    }

    public function findByName(Request $request)
    {
        return response()->json(['data' => $this->jobs->findByName($request->name)]);
    }

    /**
     * @return void
     */
    public function show()
    {
        return $this->jobs->paginate(20, ['employer'])
            ?? null;
    }

    public function edit(Request $request)
    {
        return response()->json(['data' => $this->jobs->update($request->all(), $request->job_id)]);
    }
}
