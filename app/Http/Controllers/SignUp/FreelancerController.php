<?php

namespace App\Http\Controllers\SignUp;

use App\Container\Http\Ip;
use App\Http\Controllers\Controller;
use App\Http\Requests\Freelancer\RegistrationRequest;
use App\Model\Freelancer\Freelancer;
use App\Model\Freelancer\FreelancerCompany;
use App\Model\Gdpr\Gdpr;
use App\Repositories\Eloquent\Api\RefUrl;
use App\Traits\Controllers\SendsVerificationEmail;
use App\Traits\Controllers\StoresGdpr;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class FreelancerController extends Controller
{
    use StoresGdpr, SendsVerificationEmail;

    public function store(RegistrationRequest $request)
    {
        // get validated data
        $data = $request->validated();

        // set role to owner
        $data['role'] = 'owner';

        // inject activation token
        $data['activation_token'] = Str::random(8);

        $data['ip_address'] = Ip::client() ?? '';

        // save country of freelancer company
        $freelancerCompany = FreelancerCompany::create(Arr::only($request->company, ['country']));

        // create freelancer from company
        $freelancer = $freelancerCompany->freelancer()->create($data);

        // create profile for freelancer
        $freelancer->profile()->create([]);

        // store GDPR data
        $this->storeGdpr($freelancer);

        // send verification email
        $this->sendVerificationEmail($freelancer, 'freelancer');

        // update ref url
        $this->updateRefUrl($freelancer, $request);

        // respond with a token
        return $freelancer->createToken('onboarding')->accessToken;
    }

    public function update(RegistrationRequest $request)
    {
        $freelancer = Auth::user();
        $freelancer->update($request->validated());
        $freelancer->company->update(Arr::only($request->company, ['country']));
        return 'ok';
    }

    public function location()
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://freegeoip.app/json/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'accept: application/json',
                'content-type: application/json',
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return json_decode($response, true);
        }
    }

    public function show()
    {
        return Auth::user()->load('company');
    }

    public function updateRefUrl(Freelancer $freelancer, Request $request)
    {
        if ($request->ref && $request->ref !== null) {
            $ref = json_decode($request->ref);
            $refUrl = (new RefUrl)->show($ref->ref_url_id, 0);
            if ($refUrl && $refUrl->member_id === null) {
                $refUrl->update([
                    'member_id' => $freelancer->freelancer_id,
                    'member_type' => 'freelancer',
                ]);
            }
        }
    }
}
