<?php

namespace App\Http\Controllers\SignUp;

use App\Http\Controllers\Controller;
use App\Http\Requests\Employer\SaveRequest;
use App\Model\Employer\Companies;
use App\Repositories\Contracts\Api\SubscriptionInterface;
use App\Traits\Controllers\FreeSubscription;
use App\Traits\Controllers\SendsVerificationEmail;
use App\Traits\Controllers\StoresGdpr;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class EmployerController extends Controller
{
    use StoresGdpr, SendsVerificationEmail;

    public function store(SaveRequest $request, SubscriptionInterface $subscription)
    {
        // get validated data
        $data = $request->validated();

        // set role to owner
        $data['role'] = 'owner';

        // inject activation token
        $data['activation_token'] = Str::random(8);

        // save country of employer company
        $company = Companies::create(Arr::only($request->company, ['country']));

        // store billing to generate billing_information_id
        $company->billing()->create([]);

        // save the employer
        $employer = $company->employer()->create($data);

        // store GDPR data
        $this->storeGdpr($employer);

        // send verification email
        $this->sendVerificationEmail($employer, 'employer');

        $subscription->checkFreeSubscription($employer);

        return $employer->createToken('onboarding')->accessToken;
    }

    public function update(SaveRequest $request)
    {
        $freelancer = Auth::user();
        $freelancer->update($request->validated());
        $freelancer->company->update(Arr::only($request->company, ['country']));
        return 'ok';
    }
}
