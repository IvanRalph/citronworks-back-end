<?php

namespace App\Model\Employer;

use App\Container\Billing\Traits\Billing;
use App\Container\Messaging\MessageableInterface;
use App\Container\Messaging\Traits\Messageable;
use App\Mail\PasswordResetMail;
use App\Notifications\PasswordResetNotification;
use App\Traits\Authenticatable\CreatesAffiliateAccount;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\HasApiTokens;
use Laravel\Passport\Passport;

class Employer extends Authenticatable implements MessageableInterface
{
    use Notifiable;
    use HasApiTokens;
    use Billing;
    use Messageable;
    use CreatesAffiliateAccount;

    public $timestamps = false;

    protected $table = 'employers';

    protected $primaryKey = 'employer_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'first_name', 'last_name', 'role', 'activation_token', 'email_verified_at'
    ];

    protected $attributes = ['role' => 'owner'];

    protected $appends = [
        'full_name', 'profile_photo', 'id',
    ];

    /**ts
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activation_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getProfilePhotoAttribute()
    {
        return $this->company->logo;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clients()
    {
        return
            $this
                ->hasMany(Passport::clientModel(), 'id', 'employer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return
            $this
                ->belongsTo(Companies::class, 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function hasCompany()
    {
        return
            $this
                ->hasOne(Companies::class, 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invite()
    {
        return
            $this
                ->hasMany(Invite::class, 'employer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobs()
    {
        return
            $this
                ->hasMany(\App\Model\Job\Job::class, 'employer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subscription()
    {
        return
            $this->hasOne(\App\Model\Billing\Subscription::class, 'member_id', 'employer_id');
    }

    public function sendPasswordResetNotification($token)
    {
        Mail::to(request()->email)
            ->send(new PasswordResetMail($token, 'employer', $this));
//        $this->notify(new PasswordResetNotification($token, 'Password Reset'));
    }

    public function braintree()
    {
        return $this->morphOne(\App\Model\Billing\Braintree::class, 'member');
    }

    public function bookmarkedFreelancers()
    {
        return $this->hasMany(BookmarkFreelancer::class, 'member_id', 'id');
    }
}
