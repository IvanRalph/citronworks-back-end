<?php

namespace App\Model\Employer;

use Laravel\Passport\Client as PassportClient;

class OauthClients extends PassportClient
{
    protected $connection = 'employer';

    protected $table = 'oauth_clients';
}
