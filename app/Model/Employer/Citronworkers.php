<?php

namespace App\Model\Employer;

use Illuminate\Database\Eloquent\Model;

class Citronworkers extends Model
{
    protected $primaryKey = 'citronworkers_request_id';

    protected $table = 'citronworkers_requests';

    protected $attributes = [
        'last_name' => '',
        'company' => '',
        'company_website' => '',
        'phone' => '',
        'referer' => 'none',
        'message' => '',
    ];

    protected $guarded = 'citronworkers_request_id';
}
