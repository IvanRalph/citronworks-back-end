<?php

namespace App\Model\Employer;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'invite_employer_id';

    protected $table = 'invite_employers';

    protected $guarded = ['invite_employer_id'];

    public function employer()
    {
        return
            $this->belongsTo(Employer::class, 'employer_id');
    }

    public function setQueryStringAttribute($value)
    {
        $this->attributes['query_string'] = $value;
    }

//    TODO: Temporary fix, remove once clear
    protected $attributes
        = [
            'query_string' => 'default',
        ];
}
