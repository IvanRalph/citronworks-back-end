<?php

namespace App\Model\Employer;

use App\Model\Freelancer\Freelancer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Hire extends Pivot
{
    protected $primaryKey = 'hire_id';
    protected $table = 'hires';
    protected $fillable = ['employer_id', 'freelancer_id', 'end_date', 'hire_type'];
    protected $casts = [ 'show_hired_banner' => 'boolean' ];

    public function freelancer()
    {
        return $this->belongsTo(Freelancer::class, 'freelancer_id');
    }

    public function employer()
    {
        return $this->belongsTo(Employer::class, 'employer_id');
    }

    public function company()
    {
        return $this->belongsTo(Companies::class, 'company_id');
    }

    public function scopeNotYetDismissed(Builder $query)
    {
        $query->where('show_hired_banner', true);
    }
}
