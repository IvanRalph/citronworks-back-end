<?php

namespace App\Model\Employer;

use Illuminate\Database\Eloquent\Model;

class BookmarkFreelancer extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'bookmark_id_freelancer';

    protected $table = 'bookmark_freelancers';
    protected $fillable = ['member_id', 'freelancer_id'];

    public function member()
    {
        return
            $this
                ->belongsTo(Employer::class, 'member_id');
    }

    public function freelancer()
    {
        return
            $this
                ->belongsTo(\App\Model\Freelancer\Freelancer::class, 'freelancer_id');
    }
}
