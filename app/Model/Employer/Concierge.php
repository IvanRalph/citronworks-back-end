<?php

namespace App\Model\Employer;

use Illuminate\Database\Eloquent\Model;

class Concierge extends Model
{
    protected $primaryKey = 'concierge_request_id';

    protected $table = 'concierge_request';

    protected $attributes = [
        'last_name' => '',
        'company' => '',
        'company_website' => '',
        'phone' => '',
        'referer' => 'none',
        'message' => '',
    ];

    protected $guarded = 'concierge_request_id';
}
