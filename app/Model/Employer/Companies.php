<?php

namespace App\Model\Employer;

use App\Model\Job\JobInvite;
use App\Model\Billing\BillingInformation;
use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'company_id';
    protected $table = 'companies';
    protected $fillable = [
        'company_name', 'title', 'intro', 'description', 'company_url', 'logo',
        'address_1', 'address_2', 'city', 'zip', 'state', 'country',
    ];
    protected $attributes = [
        'company_name' => '',
        'address_1' => '',
        'address_2' => '',
        'city' => '',
        'zip' => '',
        'state' => '',
        'country' => '',
        'logo' => '',
        'title' => '',
        'intro' => '',
        'description' => '',
        'company_url' => '',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function employer()
    {
        return
            $this
                ->hasOne(Employer::class, 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function billing()
    {
        return
            $this
                ->hasOne(BillingInformation::class, 'company_id');
    }

    public function hires()
    {
        return $this->hasMany(Hire::class, 'company_id');
    }

    public function jobInvites()
    {
        return $this->hasMany(JobInvite::class, 'company_id');
    }
}
