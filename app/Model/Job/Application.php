<?php

namespace App\Model\Job;

use App\Model\Attachment\Attachment;
use App\Model\Freelancer\Freelancer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'application_id';
    protected $table = 'applications';
    protected $guarded = ['application_id'];
    protected $attributes = [
        'subject' => '',
        'message' => '',
    ];
    protected $casts = [
        'attachment' => 'json',
    ];
    protected $appends = [
        'attachment_path',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            // if attachment is null, initialize it with empty attachment
            if (empty($model->attachment)) {
                $model->attachment = ['attachment' => 0];
            }
        });
    }

    public function member()
    {
        return
            $this
                ->belongsTo(Freelancer::class, 'freelancer_id');
    }

    public function job()
    {
        return
            $this
                ->belongsTo(Job::class, 'job_id');
    }

    public function testJob()
    {
        return
            $this
                ->hasMany(Job::class, 'job_id');
    }

    public function scopeNotEmpty(Builder $query)
    {
        $query->where('subject', '!=', '');
    }

    public function getAttachmentPathAttribute()
    {
        if (empty($this->attachment['id'])) {
            return null;
        }

        $attachment = Attachment::find($this->attachment['id']);

        if (empty($attachment)) {
            return null;
        }

        return '/files/' . $attachment->attached_id . $attachment->extension;
    }
}
