<?php

namespace App\Model\Job;

use App\Model\Freelancer\Freelancer;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Bookmark extends Pivot
{
    public $timestamps = false;

    protected $primaryKey = 'bookmark_id_job';

    protected $table = 'bookmark_jobs';
    protected $fillable = ['freelancer_id'];

    public function member()
    {
        return
            $this
                ->belongsTo(Freelancer::class, 'member_id');
    }

    public function job()
    {
        return
            $this
                ->belongsTo(Job::class, 'job_id');
    }
}
