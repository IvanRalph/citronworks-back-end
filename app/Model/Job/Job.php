<?php

namespace App\Model\Job;

use App\Model\Employer\Employer;
use App\Model\Freelancer\Freelancer;
use App\Model\Skill\Skill;
use App\Model\Skill\SkillJob;
use App\Traits\Models\ResyncsSearchIndex;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Laravel\Scout\Searchable;
use App\Model\Skill\SkillProposal;

class Job extends Model
{
    use Searchable, SoftDeletes, ResyncsSearchIndex;

    protected $primaryKey = 'job_id';
    protected $fillable = [
        'employer_id',
        'title',
        'intro',
        'description',
        'country_limit',
        'job_type',
        'price_min',
        'price_max',
        'plan_status',
    ];
    protected $casts = [
        'country_limit' => 'json',
    ];
    const STATUS_PUBLIC = 'public';
    const STATUS_CLOSED = 'closed';

    /**
     * Inject the proper plan status when creating
     *  - If employer is subscribed, plan_status = paid
     *  - If employer is on free plan, plan_status = free_unverified
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->plan_status = ! empty($model->employer->getSubscription())
                ? 'paid'
                : 'free_unverified';
            $model->job_status = 'public';
        });
    }

    public function employer()
    {
        return $this->belongsTo(Employer::class, 'employer_id');
    }

    public function application()
    {
        return $this->hasOne(Application::class, 'job_id');
    }

    public function applications()
    {
        return $this->hasMany(Application::class, 'job_id');
    }

    public function bookmarks()
    {
        return $this->hasMany(Bookmark::class, 'job_id');
    }

    public function bookmarkedByFreelancers()
    {
        return $this->belongsToMany(Freelancer::class, 'bookmark_jobs', 'job_id', 'member_id')
            ->using(Bookmark::class);
    }

    public function skill()
    {
        return $this->belongsToMany(Skill::class, 'skill_jobs', 'job_id', 'skill_id')
            ->using(SkillJob::class);
    }

    public function scopeApplied(Builder $query)
    {
        $query->whereHas(
            'applications',
            fn (Builder $subQuery) => $subQuery->where('freelancer_id', Auth::id())
        );
    }

    public function scopeBookmarked(Builder $query)
    {
        $query->whereHas(
            'bookmarks',
            fn (Builder $subQuery) => $subQuery->where('member_id', Auth::id())
        );
    }

    public function scopePublic(Builder $query)
    {
        $query->where('job_status', static::STATUS_PUBLIC);
    }

    public function scopeDeactivated(Builder $query)
    {
        $query->where('job_status', static::STATUS_CLOSED);
    }

    public function scopeFreelancerVisible(Builder $query)
    {
        $query->public()->where('plan_status', '!=', 'free_unverified');
    }

    public function bookmark()
    {
        $this->bookmarkedByFreelancers()->attach(Auth::id());
    }

    public function unbookmark()
    {
        $this->bookmarkedByFreelancers()->detach(Auth::id());
    }

    public function deactivate()
    {
        $this->job_status = static::STATUS_CLOSED;
        $this->save();
    }

    public function reactivate()
    {
        $this->job_status = static::STATUS_PUBLIC;
        $this->save();
    }

    /**
     * Search and filter the Jobs
     *
     * @param array $options [ 'keywords', 'job_type', 'price_min', 'price_max' ]
     *
     * @return Scout\Builder
     */
    public static function searchFilter(array $options)
    {
        // destructure the options array
        $keywords = $options['keywords'] ?? '';
        $jobType = $options['job_type'] ?? null;
        $priceMin = $options['price_min'] ?? null;
        $priceMax = $options['price_max'] ?? null;

        // if not filtered, don't touch Algolia, simply return jobs
        // visible to freelancers
        if (empty(array_filter($options))) {
            return static::freelancerVisible()
                ->orderBy('updated_at', 'desc');
        }

        $scout = static::search($keywords);

        if (! empty($jobType)) {
            $scout->with([
                'tagFilters' => [$jobType],
            ]);
        }
        if (! empty($priceMin)) {
            $scout->where('price_max', '>=', $priceMin);
        }

        if (! empty($priceMax)) {
            $scout->where('price_min', '<=', $priceMax);
        }

        return $scout;
    }

    /**
     * Job is only searchable if it is not free_unverified
     *
     * @return bool
     */
    public function shouldBeSearchable() : bool
    {
        return $this->plan_status != 'free_unverified'
            && $this->job_status == 'public';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return array_merge(Arr::only($this->toArray(), [
            'title',
            'intro',
            'description',
            'job_type',
        ]), [
            // if not subscribed, don't add company name to searchable attributes
            'company_name' => $this->plan_status == 'paid' ? $this->employer->company->company_name : '',
            'skill' => $this->skill()->pluck('skill_name'),
            '_tags' => [$this->job_type],
            'price_min' => intval($this->price_min ?: 0),
            'price_max' => intval($this->price_max ?: 9999999999),
        ]);
    }

    public function skill_proposal()
    {
        return $this->hasOne(SkillProposal::class, 'job_id', 'job_id');
    }

    public function job_skill_proposals()
    {
        return $this->hasMany(SkillProposal::class, 'job_id', 'job_id')->whereNull('approved')->orWhere('approved', '=', '0');
    }
}
