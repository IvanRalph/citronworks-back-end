<?php

namespace App\Model\Job;

use App\Model\Employer\Companies;
use App\Model\Employer\Employer;
use App\Model\Freelancer\Freelancer;
use Illuminate\Database\Eloquent\Model;

class JobInvite extends Model
{
    protected $primaryKey = 'job_invite_id';
    protected $fillable = [
        'employer_id', 'freelancer_id', 'job_id', 'message',
    ];

    public function freelancer()
    {
        return $this->belongsTo(Freelancer::class, 'freelancer_id');
    }

    public function employer()
    {
        return $this->belongsTo(Employer::class, 'employer_id');
    }

    public function company()
    {
        return $this->belongsTo(Companies::class, 'company_id');
    }

    public function job()
    {
        return $this->belongsTo(Job::class, 'job_id');
    }
}
