<?php

namespace App\Model\Skill;

use App\Model\Job\Job;
use Illuminate\Database\Eloquent\Relations\Pivot;

class SkillJob extends Pivot
{
    protected $table = 'skill_jobs';
    protected $primaryKey = 'skills_job_id';
    protected $guarded = ['skills_job_id'];
    protected $touches = ['job'];

    public function job()
    {
        return $this->belongsTo(Job::class);
    }

    public function skill()
    {
        return $this->belongsTo(Skill::class);
    }
}
