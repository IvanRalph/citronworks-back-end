<?php

namespace App\Model\Skill;

use App\Model\Freelancer\FreelancerProfile;
use Illuminate\Database\Eloquent\Relations\Pivot;

class SkillFreelancer extends Pivot
{
    protected $table = 'skill_freelancers';
    protected $primaryKey = 'skill_freelancer_id';
    protected $guarded = ['skill_freelancer_id'];

    /**
     * If rating was updated, update the search index for freelancer
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            $model->freelancerProfile->freelancer->resyncSearchIndex();
        });

        static::deleted(function ($model) {
            $model->freelancerProfile->freelancer->resyncSearchIndex();
        });
    }

    public function skill()
    {
        return $this->belongsTo(Skill::class, 'skill_id');
    }

    public function freelancerProfile()
    {
        return $this->belongsTo(FreelancerProfile::class, 'freelancer_profile_id');
    }
}
