<?php

namespace App\Model\Skill;

use App\Model\Freelancer\Freelancer;
use App\Model\Job\Job;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Model\Skill\SkillJob;

class Skill extends Authenticatable
{
    public $timestamps = false;

    protected $table = 'skills';

    protected $primaryKey = 'skill_id';

    protected $touches = ['job'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'skill_name',
    ];

    public function category()
    {
        return $this->belongsTo(\App\Model\Category\Category::class, 'category_id');
    }

    public function relatedSkill()
    {
        return $this->hasMany(\App\Model\SkillRelation\SkillRelation::class, 'skill_id');
    }

    public function scopeSearchByName($query, $params)
    {
        return $query->where('skill_name', 'LIKE', '%' . trim($params) . '%');
    }

    public function freelancer()
    {
        return
            $this->belongsToMany(Freelancer::class, 'skill_freelancers', 'skill_id', 'freelancer_id');
    }

    public function job()
    {
        return
            $this->belongsToMany(Job::class, 'skill_jobs', 'skill_id', 'job_id');
    }
    
    public function skill_freelancers()
    {
        return $this->hasMany(\App\Model\Skill\SkillFeeelancers::class, 'skill_id', 'skill_id');
    }

    public function skill_jobs()
    {
        return $this->hasMany(SkillJob::class, 'skill_id', 'skill_id');
    }
}
