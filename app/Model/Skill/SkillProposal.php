<?php

namespace App\Model\Skill;

use App\Model\Category\Category;
use Illuminate\Database\Eloquent\Model;
use App\Model\Freelancer\Freelancer;
use App\Model\Employer\Employer;
use Illuminate\Support\Facades\Auth;
use App\Model\Job\Job;

class SkillProposal extends Model
{
    const MEMBER_TYPE_FREELANCER = 'freelancer';

    const MEMBER_TYPE_EMPLOYER = 'employer';

    //
    protected $table = 'skill_proposals';

    protected $primaryKey = 'skill_proposal_id';

    protected $fillable = [
        'skill_proposal_id',
        'skill_name',
        'member_id',
        'member_type',
        'job_id',
        'approved_by',
        'approved',
        'category_id',
        'proposal_date',
        'approved_date',
        'rating',
    ];

    protected $dates = [
        'proposal_date',
        'approved_date',
    ];

    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeFreelancer($query, $id)
    {
        return $query
                ->where('member_type', self::MEMBER_TYPE_FREELANCER)
                ->where('member_id', $id);
    }

    public function scopeEmployer($query, $id)
    {
        return $query
            ->where('member_type', self::MEMBER_TYPE_EMPLOYER)
            ->where('member_id', $id);
    }

    public function freelancer()
    {
        return $this->belongsTo(Freelancer::class, 'member_id', 'freelancer_id');
    }

    public function employer()
    {
        return $this->belongsTo(Employer::class, 'member_id', 'employer_id');
    }

    public function job()
    {
        return $this->belongsTo(Job::class, 'job_id', 'job_id');
    }

    public function scopeMember($query, $type)
    {
        $query->when($type === self::MEMBER_TYPE_FREELANCER, function ($q) {
            return $q->with('freelancer');
        });
        $query->when($type === self::MEMBER_TYPE_EMPLOYER, function ($q) {
            return $q->with('employer');
        });

        return $query;
    }

    public function createAll(array $model = [], $type)
    {
        if ($model && count($model)) {
            foreach ($model as $m) {
                $this->create(
                    array_replace(collect($m)->only('skill_name')->toArray(), [
                        'member_type' => $type,
                        'member_id' => Auth::user()->getKey(),
                        'category_id' => 1,
                        'job_id' => null
                    ])
                );
            }
        }
    }

    public function filterDelete(array $model = [])
    {
        if ($model && count($model)) {
            foreach ($model as $m) {
                if (isset($m['deleted']) && $m['deleted']) {
                    $this->whereSkillProposalId($m['skill_proposal_id'])->delete();
                }
            }
        }
    }
}
