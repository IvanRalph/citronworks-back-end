<?php

namespace App\Model\Skill;

use Illuminate\Database\Eloquent\Model;
use App\Model\Freelancer\FreelancerProfile;
use App\Model\Skill\Skill;
use App\Model\Freelancer\Freelancer;

class SkillFeeelancers extends Model
{
    protected $table = 'skill_freelancers';

    protected $primaryKey = 'skill_freelancer_id';

    protected $fillable = [
        'skill_freelancer_id',
        'freelancer_profile_id',
        'skill_id',
        'rating',
        'skill_order',
    ];

    public function profiles()
    {
        return $this
            ->belongsTo(
                FreelancerProfile::class,
                'freelancer_profile_id',
                'freelancer_profile_id'
            );
    }

    public function skill()
    {
        return
            $this->belongsTo(
                Skill::class,
                'skill_id',
                'skill_id'
            );
    }

    public function skill_feelancer()
    {
        return $this
            ->belongsTo(
                Freelancer::class,
                'freelancer_id',
                'freelancer_id'
            );
    }
}
