<?php

namespace App\Model\PresentationInvite;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    public $timestamps = false;

    protected $connection = 'invite';

    protected $table = 'invite';

    protected $guarded = ['invite_id'];

    protected $attributes = ['source' => 'https://citronworks.com'];
}
