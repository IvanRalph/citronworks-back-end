<?php

namespace App\Model\Invite;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    protected $primaryKey = 'invite_id';

    protected $table = 'invite';

    protected $guarded = ['invite_id'];

    public function inviteable()
    {
        return
            $this->morphTo();
    }

    public function employer()
    {
        return
            $this->belongsTo(Employer::class, 'employer_id');
    }
}
