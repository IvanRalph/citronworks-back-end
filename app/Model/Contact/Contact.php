<?php

namespace App\Model\Contact;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $primaryKey = 'contact_id';

    protected $table = 'contact';

    protected $guarded = 'contact_id';
}
