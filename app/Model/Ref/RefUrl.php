<?php

namespace App\Model\Ref;

use Illuminate\Database\Eloquent\Model;

class RefUrl extends Model
{
    protected $table = 'ref_urls';

    protected $primaryKey = 'ref_url_id';

    protected $casts = [
        'utm' => 'array',
    ];

    protected $fillable = [
        'ref_url_id',
        'member_id',
        'member_type',
        'ref_url',
        'ip_address',
        'landing_url',
        'utm',
        'return_visit',
    ];
}
