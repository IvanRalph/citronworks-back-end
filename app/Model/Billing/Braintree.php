<?php

namespace App\Model\Billing;

use Illuminate\Database\Eloquent\Model;

class Braintree extends Model
{
    protected $table = 'braintree';

    protected $primaryKey = 'braintree_id';

    protected $fillable = [
        'member_id',
        'member_type',
        'braintree_id',  //braintree api id
        'card_brand',
        'paypal_email',
        'card_last_digits',
    ];

    public function member()
    {
        return $this->morphTo('member', 'member_type', 'member_id');
    }
}
