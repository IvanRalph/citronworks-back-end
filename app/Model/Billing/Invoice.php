<?php

namespace App\Model\Billing;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'receipts';

    protected $primaryKey = 'receipt_id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'receipt_date',
    ];

    protected $guarded = ['receipt_id'];

    public function invoicable()
    {
        return
            $this->morphTo();
    }

    public function braintree()
    {
        return $this->belongsTo(\App\Model\Billing\Braintree::class, 'braintree_id', 'braintree_id');
    }
}
