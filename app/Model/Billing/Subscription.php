<?php

namespace App\Model\Billing;

use App\Container\Billing\Plan;
use Braintree\Subscription as BraintreeSubscription;
use Carbon\Carbon;
use Braintree\Plan as BraintreePlan;
use Exception;
use Illuminate\Database\Eloquent\Model;
use LogicException;

class Subscription extends Model
{
    protected $table = 'subscriptions';

    protected $primaryKey = 'subscription_id';

    protected $guarded = ['subscription_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'next_billing', 'billing_interval',
    ];

    /**
     * Indicates plan changes should be prorated.
     *
     * @var bool
     */
    protected $prorate = true;

    /**
     * Determine if the subscription is active, on trial, or within its grace period.
     *
     * @return bool
     */
    public function valid()
    {
        return $this->active() || $this->onGracePeriod();
    }

    /**
     * Determine if the subscription is active.
     *
     * @return bool
     */
    public function active()
    {
        return filter_var($this->is_active, FILTER_VALIDATE_BOOLEAN) && $this->onGracePeriod();
    }

    /**
     * Determine if the subscription is no longer active.
     *
     * @return bool
     */
    public function cancelled()
    {
        return ! filter_var($this->is_active, FILTER_VALIDATE_BOOLEAN) && ! $this->onGracePeriod();
    }

    /**
     * Determine if the subscription is within its grace period after cancellation.
     *
     * @return bool
     */
    public function onGracePeriod()
    {
        if (Carbon::now()->lt(Carbon::instance($this->next_billing))) {
            return true;
        }

        return false;
    }

    /**
     * Cancel the subscription immediately.
     *
     * @return $this
     */
    public function cancelNow()
    {
        $subscription = $this->asBraintreeSubscription();

        BraintreeSubscription::cancel($subscription->id);

        $this->markAsCancelled();

        return $this;
    }

    /**
     * Mark the subscription as cancelled.
     *
     * @return void
     */
    public function markAsCancelled()
    {
        $this->fill(['next_billing' => Carbon::now()])->save();
    }

    /**
     * Resume the cancelled subscription.
     *
     * @return $this
     * @throws \LogicException
     */
    public function resume()
    {
        if (! $this->onGracePeriod()) {
            throw new LogicException('Unable to resume subscription that is not within grace period.');
        }

        $subscription = $this->asBraintreeSubscription();

        BraintreeSubscription::update($subscription->id, [
            'neverExpires' => true,
            'numberOfBillingCycles' => null,
        ]);

        $this->next_billing = $subscription->billingPeriodEndDate;
        $this->is_active = 'true';

        $this->save();

        return $this;
    }

    public function pause()
    {
        $subscription = $this->asBraintreeSubscription();

        BraintreeSubscription::update($subscription->id, [
            'numberOfBillingCycles' => $subscription->currentBillingCycle,
        ]);

        $this->next_billing = $subscription->billingPeriodEndDate;
        $this->is_active = 'false';

        $this->save();

        return $this;
    }

    /**
     * Indicate that plan changes should not be prorated.
     *
     * @return $this
     */
    public function noProrate()
    {
        $this->prorate = false;

        return $this;
    }

    /**
     * Get the subscription as a Braintree subscription object.
     *
     * @return \Braintree\Subscription
     */
    public function asBraintreeSubscription() : BraintreeSubscription
    {
        return BraintreeSubscription::find($this->braintree_id);
    }

    public function member()
    {
        return $this->morphTo('member', 'member_type', 'member_id');
    }

    /**
     * Change freelancerCompany|Employer plan monthly|yearly
     * @param $subscription
     * @param $plan
     * @return $this
     * @throws Exception
     */
    public function swap($subscription, $plan)
    {
        if ($this->subscription_name === $subscription) {
            throw new Exception("Cannot Upgrade in the same ${$subscription}");
        }

        $plan = Plan::findPlan($plan);

        if ($this->wouldChangeBillingFrequency($plan) && $this->prorate) {
            return $this->swapAcrossFrequencies($plan);
        }

        $subscription = $this->asBraintreeSubscription();

        $response = BraintreeSubscription::update($subscription->id, [
            'planId' => $plan->id,
            'price' => number_format($plan->price, 2, '.', ''),
            'neverExpires' => true,
            'numberOfBillingCycles' => null,
            'options' => [
                'prorateCharges' => $this->prorate,
            ],
        ]);

        if ($response->success) {
            $this->fill([
                'braintree_plan' => $plan->id,
                'next_billing' => $subscription->nextBillingDate,
            ])->save();
        } else {
            throw new Exception('Braintree failed to swap plans: ' . $response->message);
        }

        return $this;
    }

    /**
     * Determine if the given plan would alter the billing frequency.
     *
     * @param  \Braintree\Plan  $plan
     * @return bool
     * @throws \Exception
     */
    protected function wouldChangeBillingFrequency($plan)
    {
        return $plan->billingFrequency !== Plan::findPlan($this->braintree_plan)->billingFrequency;
    }

    /**
     * Swap the subscription to a new Braintree plan with a different frequency.
     * @param $plan
     * @return $this
     * @throws \Exception
     */
    protected function swapAcrossFrequencies($plan) : self
    {
        $currentPlan = Plan::findPlan($this->braintree_plan);
        $options = [];

        $toMonthly = $this->switchingToMonthlyPlan($currentPlan, $plan);
        if ($toMonthly) {
            //change and upgrade to monthly
            //Note: no prorate
//            $discount = $this->getDiscountForSwitchToMonthly($currentPlan, $plan);
//            if ($discount->amount > 0 && $discount->numberOfBillingCycles > 0) {
//                $options = ['discounts' => ['add' => [
//                    [
//                        'inheritedFromId' => 'plan-credit',
//                        'amount' => (float) $discount->amount,
//                        'numberOfBillingCycles' => $discount->numberOfBillingCycles,
//                    ],
//                ]]];
//            }
            $options = ['firstBillingDate' => Carbon::parse($this->next_billing)->addYear()->addDay()];
            auth()->user()->newSubscription('monthly', $plan->id, null)->update([], $options, $toMonthly);
        } else {
            //upgrade to yearly subscription
            $options = ['firstBillingDate' => Carbon::parse($this->next_billing)->addDay()];
            auth()->user()->newSubscription('yearly', $plan->id, null)->update([], $options, $toMonthly);
        }

        return $this;
    }

    /**
     * Determine if the user is switching form yearly to monthly billing.
     *
     * @param  \Braintree\Plan  $currentPlan
     * @param  \Braintree\Plan  $plan
     * @return bool
     */
    protected function switchingToMonthlyPlan(BraintreePlan $currentPlan, BraintreePlan $plan)
    {
        return $currentPlan->billingFrequency == 12 && $plan->billingFrequency == 1;
    }

    /**
     * Get the discount to apply when switching to a monthly plan.
     *
     * @param  \Braintree\Plan  $currentPlan
     * @param  \Braintree\Plan  $plan
     * @return object
     */
    protected function getDiscountForSwitchToMonthly(BraintreePlan $currentPlan, BraintreePlan $plan)
    {
        return (object) [
            'amount' => $plan->price,
            'numberOfBillingCycles' => floor(
                $this->moneyRemainingOnYearlyPlan($currentPlan) / $plan->price
            ),
        ];
    }

    /**
     * Get the discount to apply when switching to a yearly plan.
     *
     * @param  BraintreePlan  $currentPlan
     * @param  BraintreePlan  $plan
     * @return object
     */
    protected function getDiscountForSwitchToYearly(BraintreePlan $currentPlan)
    {
        return (object) [
            'amount' => $this->moneyRemainingOnMonthlyPlan($currentPlan),
            'numberOfBillingCycles' => 1,
        ];
    }

    /**
     * Calculate the amount of discount to apply to a swap to yearly billing.
     *
     * @param  BraintreePlan $plan
     * @return float
     */
    protected function moneyRemainingOnMonthlyPlan(BraintreePlan $plan)
    {
        return ($plan->price /  Carbon::now()->daysInMonth) * Carbon::today()->diffInDays(Carbon::instance(
            $this->asBraintreeSubscription()->billingPeriodEndDate
        ), false);
    }

    /**
     * Calculate the amount of discount to apply to a swap to monthly billing.
     *
     * @param  \Braintree\Plan  $plan
     * @return float
     */
    protected function moneyRemainingOnYearlyPlan(BraintreePlan $plan)
    {
        return ($plan->price / 365) * Carbon::today()->diffInDays(Carbon::instance(
            $this->asBraintreeSubscription()->billingPeriodEndDate
        ), false);
    }

    public function subscriptionStatus()
    {
        if ($this->active()) {
            return 'active';
        }

        if ($this->cancelled()) {
            return  'canceled';
        }

//        TODO: can be removed once 3 months free for 500 first employers promo is done
        if ($this->promo()) {
            return 'active';
        }

        return 'paused';
    }

    public function invoice()
    {
        return $this->hasMany(\App\Model\Billing\Invoice::class, 'braintree_id', 'braintree_id');
    }

    public function hasCoupons()
    {
        if (!$this->braintree_id) {
            return false;
        }

        return $this->asBraintreeSubscription()->discounts ? true : false;
    }

    public function coupons()
    {
        if (!$this->braintree_id) {
            return null;
        }

        return $this->asBraintreeSubscription()->discounts;
    }

//    TODO: can be removed once 3 months free for 500 first employers promo is done
    public function promo()
    {
        return $this->subscription_id && !$this->braintree_id;
    }
}
