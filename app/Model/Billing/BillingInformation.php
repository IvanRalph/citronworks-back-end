<?php

namespace App\Model\Billing;

use Illuminate\Database\Eloquent\Model;

class BillingInformation extends Model
{
    public $timestamps = false;

    protected $table = 'billing_information';

    protected $primaryKey = 'billing_information_id';

    protected $guarded = ['billing_information_id'];

    protected $fillable = [
        'company_type', 'address_1', 'address_2', 'zip', 'state', 'country', 'city'
    ];

    protected $attributes = [
        'company_type' => 'employer', 'address_1' => '', 'address_2' => '', 'zip' => '', 'state' => '', 'country' => '', 'city' => ''
    ];

    public function company()
    {
        return
            $this->belongsTo(\App\Model\Employer\Companies::class, 'company_id');
    }

    public function employer()
    {
        return
            $this->belongsTo(\App\Model\Employer\Employer::class, 'employer_id');
    }
}
