<?php

namespace App\Model\FreelancerCompany;

use Illuminate\Database\Eloquent\Model;

class FreelancerCompanyInformation extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $primaryKey = 'freelancer_company_information_id';

    /**
     * @var string
     */
    protected $table = 'freelancer_company_information';

    /**
     * @var array
     */
    protected $guarded = ['freelancer_company_information_id'];

    /**
     * @var array
     */
    protected $attributes = [
        'company_name' => '', 'address_1' => '', 'street_1' => '', 'address_2' => '', 'street_2' => '', 'zip' => '', 'state' => '', 'country' => '', 'logo' => 'default.jpg', 'title' => '', 'intro' => '', 'description' => '', 'company_url' => '',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function freeLancerCompany()
    {
        return
            $this
                ->belongsTo(FreelancerCompany::class, 'freelancer_company_id');
    }
}
