<?php

namespace App\Model\FreelancerCompany;

use App\Container\Billing\Traits\Billing;
use App\Notifications\PasswordResetNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Laravel\Passport\Passport;

class FreelancerCompany extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;
    use Billing;

    public $timestamps = false;

    protected $table = 'freelancer_company';

    protected $primaryKey = 'freelancer_company_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'first_name', 'last_name', 'role',
    ];

    /**
     * @var array
     */
    protected $attributes = ['role' => 'owner'];

    /**
     * @var array
     */
    protected $guarded = ['freelancer_company_id'];

    /**ts
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clients()
    {
        return
            $this
                ->hasMany(Passport::clientModel(), 'id', 'freelancer_company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function gdpr()
    {
        return
            $this
                ->morphOne(\App\Model\Gdpr\Gdpr::class, 'gdprable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function invite()
    {
        return
            $this
                ->morphMany(\App\Model\Invite\Invite::class, 'inviteable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function information()
    {
        return
            $this
                ->hasOne(FreelancerCompanyInformation::class, 'freelancer_company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function billingInformation()
    {
        return
            $this
                ->morphOne(\App\Model\Billing\BillingInformation::class, 'companyable');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token, 'Password Reset'));
    }
}
