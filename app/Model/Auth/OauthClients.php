<?php

namespace App\Model\Auth;

use Laravel\Passport\Client as PassportClient;

class OauthClients extends PassportClient
{
    protected $table = 'oauth_clients';

    protected $attributes = ['provider' => ''];
}
