<?php

namespace App\Model\Auth;

use Laravel\Passport\Client as PassportClient;
use Laravel\Passport\Passport;

class OauthPersonalAccessClient extends PassportClient
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_personal_access_clients';

    /**
     * The guarded attributes on the model.
     *
     * @var array
     */
    protected $guarded = [];

//    /**
//     * Get all of the authentication codes for the client.
//     *
//     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
//     */
//    public function client()
//    {
//        return $this->belongsTo(Passport::clientModel());
//    }
}
