<?php

namespace App\Model\SkillRelation;

use Illuminate\Database\Eloquent\Model;

class SkillRelation extends Model
{
    public $timestamps = false;

    protected $table = 'skill_relations';

    protected $primaryKey = 'skill_relation_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'skill_id',
        'related_skill_id',
        'count',
    ];

    public function skill()
    {
        return $this->belongsTo(\App\Model\Skill\Skill::class, 'skill_relation_id');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeMostRelated($query)
    {
        return $query->orderBy('count', 'DESC');
    }
}
