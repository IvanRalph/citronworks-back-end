<?php

namespace App\Model\Freelancer;

use Illuminate\Database\Eloquent\Model;
use App\Model\Category\Category;
use App\Model\Skill\SkillFeeelancers;
use App\Model\Skill\Skill;
use App\Model\Skill\SkillFreelancer;

class FreelancerProfile extends Model
{
    public $timestamps = false;

    protected $table = 'freelancer_profile';

    protected $primaryKey = 'freelancer_profile_id';

    protected $fillable = [
        'freelancer_id', 'freelancer_type', 'title', 'intro', 'description', 'category',
        'full_time_price', 'part_time_price', 'hourly_price', 'agreed_price', 'full_time_ava', 'part_time_ava', 'hourly_ava',
    ];

    protected $attributes = [
        'freelancer_type' => 'person',
        'title' => '',
        'intro' => '',
        'description' => '',
        'category' => 1,
    ];

    protected $touches = ['freelancer'];

    protected $casts = [
        'full_time_ava' => 'boolean',
        'part_time_ava' => 'boolean',
        'hourly_time_ava' => 'boolean',
    ];

    /**
     * Mutators
     * Assign to 0 if value is null or any falsy value
     * Otherwise assign value
     *
     * @param  int|null $value
     * @return void
     */
    public function setFullTimeAvaAttribute($value)
    {
        if (!$value) {
            $this->attributes['full_time_ava'] = 0;
        } else {
            $this->attributes['full_time_ava'] = $value;
        }
    }

    /**
     * Mutators
     * Assign to 0 if value is null or any falsy value
     * Otherwise assign value
     *
     * @param  int|null $value
     * @return void
     */
    public function setPartTimeAvaAttribute($value)
    {
        if (!$value) {
            $this->attributes['part_time_ava'] = 0;
        } else {
            $this->attributes['part_time_ava'] = $value;
        }
    }

    /**
     * Mutators
     * Assign to 0 if value is null or any falsy value
     * Otherwise assign value
     *
     * @param  int|null $value
     * @return void
     */
    public function setHourlyAvaAttribute($value)
    {
        if (!$value) {
            $this->attributes['hourly_ava'] = 0;
        } else {
            $this->attributes['hourly_ava'] = $value;
        }
    }

    /**
     * Mutators
     * Assign to 0 if value is null or any falsy value
     * Otherwise assign value
     *
     * @param int|null $value
     * @return void
     */
    public function setFullTimePriceAttribute($value)
    {
        if (!$value) {
            $this->attributes['full_time_price'] = null;
        } else {
            $this->attributes['full_time_price'] = str_replace(',', '', $value);
        }
    }

    /**
     * Mutators
     * Assign to 0 if value is null or any falsy value
     * Otherwise assign value
     *
     * @param int|null $value
     * @return void
     */
    public function setPartTimePriceAttribute($value)
    {
        if (!$value) {
            $this->attributes['part_time_price'] = null;
        } else {
            $this->attributes['part_time_price'] = str_replace(',', '', $value);
        }
    }

    /**
     * Mutators
     * Assign to 0 if value is null or any falsy value
     * Otherwise assign value
     *
     * @param int|null $value
     * @return void
     */
    public function setHourlyPriceAttribute($value)
    {
        if (!$value) {
            $this->attributes['hourly_price'] = null;
        } else {
            $this->attributes['hourly_price'] = str_replace(',', '', $value);
        }
    }

    public function freelancer()
    {
        return $this->belongsTo(Freelancer::class, 'freelancer_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category', 'category_id');
    }

    public function freelancer_skills()
    {
        return
            $this->hasMany(
                SkillFeeelancers::class,
                'freelancer_profile_id',
                'freelancer_profile_id'
            );
    }

    public function freelancers_skills()
    {
        return $this->belongsToMany(
            Skill::class,
            'skill_freelancers',
            'freelancer_profile_id',
            'skill_id'
        )
            ->using(SkillFreelancer::class)
            ->withPivot('rating', 'skill_order');
    }
}
