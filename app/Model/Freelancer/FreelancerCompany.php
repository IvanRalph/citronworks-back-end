<?php

namespace App\Model\Freelancer;

use Illuminate\Database\Eloquent\Model;

class FreelancerCompany extends Model
{
    public $timestamps = false;
    protected $table = 'freelancer_company';
    protected $primaryKey = 'freelancer_company_id';
    protected $fillable = [ 'company_name', 'address_1', 'address_2', 'zip', 'state', 'country', 'logo', 'title', 'intro', 'description', 'url' ];
    protected $attributes = [
        'address_1' => '',
        'address_2' => '',
        'zip' => '',
        'state' => '',
        'country' => '',
        'logo' => '',
        'title' => '',
        'intro' => '',
        'description' => '',
        'url' => '',
        'company_name' => '',
    ];

    public function freelancer()
    {
        return $this->hasOne(Freelancer::class, 'freelancer_company_id');
    }
}
