<?php

namespace App\Model\Freelancer;

use App\Container\Billing\Traits\Billing;
use App\Container\Messaging\MessageableInterface;
use App\Container\Messaging\Traits\Messageable;
use App\Model\Employer\Hire;
use App\Mail\PasswordResetMail;
use App\Notifications\PasswordResetNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\HasApiTokens;
use Laravel\Passport\Passport;
use App\Model\Skill\SkillFreelancer;
use App\Model\Skill\SkillProposal;
use App\Traits\Authenticatable\CreatesAffiliateAccount;
use App\Traits\Models\ResyncsSearchIndex;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Laravel\Scout\Searchable;

class Freelancer extends Authenticatable implements MessageableInterface
{
    use Notifiable, HasApiTokens, Billing, Messageable, CreatesAffiliateAccount, Searchable, ResyncsSearchIndex;

    public $timestamps = true;

    public $table = 'freelancers';

    protected $primaryKey = 'freelancer_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'activation_token', 'email_verified_at', 'first_name', 'last_name', 'role', 'profile_photo', 'ip_address', 'freelancer_company_id', 'about_us',
    ];

    protected $guarded = ['freelancer_id'];

    /**ts
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activation_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    // sets default attribute values
    protected $attributes = [
        'profile_photo' => '',
        'first_name' => '',
        'last_name' => '',
        'role' => '',
        'ip_address' => 'unavailable',
    ];

    protected $appends = [
        'full_name', 'id',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function clients()
    {
        return $this->hasMany(Passport::clientModel(), 'id', 'freelancer_id');
    }

    public function freelancerCompany()
    {
        return $this->belongsTo(FreelancerCompany::class, 'freelancer_company_id');
    }

    public function profile()
    {
        return $this->hasOne(FreelancerProfile::class, 'freelancer_id');
    }

    public function hires()
    {
        return $this->hasMany(Hire::class, 'freelancer_id');
    }

    public function hiresByMyCompany()
    {
        return $this->hires()->where('company_id', Auth::user()->company_id);
    }

    public function latestHire()
    {
        return $this->hasOne(Hire::class, 'freelancer_id')
            ->latest();
    }

    public function latestUndismissedHire()
    {
        return $this->latestHire()->notYetDismissed();
    }

    public function profiles()
    {
        return $this->hasMany(FreelancerProfile::class, 'freelancer_id');
    }

    public function freelancerSkills()
    {
        return $this->hasManyThrough(
            SkillFreelancer::class,
            FreelancerProfile::class,
            'freelancer_id',
            'freelancer_profile_id',
            'freelancer_id',
            'freelancer_profile_id'
        );
    }

    public function proposedSkills()
    {
        return $this->morphMany(SkillProposal::class, 'member');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function company()
    {
        return $this->belongsTo(FreelancerCompany::class, 'freelancer_company_id');
    }

    public function freelancer_company()
    {
        return $this->belongsTo(FreelancerCompany::class, 'freelancer_company_id');
    }

    public function sendPasswordResetNotification($token)
    {
        Mail::to(request()->email)
            ->send(new PasswordResetMail($token, 'freelancer', $this));
//        $this->notify(new PasswordResetNotification($token, 'Password Reset'));
    }

    /**
     * Searches and Filters. Chooses Algolia if search keywords are given, or
     * uses local filtering if no search keywords are given.
     *
     * @param array $search [ 'keywords', 'skills', 'rates' ]
     *
     * @return Builder
     */
    public static function searchFilter(array $options)
    {
        // if no search options provided, simply return freelancers with content
        if (empty($options)) {
            return static::hasContent();
        }

        [ 'keywords' => $keywords, 'skills' => $skills, 'rates' => $rates ] = $options;

        // if not filtered, don't send a request to Algolia
        if (! static::isFiltered($keywords, $skills, $rates)) {
            return static::hasContent();
        }

        // search by keywords
        $scout = static::search($keywords);

        $parameters = [];

        // filter by skills
        if (! empty($skills)) {
            $parameters['tagFilters'] = [
                // put within an array for an OR search
                array_map(fn ($skillId) => 'skill-' . $skillId, $skills),
            ];
        }

        if (empty(Auth::user()->getSubscription())) {
            $parameters['restrictSearchableAttributes'] = [
                'title',
                'intro',
                'description',
            ];
        }

        if (! empty($parameters)) {
            $scout->with($parameters);
        }

        return static::filterByRates($scout, $rates);
    }

    public function hire() : self
    {
        $employer = Auth::user();
        $employer->company->hires()->create([
            'freelancer_id' => $this->freelancer_id,
            'employer_id' => $employer->employer_id,
        ]);
        return $this;
    }

    protected static function isFiltered(string $keywords, array $skills, array $rates) : bool
    {
        return ! empty($keywords)
            || ! empty($skills)
            || ! empty(array_filter(Arr::pluck($rates, 'enabled')));
    }

    protected static function filterByRates($scout, array $rates)
    {
        $rateTypes = ['full_time', 'part_time', 'hourly'];

        // filter by rates
        foreach ($rateTypes as $rt) {
            $rateFilter = $rates[$rt];
            $min = $rateFilter['min'];
            $max = $rateFilter['max'];
            if ($rateFilter['enabled']) {
                if (! empty($min)) {
                    $scout->where($rt . '_price', '>=', $min);
                }
                if (! empty($max)) {
                    $scout->where($rt . '_price', '<=', $max);
                }
            }
        }

        return $scout;
    }

    public function format()
    {
        return [
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
        ];
    }

    /**
     * Assign Storage Disk
     *
     * @return Illuminate\Filesystem\FilesystemAdapter
     */
    public function _storage() : FilesystemAdapter
    {
        return Storage::disk('image_storage');
    }

    /**
     * Freelancer is only searchable if its title, intro, and
     * description has content
     *
     * @return bool
     */
    public function shouldBeSearchable() : bool
    {
        $profile = $this->profile;

        if (empty($profile)) {
            return false;
        }

        return ! empty($profile->title)
            && ! empty($profile->intro)
            && ! empty($profile->description);
    }

    public function scopeHasContent(Builder $query)
    {
        $query->whereHas(
            'profile',
            fn ($subQuery) => $subQuery->where('title', '!=', '')
                ->where('intro', '!=', '')
                ->where('description', '!=', '')
        )->orderBy('updated_at', 'desc');
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $index = array_merge(
            Arr::only($this->toArray(), ['first_name', 'last_name', 'email', 'updated_at']),
            Arr::only(! empty($this->profile) ? $this->profile->toArray() : [], [
                'company_name',
                'title',
                'intro',
                'description',
                'full_time_price',
                'part_time_price',
                'hourly_price',
                'agreed_price',
                'full_time_ava',
                'part_time_ava',
                'hourly_ava',
            ]),
            [
                '_tags' => $this->freelancerSkills()
                    ->where('rating', '>=', 3)
                    ->pluck('skill_id')
                    ->map(fn ($skillId) => 'skill-' . $skillId)
                    ->all(),
            ]
        );

        // truncate searchable data if description is too long, to keep within Algolia's 10KB limit
        $index['description'] = substr(strip_tags($index['description']), 0, 9000);

        // fix an issue where algolia can't use floats in range operators
        $convertToInts = ['full_time_price', 'part_time_price', 'hourly_price', 'agreed_price'];
        foreach ($convertToInts as $ctf) {
            if (isset($index[$ctf])) {
                $index[$ctf] = intval($index[$ctf]);
            }
        }

        return $index;
    }
}
