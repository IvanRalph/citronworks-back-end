<?php

namespace App\Model\Messaging;

use App\Container\Messaging\MessageableInterface;
use App\Mail\MessageReceived;
use App\Notifications\UnreadConversationsUpdated;
use Facades\App\Services\PresenceService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphPivot;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Mail;
use Laravel\Scout\Searchable;

class Participant extends MorphPivot
{
    use Searchable;

    public $incrementing = true;

    protected $table = 'participants';

    protected $primaryKey = 'participant_id';

    protected $fillable = ['conversation_id', 'member_id', 'member_type'];

    /**
     * Conversation.
     *
     * @return BelongsTo
     */
    public function conversation() : BelongsTo
    {
        return $this->belongsTo(Conversation::class, 'conversation_id');
    }

    public function receiveMessage(MessageableInterface $sender)
    {
        // check if user has the app open
        if (
            PresenceService::isParticipantPresent($this)
            && $this->member->getKey() !== auth()->user()->getKey() // Exclude sender from receiving a notification
        ) {
            // user is using the app; notify in-app
            $this->member->notify(new UnreadConversationsUpdated);
        } else {
            // user is away; send an email if it's the first unread message in this conversation
            if (
                empty($this->num_unread_messages)
                && $this->member->getKey() !== auth()->user()->getKey() // Exclude sender from receiving a notification
            ) {
                Mail::to($this->member)->send(new MessageReceived($this->member, $sender));
            }
        }

        $this->increment('num_unread_messages');
    }

    /**
     * @return MorphTo
     */
    public function member() : MorphTo
    {
        return $this->morphTo('member', 'member_type', 'member_id')->with('company');
    }

    /**
     * Determine if the model should be searchable.
     *
     * @return bool
     */
    public function shouldBeSearchable()
    {
        return ! empty($this->conversation->lastMessage);
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $data = Arr::only($this->member->toArray(), ['first_name', 'last_name', 'email']);
        $data['updated_at'] = $this->conversation->lastMessage->created_at;
        $data['_tags'] = $this->conversation
            ->participants
            // filter out self
            ->filter(fn (Participant $participant) => $participant->participant_id != $this->participant_id)
            // add other participants as tags
            ->map(fn (Participant $participant) => $participant->member->messenger_id)
            // turn into a numeric-keyed array
            ->values()
            // turn into array
            ->all();

        return $data;
    }
}
