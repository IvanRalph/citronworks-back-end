<?php

namespace App\Model\Messaging;

use App\Events\MessageSent;
use App\Model\Employer\Employer;
use App\Model\Freelancer\Freelancer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laravel\Scout\Searchable;

class Message extends Model
{
    use Searchable;

    protected $table = 'messages';

    protected $primaryKey = 'message_id';

    protected $fillable = ['message', 'message_type'];

    protected $touches = ['conversation'];

    public static function boot()
    {
        parent::boot();

        // inject the participant ID
        static::creating(function ($model) {
            $model->participant_id = $model->conversation->currentParticipant->participant_id;
        });

        static::created(function ($model) {
            broadcast(new MessageSent($model))->toOthers();
        });
    }

    /**
     * @return BelongsTo
     */
    public function conversation() : BelongsTo
    {
        return
            $this
                ->belongsTo(Conversation::class, 'conversation_id');
    }

    /**
     * @return BelongsTo
     */
    public function participant() : BelongsTo
    {
        return
            $this
                ->belongsTo(Participant::class, 'participant_id')
                ->with('member');
    }

    /**
     * @param Conversation $conversation
     * @param $body
     * @param Participant $participant
     * @param $type
     * @param  Model $recepient
     * @return Model
     */
    public function send(
        Conversation $conversation,
        $body,
        $participant,
        $type,
        $recepient
    ) : Model {
        $message = $conversation->messages()->create([
            'message' => $body,
            'participant_id' => $participant->getKey(),
            'message_type' => $type,
        ]);

        broadcast(new MessageSent($message, $participant))->toOthers();

        return $message;
    }

    /**
     * @return Model
     */
    public function getSenderAttribute() : Model
    {
        if ($this->participant->member_type === 'freelancer') {
            $sender = Freelancer::find($this->participant->member_id);
        }

        if ($this->participant->member_type === 'employer') {
            $sender = Employer::find($this->participant->member_id);
        }

        return $sender;
    }

    /**
     * @return mixed
     */
    public function getMemberTypeAttribute()
    {
        return $this->participant->member_type;
    }

    /**
     * @param  Conversation $conversation
     * @return mixed
     */
    public function getMessages(Conversation $conversation)
    {
        return $conversation->messages;
    }

    public function getAuthIdentifierName()
    {
        return 'participant_id';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'message' => $this->message,
            'created_at' => $this->created_at,
            '_tags' => $this->conversation
                ->participants
                ->map(fn (Participant $participant) => $participant->member->messenger_id)
                ->all(),
        ];
    }
}
