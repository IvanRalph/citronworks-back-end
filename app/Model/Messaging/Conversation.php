<?php

namespace App\Model\Messaging;

use App\Container\Messaging\MessageableInterface;
use App\Model\Employer\Employer;
use Facades\App\Services\PresenceService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Conversation extends Model
{
    /**
     * @var string
     */
    protected $table = 'conversations';

    /**
     * @var string
     */
    protected $primaryKey = 'conversation_id';

    /**
     * @var array
     */
    protected $fillable = ['conversation_id'];

    /**
     * @var array
     */
    protected $appends = ['name'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('latest', function (Builder $query) {
            $query->orderBy('conversations.updated_at', 'desc');
        });
    }

    public function getNameAttribute() : string
    {
        return $this->otherParticipants()->get()
            ->pluck('member.full_name')
            ->implode(', ');
    }

    /**
     * Participants relation.
     * @return HasMany
     */
    public function participants() : HasMany
    {
        return $this->hasMany(Participant::class, 'conversation_id')
            ->with('member');
    }

    public function isParticipant(MessageableInterface $user) : bool
    {
        return $user->conversations()
            ->where('conversations.conversation_id', $this->conversation_id)
            ->exists();
    }

    /**
     * Get all participants except the currently logged-in user.
     *
     * @return HasMany
     */
    public function otherParticipants() : HasMany
    {
        if (! Auth::check()) {
            return $this->participants();
        }

        return $this->participants()
            ->whereNotIn('participant_id', Auth::user()->participants()->pluck('participant_id'));
    }

    public function currentParticipant()
    {
        $user = Auth::user();

        return $this->hasOne(Participant::class, 'conversation_id')
            ->where('member_type', $user->member_type)
            ->where('member_id', $user->id);
    }

    public function inactiveParticipants() : Collection
    {
        return $this->participants()
            ->whereNotIn(DB::raw('CONCAT(member_type, "-", member_id)'), PresenceService::idsPresentIn($this))
            ->get();
    }

    /**
     * @return HasMany
     */
    public function messages() : HasMany
    {
        return $this->hasMany(Message::class, 'conversation_id')
            ->with('participant.member')
            ->latest();
    }

    public function sendMessage(string $message) : Message
    {
        // add the message to the conversation
        $instance = $this->messages()
            ->create(compact('message'))
            ->load('participant.member');

        // if this is the first message in the conversation, make the participants searchable
        if ($this->messages()->count() === 1) {
            $this->participants->searchable();
        }

        // increment num_unread_messages counter for inactive participants
        //$this->inactiveParticipants()->each->receiveMessage(Auth::user());
        $authUser = Auth::user();
        $authUser->tableName = Str::singular($authUser->getTable());
        $this->inactiveParticipants()->each(function ($participant) use ($authUser) {
            if ($participant->member_type != $authUser->tableName) {
                $participant->receiveMessage($authUser);
            }
        });

        // return the message
        return $instance;
    }

    /**
     * Returns the 10 most recent messages. If user scrolls up, more can be loaded.
     *
     * @return [type]
     */
    public function recentMessages()
    {
        return $this->messages()->limit(20);
    }

    public function lastMessage()
    {
        return $this->hasOne(Message::class, 'conversation_id')
            ->with('participant.member')
            ->latest();
    }

    /**
     * Mark the conversation as read for the currently logged-in user.
     *
     * @return void
     */
    public function read() : void
    {
        if (! empty($this->currentParticipant->num_unread_messages)) {
            $this->currentParticipant->num_unread_messages = 0;
            $this->currentParticipant->save();
        }
    }

    public static function createOrFetchConversation(array $participantMembers) : self
    {
        if (! empty($conversation = static::fetchByParticipants($participantMembers))) {
            return $conversation;
        }

        return static::createWithParticipants($participantMembers);
    }

    protected static function fetchByParticipants(array $participantMembers) : ?self
    {
        $query = static::has('participants', count($participantMembers));

        foreach ($participantMembers as $pm) {
            $query->whereHas(
                'participants',
                fn (Builder $query) => $query->where('member_type', $pm->member_type)
                    ->where('member_id', $pm->id)
            );
        }

        return $query->first();
    }

    protected static function createWithParticipants(array $participantMembers) : self
    {
        // create conversation
        $conversation = static::create();

        // attach conversation to participant members
        foreach ($participantMembers as $pm) {
            $pm->conversations()->attach($conversation);
        }

        // return the conversation
        return $conversation;
    }

    public function getSubscription($member_id)
    {
        return Employer::with('subscription')->find($member_id);
    }
}
