<?php

namespace App\Model\Category;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Category extends Authenticatable
{
    public $timestamps = false;

    protected $table = 'categories';

    protected $primaryKey = 'category_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_name',
    ];

    public function skills()
    {
        return $this->hasMany(\App\Model\Skill\Skill::class, 'category_id');
    }

    public function skillProposals()
    {
        return $this->hasMany(\App\Model\Skill\SkillProposal::class, 'category_id');
    }

    public function scopeSearchByName($query, $params)
    {
        return $query->where('category_name', 'LIKE', '%' . trim($params) . '%');
    }
}
