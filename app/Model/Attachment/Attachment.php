<?php

namespace App\Model\Attachment;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'attached_id';

    protected $table = 'attachments';

    protected $guarded = ['attached_id'];
}
