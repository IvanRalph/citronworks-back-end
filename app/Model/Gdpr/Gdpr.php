<?php

namespace App\Model\Gdpr;

use Illuminate\Database\Eloquent\Model;

class Gdpr extends Model
{
    protected $primaryKey = 'gdpr_id';

    protected $table = 'gdpr';

    protected $dates = [
        'policy_date' => 'Y-m-d',
        'delete_request_date',
    ];

    protected $attributes = ['delete_request_date' => '1970-01-01 00:00:00'];

    protected $guarded = ['gdpr_id'];
}
