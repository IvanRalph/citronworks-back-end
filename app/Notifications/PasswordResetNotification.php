<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;

class PasswordResetNotification extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Mail Header Title
     *
     * @var string
     */
    public $headerTitle;

    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @param string $headerTitle
     */
    public function __construct($token, $headerTitle)
    {
        $this->token = $token;
        $this->headerTitle = $headerTitle;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }
        //Vue application URL;
        $appUrl = config('app.web_app_url') . '/resetpassword/?token=' . $this->token . '&email=' . $notifiable->getEmailForPasswordReset();

        return (new MailMessage)
            ->markdown('notifications.reset', [
                'firstName' => $notifiable->first_name,
                'headerTitle' => $this->headerTitle,
            ])
            ->subject(Lang::get('Reset Password'))
            ->line(Lang::get('Someone recently requested a password change for your CitronWorks account. If this was you, you can set a new password here:'))
            ->action(Lang::get('Reset Password'), $appUrl)
            ->line(Lang::get('If you don\'t want to change your password or didn\'t request this, just ignore and delete this message.'));
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param  \Closure  $callback
     * @return void
     */
    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
