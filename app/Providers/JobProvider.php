<?php

namespace App\Providers;

use App\Repositories\Contracts\Api\JobInterface;
use App\Repositories\Contracts\Web\JobsInterface;
use App\Repositories\Eloquent\Api\JobDataManipulation;
use App\Repositories\Eloquent\Web\JobsEloquent;
use Illuminate\Support\ServiceProvider;

class JobProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this
            ->app
            ->bind(JobInterface::class, JobDataManipulation::class);
        $this->app->bind(JobsInterface::class, JobsEloquent::class);
    }
}
