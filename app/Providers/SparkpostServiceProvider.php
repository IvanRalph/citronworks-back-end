<?php

namespace App\Providers;

use App\Container\Sparkpost\Sparkpost;
use GuzzleHttp\Client;
use Illuminate\Mail\TransportManager;
use Illuminate\Support\ServiceProvider;

class SparkpostServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->extend('swift.transport', function (TransportManager $manager) {
            $manager->extend('sparkpost', function () {
                $config = config('services.sparkpost', []);
                $sparkpostOptions = $config['options'] ?? [];
                $guzzleOptions = $config['guzzle'] ?? [];
                $client = $this->app->make(Client::class, $guzzleOptions);

                return new Sparkpost($client, $config['secret'], $sparkpostOptions);
            });

            return $manager;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
