<?php

namespace App\Providers;

use App\Repositories\Contracts\Api\FreelancerInterface;
use App\Repositories\Eloquent\Api\FreelancerDataManipulation;
use App\Repositories\Contracts\Web\FreelancerInterface as FreelancerWebInterface;
use App\Repositories\Eloquent\Web\FreelancerEloquent;
use Illuminate\Support\ServiceProvider;

class FreelancerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(FreelancerInterface::class, FreelancerDataManipulation::class);
        $this->app->bind(FreelancerWebInterface::class, FreelancerEloquent::class);
    }
}
