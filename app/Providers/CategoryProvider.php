<?php

namespace App\Providers;

use App\Repositories\Contracts\Api\CategoryInterface;
use App\Repositories\Eloquent\Api\CategoryDataManipulation;
use App\Repositories\Eloquent\Web\CategoryEloquent;
use App\Repositories\Contracts\Web\CategoryInterface as CategoryWebInterface;
use Illuminate\Support\ServiceProvider;

class CategoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this
            ->app
            ->bind(CategoryInterface::class, CategoryDataManipulation::class);
        $this->app->bind(CategoryWebInterface::class, CategoryEloquent::class);
    }
}
