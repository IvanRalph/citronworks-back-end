<?php

namespace App\Providers;

use App\Model\Employer\Employer;
use App\Model\Freelancer\Freelancer;
use App\Repositories\Contracts\Api\SubscriptionInterface;
use App\Repositories\Eloquent\Api\Subscription;
use Braintree_Configuration;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class BillingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Braintree_Configuration::environment(config('braintree.environment'));
        Braintree_Configuration::merchantId(config('braintree.merchant_id'));
        Braintree_Configuration::publicKey(config('braintree.public_key'));
        Braintree_Configuration::privateKey(config('braintree.private_key'));

        Relation::morphMap([
            'freelancer' => Freelancer::class,
            'employer' => Employer::class,
        ]);

        $this->app->bind(SubscriptionInterface::class, Subscription::class);
    }
}
