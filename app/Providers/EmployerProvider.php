<?php

namespace App\Providers;

use App\Repositories\Contracts\Api\EmployerInterface;
use App\Repositories\Eloquent\Api\EmployerDataManipulation;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Eloquent\Web\EmployerEloquent;
use App\Repositories\Contracts\Web\EmployerInterface as EmployerWebInterface;

class EmployerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app
             ->bind(EmployerInterface::class, EmployerDataManipulation::class);
            
        $this->app->bind(EmployerWebInterface::class, EmployerEloquent::class);
    }
}
