<?php

namespace App\Providers;

use App\Repositories\Contracts\Api\SkillInterface;
use App\Repositories\Eloquent\Api\SkillDataManipulation;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Eloquent\Web\SkillsEloquent;
use App\Repositories\Contracts\Web\SkillsInterface;
use App\Repositories\Contracts\Web\SkillsProposalInterface;
use App\Repositories\Eloquent\Web\SkillProposalEloquent;

class SkillProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this
            ->app
            ->bind(SkillInterface::class, SkillDataManipulation::class);
        $this->app->bind(SkillsInterface::class, SkillsEloquent::class);

        $this
            ->app
                ->bind(SkillsProposalInterface::class, SkillProposalEloquent::class);
    }
}
