<?php

namespace App\Providers;

use App\Container\HubSpot\Form\FormIntegration;
use Illuminate\Support\ServiceProvider;

class HubSpotProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('hubspot', function ($app, $param) {
            return
                new FormIntegration($param);
        });
    }
}
