<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Contracts\Web\StatisticsInterface;
use App\Repositories\Eloquent\Web\StatisticsFreelancerDataEloquent;

class StatsDataProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this
            ->app
                ->bind(StatisticsInterface::class, StatisticsFreelancerDataEloquent::class);
    }
}
