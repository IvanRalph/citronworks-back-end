<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use
    App\Repositories\Contracts\Api\RefUrlInterface;
use App\Repositories\Eloquent\Api\RefUrl;

class ReferrerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(RefUrlInterface::class, RefUrl::class);
    }
}
