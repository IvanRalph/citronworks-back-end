<?php

namespace App\Providers;

use App\Container\TestAbc;
use App\Container\TestAbcInterface;
use Illuminate\Support\ServiceProvider;

class TestAbcProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this
            ->app
            ->bind('TestAbc', function () {
                return
                new \App\Container\TestAbc;
            });

        $this
            ->app
            ->bind(TestAbcInterface::class, TestAbc::class);
    }
}
