<?php

namespace App\Providers;

use App\Container\SkillRelations\SkillRelationsInterface;
use App\Container\SkillRelations\SkillRelations;
use Illuminate\Support\ServiceProvider;

class SkillRelationProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this
            ->app
            ->bind(SkillRelationsInterface::class, SkillRelations::class);
    }
}
