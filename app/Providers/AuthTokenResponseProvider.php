<?php

namespace App\Providers;

use App\Container\Auth\TokenResponse;
use Illuminate\Support\ServiceProvider;

class AuthTokenResponseProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('api-authentication', function ($app, $param) {
            return new TokenResponse($param);
        });
    }
}
