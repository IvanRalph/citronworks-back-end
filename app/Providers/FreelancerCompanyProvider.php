<?php

namespace App\Providers;

use App\Repositories\Contracts\Api\FreelancerCompanyInterface;
use App\Repositories\Eloquent\Api\FreelancerCompanyDataManipulation;
use Illuminate\Support\ServiceProvider;

class FreelancerCompanyProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this
            ->app
            ->bind(FreelancerCompanyInterface::class, FreelancerCompanyDataManipulation::class);
    }
}
