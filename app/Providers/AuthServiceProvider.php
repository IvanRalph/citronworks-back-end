<?php

namespace App\Providers;

use App\Model\Auth\OauthClients;
use App\Model\Auth\OauthAccessToken;
use App\Model\Auth\OauthCodes;
use App\Model\Auth\OauthPersonalAccessClient;
use Laravel\Passport\Passport;
use Illuminate\Http\Request;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model\Messaging\Conversation' => 'App\Policies\ConversationPolicy',
        'App\Model\Job\Job' => 'App\Policies\JobPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     * @oaram Request $request
     * @return void
     */

    /**
     * Register any authentication / authorization services.
     *
     * @param Request $request
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        Passport::useClientModel(OauthClients::class);
        Passport::useTokenModel(OauthAccessToken::class);
        Passport::useAuthCodeModel(OauthCodes::class);
        Passport::usePersonalAccessClientModel(OauthPersonalAccessClient::class);
//        Passport::personalAccessClientId(config('tokens.personal.client_id'));
    }
}
