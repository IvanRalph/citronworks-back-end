<?php

namespace App\Facade;

class TestAbcFacade extends \Illuminate\Support\Facades\Facade
{
    public static function getFacadeAccessor()
    {
        return 'TestAbc';
    }
}
