<?php

namespace App\Policies;

use App\Container\Messaging\MessageableInterface;
use App\Model\Messaging\Conversation;
use Illuminate\Auth\Access\HandlesAuthorization;

class ConversationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any conversations.
     *
     * @param  MessageableInterface $user
     * @return mixed
     */
    public function viewAny(MessageableInterface $user)
    {
        //
    }

    /**
     * Determine whether the user can view the conversation.
     *
     * @param  MessageableInterface $user
     * @param  \App\Conversation    $conversation
     * @return mixed
     */
    public function view(MessageableInterface $user, Conversation $conversation)
    {
        //
    }

    /**
     * Determine whether the user can create conversations.
     *
     * @param  MessageableInterface $user
     * @return mixed
     */
    public function create(MessageableInterface $user) : bool
    {
        // never allow freelancer to create a new conversation
        if ($user->member_type === 'freelancer') {
            return false;
        }

        // only allow if Employer has an active subscription
        return ! empty($user->getSubscription());
    }

    /**
     * Determine whether the user can send a message to the conversation
     *
     * @param MessageableInterface $user
     * @param Conversation         $conversation
     *
     * @return [type]
     */
    public function sendMessage(MessageableInterface $user, Conversation $conversation) : bool
    {
        return $conversation->isParticipant($user);
    }

    /**
     * Determine if user can read the conversation
     *
     * @param MessageableInterface $user
     * @param Conversation         $conversation
     *
     * @return bool
     */
    public function read(MessageableInterface $user, Conversation $conversation) : bool
    {
        return $conversation->isParticipant($user);
    }

    /**
     * Determine whether the user can update the conversation.
     *
     * @param  MessageableInterface $user
     * @param  \App\Conversation    $conversation
     * @return mixed
     */
    public function update(MessageableInterface $user, Conversation $conversation)
    {
        //
    }

    /**
     * Determine whether the user can delete the conversation.
     *
     * @param  MessageableInterface $user
     * @param  \App\Conversation    $conversation
     * @return mixed
     */
    public function delete(MessageableInterface $user, Conversation $conversation)
    {
        //
    }

    /**
     * Determine whether the user can restore the conversation.
     *
     * @param  MessageableInterface $user
     * @param  \App\Conversation    $conversation
     * @return mixed
     */
    public function restore(MessageableInterface $user, Conversation $conversation)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the conversation.
     *
     * @param  MessageableInterface $user
     * @param  \App\Conversation    $conversation
     * @return mixed
     */
    public function forceDelete(MessageableInterface $user, Conversation $conversation)
    {
        //
    }
}
