<?php

namespace App\Policies;

use App\Model\Admin\User;
use App\Model\Employer\Employer;
use App\Model\Freelancer\Freelancer;
use App\Model\Job\Job;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Access\Response;

class JobPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any jobs.
     *
     * @param  \App\Model\Admin\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    public function view(Authenticatable $user, Job $job)
    {
        if ($user->member_type == 'employer') {
            return $job->employer_id == $user->employer_id;
        }

        if ($user->member_type == 'freelancer') {
            return $job->shouldBeSearchable();
        }

        return false;
    }

    /**
     * Determine whether the user can create jobs.
     *
     * @param  Employer $employer
     * @return mixed
     */
    public function create(Employer $employer)
    {
        if (!$employer->getSubscription() && $employer->jobs->count() >= 1) {
            return Response::deny('Ooops! you can only have one job active on the Free plan. Please upgrade to add more.');
        }
        if ($employer->getSubscription() && $employer->getSubscription()->braintree_plan === 'Silver-monthly' && $employer->jobs()->whereIn('job_status', ['public', 'members_only'])->count() >= 3) {
            return Response::deny('Ooops! you can only have three jobs active on the Paid plan. Please deactivate one of your other open jobs to post this.');
        }
        return true;
        // TODO: Confirm if free employers can post jobs
        // return ! empty($this->employer->subscription)
    }

    /**
     * Determine whether the user can update the job.
     *
     * @param  Employer $user
     * @param  Job      $job
     * @return mixed
     */
    public function update(Employer $employer, Job $job)
    {
        return $job->employer_id == $employer->employer_id;
    }

    /**
     * Determine whether the user can delete the job.
     *
     * @param  Employer $employer
     * @param  Job      $job
     * @return mixed
     */
    public function delete(Employer $employer, Job $job)
    {
        return $job->employer_id == $employer->employer_id;
    }

    public function deactivate(Employer $employer, Job $job)
    {
        return $job->employer_id == $employer->employer_id;
    }

    public function apply(Freelancer $freelancer, Job $job)
    {
        return $job->shouldBeSearchable();
    }

    public function bookmark(Freelancer $freelancer, Job $job)
    {
        return $job->shouldBeSearchable();
    }

    public function unbookmark(Freelancer $freelancer, Job $job)
    {
        return $job->shouldBeSearchable();
    }

    public function reactivate(Employer $employer, Job $job)
    {
        if (!$employer->getSubscription() && $employer->jobs->count() >= 1) {
            return Response::deny('Ooops! you can only have one job active on the Free plan. Please upgrade to add more.');
        }
        if ($employer->getSubscription() && $employer->getSubscription()->braintree_plan === 'Silver-monthly' && $employer->jobs()->whereIn('job_status', ['public', 'members_only'])->count() >= 3) {
            return Response::deny('Ooops! you can only have three jobs active on the Paid plan. Please deactivate one of your other open jobs to post this.');
        }
        return $job->employer_id == $employer->employer_id;
    }

    /**
     * Determine whether the user can restore the job.
     *
     * @param  \App\Model\Admin\User $user
     * @param  \App\Model\Job\Job    $job
     * @return mixed
     */
    public function restore(User $user, Job $job)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the job.
     *
     * @param  \App\Model\Admin\User $user
     * @param  \App\Model\Job\Job    $job
     * @return mixed
     */
    public function forceDelete(User $user, Job $job)
    {
        //
    }
}
