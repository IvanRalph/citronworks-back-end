<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactFormMail extends Mailable
{
    use Queueable, SerializesModels;

    public $websiteUrl;

    public $citronworksLogo;

    public $websiteUrlSupport;

    public $websiteUrlUnsubscribe;

    public $firstName;

    public $lastName;

    public $email;

    public $formSubject;

    public $formMessage;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($model)
    {
        $this->websiteUrl = 'https://www.citronworks.com';
        $this->citronworksLogo = 'https://www.citronworks.com/images/citronworkslogo.png';
        $this->websiteUrlSupport = 'mailto:team@citronworks.com';
        $this->websiteUrlUnsubscribe = 'mailto:team@citronworks.com';
        $this->firstName = $model->first_name;
        $this->lastName = $model->last_name;
        $this->email = $model->email;
        $this->formSubject = $model->subject;
        $this->formMessage = $model->message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return
            $this
                ->subject($this->formSubject)
                ->replyTo($this->email)
                ->view('mail.contactform')
                ->text('mail.contactform_plain');
    }
}
