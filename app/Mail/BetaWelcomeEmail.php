<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class BetaWelcomeEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $websiteUrl;

    public $citronworksLogo;

    public $firstName;

    public $websiteUrlSupport;

    public $websiteUrlUnsubscribe;

    public $memberType;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($firstName, $memberType)
    {
        $this->firstName = $firstName;
        $this->websiteUrl = 'https://www.citronworks.com';
        $this->citronworksLogo = 'https://www.citronworks.com/images/citronworkslogo.png';
        $this->websiteUrlSupport = 'mailto:team@citronworks.com';
        $this->websiteUrlUnsubscribe = 'mailto:team@citronworks.com';
        $this->memberType = $this->memberPlural($memberType);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Welcome ' . $this->firstName)
            ->replyTo('team@citronworks.com')
            ->view('mail.welcome_beta')
            ->text('mail.welcome_beta_plain');
    }

    protected function memberPlural($memberType)
    {
        if (Str::contains('company', $memberType)) {
            return Str::replaceLast('company', 'companies', $memberType);
        }

        return Str::plural($memberType);
    }
}
