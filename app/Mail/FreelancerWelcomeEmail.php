<?php

namespace App\Mail;

use App\Model\Freelancer\Freelancer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FreelancerWelcomeEmail extends Mailable
{
    use Queueable, SerializesModels;

    public Freelancer $freelancer;
    public string $temporaryAffiliatePassword;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Freelancer $freelancer, string $temporaryAffiliatePassword)
    {
        $this->freelancer = $freelancer;
        $this->temporaryAffiliatePassword = $temporaryAffiliatePassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Welcome to Citronworks!')
            ->view('mail.html.freelancer-welcome-email')
            ->text('mail.text.freelancer-welcome-email');
    }
}
