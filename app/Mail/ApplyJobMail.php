<?php

namespace App\Mail;

use App\Model\Attachment\Attachment;
use App\Model\Job\Application;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApplyJobMail extends Mailable
{
    use Queueable, SerializesModels;

    public $websiteUrl;

    public $citronworksLogo;

    public $websiteUrlSupport;

    public $websiteUrlUnsubscribe;

    public $jobApplication;

    public $firstName;

    public $lastName;

    public $email;

    public $jobTitle;

    public $jobSubject;

    public $applicantEmail;

    public $attachmentLink;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->websiteUrl = 'https://www.citronworks.com';
        $this->citronworksLogo = 'https://www.citronworks.com/images/citronworkslogo.png';
        $this->websiteUrlSupport = 'mailto:team@citronworks.com';
        $this->websiteUrlUnsubscribe = 'mailto:team@citronworks.com';
        $this->jobTitle = $application->job->title;
        $this->firstName = ucfirst($application->member->first_name);
        $this->lastName = ucfirst($application->member->last_name);
        $this->email = $application->member->email;
        $this->jobApplication = $application->message;
        $this->jobSubject = $application->subject;
        $this->subject = $application->subject;
        $this->applicantEmail = $application->member->email;
        $this->attachmentLink = $this->attachment($application->attachment);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject($this->jobSubject)->replyTo($this->applicantEmail)->view('mail.jobapplication')->text('mail.jobapplication_plain');

        \File::exists($this->attachmentLink) ? $view->attach($this->attachmentLink) : $view;
    }

    /**
     * @param $a
     * @return string|null
     */
    protected function attachment($a)
    {
        if (empty($a['id'])) {
            return null;
        }

        $attachment = Attachment::where('attached_id', $a['id'])->first();

        if (empty($attachment)) {
            return null;
        }

        return public_path('files/' . $attachment->label . $attachment->extension);
    }
}
