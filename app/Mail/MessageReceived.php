<?php

namespace App\Mail;

use App\Container\Messaging\MessageableInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessageReceived extends Mailable
{
    use Queueable, SerializesModels;

    public MessageableInterface $recipient;

    public MessageableInterface $sender;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(MessageableInterface $recipient, MessageableInterface $sender)
    {
        $this->recipient = $recipient;
        $this->sender = $sender;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.html.message-received')
            ->text('mail.text.message-received');
    }
}
