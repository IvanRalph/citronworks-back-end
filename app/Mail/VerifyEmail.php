<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $websiteUrl;

    public $verifyUrl;

    public $firstName;

    public $websiteUrlSupport;

    public $websiteUrlUnsubscribe;

    public $citronworksLogo;

    protected $email;

    protected $activationToken;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $activationToken, $type)
    {
        $this->firstName = $user->first_name;
        $this->email = $user->email;
        $this->activationToken = $activationToken;
        $this->websiteUrl = 'https://www.citronworks.com';
        $this->verifyUrl = $this->verifyUser($type);
        $this->citronworksLogo = 'https://www.citronworks.com/images/citronworkslogo.png';
        $this->websiteUrlSupport = 'mailto:team@citronworks.com';
        $this->websiteUrlUnsubscribe = 'mailto:team@citronworks.com';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.verify')
            ->replyTo('team@citronworks.com')
            ->text('mail.verify_plain');
    }

    protected function verifyUser($type)
    {
        //
        return config('app.web_app_url') . '/login?t=' . $this->activationToken;
    }
}
