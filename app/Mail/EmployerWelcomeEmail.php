<?php

namespace App\Mail;

use App\Model\Employer\Employer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmployerWelcomeEmail extends Mailable
{
    use Queueable, SerializesModels;

    public Employer $employer;
    public string $temporaryAffiliatePassword;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Employer $employer, string $temporaryAffiliatePassword)
    {
        $this->employer = $employer;
        $this->temporaryAffiliatePassword = $temporaryAffiliatePassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Welcome to Citronworks!')
            ->view('mail.html.employer-welcome-email')
            ->text('mail.text.employer-welcome-email');
    }
}
