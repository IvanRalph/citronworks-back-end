<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CitronworkersRequestEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $websiteUrl;

    public $citronworksLogo;

    public $websiteUrlSupport;

    public $websiteUrlUnsubscribe;

    public $firstName;

    public $lastName;

    public $email;

    public $message;

    public $company;

    public $companyWebsite;

    public $phone;

    public $referer;

    public $model;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($model)
    {
        $this->websiteUrl = 'https://www.citronworks.com';
        $this->citronworksLogo = 'https://www.citronworks.com/images/citronworkslogo.png';
        $this->websiteUrlSupport = 'mailto:team@citronworks.com';
        $this->websiteUrlUnsubscribe = 'mailto:team@citronworks.com';
        $this->firstName = $model->first_name;
        $this->lastName = $model->last_name;
        $this->email = $model->email;
        $this->message = (string) $model->message;
        $this->company = $model->company;
        $this->companyWebsite = $model->company_website;
        $this->phone = $model->phone;
        $this->referer = $model->referer;
        $this->model = $model;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return
            $this
                ->subject('New CitronWorkers Team Management Inquiry')
                ->replyTo($this->model->email)
                ->view('mail.citronworkers')
                ->text('mail.citronworkers_plain');
    }
}
