<?php

namespace App\Mail;

use App\Model\Job\JobInvite;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class JobInviteMail extends Mailable
{
    use Queueable, SerializesModels;

    public JobInvite $jobInvite;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(JobInvite $jobInvite)
    {
        $this->jobInvite = $jobInvite;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $jobInvite = $this->jobInvite->load('freelancer', 'employer', 'company', 'job');
        if (! empty($jobInvite->company->company_name)) {
            $senderName = $jobInvite->company->company_name;
        } else {
            $senderName = $jobInvite->employer->full_name;
        }

        return $this->subject('You have been invited to a Job')
            ->view('mail.html.job-invite', compact('jobInvite', 'senderName'))
            ->text('mail.text.job-invite', compact('jobInvite', 'senderName'));
    }
}
