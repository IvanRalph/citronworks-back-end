<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordResetMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Mail Header Title
     *
     * @var string
     */
    public $headerTitle;

    public $websiteUrl;

    public $citronworksLogo;

    public $firstName;

    public $websiteUrlReset;

    public $websiteUrlSupport;

    public $websiteUrlUnsubscribe;

    public $memberType;

    public $userEmail;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @param string $headerTitle
     */
    public function __construct($token, $memberType, $user)
    {
        $this->token = $token;
        $this->userEmail = $user->email;
        $this->memberType = $memberType;
        $this->websiteUrl = 'https://www.citronworks.com';
        $this->citronworksLogo = 'https://www.citronworks.com/images/citronworkslogo.png';
        $this->firstName = $user->first_name;
        $this->websiteUrlReset = $this->resetLink();
        $this->websiteUrlSupport = 'mailto:team@citronworks.com';
        $this->websiteUrlUnsubscribe = 'mailto:team@citronworks.com';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Password Reset')
            ->replyTo('team@citronworks.com')
            ->view('mail.reset')
            ->text('mail.reset_plain');
    }

    protected function resetLink()
    {
        return config('app.web_app_url') .
            '/resetpassword/?token=' .
            $this->token .
            '&email=' .
            rawurlencode(str_replace('+', '%2B', $this->userEmail)) .
            '&memberType=' . $this->memberType;
    }
}
