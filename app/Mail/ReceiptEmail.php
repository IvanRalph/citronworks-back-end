<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReceiptEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $invoice;

    public $websiteUrl;

    public $citronworksLogo;

    public $websiteUrlSupport;

    public $websiteUrlUnsubscribe;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $invoice)
    {
        $this->user = $user;
        $this->invoice = $invoice;
        $this->websiteUrl = 'https://www.citronworks.com';
        $this->citronworksLogo = 'https://www.citronworks.com/images/citronworkslogo.png';
        $this->websiteUrlSupport = 'mailto:team@citronworks.com';
        $this->websiteUrlUnsubscribe = 'mailto:team@citronworks.com';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.receipt_email')
            ->text('mail.receipt_email_plain')
            ->attach(
                public_path('invoices/' . $this->invoice->receipt_number . '.pdf'),
                [
                    'mime' => 'application/pdf'
                ]
            );
    }
}
