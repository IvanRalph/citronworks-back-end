<?php

namespace App\Observers;

use Model\Skill\Skill;

class ElasticsearchSkillObserver
{
    private $elasticsearch;

    public function __construct(Client $elasticsearch)
    {
        $this->elasticsearch = $elasticsearch;
    }

    public function created(Skill $skill)
    {
        $this->elasticsearch->index([
            'index' => 'acme',
            'type'  => 'skills',
            'id'    => $skill->id,
            'body'  => $skill->toArray(),
        ]);
    }

    public function updated(Skill $skill)
    {
        $this->elasticsearch->index([
            'index' => 'acme',
            'type'  => 'skills',
            'id'    => $skill->id,
            'body'  => $skill->toArray(),
        ]);
    }

    public function deleted(Skill $skill)
    {
        $this->elasticsearch->index([
            'index' => 'acme',
            'type'  => 'skills',
            'id'    => $skill->id,
        ]);
    }
}
