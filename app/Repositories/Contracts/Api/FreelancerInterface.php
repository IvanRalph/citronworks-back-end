<?php

namespace App\Repositories\Contracts\Api;

interface FreelancerInterface
{
    /**
     * Employers list of collection
     */
    public function collection();

    /**
     * Store employee's details
     *
     * @param  array $input
     * @return mixed
     */
    public function store(array $input);

    /**
     * Show employees details
     *
     * @param $id
     * @return mixed
     */
    public function details($email, $id = null);

    /**
     * Update employees details
     *
     * @param $freelancerId
     * @param  array $input
     * @return mixed
     */
    public function update($freelancerId, array $input);

    /**
     * Destroy employees details
     *
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @param  array $input
     * @param  array $fCompany
     * @return mixed
     */
    public function storeFreelancer(array $input, $fCompany);

    /**
     * @param $freelancer
     * @param $input
     * @return mixed
     */
    public function storeProfile($freelancer);

    /**
     * @param $freelancer
     * @param $input
     * @return mixed
     */
    public function storeProfilePhoto($freelancer, $input);

    /**
     * @param $freelancer
     * @param $input
     * @return mixed
     */
    public function updateProfile($freelancer, $input);

    /**
     * @param  array $input
     * @return mixed
     */
    public function findFreelancer(array $input);

    /**
     * @param $id
     * @return mixed
     */
    public function findViaId($id);

    /**
     * @return mixed
     */
    public function storeFreelancerCompany($input);

    /**
     * @param $freelancer
     * @param $input
     * @return mixed
     */
    public function storeFreelancerSkills($freelancer, $input);

    /**
     * @param $freelancer
     * @param $skill_id
     * @return mixed
     */
    public function deleteFreelancerSkill($freelancer, $skill_id);

    /**
     * @param $freelancer
     * @param  array $query
     * @return mixed
     */
    public function freelancerSkills($freelancer, $query = []);

    /**
     * @param $freelancer
     * @param $skill_id
     * @return mixed
     */
    public function showFreelancerSkill($freelancer, $skill_id);

    /**
     * @param $freelancer
     * @return mixed
     *
     */
    public function onBoardSkillProposals($freelancer);

    /**
     * @param $freelancer
     * @param $input
     * @return mixed
     */
    public function onBoardSkillProposalsStore($freelancer, $input);

    /**
     * @param $id
     * @return mixed
     */
    public function onBoardSkillProposalsDelete($id);
}
