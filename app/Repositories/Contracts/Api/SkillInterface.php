<?php

namespace App\Repositories\Contracts\Api;

interface SkillInterface
{
    /**
     * @param string $query
     * @return mixed
     */
    public function search($query = '');

    /**
     * Skill list of collection
     *
     * @param array $relationships
     * @param int $limit
     */
    public function collection($relationship = [], $limit);

    /**
     * @param array $input
     * @param bool $active
     * @return mixed
     */
    public function store($input, $active = true);

    /**
     * Show skill details
     *
     * @param $id
     * @return mixed
     */
    public function show($skillId, $noData = true, $relationships = []);

    /**
     * @param string $skillName
     * @return mixed
     */
    public function findByName($skillName);

    /**
     * @param array $input
     * @param int $skillId
     * @return mixed
     */
    public function update($input, $skillId);

    /**
     * @param int $skillId
     * @return mixed
     */
    public function destroy($skill);

    /**
     * @param string $query
     * @return mixed
     */
    public function proposals($query = '');

    /**
     * @param $id
     * @return mixed
     */
    public function findProposal($id);

    /**
     * @param $input
     * @return mixed
     */
    public function skillProposalCreate($input);

    /**
     * @param $input
     * @param $id
     * @return mixed
     */
    public function skillProposalUpdate($input, $id);

    /**
     * @param $proposal
     * @return mixed
     */
    public function destroyProposal($proposal);
}
