<?php

namespace App\Repositories\Contracts\Api;

interface CategoryInterface
{
    /**
     * @param string $query
     * @return mixed
     */
    public function search($query);

    /**
     * Categories collection
     */
    public function collection(array $relationships, $limit);

    /**
     * @param array $input
     * @return mixed
     */
    public function store(array $input);

    /**
     * @param int $id
     * @return mixed
     */
    public function show($categoryId, bool $noData, array $relationships);

    /**
     * @param $input
     * @param $categoryId
     * @return mixed
     */
    public function update($input, $categoryId);

    /**
     * @param $categoryId
     * @return mixed
     */
    public function destroy($category);
}
