<?php

namespace App\Repositories\Contracts\Api;

interface FreelancerCompanyInterface
{
    public function findFreelancerCompany($input);

    public function store($input);

    public function storeInformation($company);

    public function storeBilling($company);

    public function storeInvitation($company, $input);

    public function updateInformation($company, $input);

    public function updateBilling($company, $input);

    public function updateLogo($company, $input);

    public function create();

    public function show($id);

    public function edit($id);

    public function update($freelancerCoId, $input);

    public function destroy($id);

    public function findViaId($id);
}
