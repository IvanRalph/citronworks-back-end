<?php

namespace App\Repositories\Contracts\Api;

interface SubscriptionInterface
{
    /**
     * @param $billingInformation
     * @param $subscription
     * @return mixed
     */
    public function subscribe($billingInformation, $subscription);

    public function pauseSubscription($request);

    public function resumeSubscription($request);

    public function checkFreeSubscription($employer);
}
