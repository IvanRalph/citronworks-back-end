<?php

namespace App\Repositories\Contracts\Api;

interface RefUrlInterface
{
    /**
     * Get data by ID
     *
     * @param [type] $tokenId
     * @param boolean $toObject
     * @return void
     */
    public function show($tokenId, $toObject = true);

    /**
     * Insert data to Ref
     *
     * @param array $data
     * @return void
     */
    public function insert(array $data = []);
}
