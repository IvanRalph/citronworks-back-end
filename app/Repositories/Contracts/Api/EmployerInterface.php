<?php

namespace App\Repositories\Contracts\Api;

use Illuminate\Database\Eloquent\Model;

interface EmployerInterface
{
    /**
     * Employers list of collection
     */
    public function collection();

    // /**
    //  * Show employees details
    //  *
    //  * @param $id
    //  * @return mixed
    //  */
    // public function show();

    /**
     * Destroy employees details
     *
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @param $input
     * @return mixed
     */
    public function storeCompany($input);

    /**
     * @param $employer
     * @return mixed
     */
    public function storeBillingId($employer);

    /**
     * @param $company
     * @param $input
     * @return mixed
     */
    public function storeEmployer($company, $input);

    // /**
    //  * @param $employer
    //  * @param $input
    //  * @return mixed
    //  */
    // public function updateBillingInformation($employer, $input);

    /**
     * @param  array $input
     * @return mixed
     */
    public function findAndEmployer(array $input);

    /**
     * @param $id
     * @return mixed
     */
    public function findViaId($id);

    /**
     * @param $employer
     * @param $input
     * @return mixed
     */
    public function updateCompanyByEmployer($employer, $input);

    /**
     * @param $employer
     * @param $input
     * @return mixed
     */
    public function invitation($employer, $input);

    /**
     * @param  array $input
     * @return mixed
     */
    public function braintreeDetails(array $input);

    /**
     * Fetch employer's company
     *
     * @return oject
     */
    public function company();

    /**
     * Update employer's company info
     *
     * @return object
     */
    public function updateCompany(array $data = []): Model;
}
