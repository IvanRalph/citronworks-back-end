<?php

namespace App\Repositories\Contracts\Web;

interface EmployerInterface
{
    public function paginate();

    public function show($data);
}
