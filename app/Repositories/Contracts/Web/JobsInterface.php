<?php

namespace App\Repositories\Contracts\Web;

interface JobsInterface
{
    /**
     * Show skill details
     *
     * @param       $jobId
     * @param bool  $toObject
     * @param array $relationships
     * @return mixed
     */
    public function show($jobId, bool $toObject, array $relationships);

    /**
     * Show skill details
     *
     * @param int   $per_page
     * @param array $with
     * @return mixed
     */
    public function paginate(int $per_page, array $with = []);

    /**
     * @param string $jobName
     * @param array  $with
     * @param int    $per_Page
     * @return mixed
     */
    public function findByName($jobName, int $per_Page);

    /**
     * @param array $input
     * @param int   $jobId
     * @return mixed
     */
    public function update($input, $jobId);

    /**
     * @param int $job
     * @return mixed
     */
    public function destroy($job);

    /**
     * @param array $job
     * @return mixed
     */
    public function post(array $job);
}
