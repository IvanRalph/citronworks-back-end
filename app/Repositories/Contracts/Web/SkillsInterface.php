<?php

namespace App\Repositories\Contracts\Web;

interface SkillsInterface
{
    /**
     * Show skill details
     *
     * @param $id
     * @return mixed
     */
    public function show($skillId, bool $toObject, array $relationships);

    /**
     * Show skill details
     *
     * @param $id
     * @return mixed
     */
    public function paginate(int $per_page, array $with);

    /**
     * @param string $skillName
     * @return mixed
     */
    public function findByName($skillName, array $with, int $per_Page);

    /**
     * @param array $input
     * @param int $skillId
     * @return mixed
     */
    public function update($input, $skillId);

    /**
     * @param int $skillId
     * @return mixed
     */
    public function destroy($skill);

    /**
     * @param int $skillId
     * @return mixed
     */
    public function post(array $skill);
}
