<?php

namespace App\Repositories\Contracts\Web;

use Illuminate\Http\Request;

interface FreelancerInterface
{
    /**
     * Employers list of collection
     */
    public function collection();

    /**
     * Store employee's details
     *
     * @param array $input
     * @return mixed
     */
    public function store(array $input);

    /**
     * Show employees details
     *
     * @param $id
     * @return mixed
     */
    public function details($email, $id = null);

    /**
     * Show employees details
     *
     * @param [type] $skillId
     * @param boolean $toObject
     * @param array $relationships
     * @return void
     */
    public function show($skillId, bool $toObject, array $relationships);

    /**
     * Update employees details
     *
     * @param $freelancerId
     * @param array $input
     * @return mixed
     */
    public function update(array $input = [], $freelancerId = null);

    /**
     * Destroy employees details
     *
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @param array $input
     * @param array $fCompany
     * @return mixed
     */
    public function storeFreelancer(array $input);

    /**
     * @param $freelancer
     * @param $input
     * @return mixed
     */
    public function storeProfile($freelancer, $input);

    /**
    * @param $freelancer
    * @param $input
    * @return mixed
    */
    public function storeSkill($input, $freelancer);

    /**
     * @param $freelancer
     * @param $input
     * @return mixed
     */
    public function storeProfilePhoto($freelancer, $input);

    /**
     * @param $freelancer
     * @param $input
     * @return mixed
     */
    public function updateCompany($freelancer, $input);

    /**
     * @param array $input
     * @return mixed
     */
    public function findFreelancer(Request $request, int $per_page, $relationships = []);

    /**
     * @param $freelancer
     * @param $skill_id
     * @return mixed
     */
    public function deleteFreelancerSkill($freelancer, $skill_id);

    /**
    * @param $freelancer
    * @param $skill_id
    * @return mixed
    */
    public function paginate($per_page, $relationships = []);
}
