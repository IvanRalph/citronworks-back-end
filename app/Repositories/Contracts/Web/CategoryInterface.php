<?php

namespace App\Repositories\Contracts\Web;

use Illuminate\Database\Eloquent\Collection;

interface CategoryInterface
{
    /**
     * @param string $query
     * @return mixed
     */
    public function search($query);

    /**
     * Categories collection
     */
    public function collection(array $relationships, $limit);

    /**
     * @param array $input
     * @return mixed
     */
    public function store(array $input);

    /**
     * @param int $id
     * @return mixed
     */
    public function show($categoryId, $toObject = false, array $relationships = []);

    /**
     * @return App\Model\Category\Category;
     */
    public function all() : Collection;

    /**
     * @param $input
     * @param $categoryId
     * @return mixed
     */
    public function update($input, $categoryId);

    /**
     * @param $categoryId
     * @return mixed
     */
    public function destroy($category);

    /**
    * @param $categoryId
    * @return mixed
    */
    public function paginate($perpage = 10, string $orderby = 'category_id', $order = 'DESC');
}
