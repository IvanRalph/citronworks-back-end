<?php

namespace App\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Model;
use RuntimeException;

abstract class RepositoryAbstract
{
    protected $entity;

    public function __construct()
    {
        $this->entity = $this->resolver();
    }

    abstract public function entity();

    public function reset()
    {
        $this->entity = $this->resolver();
    }

    public function __call($method, $parameters)
    {
        if (method_exists($this->entity, 'scope' . ucfirst($method))) {
            $this->entity = $this->entity->{$method}(...$parameters);

            return $this;
        }
        $this->entity = call_user_func_array([$this->entity, $method], $parameters);

        return $this;
    }

    public function __get($name)
    {
        return $this->entity->{$name};
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function resolver()
    {
        $model = app()->make($this->entity());

        if (!$model instanceof Model) {
            throw new RuntimeException(
                "Class {$this->entity()} must be an instance of Illuminate\\Database\\Eloquent\\Model"
            );
        }

        return $model;
    }
}
