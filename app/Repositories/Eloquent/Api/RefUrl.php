<?php

namespace App\Repositories\Eloquent\Api;

use App\Model\Ref\RefUrl as RefModel;
use App\Repositories\Contracts\Api\RefUrlInterface;

class RefUrl implements RefUrlInterface
{
    protected $model;

    protected $object;

    public function __construct()
    {
        $this->model = new RefModel;
    }

    /**
     * Show Data
     *
     * @param [type]  $tokenId
     * @param boolean $toObject
     */
    public function show($tokenId, $toObject = true)
    {
        if ($toObject) {
            $this->object =
                $this->model->find($tokenId);

            return $this;
        }

        return
            $this->model->find($tokenId)
                ?? null;
    }

    /**
     * Insert Data
     *
     * @param  array $data
     * @return void
     */
    public function insert(array $data = [])
    {
        return
            $this->model->create($data)
                ?? null;
    }

    /**
     * Update data
     *
     * @param  array $data
     * @return void
     */
    public function update(array $data = [])
    {
        if (!empty($this->object)) {
            return $this->object->update($data);
        }
    }
}
