<?php

namespace App\Repositories\Eloquent\Api;

use App\Model\Skill\Skill;
use App\Model\Skill\SkillProposal;
use App\Repositories\Contracts\Api\SkillInterface;
use App\Helpers\Constants;
use Carbon\Carbon;

class SkillDataManipulation implements SkillInterface
{
    protected $model;

    protected $collection;

    protected $skill;

    /**
     * Inject model in constructor so that
     * it is only in one place if we have to change the model
     *
     * @param Skill $model
     */
    public function __construct(Skill $model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function search_skill($query = '')
    {
        $skill_list =
                $this->model
                    ->with('category')
                    ->searchByName($query)
                    ->get();
        $related_skills = [];

        if (!$skill_list->isEmpty()) {
            foreach ($skill_list as $i => $skill) {
                $related_skills[] =
                    $skill
                        ->relatedSkill()
                        ->mostRelated()
                        ->orderBy('count', 'desc')
                         ->first();
            }
        }
        if (count($related_skills)) {
            $mostRelevant =
                collect($related_skills)
                    ->where('skill_id', '!=', null)
                    ->sortByDesc('count')
                    ->pluck('skill_id')
                    ->toArray();

            if (count($mostRelevant)) {
                $ids_ordered =
                implode(',', $mostRelevant);

                $skills =
                $this->model
                    ->whereIn('skill_id', $mostRelevant)
                    // ->orderByRaw("FIELD(skill_id, $ids_ordered)")
                    ->orderByRaw(\DB::raw('FIELD(skill_id, ' . $ids_ordered . ' )'))
                    ->get();

                $m =
                $skills->merge($skill_list);

                return $m->unique();
            }
        }

        return $skill_list->unique();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function search($query = '')
    {
        $skills =
            $skill_list =
                $this->model
                    ->with('category')
                    ->searchByName($query)
                    ->get();

        if (!$skill_list->isEmpty()) {
            $skill_list->each(function ($skill, $parent_key) use ($skills) {
                $related_skills =
                    $skill
                        ->relatedSkill()
                        ->mostRelated()
                        ->take('20')
                        ->select('related_skill_id')
                        ->get();

                if (!$related_skills->isEmpty()) {
                    $this->model
                            ->with('category')
                            ->whereIn('skill_id', $related_skills->toArray())
                            ->each(function ($re_skill, $key) use ($skills, $parent_key) {
                                $correct_key = $key + ($parent_key + 1);
                                $skills->splice($correct_key, 0, [$re_skill]);
                            });
                }
            });
        }

        return $skills->unique();
    }

    /**
     * Return all resources
     *
     * @return
     */
    public function collection($relationship = [], $limit)
    {
        return $this->model->with($relationship)->paginate($limit);
    }

    /**
     * Check the requested resource exists
     *
     * @param $skillId
     */
    public function findByName($skillName)
    {
        return $this->model->where('skill_name', $skillName)->first();
    }

    /**
     * Check the requested resource exists
     *
     * @param $skillId
     */
    public function exists($skillId)
    {
        if (!$skillId) {
            return false;
        }

        return $this->model->where('skill_id', $skillId)->exists();
    }

    /**
     * Fetch the requested resource
     *
     * @param $skillId
     */
    public function show($skillId, $noData = true, $relationships = [])
    {
        return ($noData == true) ?
            $this->model
                ->with($relationships)
                ->where('skill_id', $skillId)
                ->first()
            : $this->model
                ->where('skill_id', $skillId)
                ->first();
    }

    /**
     * @param $skillId
     * @return mixed
     */
    public function category($skillId)
    {
        $skill = $this->model->find($skillId);
        $category = $skill->category;

        return $category;
    }

    /**
     * Store skill's details
     *
     * @param array $input
     * @return mixed
     */
    public function store($input, $active = true)
    {
        return $this->model->create($input);
    }

    /**
     * @param array $input
     * @param int $skillId
     * @return mixed|null
     */
    public function update($input, $skillId)
    {
        $skill = $this->model->find($skillId);

        return $skill ?
            tap($skill)->update($input) :
            $skill;
    }

    /**
     * Remove a resources
     *
     * @param  [type] $skillId
     * @return [type]
     */
    public function destroy($skill)
    {
        return $skill ?
            $skill->delete() :
            null;
    }

    /**
     * Query is an associative array of skill proposal columns -> value
     *  e.g. $query => [ 'category_id' => 1 ]
     *
     * @param array $query
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public function proposals($query = [])
    {
        $proposals = new SkillProposal();

        if (!empty($query)) {
            $proposals = $this->findBy($proposals, $query);
        }

        return $proposals->with('category')->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findProposal($id)
    {
        return SkillProposal::with('category')
                    ->find($id);
    }

    /**
     * @param $input
     * @return mixed
     */
    public function skillProposalCreate($input)
    {
        $input['proposal_date'] = Carbon::now()->toDateTimeString();

        return SkillProposal::create($input);
    }

    /**
     * @param $input
     * @param $id
     * @return mixed
     */
    public function skillProposalUpdate($input, $id)
    {
        $skill_proposal = SkillProposal::find($id);

        return $skill_proposal ?
            tap($skill_proposal)->update($input) :
            $skill_proposal;
    }

    /**
     * @param $proposal
     * @return mixed
     */
    public function destroyProposal($proposal)
    {
        return $proposal ?
            $proposal->delete() :
            null;
    }

    /**
     * Find by function based on the model attributes
     *
     *   e.g.  skill.category_id
     *
     *          $where = [
     *              'category_id' => 23
     *          ]
     *
     *
     * @param $model
     * @param $where
     * @return mixed
     */
    private function findBy($model, $where)
    {
        $model = $model->where(function ($query) use ($where) {
            foreach ($where as $field => $value) {
                // get where type : orWhere() or where()
                $where_type = isset($value['where_type']) ? $value['where_type'] : Constants::OPERATOR_WHERE;

                // get the operator type
                $operator = isset($value['operator']) ? $value['operator'] : Constants::OPERATOR_AND;

                // get the value
                $val = isset($value['value']) ? $value['value'] : $value;

                // build query
                switch ($operator) {
                    case Constants::OPERATOR_AND:
                        $query->$where_type($field, $val);

                        break;
                    case Constants::OPERATOR_OR:
                        $query->$where_type($field, $val);

                        break;
                    case Constants::OPERATOR_LIKE:
                        $query->$where_type($field, 'LIKE', $val);

                        break;
                }
            }
        });

        return $model;
    }
}
