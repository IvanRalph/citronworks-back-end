<?php

namespace App\Repositories\Eloquent\Api;

use App\Model\Category\Category;
use App\Repositories\Contracts\Api\CategoryInterface;

class CategoryDataManipulation implements CategoryInterface
{
    protected $model;

    protected $collection;

    protected $category;

    /**
     * Inject model in constructor so that
     * it is only in one place if we have to change the model
     *
     * @param Category $model
     */
    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Resource
     */
    public function search($query = '')
    {
        return Category::with('skills')
            ->searchByName($query)
            ->get();
    }

    /**
     * Return all resources
     *
     * @return
     */
    public function collection($relationship = [], $limit)
    {
        return $this->model->with($relationship)->paginate($limit);
    }

    /**
     * Check the requested resource exists
     *
     * @param $categoryId
     */
    public function exists($categoryId)
    {
        if (!$categoryId) {
            return false;
        }

        return $this->model->where('category_id', $categoryId)->exists();
    }

    /**
     * Fetch the requested resource
     *
     * @param $categoryId
     */
    public function show($categoryId, $noData = true, $relationships = [])
    {
        return ($noData == true) ?
            $this->model
                ->with($relationships)
                ->where('category_id', $categoryId)
                ->first()
            : $this->model
                ->where('category_id', $categoryId)
                ->first();
    }

    /**
     * Fetch the requested resource
     *
     * @param $categoryId
     */
    public function find(array $where, $fields = [])
    {
        if (empty($where)) {
            return null;
        }
        // Add category Id for it is important when loading relationship after this
        if (!empty($fields) && !array_key_exists('category_id', $fields)) {
            array_unshift($fields, 'category_id');

            $this->category = $this->model->where(...$where)->first($fields);

            return$this->category;
        }

        $this->category = $this->model->where(...$where)->first();

        return$this->category;
    }

    /**
     * Store category's details
     *
     * @param array $input
     * @return mixed
     */
    public function store($input)
    {
        return $this->model->create($input);
    }

    /** Update category details
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update($input, $categoryId)
    {
        $this->category = $this->model->find($categoryId);

        return $this->category ?
            tap($this->category)->update($input) :
            $this->category;
    }

    /**
     * Remove a resources
     *
     * @param  [type] $categoryId
     * @return [type]
     */
    public function destroy($category)
    {
        return $category->delete();
    }

    /**
     * @param $email
     * @param null $id
     * @return mixed
     */
    public function details($category, $id = null)
    {
        $category = Category::where('category_name', $category)
            ->with('category')
            ->first();

        return
            $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $query =
            Category::all()
            ->with('category');

        return
            $query;
    }
}
