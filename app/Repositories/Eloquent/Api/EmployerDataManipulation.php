<?php

namespace App\Repositories\Eloquent\Api;

use App\Model\Billing\BillingInformation;
use App\Model\Employer\BookmarkFreelancer;
use App\Model\Employer\Companies as Company;
use App\Model\Employer\Employer as EmployerModel;
use App\Model\Employer\Invite;
use App\Repositories\Contracts\Api\EmployerInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class EmployerDataManipulation implements EmployerInterface
{
    protected $company;

    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @param array $relationship
     * @param array $fields
     * @return
     */
    public function collection($relationship = [], $fields = [])
    {
        //
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findViaId($id)
    {
        $quey =
            EmployerModel::where('employer_id', $id)->first();

        return
            $quey;
    }

    /**
     * Update employees details.
     *
     * @param  integer                           $id
     * @param  array                             $input
     * @return mixed|App\Model\Employer\Employer
     */
    public function update($employerId, $input)
    {
        unset($input['country']);

        $query = EmployerModel::find($employerId);
        $query->update($input);

        return
            $query;
    }

    /**
     * Remove a resources.
     *
     * @param  integer $employerId
     * @return bool
     */
    public function destroy($employerId)
    {
        return $this->model->destroy($employerId);
    }

    /**
     * @param $input
     * @return Company|mixed
     */
    public function storeCompany($input)
    {
        $company = new Company(['country' => $input['country']]);
        $company
            ->save();

        return
            $company;
    }

    /**
     * @param $company
     */
    public function storeBillingId($company)
    {
        $billing = new BillingInformation();
        $billing
            ->company()
            ->associate($company)
            ->save();

        return
            $billing;
    }

    /**
     * @param $company
     * @param $input
     * @return EmployerModel|mixed
     */
    public function storeEmployer($company, $input)
    {
        $employer = new EmployerModel($input);
        $employer->company()
            ->associate($company)
            ->save();

        return
            $employer;
    }

    // /**
    //  * @param $employer
    //  * @param $input
    //  * @return mixed
    //  */
    // public function storeEmployerCompanyLogo($employer, $input)
    // {
    //     $company =
    //         Company::where('company_id', $employer['company_id'])->first();

    //     $company
    //         ->update(['logo' => $input]);

    //     return
    //         $company;
    // }

    // /**
    //  * @param $employer
    //  * @param $input
    //  * @return BillingInformation
    //  */
    // public function updateBillingInformation($employer, $input)
    // {
    //     $billing =
    //         BillingInformation::where('billing_information_id', $employer['billing_information_id'])->first();
    //     $billing
    //         ->update($input);

    //     return
    //         $billing;
    // }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int                       $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($data)
    // {
    //     $query = EmployerModel::where('employer_id', $data['employer_id'])->first();
    //     $query->load('company.billing');

    //     return
    //         $query;
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @param  array $input
     * @return mixed
     */
    public function findAndEmployer(array $input)
    {
        $result = EmployerModel::where('email', $input['email'])
                ->with('company', 'company.billing')
                ->first();

        return $result;
    }

    /**
     * @param $employer
     * @param $input
     * @return mixed
     */
    public function updateCompanyByEmployer($employer, $input)
    {
        $company =
            Company::where('company_id', $employer['company_id'])->first();
        $company
               ->update($input);

        return
            $company;
    }

    /**
     * @param $employer
     * @param $input
     * @return Invite|array|int|mixed|string
     */
    public function invitation($employer, $input)
    {
        $invite = [];
        foreach ($input as $invite => $inviteVal) {
            $invite = new Invite($inviteVal);
            $invite
                ->employer()
                ->associate($employer)
                ->save();
        }

        return $invite['employer']['invite'];
    }

    /**
     * @param  array $input
     * @return mixed
     */
    public function braintreeDetails(array $input)
    {
        $employer =
            EmployerModel::where('email', $input['email'])
                ->first();

        return $employer->asBraintreeCustomer();
    }

    public function sendVerificationEmail($employer, $token)
    {
        Mail::to($employer->email)
            ->send(new \App\Mail\VerifyEmail($employer, $token, 'employer'));
    }

    public function company() : Model
    {
        return Auth::user()->company()->first();
    }

    public function updateCompany(array $data = []) : Model
    {
        $this->company()->update($data);

        return $this->company();
    }

    public function getBookmarkedFreelancers($employer)
    {
        return $employer->bookmarkedFreelancers()->with('freelancer.profile', 'freelancer.freelancerSkills.skill')->paginate(10);
    }

    public function storeBookmarkedFreelancer($employer, $request)
    {
        $bookmarkedFreelancer = BookmarkFreelancer::firstOrCreate([
            'member_id' => $employer->id,
            'freelancer_id' => $request->freelancer_id,
        ]);
        return response($bookmarkedFreelancer);
    }

    public function deleteBookmarkedFreelancer($employer, $id)
    {
        $bookmark = BookmarkFreelancer::where('bookmark_id_freelancer', $id)->where('member_id', $employer->employer_id)->firstOrFail();
        return response(['deleted' => $bookmark->delete()]);
    }
}
