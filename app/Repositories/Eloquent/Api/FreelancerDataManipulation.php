<?php

namespace App\Repositories\Eloquent\Api;

use App\Model\Freelancer\FreelancerCompany;
use App\Model\Freelancer\Freelancer;
use App\Model\Freelancer\FreelancerProfile;
use App\Model\Skill\SkillProposal;
use App\Repositories\Contracts\Api\FreelancerInterface;
use App\Model\SkillRelation\SkillRelation;
use Illuminate\Support\Facades\Mail;

class FreelancerDataManipulation implements FreelancerInterface
{
    /**
     * @var SkillProposal
     */
    private $skill_proposals;
    /**
     * @var SkillRelation
     */
    private $skill_relations;

    /**
     * Inject model in constructor so that
     * it is only in one place if we have to change the model
     *
     */
    public function __construct()
    {
        $this->skill_proposals = new SkillProposal();
        $this->skill_relations = new SkillRelation();
    }

    public function storeFreelancerCompany($input)
    {
        $company = new FreelancerCompany(['country' => $input['country']]);
        $company
            ->save();

        return
            $company;
    }

    /**
     * Update employees details
     *
     * @param $freelancerId
     * @param  array $input
     * @return mixed
     */
    public function update($freelancerId, $input)
    {
        $map = array_filter($input, function ($key) {
            return $key !== 'profile';
        });

        $query = Freelancer::find($freelancerId);
        $query->update($map);
        $query->freelancerCompany()->update(['country' => in_array('country', $map) ? $map['country'] : $query->freelancerCompany->country]);

        return $query;
    }

    /**
     * Remove a resources
     *
     * @param  [type] $freelancerId
     * @return [type]
     */
    public function destroy($freelancerId)
    {
        //
    }

    /**
     * @param $email
     * @param  null  $id
     * @return mixed
     */
    public function details($email, $id = null)
    {
        $freelancer = Freelancer::where('email', $email)->with('profile', 'company', 'profile.freelancers_skills')->first();

        return
            $freelancer;
    }

    /**
     * @param array $input
     * @param $fCompany
     * @return Freelancer|mixed
     */
    public function storeFreelancer(array $input, $fCompany)
    {
        $freelancer = new Freelancer($input);
        $freelancer
            ->freelancerCompany()
            ->associate($fCompany)
            ->save();

        return
            $freelancer;
    }

    /**
     * @param  array $input
     * @return mixed
     */
    public function findFreelancer(array $input)
    {
        $query =
            Freelancer::where('email', $input['email'])->with('profile', 'company')->first();

        return
            $query;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findViaId($id)
    {
        $quey =
            Freelancer::where('freelancer_id', $id)
                ->first();

        return
            $quey;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findViaEmail($email)
    {
        $quey =
            Freelancer::where('email', $email)
                ->first();

        return
            $quey;
    }

    /**
     * @param $freelancer
     * @param $input
     * @return FreelancerProfile|mixed
     */
    public function storeProfile($freelancer)
    {
        $profile = new FreelancerProfile();
        $profile
            ->freelancer()
            ->associate($freelancer)
            ->save();

        return $profile;
    }

    /**
     * @param $freelancer
     * @param $input
     * @return mixed
     */
    public function updateProfile($freelancer, $input)
    {
        $freelancer->profile->update($input['profile']);

        return $freelancer->load('profile');
    }

    /**
     * @param $freelancer
     * @param $input
     * @return mixed
     */
    public function storeProfilePhoto($freelancer, $input)
    {
        $freelancer
            ->update(['profile_photo' => $input['profile_photo']]);

        return
            $freelancer;
    }

    /**
     * @param $freelancer
     * @param $input
     * @return mixed
     */
    public function storeFreelancerSkills($freelancer, $input)
    {
        if ($freelancer
            ->skill
            ->contains($input['skill_id'])
        ) {
            $freelancer
                ->skill()
                ->updateExistingPivot($input['skill_id'], $input);
        } else {
            $freelancer
                ->skill()
                ->attach(
                    $input['skill_id'],
                    [
                        'rating' => $input['rating'],
                        'skill_order' => $input['skill_order'],
                    ]
                );
        }

        return $freelancer
            ->skill()
            ->wherePivot('skill_id', $input['skill_id'])
            ->get();
    }

    public function storeFreelancerProfileSkills($profile, $input)
    {
        if ($profile
            ->freelancers_skills
            ->contains($input['skill_id'])
        ) {
            $profile
                ->freelancers_skills()
                ->updateExistingPivot($input['skill_id'], $input);
        } else {
            $profile
                ->freelancers_skills()
                ->attach(
                    $input['skill_id'],
                    [
                        'rating' => $input['rating'],
                        'skill_order' => $input['skill_order'],
                    ]
                );
        }

        return $profile
            ->freelancers_skills()
            ->wherePivot('skill_id', $input['skill_id'])
            ->get();
    }

    /**
     * @param $freelancer
     * @param  array $query
     * @return mixed
     */
    public function freelancerSkills($freelancer, $query = [])
    {
        $skills = $freelancer->skill();

        if (!empty($query)) {
            $sort_params = array_key_exists('sort', $query) ?
                $query['sort'] :
                null;
            if ($sort_params) {
                $orientation = strpos($sort_params, '-') !== false ? 'DESC' : 'ASC';
                $sort = 'pivot_' . str_replace('-', '', $sort_params);
            } else {
                $orientation = 'DESC';
                $sort = 'pivot_rating';
            }

            return $skills
                ->orderBy($sort, $orientation)
                ->get();
        }

        return
            $skills->get();
    }

    public function freelancerProfileSkills($profile, $query = [])
    {
        $skills = $profile
            ->freelancers_skills();

        if (!empty($query)) {
            $sort_params = array_key_exists('sort', $query) ?
                $query['sort'] :
                null;

            if ($sort_params) {
                $orientation = strpos($sort_params, '-') !== false ? 'DESC' : 'ASC';
                $sort = 'pivot_' . str_replace('-', '', $sort_params);
            } else {
                $orientation = 'DESC';
                $sort = 'pivot_rating';
            }

            return $skills
                ->orderBy($sort, $orientation)
                ->get();
        }

        return $skills->get();
    }

    /**
     * @param $freelancer
     * @param $skill_id
     * @return mixed
     */
    public function showFreelancerSkill($freelancer, $skill_id)
    {
        return $freelancer
            ->skill()
            ->where('skill_freelancers.skill_id', $skill_id)
            ->get();
    }

    /**
     * @param $freelancer
     * @param $skill_id
     * @return mixed
     */
    public function deleteFreelancerSkill($freelancer, $skill_id)
    {
        return $freelancer
            ->skill()
            ->detach($skill_id);
    }

    /**
     * @param $freelancer
     * @param $skill_id
     * @return mixed
     */
    public function deleteFreelancerProfileSkill($profile, $skill_id)
    {
        return $profile
            ->freelancers_skills()
            ->detach($skill_id);
    }

    /**
     * @param $freelancer
     * @return mixed
     */
    public function onBoardSkillProposals($freelancer)
    {
        return $this->skill_proposals
            ->with('category')
            ->freelancer($freelancer->freelancer_id)
            ->get();
    }

    /**
     * @param $freelancer
     * @param $input
     * @return mixed|void
     */
    public function onBoardSkillProposalsStore($freelancer, $input)
    {
        $params = [
            'skill_name' => $input['skill_name'],
            'member_id' => $freelancer->freelancer_id,
            'member_type' => SkillProposal::MEMBER_TYPE_FREELANCER,
        ];

        $data = [
            'skill_name' => $input['skill_name'],
            'member_id' => $freelancer->freelancer_id,
            'member_type' => SkillProposal::MEMBER_TYPE_FREELANCER,
            'category_id' => 1,
            'rating' => $input['rating'],
        ];

        if (array_key_exists('skill_proposal_id', $input)) {
            $params['skill_proposal_id'] = $input['skill_proposal_id'];
            $data['skill_proposal_id'] = $input['skill_proposal_id'];
        }

        $proposal = $this->skill_proposals->updateOrCreate($params, $data);

        return $this->skill_proposals
            ->with('category')
            ->freelancer($freelancer->freelancer_id)
            ->where('skill_proposal_id', $proposal->skill_proposal_id)
            ->get();
    }

    /**
     * Create Skill Relation
     *
     * @param  [type] $skillId
     * @param  [type] $relSkillId
     * @return void
     */
    public function createSkillRelation($skillId, $relSkillId)
    {
        $skillsRel =
            $this->skill_relations
                ->where('skill_id', '=', $skillId)
                ->where('related_skill_id', '=', $relSkillId)
                ->first();

        if ($skillsRel !== null) {
            return
                tap($skillsRel)
                    ->update(['count' => $skillsRel->count + 1]);
        }

        return $this->skill_relations
            ->create([
                'skill_id' => $skillId,
                'related_skill_id' => $relSkillId,
                'count' => 1,
            ]);
    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function onBoardSkillProposalsDelete($id)
    {
        return $this->skill_proposals->destroy($id);
    }

    /**
     * Return all resources
     *
     * @return
     */
    public function collection($relationship = [], $fields = [])
    {
        //
    }

    /**
     * Fetch the requested resource
     *
     * @param $freelancerId
     */
    public function show($freelancerId, $fields = [])
    {
        //
    }

    /**
     * Fetch the requested resource
     *
     * @param $freelancerId
     */
    public function find(array $where, $fields = [])
    {
        //
    }

    /**
     * Store employee's details
     *
     * @param  array $input
     * @return mixed
     */
    public function store($input)
    {
        //
    }

    public function sendVerificationEmail($freelancer, $token)
    {
        Mail::to($freelancer->email)
            ->send(new \App\Mail\VerifyEmail($freelancer, $token, 'freelancer'));
    }
}
