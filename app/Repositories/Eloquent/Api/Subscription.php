<?php

namespace App\Repositories\Eloquent\Api;

use App\Repositories\Contracts\Api\SubscriptionInterface;
use App\Repositories\Eloquent\BaseRepository;
use App\Model\Billing\Subscription as SubscriptionModel;
use Carbon\Carbon;

class Subscription extends BaseRepository implements SubscriptionInterface
{
    protected $subscriber;

    public function entity()
    {
        return SubscriptionModel::class;
    }

    /**
     * @param $billingInformation
     * @param $subscription
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function subscribe($subscriber, $subscription)
    {
        $this->subscriber = $subscriber;

        return $this->newSubscription($subscription);
    }

    protected function newSubscription($subscription)
    {
        //params 'subscription yearly', 'plan Gold-yearly|Silver-yearly', 'coupon'
        //TODO: subscription type and plan type
        if ($subscription['subscription_type'] == 'monthly') {
            if ($subscription['plan_type'] == 'Silver') {
                $subscription['coupon'] = $subscription['coupon'] ? $subscription['coupon'] . 'SILVERMONTHLY' : $subscription['coupon'];
                $this
                    ->subscriber
                    ->newSubscription('monthly', 'Silver-monthly', $subscription['coupon'])->create($subscription['token']);
            } elseif ($subscription['plan_type'] == 'Test') {
                $this
                    ->subscriber
                    ->newSubscription('monthly', 'test-plan', $subscription['coupon'])->create($subscription['token']);
            } else {
                $subscription['coupon'] = $subscription['coupon'] ? $subscription['coupon'] . 'GOLDMONTHLY' : $subscription['coupon'];
                $this
                    ->subscriber
                    ->newSubscription('monthly', 'Gold-monthly', $subscription['coupon'])->create($subscription['token']);
            }
        } else {
            if ($subscription['plan_type'] == 'Silver') {
                $subscription['coupon'] = subscription['coupon'] ? $subscription['coupon'] . 'SILVERYEARLY' : subscription['coupon'];
                $this
                    ->subscriber
                    ->newSubscription('yearly', 'Silver-yearly', $subscription['coupon'])->create($subscription['token']);
            } else {
                $subscription['coupon'] = subscription['coupon'] ? $subscription['coupon'] . 'GOLDYEARLY' : subscription['coupon'];
                $this
                    ->subscriber
                    ->newSubscription('yearly', 'Gold-yearly', $subscription['coupon'])->create($subscription['token']);
            }
        }

        return
            $this
                ->subscriber
                ->load('company.billing', 'subscription');
    }

    public function count($filter = array())
    {
        return $this->entity->where($filter)->count();
    }

    public function pause($id)
    {
        return $this->entity->where('braintree_id', $id)->first()->pause();
    }

    public function resume($id)
    {
        return $this->entity->where('braintree_id', $id)->first()->resume();
    }

    public function pauseSubscription($request)
    {
        $this->pause($request['subscription']['id']);
    }

    public function resumeSubscription($request)
    {
        $this->resume($request['subscription']['id']);
    }

    /**
     * Checks if 3 months free subscribers are less than 500
     * @param $employer
     * @return mixed
     */
    public function checkFreeSubscription($employer)
    {
        if ($this->entity->count('braintree_id', '') <= 500) {
            return $employer->subscriptions()->create([
                'member_type' => 'employer',
                'braintree_id' => '',
                'braintree_plan' => 'Silver-monthly',
                'is_active' => 'true',
                'next_billing' => Carbon::now()->addMonths(3),
                'subscription_name' => 'monthly'
            ]);
        }
    }
}
