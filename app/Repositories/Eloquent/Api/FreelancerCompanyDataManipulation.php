<?php

namespace App\Repositories\Eloquent\Api;

use App\Model\Billing\BillingInformation;
use App\Model\FreelancerCompany\FreelancerCompany;
use App\Model\FreelancerCompany\FreelancerCompanyInformation;
use App\Model\Gdpr\Gdpr;
use App\Model\Invite\Invite;
use App\Repositories\Contracts\Api\FreelancerCompanyInterface;

class FreelancerCompanyDataManipulation implements FreelancerCompanyInterface
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findViaId($id)
    {
        $quey =
            FreelancerCompany::where('freelancer_company_id', $id)->first();

        return
            $quey;
    }

    public function findFreelancerCompany($input)
    {
        return
            FreelancerCompany::where('freelancer_company', $input['email'])
                ->first();
    }

    /**
     * @param $input
     * @return FreelancerCompany
     */
    public function store($input)
    {
        $query = new FreelancerCompany($input);
        $query->save();

        return
            $query;
    }

    /**
     * @param $company
     * @return FreelancerCompanyInformation
     */
    public function storeInformation($company)
    {
        $query = new FreelancerCompanyInformation();
        $query
            ->freeLancerCompany()
            ->associate($company)
            ->save();

        return
            $query;
    }

    /**
     * @param $company
     */
    public function storeBilling($company)
    {
        $billing = new BillingInformation();
        $billing
            ->companyable()
            ->associate($company)
            ->save();
    }

    /**
     * @param $company
     * @param $input
     * @return FreelancerCompanyInvite|array|int|string
     */
    public function storeInvitation($company, $input)
    {
        $invite = [];
        foreach ($input as $invite => $inviteVal) {
            $invite = new Invite($inviteVal);
            $invite
                ->inviteable()
                ->associate($company)
                ->save();
        }

        return
            $invite;
    }

    /**
     * @param $company
     * @param $input
     */
    public function storeGdpr($company, $input)
    {
        $gdpr = new Gdpr([
            'source' => 'CitronApp', 'email' => $input['email'], 'policy_version' => 'v2.1.0', 'member_type' => 'employer', 'jurisdiction' => 'ca', 'delete_request' => 'no', 'policy_date' => now(), 'delete_request_date' => now(),
        ]);

        $gdpr
            ->gdprable()
            ->associate($company)
            ->save();
    }

    /**
     * @param $company
     * @param $input
     * @return mixed
     */
    public function updateInformation($company, $input)
    {
        $company
            ->information()
            ->update($input);

        return
            $company;
    }

    public function updateBilling($company, $input)
    {
        $company
            ->billingInformation()
            ->update($input);

        return
            $company;
    }

    /**
     * @param $company
     * @param $input
     * @return mixed
     */
    public function updateLogo($company, $input)
    {
        $company
            ->information()
            ->update(['logo' => $input]);

        return
            $company;
    }

    public function show($param)
    {
        $query = FreelancerCompany::where('freelancer_company_id', $param['freelancer_company_id'])
            ->with('gdpr', 'information', 'billingInformation', 'invite')
            ->first();

        return $query;
    }

    public function edit($id)
    {
        //
    }

    public function update($freelancerCoId, $input)
    {
        $query = FreelancerCompany::find($freelancerCoId);
        $query
            ->update($input);

        return
            $query;
    }

    public function destroy($id)
    {
        //
    }
}
