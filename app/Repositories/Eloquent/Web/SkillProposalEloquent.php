<?php
namespace App\Repositories\Eloquent\Web;

use App\Repositories\Contracts\Web\SkillsProposalInterface;
use App\Repositories\Eloquent\Web\SkillsEloquent;
use App\Model\Skill\SkillProposal;
use App\Model\Skill\SkillFeeelancers;
use App\Model\Freelancer\Freelancer;
use App\Model\Skill\SkillJob;
use Auth;
use Carbon\Carbon;

class SkillProposalEloquent implements SkillsProposalInterface
{
    protected $model;

    protected $skill;

    protected $object;

    public function __construct()
    {
        $this->model = new SkillProposal;
        $this->skill = new SkillsEloquent;
    }

    public function show($skillProposalId, $toObject = 1)
    {
        if (!$toObject) {
            return
                $this->model
                    ->find($skillProposalId);
        }
        
        $this->object =
            $this->model->find($skillProposalId);
        
        return $this;
    }
    /**
     * @return void
     */
    public function delete()
    {
        if (!empty($this->object)) {
            return $this->object->delete();
        }
    }
    /**
     * @param array $data
     * @param [type] $skillProposalId
     * @return void
     */
    public function update(
        $data = [],
        $skillProposalId = null
    ) {
        if (!empty($this->object) && $skillProposalId) {
            return
                $this->object->update($data);
        }
        
        return
            $this->model
                ->find($skillProposalId)
                ->update($data);
    }
    /**
     * @param array $data
     * @param [type] $skillProposalId
     * @return void
     */
    public function updateToSkill(
        $memberType = 'freelancer',
        $data = []
    ) {
        if (!empty($this->object)) {
            $proposedData = [
                'category_id' => $data['category_id'],
                'skill_name' => $data['skill_name'],
                'approved_by' => Auth::guard('web')->user()->id,
                'approved' => '1',
                'approved_date' => Carbon::now()
            ];
            $skillProposal =
                    $this->object->update($proposedData);
            
            $skill =
                $this->skill->create(collect($data)->only([
                    'category_id',
                    'skill_name'
                ])->toArray());
            if ($data['member_type'] === 'freelancer') {
                $profile = Freelancer::find($data['freelancer_id'])->profile()->first();
                SkillFeeelancers::create(array_replace(
                    $profile->only('freelancer_profile_id'),
                    [
                    'skill_id' => $skill->skill_id,
                    'rating' => $data['rating'],
                    'skill_order' => 1
                ]
                ));
            } else {
                SkillJob::create(
                    array_replace(
                        collect($data)->only(['job_id'])->toArray(),
                        [
                            'skill_id' => $skill->skill_id,
                            'skill_order' => 1
                        ]
                    )
                );
            }

            return
                array_replace(
                    $proposedData,
                    ['skill_proposal_id' => $data['skill_proposal_id']
                ]
                );
        }
        return [];
    }

    public function revert($data = [])
    {
        if (!empty($this->object)) {
            $profile = Freelancer::find($data['freelancer_id'])->profile()->first();
            $proposedData = [
                'approved_by' => null,
                'approved' => '0',
                'approved_date' => null
            ];
            $skillProposal =
                    $this->object->update($proposedData);
            $skill = $this->skill->findBySkillName($data['skill_name']);
            SkillFeeelancers::whereSkillId($skill->skill_id)
                ->whereFreelancerProfileId($profile->freelancer_profile_id)
                ->delete();

            $skill->delete();

            return
                array_replace(
                    $proposedData,
                    ['skill_proposal_id' => $data['skill_proposal_id']
                ]
                );
        }
        return [];
    }
}
