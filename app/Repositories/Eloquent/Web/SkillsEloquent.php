<?php

namespace App\Repositories\Eloquent\Web;

use App\Repositories\Contracts\Web\SkillsInterface;
use App\Model\Skill\Skill;
use App\Model\Skill\SkillProposal;
use App\Model\Skill\SkillFeeelancers;

class SkillsEloquent implements SkillsInterface
{
    /**
     * @var Object
     */
    public $obj;

    /**
     * @var Object
     */
    protected $data;

    /**
     * @var Object
     */
    protected $model;

    protected $skillFreelancer;

    /**
     * @param [type] $model
     */
    public function __construct($model = Skill::class)
    {
        $this->model = new $model;
        $this->skillFreelancer = new SkillFeeelancers;
    }

    /**
     * Show skill details
     *
     * @param $id
     * @return mixed
     */
    public function show($skillId, $toObject = false, array $relationships = [])
    {
        if ($toObject) {
            $this->obj = (!empty($relationships))
                ? $this->model->whereSkillId($skillId)->with($relationships)->first()
                : $this->model->find($skillId);

            return $this;
        }

        return $relationships
            ? $this->model->whereSkillId($skillId)->with($relationships)->first()
            : $this->model->find($skillId);
    }

    /**
     * Show skill details
     *
     * @param $id
     * @return mixed
     */
    public function paginate($perpage, $with = [])
    {
        if (!empty($this->obj)) {
            $this->obj =
                $this->obj->with($with)->paginate($perpage);

            return $this->obj
                ?? null;
        }

        return Skill::with($with)->paginate($perpage)
            ?? null;
    }

    /**
     * @param string $search
     * @return $mixed
     */
    protected function foreach($search)
    {
        return function ($q) use ($search) {
            foreach ($search as $s) {
                $q->orWhere('skill_name', 'like', "{$s}%");
            }
        };
    }

    /**
     * @param string $skillName
     * @return mixed
     */
    public function findByName($skillName, $with, $perpage = false)
    {
        return $perpage
                ? Skill::with($with)->where('skill_name', 'like', "{$skillName}%")->paginate($perpage)
                : Skill::with($with)->where('skill_name', 'like', "{$skillName}%")->get();
    }

    /**
     * @param array $input
     * @param int $skillId
     * @return mixed
     */
    public function update($input, $skillId)
    {
        $s = $this->model->find($skillId);

        return
            array_key_exists('category', $input) ?
            tap($s)->update(array_replace($input, ['category_id' => $input['category']['category_id']])) :
            tap($s)->update($input);
    }

    /**
     * @param int $skillId
     * @return mixed
     */
    public function destroy($skill = null)
    {
        if ($this->obj !== null) {
            return $this->obj->delete() ?? null;
        }

        return $skill ? $skill->delete() : null;
    }

    /**
     * Undocumented function
     *
     * @param array $skill
     * @return void
     */
    public function post($skill = [])
    {
        return !empty($skill)
            ? $this->model->create($skill)
            : null;
    }

    public function proposals($toObject = 1)
    {
        if ($toObject) {
            $this->obj = new SkillProposal;

            return $this;
        }
        $request = new SkillProposal();

        return $request;
    }
    
    /**
     * @param array $data
     * @return void
     */
    public function create($data = [])
    {
        return
            $this->model->create($data);
    }

    public function findBySkillName($name)
    {
        return
            $this->model->whereSkillName($name)->first();
    }

    public function in(array $data = [], string $select = '', array $with = [])
    {
        if ($with) {
            if ($select) {
                return $this->model->with($with)->whereIn('skill_name', $data)->pluck($select);
            }
            return $this->model->with($with)->whereIn('skill_name', $data)->get();
        }
        if ($select) {
            return $this->model->with($with)->whereIn('skill_name', $data)->pluck($select);
        }
        return $this->model->whereIn('skill_name', $data)->get();
    }

    public function getFreelancersBySkills(array $skills = [], array $with = [])
    {
        return
            $this->skillFreelancer
                ->distinct()
                ->select('freelancer_profile_id')
                ->whereIn('skill_id', $skills)
                ->with($with)
                    ->get();
    }
}
