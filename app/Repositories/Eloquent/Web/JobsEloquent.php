<?php

namespace App\Repositories\Eloquent\Web;

use App\Model\Job\Job;
use App\Repositories\Contracts\Web\JobsInterface;

class JobsEloquent implements JobsInterface
{
    protected $model;

    /**
     * JobsEloquent constructor.
     * @param string $model
     */
    public function __construct($model = Job::class)
    {
        $this->model = $model;
    }

    /**
     * Show skill details
     *
     * @param       $jobId
     * @param bool  $toObject
     * @param array $relationships
     * @return mixed
     */
    public function show($jobId, bool $toObject, array $relationships)
    {
        // TODO: Implement show() method.
    }

    /**
     * Show skill details
     *
     * @param int   $per_page
     * @param array $with
     * @return mixed
     */
    public function paginate(int $per_page, array $with = [])
    {
        if (!empty($this->obj)) {
            $this->obj =
                $this->obj->paginate($per_page);

            return $this->obj
                ?? null;
        }

        return Job::with($with)->orderByRaw('FIELD(plan_status, "free_unverified", "free_verified", "paid")')->paginate($per_page)
            ?? null;
    }

    /**
     * @param string $jobName
     * @param array  $with
     * @param int    $per_Page
     * @return mixed
     */
    public function findByName($jobName, $perPage = false)
    {
        return $perPage
            ? Job::with('employer')->where('title', 'like', "{$jobName}%")->paginate($perPage)
            : Job::with('employer')->where('title', 'like', "{$jobName}%")->get();
    }

    /**
     * @param array $input
     * @param int   $jobId
     * @return mixed
     */
    public function update($input, $jobId)
    {
        return Job::find($jobId)->update($input);
    }

    /**
     * @param int $job
     * @return mixed
     */
    public function destroy($job)
    {
        // TODO: Implement destroy() method.
    }

    /**
     * @param array $job
     * @return mixed
     */
    public function post(array $job)
    {
        // TODO: Implement post() method.
    }
}
