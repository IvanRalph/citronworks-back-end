<?php

namespace App\Repositories\Eloquent\Web;

use App\Repositories\Eloquent\BaseRepository;
use App\Repositories\Contracts\Web\StatisticsInterface;
use App\Model\Freelancer\Freelancer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class StatisticsFreelancerDataEloquent extends BaseRepository implements StatisticsInterface
{
    protected $class = Freelancer::class;

    protected $object;

    protected static $instance;

    public function entity($model = null)
    {
        return $this->class;
    }

    public function makeModel(Model $class)
    {
        $this->entity = $class;

        return $this;
    }

    public function getLastDate($column, $rangeScope)
    {
        $subs = '';
        switch ($rangeScope) {
            case 'last_30_days':
                $subs = Carbon::now()->subDays(30);

                break;
            case '6_months':
                $subs = Carbon::now()->subMonths(6);

                break;
            case '12_months':
                $subs = Carbon::now()->subYear(1);

                break;
            case '24_hours':
                $subs = Carbon::now()->addHours(24);

                break;
            case '7_days':
                $subs = Carbon::now()->subDays(7);

                break;
            case '30_days':
                $subs = Carbon::now()->subDays(30);

                break;
        }

        return $subs;
    }

    /**
     * Get object from given date
     *
     * @param [type] $column
     * @param [type] $scope
     * @param integer $toObject
     * @return void
     */
    public function showFromDate($column, $scope, $toObject = 0)
    {
        if (!empty($this->object)) {
            $this->object =
                $this->object
                    ->whereDate($column, '>', $this->getLastDate($column, $scope))
                    ->orderBy($column)->get();

            return $this;
        }

        if ($toObject) {
            $this->object =
                $this->entity
                    ->whereDate($column, '>', $this->getLastDate($column, $scope))
                    ->orderBy($column)->get();

            return $this;
        }

        return $this->entity
                    ->whereDate($column, '>', $this->getLastDate($column, $scope))
                        ->orderBy($column)->get();
    }

    /**
     * Group object by given date
     *
     * @param [type] $format
     * @return Collection
     */
    public function groupBy($format) : Collection
    {
        return $this->object = $this->object->groupBy(function ($date) use ($format) {
            return Carbon::parse($date->created_at)->format($format);
        })
            ?? null;
    }

    public function raw($exp)
    {
        $this->object = $this->entity->selectRaw($exp);

        return $this;
    }

    public function filterSource()
    {
        return $this->object = $this->object->groupBy(function ($group) {
            if (isset($group->utm['utm_source'], $group->utm['utm_medium'])) {
                return $group->utm['utm_source'] . '/' . $group->utm['utm_medium'];
            }
        });
    }

    public function filterBy($utmScope = 'utm_term')
    {
        return $this->object = $this->object->groupBy(function ($group) use ($utmScope) {
            if (isset($group->utm[$utmScope])) {
                return $group->utm[$utmScope];
            }
        });
    }

    public function show($freelancerId = null, $toObject = 1)
    {
        if (!$freelancerId) {
            if ($toObject) {
                $this->object = $this->entity;

                return $this;
            }
        }
    }

    public function count()
    {
        return $this->object->count();
    }
}
