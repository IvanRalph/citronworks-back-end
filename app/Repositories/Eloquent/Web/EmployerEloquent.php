<?php
namespace App\Repositories\Eloquent\Web;

use App\Repositories\Contracts\Web\EmployerInterface;
use  App\Model\Employer\Employer;
use Illuminate\Database\Eloquent\Model;

class EmployerEloquent implements EmployerInterface
{
    /**
     * Object handler
     */
    protected Model $model;

    public function __construct()
    {
        $this->model = new Employer;
    }
    
    /**
     * Show employer
     *
     * @param [type] $paged
     * @return void
     */
    public function paginate($paged = null)
    {
        return $this->model->paginate($paged);
    }

    public function show($data)
    {
        $searchv = preg_split('/\s+/', $data, -1, PREG_SPLIT_NO_EMPTY);

        return $this->model->where(function ($q) use ($searchv) {
            foreach ($searchv as $key => $value) {
                $q->orWhere('first_name', 'like', "%{$value}%")
                    ->orWhere('last_name', 'like', "%{$value}%")
                    ->orWhere('email', 'like', "%{$value}%");
            }
        })->get();
    }
}
