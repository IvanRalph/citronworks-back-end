<?php

namespace App\Repositories\Eloquent\Web;

use App\Repositories\Contracts\Web\CategoryInterface;
use App\Model\Category\Category;
use Illuminate\Database\Eloquent\Collection;

class CategoryEloquent implements CategoryInterface
{
    protected $model;

    public $obj;

    public function __construct($model = Category::class)
    {
        $this->model = new Category;
    }

    /**
     * @param string $query
     * @return mixed
     */
    public function search($query)
    {
        //
    }

    /**
     * Categories collection
     */
    public function collection(array $relationships, $limit)
    {
        //
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function store(array $input)
    {
        return $this->model->create($input)
            ?? null;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show($categoryId, $toObject = false, array $relationships = [])
    {
        if ($toObject) {
            $this->obj = (!empty($relationships))
                ? $this->model->whereCategoryId($categoryId)->with($relationships)->first()
                : $this->model->find($categoryId);

            return $this;
        }

        return $relationships
            ? $this->model->whereCategoryId($categoryId)->with($relationships)->first()
            : $this->model->find($categoryId);
    }

    /**
     * @return App\Model\Category\Category;
     */
    public function all() : Collection
    {
        return $this->model->all()
            ?? null;
    }

    /**
     * @param $input
     * @param $categoryId
     * @return mixed
     */
    public function update($input, $categoryId)
    {
        $s = $this->model->find($categoryId);

        return
            array_key_exists('category', $input) ?
            tap($s)->update(array_replace($input, ['category_id' => $input['category']['category_id']])) :
            tap($s)->update($input);
    }

    /**
     * @param $categoryId
     * @return mixed
     */
    public function destroy($category = null)
    {
        if ($this->obj !== null) {
            return $this->obj->delete() ?? null;
        }

        return $category ? $category->delete() : null;
    }

    public function paginate($perpage = 10, string $orderby = 'category_id', $order = 'DESC')
    {
        return $this->model->orderBy($orderby, $order)->paginate($perpage) ?? null;
    }
}
