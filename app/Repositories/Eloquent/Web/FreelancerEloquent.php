<?php

namespace App\Repositories\Eloquent\Web;

use Illuminate\Http\Request;
use App\Repositories\Contracts\Web\FreelancerInterface;
use App\Model\Freelancer\Freelancer;
use App\Model\Freelancer\FreelancerCompany;
use App\Model\Skill\SkillFeeelancers;
use App\Libraries\CsvWriter;

class FreelancerEloquent implements FreelancerInterface
{
    protected $model;

    public $obj;

    protected CsvWriter $writer;
    protected static array $writer_header = [
        'ID',
        'Email',
        'First Name',
        'Last Name',
        'Matched Skills'
    ];

    public function __construct($model = Freelancer::class)
    {
        $this->model = new Freelancer;
        $this->writer = new CsvWriter('freelancers-data');
    }

    /**
     * Employers list of collection
     */
    public function collection()
    {
        //
    }

    /**
     * Store employee's details
     *
     * @param array $input
     * @return mixed
     */
    public function store(array $input)
    {
        //
    }

    /**
     * Show employees details
     *
     * @param $id
     * @return mixed
     */
    public function details($email, $id = null)
    {
        //
    }

    /**
     * Update employees details
     *
     * @param $freelancerId
     * @param array $input
     * @return mixed
     */
    public function update(array $input = [], $freelancerId = null)
    {
        $s = $this->model->find($freelancerId);

        return
            $this->obj !== null ?
                $this->obj->update($input) :
                tap($s)->update($input);
    }

    /**
     * Undocumented function
     *
     * @param [type] $skill
     * @param [type] $toObject
     * @param [type] $relationship
     * @return void
     */
    public function show($freelancerId, $toObject = false, $relationships = [])
    {
        if ($toObject) {
            $this->obj = (!empty($relationships))
                ? $this->model->whereFreelancerId($freelancerId)->with($relationships)->first()
                : $this->model->find($freelancerId);

            return $this;
        }

        return $relationships
            ? $this->model->whereFreelancerId($freelancerId)->with($relationships)->first()
            : $this->model->find($freelancerId);
    }

    public function showProfile($freelancerprofilId)
    {
        return
            $this->model
                ->profile()
                ->whereFreelancerProfileId($freelancerprofilId)
                ->first();
    }

    /**
     * Destroy employees details
     *
     * @param $id
     * @return mixed
     */
    public function destroy($freelnaceData = null)
    {
        if ($this->obj !== null) {
            return $this->obj->delete() ?? null;
        }

        return $freelnaceData ? $freelnaceData->delete() : null;
    }

    /**
     * @param array $input
     * @param array $fCompany
     * @return mixed
     */
    public function storeFreelancer(array $input, $toObject = true)
    {
        if ($toObject) {
            $this->obj = $this->model->create($input);

            return $this;
        }

        return $this->model->create($input) ?? null;
    }

    /**
     * @param $freelancer
     * @param $input
     * @return mixed
     */
    public function storeProfile($freelancer, $input)
    {
        if (isset($input['skills'])) {
            unset($input['skills']);
        }

        if ($this->obj !== null) {
            return $this->obj->profile()->create($input)
                ?? null;
        }
        if (gettype($freelancer) === 'object') {
            return tap($freelancer)->profile()->create($input)
                ?? null;
        }
        /** If freelnacer Id */
        return $this->model->find($freelancer)
                ->profile()
                ->create($input)
                    ?? null;
    }

    /**
    * @param $freelancer
    * @param $input
    * @return mixed
    */
    public function storeSkill($input, $freelancer = 0)
    {
        if ($this->obj !== null) {
            foreach ($input as $data) {
                $u = $this->obj->skill()->attach(collect($data)->only('skill_id'));
                tap($this->obj->skill())->updateExistingPivot($data['skill_id'], ['rating' => $data['pivot']['rating']]);
            }

            return $u ?? null;
        }
        if (gettype($freelancer) === 'object') {
            foreach ($input as $data) {
                $u = tap($freelancer)->skill()->attach(collect($data)->only('skill_id'));
                tap($this->obj->skill())->updateExistingPivot($data['skill_id'], ['rating' => $data['pivot']['rating']]);
            }

            return $u ?? null;
        }
        /** If freelnacer Id */
        foreach ($input as $data) {
            $u = $this->model->find($freelancer)
                ->skill()
                ->attach(collect($data)->only('skill_id'))
                    ?? null;
            tap($this->obj->skill())->updateExistingPivot($data['skill_id'], ['rating' => $data['pivot']['rating']]);
        }

        return $u
                ?? null;
    }

    public function storeProfileSkill($freelancerProfileId, $skill)
    {
        return
            SkillFeeelancers::create([
                'freelancer_profile_id' => $freelancerProfileId,
                'skill_id' => $skill['skill']['skill_id'],
                'rating' => $skill['rating'],
            ]);
    }

    public function updateProfileSkill($skillFreelancerId, $skill)
    {
        return
            SkillFeeelancers::whereSkillFreelancerId($skillFreelancerId)
                ->update(collect($skill)
                ->only(['rating'])->all());
    }

    public function removeProfileSkill($skillFreelancerId)
    {
        return
            SkillFeeelancers::whereSkillFreelancerId($skillFreelancerId)
                ->delete();
    }

    /**
     * @param $freelancer
     * @param $input
     * @return mixed
     */
    public function storeProfilePhoto($freelancer, $input)
    {
        //
    }

    /**
     * @param $freelancer
     * @param $input
     * @return mixed
     */
    public function updateCompany($freelancer, $input)
    {
        //
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function findFreelancer(Request $request, int $per_page = null, $relationships = [])
    {
        $search = explode(' ', $request->name);

        return Freelancer::where(function ($q) use ($search) {
            foreach ($search as $s) {
                $q
                    ->where('freelancer_id', 'like', "{$s}%")
                    ->orWhere('first_name', 'like', "{$s}%")
                    ->orWhere('last_name', 'like', "{$s}%")
                    ->orWhere('email', 'like', "{$s}%");
            }
        })
            ->with($relationships)
            ->paginate($per_page)
            ?? null;
    }

    /**
     * @param $freelancer
     * @param $skill_id
     * @return mixed
     */
    public function deleteFreelancerSkill($freelancer, $skill_id)
    {
        //
    }

    /**
     * @param int $per_page
     * @return mixed
     */
    public function paginate($per_page, $relationships = [])
    {
        return Freelancer::with($relationships)->paginate($per_page)
            ?? null;
    }

    public function entity()
    {
        return Freelancer::class;
    }

    /**
     * @param [type] $companyName
     * @return void
     */
    public function findCompany($companyName)
    {
        return FreelancerCompany::where('company_name', 'like', "{$companyName}%")->get()
            ?? null;
    }

    public function storeFreelancerCompany($input)
    {
        return FreelancerCompany::create(array_replace($input, [
            'country' => $input['country'],
        ]));
    }

    /**
     * @param array $input
     * @param $fCompany
     * @return Freelancer|mixed
     */
    public function storeFreelancerData(array $input, $fCompany)
    {
        $this->obj = new Freelancer($input);
        $this->obj
            ->freelancerCompany()
            ->associate($fCompany)
            ->save();

        return
            $this;
    }

    public function showAll($class)
    {
        return $class::all() ?? null;
    }

    public function write($data, $payload = null)
    {
        if ($payload) {
            $this->writer->addHeadColumn($payload);
            $this->writer->addRow('<br />');
        }

        $this->writer->addHeadColumn(self::$writer_header);

        $data->each(fn ($d) => $this->writer->addRow($d));

        $this->writer->close();
    }
}
