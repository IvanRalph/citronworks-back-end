<?php

use Illuminate\Database\Seeder;
use App\Model\Freelancer\FreelancerCompany;
use App\Model\Freelancer\FreelancerProfile;
use App\Model\Skill\SkillFreelancer;
use App\Model\Freelancer\Freelancer;

class FreelancerCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(FreelancerCompany::class, 500)
            ->create()
            ->each(function ($company) {
                $profile = $company->freelancer()->save(factory(Freelancer::class)->make());
                $profile->profile()->save(factory(FreelancerProfile::class)->make());
                $profile->skill()->sync(factory(SkillFreelancer::class, 10)->make());
            });
    }
}
