<?php

use App\Model\Employer\Companies;
use App\Model\Employer\Employer;
use App\Model\Billing\BillingInformation;
use App\Model\Billing\Subscription;
use Illuminate\Database\Seeder;

class EmployerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Companies::class, 500)
            ->create()
            ->each(function ($company) {
                $employer = $company->employer()->save(factory(Employer::class)->make());
                $company->billing()->save(factory(BillingInformation::class)->make());

                // subscribe the first 3 employers
                if ($employer->employer_id <= 3) {
                    $employer->subscriptions()->save(factory(Subscription::class)->make());
                }
            });
    }
}
