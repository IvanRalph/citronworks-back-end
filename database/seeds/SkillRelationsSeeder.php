<?php

use Illuminate\Database\Seeder;
use App\Model\Category\Category;
use App\Model\Skill\Skill;
use App\Model\SkillRelation\SkillRelation;

class SkillRelationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Category::orderBy('category_id')
            ->get()
            ->each(function ($category) {
                // dd($category);
                $skills = Skill::where('category_id', $category->category_id)
                        ->get()
                        ->toArray();

                foreach ($skills as $skill) {
                    $related_skills = Skill::where('skill_id', '!=', $skill['skill_id'])
                        ->take(100)
                        ->select('skill_id')
                        ->get()
                        ->toArray();

                    foreach ($related_skills as $related_skill) {
                        $data = [];
                        $data[] = [
                            'skill_id' => $skill['skill_id'],
                            'related_skill_id' => $related_skill['skill_id'],
                            'count' => 1,
                        ];
                        SkillRelation::insert($data);
                    }
                }
            });
    }
}
