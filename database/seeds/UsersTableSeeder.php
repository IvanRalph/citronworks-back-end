<?php

use Illuminate\Database\Seeder;
use App\Model\Admin\User;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (count($this->users())) {
            foreach ($this->users() as $u) {
                User::create($u);
            }
        }
    }

    public function users()
    {
        return [
            ['name' => 'Nick Brandt', 'email' => 'nick@citronworks.com', 'email_verified_at' => Carbon::now(), 'password' => 'mhDEs3#Bw6$yLR_t'],
            ['name' => 'Leo Gestetner', 'email' => 'leo@citronworks.com', 'email_verified_at' => Carbon::now(), 'password' => 'cd!u$D3S#&#2HeW&'],
        ];
    }
}
