<?php

use Illuminate\Database\Seeder;
use App\Model\Admin\User;
use Carbon\Carbon;

class UserAdminSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Aiza P.',
            'email' => 'aiza.p@zenvoy.com',
            'email_verified_at' => Carbon::now(),
            'password' => 'mSAS=Lm8#%Rud6FF',
        ]);
    }
}
