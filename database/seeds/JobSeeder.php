<?php

use App\Model\Job\Job;
use App\Model\Skill\SkillJob;
use Illuminate\Database\Seeder;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Job::class, 50)->create()
            ->each(function ($job) {
                $job->skill()->sync(factory(SkillJob::class, 10)->make());
            });

        // make some jobs verified
        Job::where('plan_status', 'free_unverified')
            ->limit(20)
            ->get()
            ->each(function ($job) {
                $this->command->getOutput()->writeLn('Job ID: ' . $job->job_id . ' is now verified. Employer: ' . $job->employer->email);
                $job->update([
                    'plan_status' => 'free_verified',
                ]);
            });
    }
}
