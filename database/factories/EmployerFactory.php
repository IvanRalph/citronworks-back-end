<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Model\Employer\Employer;
use Faker\Generator as Faker;

$factory->define(Employer::class, function (Faker $faker) {
    return [
        'role' => $faker->randomElement(['owner', 'admin', 'read_only']),
        'email' => $faker->unique()->safeEmail,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'password' => 'password', // password
    ];
});
