<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Model\Freelancer\FreelancerProfile;
use Faker\Generator as Faker;

$factory->define(FreelancerProfile::class, function (Faker $faker) {
    return [
        'freelancer_type' => (string) $faker->randomElement(['person', 'persona']),
        'title' => $faker->text($maxNbChars = 80),
        'intro' => $faker->text($maxNbChars = 130),
        'description' => $faker->text($maxNbChars = 2000),
        'full_time_price' => $faker->randomNumber(4),
        'part_time_price' => $faker->randomNumber(3),
        'hourly_price' => $faker->randomNumber(2),
        'agreed_price' => $faker->randomFloat(2, 500, 10000),
        'full_time_ava' => $faker->randomElement([true, false]),
        'part_time_ava' => $faker->randomElement([true, false]),
        'hourly_ava' => $faker->randomElement([true, false]),
    ];
});
