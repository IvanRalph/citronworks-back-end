<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Model\Freelancer\FreelancerCompany;
use Faker\Generator as Faker;

$factory->define(FreelancerCompany::class, function (Faker $faker) {
    return [
        'country' => $faker->countryCode,
        'intro' => $faker->text($maxNbChars = 255),
        'description' => $faker->text($maxNbChars = 500),
        'address_1' => $faker->address,
        'address_2' => $faker->address,
        'zip' => $faker->randomNumber(4),
        'url' => $faker->url,
        'state' => $faker->state,
    ];
});
