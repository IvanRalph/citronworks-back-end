<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Model\Employer\Companies;
use Faker\Generator as Faker;

$factory->define(Companies::class, function (Faker $faker) {
    return [
        'company_name' => $faker->company,
        'address_1' => $faker->address,
        'address_2' => $faker->address,
        'zip' => $faker->randomNumber(4),
        'state' => $faker->state,
        'country' => $faker->countryCode,
        // 'logo' => 'default.jpg',
        'intro' => $faker->text(200),
        'description' => $faker->imageUrl(250, 250),
        'company_url' => $faker->url,
    ];
});
