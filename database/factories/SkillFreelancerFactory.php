<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Model\Skill\SkillFreelancer;
use Faker\Generator as Faker;

$factory->define(SkillFreelancer::class, function (Faker $faker) {
    return [
        'skill_id' => $faker->numberBetween(1, 3162),
    ];
});
