<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Model\Freelancer\Freelancer;
use Faker\Generator as Faker;

$factory->define(Freelancer::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => 'password',
        'role' => 'freelancer', // password
        'profile_photo' => $faker->imageUrl(250, 250),
        'ip_address' => $faker->ipv4,
        'about_us' => $faker->randomElement(['linkedin', 'facebook', 'google']),
    ];
});
