<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Model\Job\Job;
use Faker\Generator as Faker;

$factory->define(Job::class, function (Faker $faker) {
    return [
        'employer_id' => $faker->randomElement([1, 2, 3, 4, 5]),
        'title' => $faker->text($maxNbChars = 30),
        'intro' => $faker->text($maxNbChars = 200),
        'description' => $faker->text($maxNbChars = 500),
        'country_limit' => json_encode(['limit' => null]),
        'job_type' => $faker->randomElement(['full-time', 'part-time', 'hourly']),
        'price_min' => $faker->randomFloat($nbMaxDecimals = null, $min = 0, $max = 1000),
        'price_max' => $faker->randomFloat($nbMaxDecimals = null, $min = 1000, $max = 5000),
        'plan_status' => $faker->randomElement(['paid', 'free_unverified', 'free_verified']),
    ];
});
