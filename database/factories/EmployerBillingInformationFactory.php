<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Model\Billing\BillingInformation;
use Faker\Generator as Faker;

$factory->define(BillingInformation::class, function (Faker $faker) {
    return [
        'address_1' => $faker->address,
        'address_2' => $faker->address,
        'zip' => $faker->randomNumber(4),
        'state' => $faker->state,
        'country' => $faker->countryCode,
    ];
});
