<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Model\Billing\Subscription;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Subscription::class, function (Faker $faker) {
    return [
        'member_type' => 'employer',
        'braintree_id' => Str::random(6),
        'braintree_plan' => 'Silver-monthly',
        'next_billing' => now()->addMonth(),
        'billing_interval' => now(),
        'subscription_name' => 'monthly',
        'is_active' => true,
    ];
});
