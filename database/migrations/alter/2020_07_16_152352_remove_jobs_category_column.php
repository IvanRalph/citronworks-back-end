<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RemoveJobsCategoryColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // workaround because changing enums is not supported by doctrine/dbal
        DB::unprepared('ALTER TABLE `jobs` MODIFY COLUMN `job_type` enum("project", "full-time", "part-time", "hourly") NOT NULL');
        Schema::table('jobs', function (Blueprint $table) {
            $table->dropColumn(['category', 'skills']);
        });

        Schema::table('skill_jobs', function (Blueprint $table) {
            $table->dropColumn('skill_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // workaround because changing enums is not supported by doctrine/dbal
        DB::unprepared('ALTER TABLE `jobs` MODIFY COLUMN `job_type` enum("project", "full-time", "part-time") NOT NULL');
        Schema::table('jobs', function (Blueprint $table) {
            $table->string('category')->collation('utf8mb4_unicode_ci');
            $table->string('skills')->collation('utf8mb4_unicode_ci');
        });

        Schema::table('skill_jobs', function (Blueprint $table) {
            $table->integer('skill_order');
        });
    }
}
