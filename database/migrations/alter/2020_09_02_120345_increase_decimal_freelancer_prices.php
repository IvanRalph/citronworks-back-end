<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IncreaseDecimalFreelancerPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // workaround as per: https://github.com/doctrine/dbal/issues/3161#issuecomment-542814085
        DB::connection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::table('freelancer_profile', function (Blueprint $table) {
            $table->decimal('full_time_price', 9, 2)->collation('utf8_general_ci')->nullable()->change();
            $table->decimal('part_time_price', 9, 2)->collation('utf8_general_ci')->nullable()->change();
            $table->decimal('hourly_price', 9, 2)->collation('utf8_general_ci')->nullable()->change();
            $table->decimal('agreed_price', 9, 2)->collation('utf8_general_ci')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // workaround as per: https://github.com/doctrine/dbal/issues/3161#issuecomment-542814085
        DB::connection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::table('freelancer_profile', function (Blueprint $table) {
            $table->decimal('full_time_price', 6, 2)->collation('utf8_general_ci')->nullable()->change();
            $table->decimal('part_time_price', 6, 2)->collation('utf8_general_ci')->nullable()->change();
            $table->decimal('hourly_price', 6, 2)->collation('utf8_general_ci')->nullable()->change();
            $table->decimal('agreed_price', 6, 2)->collation('utf8_general_ci')->nullable()->change();
        });
    }
}
