<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IncreaseJobPricingLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // workaround as per: https://github.com/doctrine/dbal/issues/3161#issuecomment-542814085
        DB::connection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::table('jobs', function (Blueprint $table) {
            $table->decimal('price_min', 9, 2)->collation('utf8_general_ci')->nullable()->change();
            $table->decimal('price_max', 9, 2)->collation('utf8_general_ci')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // workaround as per: https://github.com/doctrine/dbal/issues/3161#issuecomment-542814085
        DB::connection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::table('jobs', function (Blueprint $table) {
            $table->decimal('price_min', 6, 2)->collation('utf8_general_ci')->nullable()->change();
            $table->decimal('price_max', 6, 2)->collation('utf8_general_ci')->nullable()->change();
        });
    }
}
