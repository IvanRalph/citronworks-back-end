<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSkillsProposalsAddJobId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('skill_proposals', function (Blueprint $table) {
            $table->bigInteger('job_id')->nullable()->unsigned()->after('member_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('skill_proposals', function (Blueprint $table) {
            $table->dropColumn('job_id');
        });
    }
}
