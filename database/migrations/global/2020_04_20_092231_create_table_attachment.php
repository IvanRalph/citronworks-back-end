<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAttachment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->bigIncrements('attached_id');
            $table->enum('type', ['gif', 'pdf', 'jpg', 'png']);
            $table->string('size')->collation('utf8mb4_unicode_ci');
            $table->string('label')->collation('utf8mb4_unicode_ci');
            $table->string('mime', 20)->collation('utf8mb4_unicode_ci');
            $table->string('extension', 20)->collation('utf8mb4_unicode_ci');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
    }
}
