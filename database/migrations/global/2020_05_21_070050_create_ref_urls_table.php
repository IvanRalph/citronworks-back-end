<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_urls', function (Blueprint $table) {
            $table->bigIncrements('ref_url_id');
            $table->bigInteger('member_id')->unsigned()->nullable();
            $table->enum('member_type', ['freelancer', 'employer'])->nullable();
            $table->string('ref_url', 256);
            $table->ipAddress('ip_address', 24);
            $table->string('landing_url', 200);
            $table->json('utm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_urls');
    }
}
