<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReturnVisitToRefUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_urls', function (Blueprint $table) {
            $table->bigInteger('return_visit')->nullable()->after('utm');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_urls', function (Blueprint $table) {
            $table->dropIfExists('return_visit');
        });
    }
}
