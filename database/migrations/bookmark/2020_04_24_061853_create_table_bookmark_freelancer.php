<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableBookmarkFreelancer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookmark_freelancers', function (Blueprint $table) {
            $table->bigIncrements('bookmark_id_freelancer');

            $table->bigInteger('member_id')
                ->unsigned();
            $table->foreign('member_id')
                ->references('employer_id')
                ->on('employers');

            $table->bigInteger('freelancer_id')
                ->unsigned();
            $table->foreign('freelancer_id')
                ->references('freelancer_id')
                ->on('freelancers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookmark_freelancers');
    }
}
