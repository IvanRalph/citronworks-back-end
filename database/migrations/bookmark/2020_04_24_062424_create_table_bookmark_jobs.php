<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableBookmarkJobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookmark_jobs', function (Blueprint $table) {
            $table->bigIncrements('bookmark_id_job');
            $table->bigInteger('member_id')
                ->unsigned();
            $table->foreign('member_id')
                ->references('freelancer_id')
                ->on('freelancers')
                ->onDelete('cascade');

            $table->bigInteger('job_id')
                ->unsigned();
            $table->foreign('job_id')
                ->references('job_id')
                ->on('jobs')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookmark_jobs');
    }
}
