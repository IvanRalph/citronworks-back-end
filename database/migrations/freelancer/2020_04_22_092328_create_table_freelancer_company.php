<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFreelancerCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freelancer_company', function (Blueprint $table) {
            $table->bigIncrements('freelancer_company_id');
            $table->string('company_name', 255)->collation('utf8mb4_unicode_ci');
            $table->string('address_1', 255)->collation('utf8mb4_unicode_ci');
            $table->string('address_2', 255)->collation('utf8mb4_unicode_ci');
            $table->string('zip', 15)->collation('utf8mb4_unicode_ci');
            $table->string('state', 255)->collation('utf8mb4_unicode_ci');
            $table->string('country', 2)->collation('utf8_general_ci');
            $table->string('logo', 100)->collation('utf8_general_ci');
            $table->string('title', 255)->collation('utf8mb4_unicode_ci');
            $table->text('intro')->collation('utf8mb4_unicode_ci');
            $table->text('description')->collation('utf8mb4_unicode_ci');
            $table->string('url', 255)->collation('utf8mb4_unicode_ci');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_freelancer_company');
    }
}
