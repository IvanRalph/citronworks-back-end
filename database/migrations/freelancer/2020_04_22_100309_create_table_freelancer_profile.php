<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFreelancerProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freelancer_profile', function (Blueprint $table) {
            $table->bigIncrements('freelancer_profile_id');
            $table->bigInteger('freelancer_id')->unsigned();
            $table->foreign('freelancer_id')
                ->references('freelancer_id')
                ->on('freelancers')
                ->onDelete('cascade');
            $table->enum('freelancer_type', ['persona', 'person'])->collation('utf8_general_ci');
            $table->string('title', 255)->collation('utf8_general_ci');
            $table->text('intro')->collation('utf8mb4_unicode_ci');
            $table->longText('description')->collation('utf8mb4_unicode_ci');
            $table->integer('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freelancer_profile');
    }
}
