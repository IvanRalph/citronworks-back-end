<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFreelancer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freelancers', function (Blueprint $table) {
            $table->bigIncrements('freelancer_id');
            $table->bigInteger('freelancer_company_id')->unsigned();
            $table->foreign('freelancer_company_id')
                ->references('freelancer_company_id')
                ->on('freelancer_company')
                ->onDelete('cascade');
            $table->string('first_name', 255)->collation('utf8mb4_unicode_ci');
            $table->string('last_name', 255)->collation('utf8mb4_unicode_ci');
            $table->string('email', 255)->collation('utf8_general_ci');
            $table->string('password')->collation('utf8mb4_unicode_ci');
            $table->enum('role', ['owner', 'admin', 'freelancer'])->collation('utf8_general_ci');
            $table->string('profile_photo', 100)->collation('utf8_general_ci');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freelancers');
    }
}
