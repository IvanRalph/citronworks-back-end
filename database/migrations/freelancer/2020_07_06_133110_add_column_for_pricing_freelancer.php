<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnForPricingFreelancer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('freelancer_profile', function (Blueprint $table) {
            $table->decimal('full_time_price', 6, 2)->collation('utf8_general_ci')->nullable()->default(0);
            $table->decimal('part_time_price', 6, 2)->collation('utf8_general_ci')->nullable()->default(0);
            $table->decimal('hourly_price', 6, 2)->collation('utf8_general_ci')->nullable()->default(0);
            $table->decimal('agreed_price', 6, 2)->collation('utf8_general_ci')->nullable()->default(0);
            $table->boolean('full_time_ava')->collation('utf8_general_ci')->nullable()->default(0);
            $table->boolean('part_time_ava')->collation('utf8_general_ci')->nullable()->default(0);
            $table->boolean('hourly_ava')->collation('utf8_general_ci')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('freelancer_profile', function (Blueprint $table) {
            //
        });
    }
}
