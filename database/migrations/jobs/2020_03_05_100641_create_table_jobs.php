<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableJobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('job_id');
            $table->bigInteger('employer_id')->unsigned();
            $table->foreign('employer_id')
                ->references('employer_id')
                ->on('employers')
                ->onDelete('cascade');
            $table->string('title')->collation('utf8_general_ci');
            $table->text('intro')->collation('utf8mb4_unicode_ci');
            $table->longText('description')->collation('utf8mb4_unicode_ci');
            $table->string('category')->collation('utf8mb4_unicode_ci');
            $table->string('skills')->collation('utf8mb4_unicode_ci');
//            $table->unsignedBigInteger('category_id');
//            $table->json('skill_ids');
            $table->json('country_limit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
