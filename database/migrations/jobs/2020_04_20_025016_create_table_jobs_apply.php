<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableJobsApply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->bigIncrements('application_id');
            $table->bigInteger('freelancer_id')->unsigned();
            $table->foreign('freelancer_id')
                ->references('freelancer_id')
                ->on('freelancers')
                ->onDelete('cascade');
            $table->bigInteger('job_id')->unsigned();
            $table->foreign('job_id')
                ->references('job_id')
                ->on('jobs')
                ->onDelete('cascade');
            $table->string('subject')->collation('utf8mb4_unicode_ci');
            $table->text('message')->collation('utf8mb4_unicode_ci');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
