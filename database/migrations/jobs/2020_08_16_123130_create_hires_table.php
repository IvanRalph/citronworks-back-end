<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHiresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hires', function (Blueprint $table) {
            $table->bigIncrements('hire_id');
            $table->bigInteger('company_id')->unsigned();
            $table->foreign('company_id')
                ->references('company_id')
                ->on('companies');
            $table->bigInteger('employer_id')->unsigned();
            $table->foreign('employer_id')
                ->references('employer_id')
                ->on('employers');
            $table->bigInteger('freelancer_id')->unsigned();
            $table->foreign('freelancer_id')
                ->references('freelancer_id')
                ->on('freelancers');
            $table->enum('hire_type', ['full-time', 'part-time', 'hourly'])->nullable();
            $table->dateTime('end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hires');
    }
}
