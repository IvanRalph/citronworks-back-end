<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_invites', function (Blueprint $table) {
            $table->bigIncrements('job_invite_id');
            $table->bigInteger('company_id')->unsigned();
            $table->foreign('company_id')
                ->references('company_id')
                ->on('companies');
            $table->bigInteger('employer_id')->unsigned();
            $table->foreign('employer_id')
                ->references('employer_id')
                ->on('employers');
            $table->bigInteger('freelancer_id')->unsigned();
            $table->foreign('freelancer_id')
                ->references('freelancer_id')
                ->on('freelancers');
            $table->bigInteger('job_id')->unsigned();
            $table->foreign('job_id')
                ->references('job_id')
                ->on('jobs');
            $table->text('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_invites');
    }
}
