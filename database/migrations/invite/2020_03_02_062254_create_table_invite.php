<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableInvite extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('invite')->create('invite', function (Blueprint $table) {
            $table->bigIncrements('invite_id');
            $table->bigInteger('affiliate_id')->nullable();
            $table->string('source', 128)->collation('utf8_general_ci')->nullable();
            $table->string('first_name', 255)->collation('utf8mb4_unicode_ci');
            $table->string('last_name', 255)->collation('utf8mb4_unicode_ci');
            $table->string('email', 255)->collation('utf8_general_ci');
            $table->enum('member_category', ['employer', 'freelancer_company', 'no_value'])->collation('utf8_general_ci');
            $table->string('company_name', 255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('company_url', 255)->collation('utf8_general_ci')->nullable();
            $table->string('phone', 30)->collation('utf8_general_ci')->nullable();
            $table->dateTime('created_at')->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invite');
    }
}
