<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployerCitronworkersRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citronworkers_requests', function (Blueprint $table) {
            $table->bigIncrements('citronworkers_request_id');
            $table->string('first_name', 255)->collation('utf8mb4_unicode_ci');
            $table->string('last_name', 255)->collation('utf8mb4_unicode_ci');
            $table->string('email', 255)->collation('utf8_general_ci');
            $table->string('company', 255)->collation('utf8_general_ci');
            $table->string('company_website', 255)->collation('utf8_general_ci');
            $table->string('phone', 255)->collation('utf8_general_ci');
            $table->string('referer', 255)->collation('utf8mb4_unicode_ci');
            $table->text('message')->collation('utf8mb4_unicode_ci');
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4 ';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citronworkers_requests');
    }
}
