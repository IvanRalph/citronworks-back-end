<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableBillingInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_information', function (Blueprint $table) {
            $table->bigIncrements('billing_information_id');
            $table->bigInteger('company_id')->unsigned();
            $table->foreign('company_id')
                ->references('company_id')
                ->on('companies')
                ->onDelete('cascade');
            $table->enum('company_type', ['freelancer', 'employer'])->collation('utf8_general_ci');
            $table->string('address_1', 255)->collation('utf8_general_ci');
            $table->string('address_2', 255)->collation('utf8_general_ci');
            $table->string('zip', 15)->collation('utf8mb4_unicode_ci');
            $table->string('state', 255)->collation('utf8mb4_unicode_ci');
            $table->string('country', 100)->collation('utf8_general_ci');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_information');
    }
}
