<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkillProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill_proposals', function (Blueprint $table) {
            $table->bigIncrements('skill_proposal_id');
            $table->string('skill_name', 255)->collation('utf8_general_ci');
            $table->integer('member_id')->nullable();
            $table->enum('member_type', ['freelancer', 'employer'])->nullable();
            $table->integer('approved_by')->nullable();
            $table->enum('approved', ['0', '1'])->nullable();
            $table->unsignedBigInteger('category_id');
            $table->dateTime('proposal_date')->nullable();
            $table->dateTime('approved_date')->nullable();
            $table->integer('rating')->nullable();

            $table->foreign('category_id')
                ->references('category_id')
                ->on('categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skill_proposals');
    }
}
