<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkillRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill_relations', function (Blueprint $table) {
            $table->bigIncrements('skill_relation_id');
            $table->unsignedBigInteger('skill_id');
            $table->unsignedBigInteger('related_skill_id');
            $table->integer('count');

            $table->foreign('skill_id')
                ->references('skill_id')
                ->on('skills')
                ->onDelete('cascade');

            $table->foreign('related_skill_id')
                ->references('skill_id')
                ->on('skills')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skill_relations');
    }
}
