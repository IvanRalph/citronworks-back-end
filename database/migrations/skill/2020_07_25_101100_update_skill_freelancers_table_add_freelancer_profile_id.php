<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSkillFreelancersTableAddFreelancerProfileId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('skill_freelancers', function (Blueprint $table) {
            $table->bigInteger('freelancer_profile_id')->unsigned()->nullable()->after('skill_freelancer_id');
            $table->foreign('freelancer_profile_id')->references('freelancer_profile_id')->on('freelancer_profile')->onDelete('cascade');

            DB::statement('ALTER TABLE `skill_freelancers` MODIFY `freelancer_id` BIGINT(20) UNSIGNED NULL;');
            // DB::statement('ALTER TABLE `freelancer_profile` MODIFY `category` INT(11) UNSIGNED NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('skill_freelancers', function (Blueprint $table) {
            $table->dropColumn('freelancer_profile_id');
        });
    }
}
