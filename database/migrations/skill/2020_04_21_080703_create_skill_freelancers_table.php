<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkillFreelancersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill_freelancers', function (Blueprint $table) {
            $table->bigIncrements('skill_freelancer_id');
            $table->unsignedBigInteger('freelancer_id')->index();
            $table->unsignedBigInteger('skill_id')->index();
            $table->integer('rating')->default(null)->nullable();
            $table->integer('skill_order')->default(null)->nullable();
            $table->timestamps();

            $table->foreign('freelancer_id')
                ->references('freelancer_id')
                ->on('freelancers')
                ->onDelete('cascade');

            $table->foreign('skill_id')
                ->references('skill_id')
                ->on('skills')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skill_freelancers');
    }
}
