<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkillJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill_jobs', function (Blueprint $table) {
            $table->bigIncrements('skills_job_id');
            $table->unsignedBigInteger('job_id')->index();
            $table->unsignedBigInteger('skill_id')->index();
            $table->integer('skill_order');
            $table->timestamps();

            $table->foreign('job_id')
                ->references('job_id')
                ->on('jobs')
                ->onDelete('cascade');

            $table->foreign('skill_id')
                ->references('skill_id')
                ->on('skills')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skill_jobs');
    }
}
