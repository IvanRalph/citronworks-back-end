<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->bigIncrements('participant_id');
            $table->unsignedBigInteger('conversation_id');
            $table->unsignedBigInteger('member_id');
            $table->enum('member_type', ['freelancer', 'employer']);
            $table->timestamps();

            $table->unique(['conversation_id', 'member_id', 'member_type'], 'participant_index');

            $table->foreign('conversation_id')
                ->references('conversation_id')
                ->on('conversations')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
