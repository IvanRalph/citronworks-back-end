<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->bigIncrements('receipt_id');
            $table->integer('invoiceable_id');
            $table->string('invoiceable_type');
            $table->string('braintree_id', 225)->collation('utf8_general_ci');
            $table->bigInteger('receipt_number');
            $table->timestamp('receipt_date')->useCurrent();
            $table->integer('amount');
            $table->enum('status', ['valid', 'invalid'])->default('valid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
