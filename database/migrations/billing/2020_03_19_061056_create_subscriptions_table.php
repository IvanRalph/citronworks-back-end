<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->bigIncrements('subscription_id');
            $table->unsignedBigInteger('member_id');
            $table->enum('member_type', ['freelancer', 'employer'])->collation('utf8_general_ci');
            $table->string('braintree_id', 11)->collation('utf8_general_ci'); //note sample id 92wdsw not integer from braintree
            $table->string('braintree_plan', 22)->collation('utf8_general_ci');
            $table->enum('is_active', ['true', 'false']);
            $table->timestamp('next_billing')->useCurrent();
            $table->timestamp('billing_interval')->useCurrent();
            $table->string('subscription_name')->collation('utf8_general_ci');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
