<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBraintreeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('braintree', function (Blueprint $table) {
            $table->bigInteger('braintree_id');
            $table->unsignedBigInteger('member_id');
            $table->enum('member_type', ['freelancer', 'employer'])->collation('utf8_general_ci');
            $table->string('card_brand', 32)->collation('utf8_general_ci');
            $table->string('paypal_email', 128)->collation('utf8_general_ci')->nullable();
            $table->smallInteger('card_last_digits');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('braintree');
    }
}
