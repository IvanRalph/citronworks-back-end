<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewTableGdpr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gdpr', function (Blueprint $table) {
            $table->bigIncrements('gdpr_id');
            $table->integer('member_id');
            $table->string('email', 255)->collation('utf8_general_ci');
            $table->string('source', 255)->collation('utf8_general_ci');
            $table->string('policy_version', 255)->collation('utf8_general_ci');
            $table->enum('user_type', ['freelancer', 'employer', 'invite', 'news_letter']);
            $table->enum('jurisdiction', ['eu', 'ca', 'us', 'other']);
            $table->enum('delete_request', ['yes', 'no']);
            $table->date('policy_date');
            $table->dateTime('delete_request_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gdpr');
    }
}
