<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title> Receipt </title>
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
    body { margin: 0; padding: 0; background: #e1e1e1; }
    div, p, a, li, td { -webkit-text-size-adjust: none; }
    .ReadMsgBody { width: 100%; background-color: #ffffff; }
    .ExternalClass { width: 100%; background-color: #ffffff; }
    body { width: 100%; height: 100%; background-color: #e1e1e1; margin: 0; padding: 0; -webkit-font-smoothing: antialiased; }
    html { width: 100%; }
    p { padding: 0 !important; margin-top: 0 !important; margin-right: 0 !important; margin-bottom: 0 !important; margin-left: 0 !important; }
    .visibleMobile { display: none; }
    .hiddenMobile { display: block; }
    
    @media print {
      body {-webkit-print-color-adjust: exact;}
    }
    
    @media only screen and (max-width: 600px) {
      body { width: auto !important; }
      table[class=fullTable] { width: 96% !important; clear: both; }
      table[class=fullPadding] { width: 85% !important; clear: both; }
      table[class=col] { width: 45% !important; }
      .erase { display: none; }
    }

    @media only screen and (max-width: 420px) {
      table[class=fullTable] { width: 100% !important; clear: both; }
      table[class=fullPadding] { width: 85% !important; clear: both; }
      table[class=col] { width: 100% !important; clear: both; }
      table[class=col] td { text-align: left !important; }
      .erase { display: none; font-size: 0; max-height: 0; line-height: 0; padding: 0; }
      .visibleMobile { display: block !important; }
      .hiddenMobile { display: none !important; }
    }
  </style>
</head>
<body>
<!-- Header -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#e1e1e1">
  <tr>
    <td height="20"></td>
  </tr>
  <tr>
    <td>
      <table width="820" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#ffffff">
        <tr>
          <td>
            <table width="820" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#ffffff">
              <tr>
                <td height="30"></td>
              </tr>
              <tr>
                <td align="center"> <img src="{{ '/images/citronworks.png' }}" width="200" height="23" alt="logo" border="0" /></td>
              </tr>
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
                <td style="font-size: 10px; color: #5b5b5b; font-family: 'Open Sans', sans-serif; line-height: 16px; vertical-align: top; text-align: center;">
                  11693 San Vicente Bld. <br/>
                  Suite 182<br/>
                  90049 Los Angeles, California USA
                </td>
              </tr>
              <tr>
                <td height="25"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table width="820" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#ffffff" style="background-color:#ffffff; color:white; -webkit-print-color-adjust: exact; ">
              <tr>
                <td height="10"></td>
              </tr>
              <tr>
                <td style="font-size: 24px; color: white; letter-spacing: -1px; font-family: 'Open Sans', sans-serif; line-height: 1; vertical-align: top; text-align: center; font-weight: 700;">
                  RECEIPT
                </td>
                
              </tr>
              <tr>
                <td height="10"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="fullPadding">
              <tbody>
              <tr>
                <td>
                  <table width="350" border="0" cellpadding="0" cellspacing="0" align="left" class="col">
                    <tbody>
                    <tr>
                      <td height="100"></td>
                    </tr>
                    <tr>
                      <td style="font-size: 14px; color: #5b5b5b; font-family: 'Open Sans', sans-serif; line-height: 20px; vertical-align: top; text-align: left;">
                        <strong> Billed To:</strong>
                      </td>
                    </tr>
                    <tr>
                      <td height="5"></td>
                    </tr>
                    <tr>
                      <td style="font-size: 14px; color: #5b5b5b; font-family: 'Open Sans', sans-serif; line-height: 20px; vertical-align: top; text-align: left;">
                        <span style="text-transform: uppercase; font-weight: 700;">{{ $owner->fullName }}</span> <br>
                        <span style="font-size: 12px; line-height: 16px;">{{$owner->company->company_name}} <br>
                        {{$owner->company->company_url}}</span>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </td>
                <td style="vertical-align:top;">
                  <table width="350" border="0" cellpadding="0" cellspacing="0" align="left" class="col">
                    <tbody>
                    <tr>
                      <td height="100"></td>
                    </tr>
                    <tr>
                      <td style="vertical-align:top; font-size: 14px; color: #5b5b5b; font-family: 'Open Sans', sans-serif; line-height: 20px; vertical-align: top; text-align: left;">
                        <strong>Receipt No.:</strong> {{ $invoice->id }}
                        <br />
                        <strong>Date:</strong> {{ $invoice->date()->toFormattedDateString() }}
                        <br />
                        <strong>PO No.:</strong> 
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<!-- /Header -->
<!-- Subscription Details -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#e1e1e1">
  <tbody>
  <tr>
    <td>
      <table width="820" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#ffffff">
        <tbody>
        <tr>
        <tr>
          <td height="40"></td>
        </tr>
        <tr>
          <td>
            <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="fullPadding">
              <tbody>
              <tr style="
    background: gray;
">
                <th style="font-size: 14px;font-family: 'Open Sans', sans-serif;color: white;font-weight: 700;line-height: 1;vertical-align: top;padding: 7px 10px 7px 10px;" width="52%" align="left">
                  Description
                </th>
                <th style="font-size: 14px;font-family: 'Open Sans', sans-serif;color: white;font-weight: 700;line-height: 1;vertical-align: top;padding: 7px 10px 7px 10px;" align="right">
                  Amount
                </th>
              </tr>
              <tr>
                <td height="1" style="background: #bebebe;" colspan="4"></td>
              </tr>
              <tr>
                <td height="10" colspan="4"></td>
              </tr>
              <tr>
                <td style="font-size: 14px;font-family: 'Open Sans', sans-serif;color: #e87721;line-height: 18px;vertical-align: top;padding: 10px 7px; font-weight: 700;" class="article">
                                      Subscription Invoice for the month of March
                                  </td>
                <td style="font-size: 14px;font-family: 'Open Sans', sans-serif;color: #1e2b33;line-height: 18px;vertical-align: top;padding: 10px 7px;" align="right">
                  $69.00
                </td>
              </tr>
              <!-- Display The Discount -->
                              <tr>
                  <td height="1" colspan="4" style="border-bottom:1px solid #e4e4e4"></td>
                </tr>
                <tr>
                  <td style="font-size: 14px;font-family: 'Open Sans', sans-serif;color: #e87721;line-height: 18px;vertical-align: top;padding: 10px 7px; font-weight: 700;" class="article">
                    Coupon ($10 discount)
                  </td>
                  <td style="font-size: 14px;font-family: 'Open Sans', sans-serif;color: #1e2b33;line-height: 18px;vertical-align: top;padding: 10px 7px;" align="right">
                    -$10.00
                  </td>
                </tr>
                <tr>
                  <td height="1" colspan="4" style="border-bottom:1px solid #e4e4e4"></td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td height="20"></td>
        </tr>
        </tbody>
      </table>
    </td>
  </tr>
  </tbody>
</table>
<!-- /Subscription Details -->
<!-- Total -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#e1e1e1">
  <tbody>
  <tr>
    <td>
      <table width="820" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#ffffff">
        <tbody>
        <tr>
          <td>

            <!-- Table Total -->
            <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="fullPadding">
              <tbody>
              <tr>
                <td style="font-size: 14px;font-family: 'Open Sans', sans-serif;color: #1e2b33;line-height: 22px;vertical-align: top;text-align:right;padding: 0 7px;font-weight: 700;">
                  Total
                </td>
                <td style="font-size: 14px;font-family: 'Open Sans', sans-serif;color: #1e2b33;line-height: 22px;vertical-align: top;text-align:right;white-space:nowrap;padding: 0 7px; font-weight: 700;" width="80">
                  $59.00
                </td>
              </tr>
              <tr>
                <td height="100"></td>
              </tr>
              </tbody>
            </table>
            <!-- /Table Total -->

          </td>
        </tr>
        </tbody>
      </table>
    </td>
  </tr>
  </tbody>
</table>
<!-- /Total -->
<!-- Information -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#e1e1e1">
  <tbody>
  <tr>
    <td>
      <table width="820" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#f5f4f1">
        <tbody>
        <tr>
        
        <tr>
          <td height="60"></td>
        </tr>
        <tr>
          <td>
            <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="fullPadding">
              <tbody>
              <tr>
                <td>
                  <table width="350" border="0" cellpadding="0" cellspacing="0" align="left" class="col">

                    <tbody>
                    <tr>
                      <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; line-height: 1; vertical-align: top; ">
                        <strong>COMPANY ADDRESS</strong>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="10"></td>
                    </tr>
                    <tr>
                      <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; line-height: 22px; vertical-align: top; ">
                        {{$owner->billingInformation->address_1}} <br>
                        @if (isset($owner->billingInformation->address_2))
                          {{ $owner->billingInformation->address_2 }}<br>
                        @endif
                        {{$owner->billingInformation->state}}, {{$invoice->billing['countryName']}}<br>
                        {{$owner->billingInformation->zip}}
                      </td>
                    </tr>
                    </tbody>
                  </table>


                  <table width="350" border="0" cellpadding="0" cellspacing="0" align="right" class="col">
                    <tbody>
                    
                    <tr>
                      <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; line-height: 1; vertical-align: top; ">
                        <strong>PAYMENT METHOD</strong>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="10"></td>
                    </tr>
                    <tr>
                      <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; line-height: 22px; vertical-align: top; ">
                        Credit Card <br/>
                        Credit Card Type: {{ $invoice->creditCard['cardType'] }} <br/>
                        Card No: xxxx-xxxx-xxxx-1234
                        <br>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td height="150"></td>
        </tr>
        
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td height="20"></td>
  </tr>
  </tbody>
</table>


</body>
</html>
