<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Citronworks Receipt</title>

    <style>
        @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);

        body {
            margin: 0;
            padding: 0;
            /*background: #dedede;*/
        }

        body {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
        }

        html {
            width: 100%;
            -webkit-print-color-adjust: exact;
        }

        .invoice-box {
            margin: 30px auto 30px;
            max-width: 800px;
            font-size: 16px;
            line-height: 24px;
            font-family: 'Open Sans', sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            /*padding: 5px;*/
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top td {
            padding-bottom: 5px;
            padding-top: 30px;
        }

        .invoice-box table tr.top table td.logo img {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.heading td {
            color: #E87722;
            font-size: 30px;
        }

        .invoice-box table tr.details td {

        }

        .invoice-box table tr.subscription-details td {
            padding-left: 80px;
            padding-top: 30px;
            padding-right: 80px;
            padding-bottom: 30px;
        }

        .invoice-box table tr.subscription-details td {
            /*border-bottom: 1px solid #eee;*/
        }

        .invoice-box table tr.total td {
            font-weight: bold;
            text-align: right;
            padding-right: 50px;
        }


        .invoice-box .billing-details td {
            padding: 0;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }
    </style>
</head>

<body>

<div class="invoice-box" style="background: white;">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr class="">
                        <td border="0" cellpadding="0" cellspacing="0" align="left" style="padding: 80px 50px 30px 80px; line-height: 16px;">
                            <span style="font-size: 14px; font-weight: 700">
                                CitronWorks, Inc. <br>
                            </span>
                            <span style="font-size: 12px;">
                                11693 San Vicente Bld.<br>
                                Suite 182<br>
                                Los Angeles, CA 90049, USA
                            </span>
                        </td>
                        <td border="0" cellpadding="0" cellspacing="0" align="left" class="logo"
                            style="vertical-align: middle; padding: 50px 80px 30px 80px;">
                            <img src="https://citronworks.com/images/citronworks-strapline.png" width="230" style="width: 230px; height: auto;">
                        </td>

                    </tr>
                </table>
            </td>
        </tr>

        <tr class="heading" border="0" cellpadding="0" cellspacing="0">
            <td>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td border="0" cellpadding="0" cellspacing="0" align="left" width="100%"
                            style="padding: 50px 80px 0 80px;">
                            RECEIPT
                        </td>
                    </tr>
                </table>

            </td>
        </tr>

        <tr class="details">
            <td>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td border="0" cellpadding="0" cellspacing="0" style="padding: 20px 80px 30px 80px;">
                            <table>
                                <tr>
                                    <td style="font-size: 14px; color: #5b5b5b; font-family: 'Open Sans', sans-serif; line-height: 20px; vertical-align: top; text-align: left;">
                                        <strong> Billed To:</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 0; font-size: 14px; color: #5b5b5b; font-family: 'Open Sans', sans-serif; line-height: 20px; vertical-align: top; text-align: left;">
                                        <span style="text-transform: uppercase; font-weight: 700;">{{ $owner->fullName }}</span>
                                        <br>
                                        @if(optional($owner->company)->company_name)
                                            <span style="font-size: 12px; line-height: 16px;">
                                            {{$owner->company->company_name}} <br>
                                            {!! $owner->company->company_url ? $owner->company->company_url . '<br>' : '' !!}
                                            {{ $owner->company->address_1 ? $owner->company->address_1 . ', ' : '' }}{{ $owner->company->address_2 ? $owner->company->address_2 . ', ' : '' }}{{ $owner->company->city ? $owner->company->city . ', ' : '' }}{{ $owner->company->state ? $owner->company->state . ', ' : '' }}{{ $owner->company->zip ? $owner->company->zip . ' ' : '' }}{{ $owner->company->country ? $owner->company->country : '' }}
                                        </span>
                                        @endif
                                        {{-- @if(optional($owner->information)->company_name)
                                            <span style="font-size: 12px; line-height: 16px;">
                                                {{$owner->information->company_name}} <br>
                                                {{$owner->information->company_url}}
                                            </span>
                                        @endif --}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td border="0" cellpadding="0" cellspacing="0" style="padding: 20px 80px 30px 0px;">
                            <table>
                                <tr>
                                    <td style="vertical-align: top; font-size: 14px; color: #5b5b5b; font-family: 'Open Sans', sans-serif; line-height: 20px; vertical-align: top; text-align: left;">
                                        <strong style="margin-right: 2px">Receipt #</strong> &nbsp;: {{ $receipt_number }}
                                        <br>
                                        <strong style="margin-left: 1px">Date</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $invoice->created_at->toFormattedDateString() }}
                                        <br>
{{--                                        <strong>PO #</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $invoice->receipt_number }}--}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="subscription-details">
            <td>
                <table width="640" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr style="background: gray;  -webkit-print-color-adjust: exact; ">
                        <th style="font-size: 14px;font-family: 'Open Sans', sans-serif;color: white;font-weight: 700;line-height: 1;vertical-align: top;padding: 10px;"
                            width="52%" align="left">
                            Description
                        </th>
                        <th style="font-size: 14px;font-family: 'Open Sans', sans-serif;color: white;font-weight: 700;line-height: 1;vertical-align: top;padding: 10px;"
                            align="right">
                            Amount
                        </th>
                    </tr>
                    {{-- <tr style=" -webkit-print-color-adjust: exact; ">
                        <td style="border-bottom: 1px solid #eee; font-size: 14px;font-family: 'Open Sans', sans-serif;line-height: 18px;vertical-align: top;padding: 15px 10px;"
                            class="article">
                            @if ($invoice->planId)
                                Subscription {{ $invoice->subscroptionDetails() }}
                            @else
                                One Off Charge
                            @endif
                        </td>
                        <td style="border-bottom: 1px solid #eee; font-size: 14px;font-family: 'Open Sans', sans-serif;color: #1e2b33;line-height: 18px;vertical-align: top;padding: 15px 10px;"
                            align="right">
                            {{ $invoice->subtotal() }}
                        </td>
                    </tr>
                    @if ($invoice->hasDiscount())
                        <tr>
                            <td style="border-bottom: 1px solid #eee; font-size: 14px;font-family: 'Open Sans', sans-serif;line-height: 18px;vertical-align: top;padding: 15px 10px;"
                                class="article">
                                Coupon ({{ implode(', ', $invoice->coupons()) }})
                            </td>
                            <td style="border-bottom: 1px solid #eee; font-size: 14px;font-family: 'Open Sans', sans-serif;color: #1e2b33;line-height: 18px;vertical-align: top;padding: 15px 7px;"
                                align="right">
                                -{{ $invoice->discount() }}
                            </td>
                        </tr>
                    @endif --}}
                    <tr style=" -webkit-print-color-adjust: exact; ">
                        <td style="font-size: 14px;font-family: 'Open Sans', sans-serif;color: #1e2b33;line-height: 18px;vertical-align: top;padding: 10px 10px; background: #f6f6f6;"
                            class="article">
                            {{ $description }}
                        </td>
                        <td style="font-size: 14px;font-family: 'Open Sans', sans-serif;color: #1e2b33;line-height: 18px;vertical-align: top;padding: 10px 10px; background: #f6f6f6;"
                            align="right">
                            {{ $transactions->subtotal() }}
                        </td>
                    </tr>

                    @if ($transactions->hasDiscount())
                        <tr>
                            <td style="border-bottom: 1px solid #eee; font-size: 14px;font-family: 'Open Sans', sans-serif;line-height: 18px;vertical-align: top;padding: 15px 10px;"
                                class="article">
                                Coupon ({{ implode(', ', $transactions->coupons()) }})
                            </td>
                            <td style="border-bottom: 1px solid #eee; font-size: 14px;font-family: 'Open Sans', sans-serif;color: #1e2b33;line-height: 18px;vertical-align: top;padding: 15px 7px;"
                                align="right">
                                -{{ $transactions->discount() }}
                            </td>
                        </tr>
                    @endif

                    <tr>
                        <td style="font-size: 14px;font-family: 'Open Sans', sans-serif;line-height: 18px;vertical-align: top;padding: 15px 10px;"
                            class="article">
                            Total
                        </td>
                        <td style="font-size: 14px;font-family: 'Open Sans', sans-serif;color: #1e2b33;line-height: 18px;vertical-align: top;padding: 15px 10px;"
                            align="right">
                            {{ $transactions->total() }}
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr class="billing-details">
            <td>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; line-height: 1; vertical-align: top; padding: 50px 80px 10px 80px;">
                                        <strong>PAYMENT METHOD</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; line-height: 22px; vertical-align: top; padding: 0 80px 0px 80px;">
                                        @if(isset($member->braintree->card_brand))
                                            Credit Card Type:  {{ $member->braintree->card_brand }} <br>
                                            Card No: xxxx-xxxx-xxxx-{{ $member->braintree->card_last_digits }}
                                            <br>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
