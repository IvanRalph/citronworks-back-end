<html>
<body style="font-family: Arial, Helvetica, sans-serif;">
<h3>New Employer Signup</h3>

<p>Name: {{ $user->full_name }}</p>
<p>Email: {{ $user->email }}</p>
<p>Company Name: {{ $user->company->company_name }}</p>
<p>Company URL: {{ $user->company->company_url ?? 'N/A' }}</p>
<p>Subscription Type: {{ $user->subscription ? $user->subscription->braintree_plan : 'Free' }}</p>
</body>
</html>