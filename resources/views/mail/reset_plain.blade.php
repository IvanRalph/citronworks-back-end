CitronWorks - <{{ $websiteUrl }}>

Hi {{ $firstName }},


Please click the button below to reset your password:

Reset password - <{{ $websiteUrlReset }}>

If it was not you who requested a password reset you should simply disregard this email.


Kind regards,

The CitronWorks team


Website - <{{ $websiteUrl }}>
Support - <{{ $websiteUrlSupport }}>
Unsubscribe - <{{ $websiteUrlUnsubscribe }}>

CitronWorks, Inc - 11693 San Vicente Blvd, #182, Los Angeles, CA 90049 USA

