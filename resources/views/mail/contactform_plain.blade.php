This is a message sent from CitronWorks' contact form
CitronWorks - <{{ $websiteUrl }}>
From: {{ $firstName }} {{ $lastName }}

Email: {{ $email }}

Subject: {{ $formSubject }}

Message:
{!! nl2br($formMessage) !!}


Website - <{{ $websiteUrl }}>
Support - <{{ $websiteUrlSupport }}>
Unsubscribe - <{{ $websiteUrlUnsubscribe }}>

CitronWorks, Inc - 11693 San Vicente Blvd, #182, Los Angeles, CA 90049 USA

