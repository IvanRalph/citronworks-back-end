CitronWorks - <{{ $websiteUrl }}>


This is an application for {{ $jobTitle }}.


Applicant: {{ $firstName }} {{ $lastName }}


Email: {{ $email }}


Subject: {{ $jobSubject }}


Cover Letter: {!! nl2br($jobApplication) !!}


{{--Attachement: {{ $attachmentLink }}--}}


-- End of job application --


Kind regards,

The CitronWorks team


Website - <{{ $websiteUrl }}>
Support - <{{ $websiteUrlSupport }}>
Unsubscribe - <{{ $websiteUrlUnsubscribe }}>

CitronWorks, Inc - 11693 San Vicente Blvd, #182, Los Angeles, CA 90049 USA

