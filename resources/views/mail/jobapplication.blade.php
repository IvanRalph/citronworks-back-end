<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        table {
            border-collapse: collapse;
            border-spacing: 0;
            mso-line-height-rule: exactly;
            mso-margin-bottom-alt: 0;
            mso-margin-top-alt: 0;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }
    </style>
    <![endif]--><!--[if gt mso 15]>
    <style type="text/css" media="all">
        table, tr, td {
            border-collapse: collapse;
        }

        tr {
            font-size: 0px;
            line-height: 0px;
            border-collapse: collapse;
        }
    </style>
    <![endif]-->
</head>
<body style="padding: 0; background-color: #ffffff; color: #595f63; font-size: 11pt; margin: 0;">
<center class="wrapper"
        style="width: 100%; table-layout: fixed; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
    <div class="webkit" style="max-width: 600px; margin: 0 auto;">
        <!--[if (gte mso 9)|(IE)]>
        <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td>
        <![endif]-->
        <table class="outer" align="center"
               style="border-spacing: 0; font-family: Arial, sans-serif; color: #595f63; font-size: 11pt; margin: 0 auto; width: 100%; max-width: 600px;">
            <tr>
                <td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
                    <table width="100%"
                           style="border-spacing: 0; font-family: Arial, sans-serif; color: #595f63; font-size: 11pt;">
                        <tr>
                            <td class="inner contents"
                                style="color: #595f63; font-size: 11pt; text-align: left; padding: 20px 20px 20px 20px;">
                                You have received an application for {{ $jobTitle }}.<br><br>

                                Applicant: {{ $firstName }} {{ $lastName }}<br>

                                Email: {{ $email }}<br>

                                Subject: {{ $jobSubject }}<br><br>

                                Cover Letter:<br>
                                {!! nl2br($jobApplication) !!}<br>

{{--                                @if($attachmentLink !== null)--}}
{{--                                    Attachment: <img src="{{ $message->embed($attachmentLink) }}"> <br><br><br>--}}
{{--                                @endif--}}

                                <!-- End of job application -->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
                    <table width="100%"
                           style="border-spacing: 0; font-family: Arial, sans-serif; color: #595f63; font-size: 11pt;">
                        <tr>
                            <td class="inner contents"
                                style="color: #595f63; font-size: 11pt; text-align: left; padding: 15px 20px 20px 20px;">
                                </td>
                            </tr></table>
</td>
                </tr>
<tr>
<td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
                        <table width="100%" style="border-spacing: 0; font-family: Arial, sans-serif; color: #595f63; font-size: 11pt;"><tr>
<td class="inner contents" style="color: #595f63; font-size: 11pt; text-align: left; padding: 15px 20px 20px 20px;">
                                    <p style="margin: 0; margin-bottom: 10px; font-family: Arial, sans-serif; font-size: 11pt; color: #595f63; text-align: left;">
                                    Kind regards,
                                    <br><br>
                                    The CitronWorks team
                                    </p>

                                <p style="margin: 0; margin-bottom: 10px; font-family: Arial, sans-serif; font-size: 11pt; color: #595f63; text-align: center;">
                                    <br><br><a href="{{ $websiteUrl }}"
                                               style="color: #2CB0EB; text-decoration: underline;">Website</a> - <a
                                        href="{{ $websiteUrlSupport }}"
                                        style="color: #2CB0EB; text-decoration: underline;">Support</a> - <a
                                        href="{{ $websiteUrlUnsubscribe }}"
                                        style="color: #2CB0EB; text-decoration: underline;">Unsubscribe</a>
                                    <br><br>
                                    CitronWorks, Inc - 11693 San Vicente Blvd, #182, Los Angeles, CA 90049 USA
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
    </div>
</center>
</body>
</html>
