<html>
<body style="font-family: Arial, Helvetica, sans-serif;">
<h3>New Job Post</h3>

<p>Job ID: {{ $job->job_id }}</p>
<p>Job Title: {{ $job->title }}</p>
<p>Employer: {{ $job->employer->full_name }}</p>
<p>Company: {{ $job->employer->company->company_name }}</p>
<p>Job Status: {{ $job->job_status }}</p>
</body>
</html>