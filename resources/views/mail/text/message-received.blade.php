Hi {{ $recipient->first_name }},

You have a new message from {{ $sender->first_name }}, {{ $sender->title }}.

@include('mail.text.footer')
