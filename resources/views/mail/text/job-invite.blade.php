Hi {{ $jobInvite->freelancer->first_name }},

You have been invited by {{ $senderName }} to apply to the Job: {{ $jobInvite->job->title }}

To view the Job:

@php
  $url = config('emails.urls.website');
  $jobLink = $url . '/freelancer/jobs/' . $jobInvite->job_id;
@endphp
1. Login to your account at {{ $url }}
2. Click this link: {{ $jobLink }}

Message:

{{ $jobInvite->message }}

@include('mail.text.footer')
