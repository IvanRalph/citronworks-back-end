Hi {{ $freelancer->first_name }},

Thank you for signing-up to CitronWorks! Click below to login and start updating your profile and searching for jobs.

Login {{ config('emails.urls.website') . '/login' }}


Your Affiliate Account

One of the benefits of signing-up is that you automatically get a free affiliate account, which means you will get monetary rewards for every new company that signs-up through your unique link!

To access your affiliate control panel, simply login to:
URL: {{ config('affiliates.login.url')}}
Username: {{ $freelancer->email }}
Password: {{ $temporaryAffiliatePassword }}

You may change your affiliate account password after logging-in.

@include('mail.text.footer')