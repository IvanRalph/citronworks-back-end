<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        table {
            border-collapse: collapse;
            border-spacing: 0;
            mso-line-height-rule: exactly;
            mso-margin-bottom-alt: 0;
            mso-margin-top-alt: 0;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }
    </style>
    <![endif]--><!--[if gt mso 15]>
    <style type="text/css" media="all">
        table, tr, td {
            border-collapse: collapse;
        }

        tr {
            font-size: 0px;
            line-height: 0px;
            border-collapse: collapse;
        }
    </style>
    <![endif]-->
</head>
<body style="padding: 0; background-color: #ffffff; font-size: 11pt; margin: 0;">
<center class="wrapper"
        style="width: 100%; table-layout: fixed; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
    <div class="webkit" style="max-width: 600px; margin: 0 auto;">
        <!--[if (gte mso 9)|(IE)]>
        <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td>
        <![endif]-->
        <table class="outer" align="center"
               style="border-spacing: 0; font-size: 11pt; margin: 0 auto; width: 100%; max-width: 600px;">
            <tr>
                <td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
                    <table width="100%" style="border-spacing: 0; font-size: 11pt;">
                        <tr>
                            <td class="inner contents"
                                style="font-size: 11pt; text-align: left; padding: 20px 20px 20px 20px;">
                                This is a message sent from CitronWorks Concierge form <br><br>
                                Referer: {{ $model->referer }}
                                <br>
                                From: {{ $model->first_name }} {{ $model->last_name }}
                                <br>
                                Email: {{ $model->email }}
                                <br>
                                Company: {{ $model->company }}
                                <br>
                                Website: {{ $model->company_website }}
                                <br>
                                Phone: {{ $model->phone }}
                                <br><br/>
                                Message:<br>
                                {!! nl2br($model->message) !!}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
    </div>
</center>
</body>
</html>
