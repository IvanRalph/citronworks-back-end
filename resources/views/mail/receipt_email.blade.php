<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--<![endif]--><meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<!--[if (gte mso 9)|(IE)]>
  <style type="text/css">
    table {
    border-collapse: collapse;
    border-spacing: 0;
    mso-line-height-rule: exactly;
    mso-margin-bottom-alt: 0;
    mso-margin-top-alt: 0;
    mso-table-lspace: 0pt; mso-table-rspace: 0pt;}
  </style>
<![endif]--><!--[if gt mso 15]>
   <style type="text/css" media="all">
   table, tr, td {border-collapse: collapse;}
   tr { font-size:0px; line-height:0px; border-collapse: collapse; }
   </style>
 <![endif]-->
</head>
<body style="padding: 0; background-color: #ffffff; color: #595f63; font-size: 11pt; margin: 0;">
  <center class="wrapper" style="width: 100%; table-layout: fixed; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
    <div class="webkit" style="max-width: 600px; margin: 0 auto;">
    <!--[if (gte mso 9)|(IE)]>
    <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
    <tr>
    <td>
    <![endif]-->
      <table class="outer" align="center" style="border-spacing: 0; font-family: 'Open Sans',Helvetica,Arial; color: #595f63; font-size: 11pt; margin: 0 auto; width: 100%; max-width: 600px;">
        <tr>
          <td style="padding: 0; color: #595f63; font-size: 11pt; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; background-color: #f6f6f6;">
            <table border="0" width="100%" style="border-spacing: 0; font-family: 'Open Sans',Helvetica,Arial; color: #595f63; font-size: 11pt; width: 100%; max-width: 600px; min-width: 100%;">
              <tr>
                <td align="center" style="padding: 0; color: #595f63; font-size: 11pt; width: 100%; padding-top: 15px; padding-bottom: 15px; padding-right: 5px; padding-left: 5px;">
                  <a href="{{ config('emails.urls.website') }}" style="color: #2CB0EB; text-decoration: underline;"><img src="{{ config('emails.urls.logo') }}" alt="CitronWorks" border="0" height="30" style="border: 0; border-width: 0; width: auto; height: 30px;"></a>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
            <table width="100%" style="border-spacing: 0; font-family: 'Open Sans',Helvetica,Arial;  color: #595f63; font-size: 11pt;">
              <tr>
                <td class="inner contents" style="color: #595f63; font-size: 11pt; text-align: left; padding: 20px 20px 20px 20px;">
                  Hi {{ $user->first_name }},
                  <br><br>
                  We have processed the following transaction:
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
            <table width="100%" style="border-spacing: 0; font-family: 'Open Sans',Helvetica,Arial;  color: #595f63; font-size: 11pt;">
              <tr>
                <td class="inner contents" style="color: #595f63; font-size: 11pt; text-align: left; padding: 10px 40px 10px 40px;">
                    Invoice Number: {{ $invoice->receipt_number }}<br>
                    Amount Paid: ${{ $invoice->amount }}<br>
                    
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
            <table width="100%" style="border-spacing: 0; font-family: 'Open Sans',Helvetica,Arial;  color: #595f63; font-size: 11pt;">
              <tr>
                <td class="inner contents" style="color: #595f63; font-size: 11pt; text-align: left; padding: 20px 20px 20px 20px;">
                  You can find your receipt in your dashboard under Transactions.
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
            <table width="100%" style="border-spacing: 0; font-family: 'Open Sans',Helvetica,Arial;  color: #595f63; font-size: 11pt;">
              <tr>
                <td class="inner contents" style="color: #595f63; font-size: 11pt; text-align: left; padding: 15px 20px 20px 20px;">
                  <p style="margin: 0; margin-bottom: 10px; font-family: 'Open Sans',Helvetica,Arial; font-size: 11pt; color: #595f63; text-align: left;">
                    Kind regards,
                    <br><br>
                    The CitronWorks team
                  </p>
                  <p style="margin: 0; margin-bottom: 10px; font-family: 'Open Sans',Helvetica,Arial; font-size: 11pt; color: #595f63; text-align: center;">
                    <br><br>
                    <a href="{{ $websiteUrl }}" style="color: #2CB0EB; text-decoration: underline;">Website</a> - <a href="{{ $websiteUrlSupport }}" style="color: #2CB0EB; text-decoration: underline;">Support</a> - <a href="{{ $websiteUrlUnsubscribe }}" style="color: #2CB0EB; text-decoration: underline;">Unsubscribe</a>
                    <br><br>
                    CitronWorks, Inc - 11693 San Vicente Blvd, #182, Los Angeles, CA 90049 USA
                  </p>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    <!--[if (gte mso 9)|(IE)]>
      </td>
      </tr>
      </table>
    <![endif]-->
    </div>
  </center>
</body>
</html>
