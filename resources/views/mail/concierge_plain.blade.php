This is a message sent from CitronWorks Concierge form

Referer: {{ $model->referer }}

From: {{ $model->firstName }} {{ $model->lastName }}

Email: {{ $model->email }}

Company: {{ $model->company }}

Website: {{ $model->companyWebsite }}

Phone: {{ $model->phone }}

Message:
{!! nl2br($model->message) !!}

