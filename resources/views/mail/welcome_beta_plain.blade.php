CitronWorks - <{{ $websiteUrl }}>

Hi {{ $firstName }},


Thank you for your interest in CitronWorks

We will open up for {{$memberType}} in around two weeks so you can onboard, post jobs, and search for freelancers.


Kind regards,

The CitronWorks team


Website - <{{ $websiteUrl }}>
Support - <{{ $websiteUrlSupport }}>
Unsubscribe - <{{ $websiteUrlUnsubscribe }}>

CitronWorks, Inc - 11693 San Vicente Blvd, #182, Los Angeles, CA 90049 USA
