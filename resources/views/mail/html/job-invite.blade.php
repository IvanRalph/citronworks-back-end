@extends('mail.html.layout')

@section('content')
  <tr>
    <td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
      <table width="100%" style="border-spacing: 0; font-family: 'Open Sans',Helvetica,Arial;  color: #595f63; font-size: 11pt;">
        <tr>
          <td class="inner contents" style="color: #595f63; font-size: 11pt; text-align: left; padding: 20px 20px 20px 20px;">
            Hi {{ $jobInvite->freelancer->first_name }},
            <br><br>
            You have been invited by {{ $senderName }} to apply to the Job: {{ $jobInvite->job->title }}<br><br>

            To view the Job:
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
      <table width="100%" style="border-spacing: 0; font-family: 'Open Sans',Helvetica,Arial;  color: #595f63; font-size: 11pt;">
        <tr>
          <td class="inner contents" style="color: #595f63; font-size: 11pt; text-align: left; padding: 10px 40px 10px 40px;">
              @php
                  $url = config('emails.urls.website');
                  $jobLink = $url . '/freelancer/jobs/' . (10000 + (int)$jobInvite->job_id);
              @endphp
              1. Login to your account at <a href="{{ $url }}">{{ $url }}</a><br>
              2. Click this link: <a href="{{ $jobLink }}">{{ $jobLink }}</a>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
      <table width="100%" style="border-spacing: 0; font-family: 'Open Sans',Helvetica,Arial;  color: #595f63; font-size: 11pt;">
        <tr>
          <td class="inner contents" style="color: #595f63; font-size: 11pt; text-align: left; padding: 20px 20px 20px 20px;">
            Message:<br><br>

            {{ $jobInvite->message }}
          </td>
        </tr>
      </table>
    </td>
  </tr>
@endsection