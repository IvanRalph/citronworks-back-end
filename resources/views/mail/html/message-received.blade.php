@extends('mail.html.layout')

@section('content')
  <tr>
    <td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
      <table width="100%" style="border-spacing: 0; font-family: 'Open Sans',Helvetica,Arial; color: #595f63; font-size: 11pt;">
        <tr>
          <td class="inner contents" style="color: #595f63; font-size: 11pt; text-align: left; padding: 20px 20px 20px 20px;">
            Hi {{ $recipient->first_name }},
          </td>
        </tr>
        <tr>
          <td class="inner contents" style="color: #595f63; font-size: 11pt; text-align: left; padding: 20px 20px 20px 20px;">
            You have a new message!
          </td>
        </tr>
        <tr>
          <table border="0" width="100%" style="border-spacing: 0; font-family: 'Open Sans',Helvetica,Arial; color: #595f63; font-size: 11pt; width: 100%; max-width: 600px; min-width: 100%;">
            <tr>
              <td align="center" style="padding: 0; color: #595f63; font-size: 11pt; width: 100%; padding-top: 15px; padding-bottom: 15px; padding-right: 5px; padding-left: 5px;">
                <img src="{{ url($sender->profile_photo ?: 'user-placeholder.png') }}" alt="CitronWorks" border="0" width="100" style="border: 0; border-width: 0; height: auto; width: 100px;">
              </td>
            </tr>
          </table>
        </tr>
        <tr>
          <td class="inner contents" style="color: #595f63; font-size: 11pt; text-align: left; padding: 20px 20px 20px 20px; text-align: center;" align="center">
            <b>{{ $sender->full_name }}</b><br>
            {{ $sender->title }}
          </td>
        </tr>
      </table>
    </td>
  </tr>
@endsection
