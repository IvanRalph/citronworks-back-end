@extends('mail.html.layout')

@section('content')
  <tr>
    <td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
      <table width="100%" style="border-spacing: 0; font-family: 'Open Sans',Helvetica,Arial;  color: #595f63; font-size: 11pt;">
        <tr>
          <td class="inner contents" style="color: #595f63; font-size: 11pt; text-align: left; padding: 20px 20px 20px 20px;">
            Hi {{ $employer->first_name }},
            <br><br>
            Thank you for signing-up to CitronWorks! Click below to login and start posting jobs and searching for freelancers.
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
      <table width="100%" style="border-spacing: 0; font-family: 'Open Sans',Helvetica,Arial;  color: #595f63; font-size: 11pt;">
        <tr>
          <td class="inner contents" style="color: #595f63; font-size: 11pt; text-align: left; padding: 20px 20px 20px 20px;">
            <div style="text-align: center;">
              <div style="display: inline-block;">
                <!--[if mso]>
                    <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{{ config('emails.urls.website') . '/login' }}" style="height:40px; v-text-anchor:middle; width:300px; mso-line-height-rule:exactly; -ms-text-size-adjust:none;" arcsize="15%" stroke="f" fillcolor="#f7941d">
                    <w:anchorlock></w:anchorlock>
                    <center>
                <![endif]-->
                <a href="{{ config('emails.urls.website') . '/login' }}" style="background-color: #f7941d; border-radius: 6px; color: #FFFFFF; display: inline-block; font-family: 'Open Sans',Helvetica,Arial;  font-size: 13px; font-weight: bold; line-height: 40px; text-align: center; text-decoration: none; width: 300px; -webkit-text-size-adjust: none;">Login</a>
                <!--[if mso]>
                  </center>
                  </v:roundrect>
                <![endif]-->
              </div>
            </div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td class="one-column" style="padding: 0; color: #595f63; font-size: 11pt;">
      <table width="100%" style="border-spacing: 0; font-family: 'Open Sans',Helvetica,Arial;  color: #595f63; font-size: 11pt;">
        <tr>
          <td class="inner contents" style="color: #595f63; font-size: 11pt; text-align: left; padding: 20px 20px 20px 20px;">
              <b>Your Affiliate Account</b><br><br>
              One of the benefits of signing-up is that you automatically get a free affiliate account, which means you will get monetary rewards for every new company that signs-up through your unique link!
              <br><br>
              To access your affiliate control panel, simply login to:<br>
              URL: <a href="{{ config('affiliates.login.url') }}">{{ config('affiliates.login.url') }}</a><br>
              Username: {{ $employer->email }}<br>
              Password: {{ $temporaryAffiliatePassword }}
              <br><br>
              You may change your affiliate account password after logging-in.
          </td>
        </tr>
      </table>
    </td>
  </tr>
@endsection