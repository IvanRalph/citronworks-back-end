Hi {{ $user->first_name }},

We have processed the following transaction:

Invoice No.: {{ $invoice->receipt_number }}
Amount Paid: {{ $invoice->amount }}

You can find the PDF document in your dashboard under Transactions.

Kind regards,

The CitronWorks team


Website - <{{ config('emails.urls.website') }}>
Support - <{{ config('emails.urls.support') }}>
Unsubscribe - <{{ config('emails.urls.unsubscribe') }}>

CitronWorks, Inc - 11693 San Vicente Blvd, #182, Los Angeles, CA 90049 USA
