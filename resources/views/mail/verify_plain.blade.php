CitronWorks - <{{ $websiteUrl }}>

Hi {{ $firstName }},


Please click the button below to verify your email:

Verify email - <{{ $verifyUrl }}>

Please contact support if you have any questions regarding your account.


Kind regards,

The CitronWorks team


Website - <{{ $websiteUrl }}>
Support - <{{ $websiteUrlSupport }}>
Unsubscribe - <{{ $websiteUrlUnsubscribe }}>

CitronWorks, Inc - 11693 San Vicente Blvd, #182, Los Angeles, CA 90049 USA

