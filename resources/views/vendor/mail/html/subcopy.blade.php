<table class="subcopy" width="100%" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td style="overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; webkit-hyphens: none; -ms-hyphens: none; hyphens: none;">
{{ Illuminate\Mail\Markdown::parse($slot) }}
</td>
</tr>
</table>
