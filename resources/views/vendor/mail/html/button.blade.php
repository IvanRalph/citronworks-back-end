<table class="action" align="center" width="100%" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td align="center">
<table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td align="center">
<table border="0" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td>
<a href="{{ $url }}" class="button" style="background-color: #b8d96e;
    border-top: 10px solid #b8d96e;
    border-right: 18px solid #b8d96e;
    border-bottom: 10px solid #b8d96e;
    border-left: 18px solid #b8d96e; color: #576874;" target="_blank">{{ $slot }}</a>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
