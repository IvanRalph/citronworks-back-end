export default {
  name: 'LoadingComponent',
  functional: true,
  props: {
    isLoading: {
      required: false,
      default: '123'
    }
  },
  render(h, { data, children, props }) {
    return (
      <div {...data}>
        {props.isLoading ? (
          <div>
            <h2>Loading</h2>
            {children}
          </div>
        ) : { children } }
      </div>
    )
  } 
}