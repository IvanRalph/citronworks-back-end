import Vue from 'vue'
import BootstrapVue, { BForm } from 'bootstrap-vue'
Vue.use(BootstrapVue, {
  components: {
    BForm
  }
})