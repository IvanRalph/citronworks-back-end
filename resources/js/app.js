import Vue from 'vue'
import './plugins/vuetify'
import './plugins/vuebootstrap.js'
import BootstrapVue from "bootstrap-vue"
import router from './router'
import store from './store'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import Default from './Layout/Wrappers/baseLayout'
import Pages from './Layout/Wrappers/pagesLayout'
import Apps from './Layout/Wrappers/appLayout'

import App from './app.vue'

Vue.config.productionTip = false;
require('./bootstrap')
window.moment = require('moment')

window.Vue = require('vue')
Vue.use(BootstrapVue)

Vue.config.ignoredElements = ['trix-editor']
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('default-layout', Default)
Vue.component('userpages-layout', Pages)
Vue.component('apps-layout', Apps)

new Vue({
  el: '#app',
  router,
  store,
  template: '<App />',
  components: { App }
});
