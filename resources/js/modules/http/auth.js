import axios from 'axios'
import store from '@store'

export const verifyIfLoggedIn = () => {
  return new Promise((resolve, reject) => {
    axios.get('/web/v1/verify-auth')
      .then(response => {
        resolve(response.data)
      })
      .catch(error => {
        reject(error.response.data)
      })
  })
}

export const routerGuard = (to, from, next) => {
  verifyIfLoggedIn().then(c => {
    if (c) {
      next()
      return
    }
    next({
      path: '/',
      query: { redirect: to.fullPath }
    })
  })
}

export default (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!Object.keys(store.getters['auth/user']).length) {
      next({
        path: '/',
        query: { redirect: to.fullPath }
      })
    } else {
      verifyIfLoggedIn().then(c => {
        if (c) {
          next()
          return
        }
        next({
          path: '/',
          query: { redirect: to.fullPath }
        })
      })
      next()
    }
  } else {
    next()
  }
}