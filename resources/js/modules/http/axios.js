import axios from 'axios'

let web = axios.create({
  baseURL: '/web/v1/'
})

export default web
