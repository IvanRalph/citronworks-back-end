import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

import auth from '@store/auth'
import freelancers from '@store/freelancer'
import skills from '@store/skills'
import category from '@store/category'
import dashboard from '@store/dashboard'
import jobs from '@store/jobs'
import employers from '@store/employer'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    auth,
    freelancers,
    skills,
    category,
    dashboard,
    jobs,
    employers
  },
  plugins: [
    ...(debug ? [createLogger()] : [])
  ]
})
