import * as types from './types'
import web from '@modules/http/axios'

export default {
  namespaced: true,

  /**
   * @state
  */
  state: {
    employers: []
  },

  /**
   * @getters
  */
  getters: {
    employers: state => state.employers
  },

  /**
   * @mutations
  */
  mutations: {
    [types.GET_EMPLOYERS] (state, d) {
      state.employers = d
    }
  },

  /**
   * @actions
  */
  actions: {
    _get_employers ({ commit }, paged) {
      let url = 'get/employers'
      if (paged) {
        url = `${url}${paged}`
      }
      return web.get(`${url}`)
        .then(d => commit(types.GET_EMPLOYERS, d.data))
    },
    findByName ({ commit }, model) {
      return web.get(`get/employers/${model}`)
        .then(d => {
          commit(types.GET_EMPLOYERS, {data: d.data})
          return d
        })
    }
  }
}