import Vue from 'vue'
import * as types from './types'
import axios from 'axios'
import web from '@modules/http/axios'
import router from '@router'

export default {
  namespaced: true,

  /**
   * @state
   */
  state: {
    user: JSON.parse(localStorage.getItem('USER_AUTH')) || {}
  },

  /**
   * @getters
   */
  getters: {
    user: state => state.user
  },

  /**
   * @mutations
   */
  mutations: {
    [types.AUTH_USER] (state, p) {
      state.user = p
      localStorage.setItem('USER_AUTH', JSON.stringify(p))
    },
    [types.AUTH_USER_LOGOUT] (state) {
      localStorage.removeItem('USER_AUTH')
      state.user = {}
    },
    [types.AUTH_PATCH_PROFILE] (state, u) {
      Vue.set(state, 'user', u)
      localStorage.setItem('USER_AUTH', JSON.stringify(u))
    }
  },

  /**
   * @actions
   */
  actions: {
    authenticate ({ commit }, model) {
      return new Promise((resolve, reject) => {
        axios.post('/web/v1/login', model)
          .then(l => {
            if (!l.data) {
              reject('Login Failed. Please check email or password.')
            }
            commit(types.AUTH_USER, l.data)
            resolve(l.data)
          })
      })
    },
    logout ({ commit }) {
      axios.get('/web/v1/logout')
        .then(() => {
          commit(types.AUTH_USER_LOGOUT)
          router.push('/')
        })
    },
    updateProfile ({ commit }, model) {
      return web.patch('admin/patch/profile', model)
        .then(s => {
          commit(types.AUTH_PATCH_PROFILE, s.data)
        })
    },
    updatePassword ({ commit }, model) {
      return web.patch('admin/patch/password', model)
        .then(p => commit(types.AUTH_PATCH_PASSWORD, p.data))
    }
  }
}
