export const GET_SKILLS = 'admin/GET_SKILLS'
export const SEARCH_SKILLS = 'admin/SEARCH_SKILLS'
export const GET_CATEGORIES = 'admin/GET_CATEGORIES'
export const GET_SKILL_PROPOSALS = 'admin/GET_SKILL_PROPOSALS'

export const PATCH_SKILL = 'admin/PATCH_SKILL'

export const DELETE_SKILL = 'admin/DELETE_SKILL'

export const POST_SKILL = 'admin/POST_SKILL'
export const APPROVE_SKILL = 'admin/APPROVE_SKILL'
export const SUGGESTEDSKILL_IS_UPDATING = 'admin/SUGGESTEDSKILL_IS_UPDATING'
export const DISAPPROVE_SKILL = 'admin/DISAPPROVE_SKILL'
export const REMOVE_SUGGESTED_SKILL = 'admin/REMOVE_SUGGESTED_SKILL'
export const IS_LOADING = 'admin/IS_LOADING'