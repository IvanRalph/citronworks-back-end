import Vue from 'vue'
import * as types from './types'
import web from '@modules/http/axios'

export default {
  namespaced: true,

  /**
   * @state
   */
  state: {
    skills: [],
    categories: [],
    suggestedSkills: [],
    stateActions: {
      isSavingSuggestedSkill: false
    },
    isLoading: false
  },

  /**
   * @getters
   */
  getters: {
    skills: state => state.skills,
    categories: state => state.categories,
    suggestedSkills: state => state.suggestedSkills,
    actions: state => state.actions,
    isLoading: state => state.isLoading
  },

  /**
   * @mutations
   */
  mutations: {
    [types.GET_SKILLS] (state, p) {
      state.skills = p
    },
    [types.SEARCH_SKILLS] (state, p) {
      state.skills = p
    },
    [types.GET_CATEGORIES] (state, c) {
      state.categories = c
    },
    [types.PATCH_SKILL] (state, { model, result }) {
      const index = state.skills.data.findIndex(f => f.skill_id === model.skill_id)
      const newCat = state.categories.find(f => f.category_id === result.category_id)
      Vue.set(state.skills.data, [index], {
        ...state.skills.data[index], 
        category: newCat, category_id: result.category_id, skill_name: result.skill_name
      })
    },
    [types.DELETE_SKILL] (state, p) {
      Vue.set(state.skills, 'data', state.skills.data.filter(d => d.skill_id !== p.data.id))
    }, 
    [types.POST_SKILL] () {},
    [types.GET_SKILL_PROPOSALS] (state, d) {
      state.suggestedSkills = d
    },
    [types.APPROVE_SKILL] (state, d){
      const index = state.suggestedSkills.data.findIndex(f => f.skill_proposal_id === d.skill_proposal_id)
      Vue.set(state.suggestedSkills.data, [index], {
        ...state.suggestedSkills.data[index],
        skill_name: d.skill_name,
        approved: d.approved
      })
    },
    [types.DISAPPROVE_SKILL] (state, d) {
      const index = state.suggestedSkills.data.findIndex(f => f.skill_proposal_id === d.skill_proposal_id)
      Vue.set(state.suggestedSkills.data, [index], {
        ...state.suggestedSkills.data[index],
        approved: d.approved
      })
    },
    [types.SUGGESTEDSKILL_IS_UPDATING] (state, v) {
      state.stateActions.isSavingSuggestedSkill = v
    },
    [types.REMOVE_SUGGESTED_SKILL] (state, id) {
      state.suggestedSkills.data = state.suggestedSkills.data.filter(d => d.skill_proposal_id !== parseInt(id))
    },
    [types.IS_LOADING] (state, e) {
      state.isLoading = e
    }
  },

  /**
   * @actions
   */
  actions: {
    showAll ({ commit }, payload) {
      if (payload !== undefined) {
        return web.get(`get/skills/${payload}`)
          .then(r => commit(types.GET_SKILLS, r.data))
      }
      return web.get('get/skills')
        .then(r => commit(types.GET_SKILLS, r.data))
    },
    allCategories ({ commit }) {
      return web.get('get/category')
      .then(g => commit(types.GET_CATEGORIES, g.data))
    },
    findByName ({ commit }, payload) {
      commit(types.IS_LOADING, true)
      return web.get(`get/skills/${payload}`)
        .then(r => {
          commit(types.SEARCH_SKILLS, r.data)
          commit(types.IS_LOADING, false)
          return r.data
        })
    },
    edit ({ commit }, model) {
      return web.patch('patch/skill', model)
        .then(p => {
          commit(types.PATCH_SKILL, { model, result: p.data })
          return p.data
        })
    },
    destroy ({ commit }, id) {
      return web.delete(`delete/skill/${id}`)
        .then(d => {
          commit(types.DELETE_SKILL, d.data)
        })
    },
    add ({ commit }, payload) {
      return web.post('post/skill', payload)
        .then((s) => {
          commit(types.POST_SKILL)
          return s.data
        })
    },
    fetchSuggestedSkills ({ commit }, page = 1) {
      return web.get(`get/skill-proposals?page=${page}`)
        .then(g => {
          commit(types.GET_SKILL_PROPOSALS, g.data)
        })
    },
    _approve_skill ({ commit }, model) {
      commit(types.SUGGESTEDSKILL_IS_UPDATING, true)
      return web.post('post/suggested-skill', model)
        .then((d) => {
          commit(types.SUGGESTEDSKILL_IS_UPDATING, false)
          commit(types.APPROVE_SKILL, d.data)
        })
    },
    _disapprove_skill ({ commit }, model) {
      commit(types.SUGGESTEDSKILL_IS_UPDATING, true)
      return web.post('post/revert-suggested-skill', model)
        .then((d) => {
          commit(types.SUGGESTEDSKILL_IS_UPDATING, false)
          commit(types.DISAPPROVE_SKILL, d.data)
        })
    },
    _delete_suggested_skill ({ commit }, model) {
      commit(types.SUGGESTEDSKILL_IS_UPDATING, true)
      return web.delete(`delete/suggested-skill/${model.skill_proposal_id}`)
        .then((d) => {
          commit(types.SUGGESTEDSKILL_IS_UPDATING, false)
          commit(types.REMOVE_SUGGESTED_SKILL, d.data)
        })
    },
    _get_all_skills ({ commit }, model) {
      commit(types.IS_LOADING, true)
      return web.post('post/get-all-skills', model)
        .then(r => {
          commit(types.IS_LOADING, false)
          return r.data
        })
    },
    download ({ commit }, { url, data }) {
      return web.get(url, {
        params: {
          data
        }
      })
        .then(e => {
          commit(types.IS_LOADING, false)
          return e
        })
    }
  }
}
