import Vue from 'vue'
import * as types from './types'
import web from '@modules/http/axios'

export default {
  namespaced: true,

  /**
   * @state
   */
  state: {
    categories: []
  },

  /**
   * @getters
   */
  getters: {
    categories: state => state.categories
  },

  /**
   * @mutations
   */
  mutations: {
    [types.GET_CATEGORIES] (state, p) {
      state.categories = p
    },
    [types.PATCH_CATEGORY] (state, p) {
      const index = state.categories.data.findIndex(f => f.category_id === p.category_id)
      Vue.set(state.categories.data, [index], { ...state.categories.data[index], category_name: p.category_name })
    },
    [types.DELETE_CATEGORY] (state, c) {
      console.log('ccc', c)
      state.categories.data = state.categories.data.filter(d => d.category_id !== c.category_id)
    },
    [types.POST_CATEGORY] (state, p) {
      state.categories.data = [
        p,
        ...state.categories.data
      ]
    }
  },

  /**
   * @actions
   */
  actions: {
    showAll({ commit }, payload = null) {
      return web.get(`get/all-categories${payload !== null ? payload : ''}`)
        .then(g => commit(types.GET_CATEGORIES, g.data))
    },
    show ({ commit }, payload = null) {
      return web.get(`get/categories${payload !== null ? payload : ''}`)
        .then(g => commit(types.GET_CATEGORIES, g.data))
    },
    update ({ commit }, payload) {
      return web.patch('patch/category', payload)
        .then(p => commit(types.PATCH_CATEGORY, p.data))
    },
    destroy ({ commit }, d) {
      return web.delete(`delete/category/${d}`)
        .then(d => commit(types.DELETE_CATEGORY, d.data))
    },
    store ({ commit }, payload) {
      return web.post('post/category', payload)
        .then(p => {
          commit(types.POST_CATEGORY, p.data)
          return p.data
        })
    }
  }
}
