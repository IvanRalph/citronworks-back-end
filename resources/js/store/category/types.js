export const GET_CATEGORIES = 'admin/GET_CATEGORIES'

export const PATCH_CATEGORY = 'admin/PATCH_CATEGORY'

export const DELETE_CATEGORY = 'admin/DELETE_CATEGORY'

export const POST_CATEGORY = 'admin/POST_CATEGORY'