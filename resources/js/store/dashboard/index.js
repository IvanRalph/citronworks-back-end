import * as types from './types'
import web from '@modules/http/axios'
import { numberWithCommas } from '@modules/data/Numbers.js'

export default {
  namespaced: true,
  
  /**
  * @state
  */
  state: {
    freelancerTotalSignUp: 0,
    utm_data: [],
    counties_data: []
  },

  /**
   * @getters
   */
  getters: {
    freelancerTotalSignUp: state => state.freelancerTotalSignUp,
    utm_data: state => state.utm_data,
    counties_data: state => state.counties_data
  },

  /**
   * @mutations
   */
  mutations: {
    [types._STATS_FREELANCER_TOTAL_SIGNUP] () {},
    [types._STATS_FREELANCER_TOTAL_COUNT_SIGNUP] (state, d) {
      state.freelancerTotalSignUp = numberWithCommas(d)
    },
    [types._STATS_FREELANCER_UTMS] (state, d) {
      state.utm_data = d.filter(f => f.tag !== '' && f.tag !== 'null')
    },
    [types._STATS_COUNTRIES] (state, d) {
      state.counties_data = d.filter(f => f.tag !== '' && f.tag !== 'null')
    }
  },

  /**
   * @action
   */
  actions: {
    _stats_freelancer_signup ({ commit }, model) {
      return web.get('get/stats/freelancer-signup', {
        params: {
          model
        }
      })
        .then((r) => {
          commit(types._STATS_FREELANCER_TOTAL_SIGNUP)
          return r
        })
    },
    _stats_freelancer_total_signup ({ commit }) {
      return web.get('get/stats/freelancer-signup-total-count')
        .then(r => {
          commit(types._STATS_FREELANCER_TOTAL_COUNT_SIGNUP, r.data)
        })
    },
    _stats_freelancer_utms ({ commit }, model) {
      return web.get('get/stats/freelancer-utms', {
        params: {
          model
        }
      })
        .then(r => {
          commit(types._STATS_FREELANCER_UTMS, r.data)
        })
    },
    _stats_countries ({ commit }, model) {
      return web.get('get/stats/freelancer-countries', {
        params: {
          model
        }
      })
        .then(r => {
          commit(types._STATS_COUNTRIES, r.data)
        })
    }
  }
}