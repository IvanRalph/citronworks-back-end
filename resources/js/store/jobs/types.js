export const GET_JOBS = 'admin/GET_JOBS'
export const SEARCH_JOBS = 'admin/SEARCH_JOBS'

export const PATCH_JOB = 'admin/PATCH_JOBS'

export const DELETE_JOB = 'admin/DELETE_JOBS'

export const POST_JOB = 'admin/POST_JOBS'

export const UPDATE_JOB_COUNT = 'admin/UPDATE_JOB_COUNT'