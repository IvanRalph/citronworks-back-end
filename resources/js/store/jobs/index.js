import * as types from './types'
import web from '@modules/http/axios'

export default {
  namespaced: true,

  /**
   * @state
   */
  state: {
    jobs: [],
    unverifiedJobCount: 0
  },

  /**
   * @getters
   */
  getters: {
    jobs: state => state.jobs,
  },

  /**
   * @mutations
   */
  mutations: {
    [types.GET_JOBS] (state, p) {
      state.jobs = p
    },
    [types.PATCH_JOB] (state, p) {
      state.jobs = p
    },
    [types.SEARCH_JOBS] (state, p) {
      state.jobs = p
    },
    [types.UPDATE_JOB_COUNT] (state, p) {
      state.unverifiedJobCount = p
    },
  },

  /**
   * @actions
   */
  actions: {
    showAll ({ commit }, payload) {
      if (payload !== undefined) {
        return web.get(`get/jobs/${payload}`)
          .then(r => {
            commit(types.GET_JOBS, r.data)
            return r.data;
          })
      }
      return web.get('get/jobs')
        .then(r => {
          commit(types.GET_JOBS, r.data)
          return r.data;
        })
    },
    findByName ({ commit }, payload) {
      return web.get(`get/jobs/${payload}`)
        .then(r => {
          commit(types.SEARCH_JOBS, r.data)
          return r.data
        })
    },
    edit ({ commit }, model) {
      return web.patch('patch/jobs', model)
        .then(p => {
          commit(types.PATCH_JOB, { model, result: p.data })
          return p.data
        })
    },

    updateJobCount ({ commit }, count){
      commit(types.UPDATE_JOB_COUNT, count);
    }
  }
}
