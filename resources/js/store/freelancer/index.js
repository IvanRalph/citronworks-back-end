import Vue from 'vue'
import * as types from './types'
import web from '@modules/http/axios'

export default {
  namespaced: true,

  /**
   * @state
   */
  state: {
    freelancers: [],
    company: [],
    actions: {
      freelancers: {
        isCreating: false,
        isCompleted: false,
        isUpdating: false,
        isUpdated: false
      },
      freelancers_profile: {
        isUpdating: false,
        isUpdated: false
      },
      freelancers_company: {
        isUpdating: false,
        isUpdated: false
      }
    },
  },

  /**
   * @getters
   */
  getters: {
    freelancers: state => state.freelancers,
    company: state => state.company,
    actions: state => state.actions
  },

  /**
   * @mutations
   */
  mutations: {
    [types.GET_FREELANCERS](state, d) {
      state.freelancers = d
    },
    [types.FIND_FREELANCERS](state, d) {
      state.freelancers = d
    },
    [types.DELETE_FREELANCER](state, id) {
      Vue.set(state.freelancers, 'data', state.freelancers.data.filter(d => d.freelancer_id !== id.freelancer_id))
    },
    [types.PATCH_FREELANCER]() { },
    [types.GET_FREELANCE_COMPANY](state, companies) {
      state.company = companies
    },
    [types.CREATE_FREELANCER](state, p) {
      state.freelancers.data = [p, ...state.freelancers.data]
    },
    [types.FREELANCER_CREATING] (state, v) {
      state.actions.freelancers.isCreating = v
    },
    [types.FREELANCER_ISCREATED] (state, v) {
      state.actions.freelancers.isCompleted = v
    },
    [types.FREELANCER_ISUPDATING] (state, v) {
      state.actions.freelancers.isUpdating = v
    },
    [types.FREELANCER_ISUPDATED] (state, v) {
      state.actions.freelancers.isUpdated = v
    },
    [types.FREELANCER_COMPANY_ISUPDATING] (state, v) {
      state.actions.freelancers_company.isUpdating = v
    },
    [types.FREELANCER_COMPANY_ISUPDATED] (state, v) {
      state.actions.freelancers_company.isUpdated = v
    },
    [types.FREELANCER_PROFILE_ISUPDATING] (state, v) {
      state.actions.freelancers_profile.isUpdating = v
    },
    [types.FREELANCER_PROFILE_ISUPDATED] (state, v) {
      state.actions.freelancers_profile.isUpdated = v
    },
    [types.FREELANCER_UPDATE_DATA] (state, d) {
      const index = state.freelancers.data.findIndex(f => f.freelancer_id === d.obj.freelancer_id)
      Vue.set(
        state.freelancers.data, [index],
        {
        ...state.freelancers.data[index],
        ...d.obj
        }
      )
    },
    [types.FREELANCER_UPDATE_PROFILE_DATA] (state, d) {
      const index = state.freelancers.data.findIndex(f => f.freelancer_id === d.freelancer_id)
      Vue.set(
        state.freelancers.data, [index],
        {
          ...state.freelancers.data[index],
          ...d
        }
      )
    }
  },

  /**
   * @actions
   */
  actions: {
    fetchData({ commit }) {
      return web.get('get/freelancers')
        .then(g => {
          commit(types.GET_FREELANCERS, g.data)
        })
    },
    findByName({ commit }, payload) {
      return web.get(`get/freelancers/${payload}`)
        .then(g => {
          commit(types.FIND_FREELANCERS, g.data)
          return g.data
        })
    },
    destroy({ commit }, id) {
      return web.delete(`delete/freelancer/${id}`)
        .then(d => {
          commit(types.DELETE_FREELANCER, d.data)
          return d.data
        })
    },
    patchFreelancer({ commit }, payload) {
      return web.patch('patch/freelancer', payload)
        .then(p => {
          commit(types.PATCH_FREELANCER, p.data)
        })
    },
    create({ commit }, payload) {
      commit(types.FREELANCER_CREATING, true)
      return web.post('post/freelancer', payload)
        .then(() => {
          commit(types.FREELANCER_CREATING, false)
          commit(types.FREELANCER_ISCREATED, true)
        })
    },
    _reset_freelancer_form({ commit }) {
      commit(types.FREELANCER_ISCREATED, false)
    },
    findFreelanceCompany({ commit }, name) {
      return web.get(`get/freelancer-company/${name}`)
        .then(r => commit(types.GET_FREELANCE_COMPANY, r.data))
    },
    _update_freelancer ({ commit }, model) {
      commit(types.FREELANCER_ISUPDATING, true)
      return web.patch('patch/freelancer', model)
        .then((d) => {
          commit(types.FREELANCER_ISUPDATING, false)
          commit(types.FREELANCER_ISUPDATED, true)
          commit(types.FREELANCER_UPDATE_DATA, d.data)
          setTimeout(() => {
            commit(types.FREELANCER_ISUPDATED, false)
          }, 4000);
        })
    },
    _update_freelancer_company ({ commit }, model) {
      commit(types.FREELANCER_COMPANY_ISUPDATING, true)
      return web.patch('patch/freelancer-company', model)
        .then(() => {
          commit(types.FREELANCER_COMPANY_ISUPDATING, false)
          commit(types.FREELANCER_COMPANY_ISUPDATED, true)
          setTimeout(() => {
            commit(types.FREELANCER_COMPANY_ISUPDATED, false)
          }, 4000);
        })
    },
    _update_freelancer_profile ({ commit }, { model, id }) {
      commit(types.FREELANCER_PROFILE_ISUPDATING, true)
      return web.patch(`patch/freelancer-profile/${id}`, model)
        .then((d) => {
          commit(types.FREELANCER_UPDATE_PROFILE_DATA, d.data)
          commit(types.FREELANCER_PROFILE_ISUPDATING, false)
          commit(types.FREELANCER_PROFILE_ISUPDATED, true)
          setTimeout(() => {
            commit(types.FREELANCER_PROFILE_ISUPDATED, false)
          }, 4000);
          return d
        })
    }
  }
}
