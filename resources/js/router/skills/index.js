export default [{
  path: '/skills/view',
  name: 'skills',
  meta: { requiresAuth: true },
  component: () => import('@components/Skills')
}, {
  path: '/suggested-skills/view',
  name: 'suggested-skills',
  meta: { requiresAuth: true },
  component: () => import('@components/Freelancers/Suggested-skills/index.vue')
}, {
  path: '/category/view',
  name: 'category-skills',
  meta: { requiresAuth: true },
  component: () => import('@components/Category/index.vue')
}]