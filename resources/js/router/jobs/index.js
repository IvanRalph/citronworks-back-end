export default [{
  path: '/employer/view',
  name: 'View Employers',
  meta: { requiresAuth: true },
  component: () => import('@components/Employers')
  }, {
  path: '/jobs/view',
  name: 'jobs',
  meta: { requiresAuth: true },
  component: () => import('@components/Jobs')
}]