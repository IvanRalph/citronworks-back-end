import Vue from 'vue'
import VueRouter from 'vue-router'
import Validate from '@modules/http/auth.js'

import dashboard from './dashboard'
import freelancers from './freelancers/'
import skills from './skills'
import jobs from './jobs'

Vue.use(VueRouter)

const routes = [
  ...dashboard,
  ...freelancers,
  ...skills,
  ...jobs
]

const router =  new VueRouter({
  mode: 'history',
  base: 'cwadmin',
  history: true,
  scrollBehavior() {
    return window.scrollTo({ top: 0, behavior: 'smooth' });
  },
  routes
})

router.beforeEach((to, from, next) => Validate(to, from, next))

export default router