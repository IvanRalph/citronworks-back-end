import { routerGuard } from '@modules/http/auth.js'

export default [{
  path: '/dashboard',
  name: 'analytics',
  meta: { requiresAuth: true },
  beforeRouteEnter: (to, from, next) => routerGuard(to, from, next),
  component: () => import('../../components/Dashboard/index.vue'),
}, {
  path: '/profile',
  name: 'profile',
  meta: { requiresAuth: true },
  beforeRouteEnter: (to, from, next) => routerGuard(to, from, next),
  component: () => import('../../Layout/Components/Pages/profile.vue')
}, {
  path: '/security',
  name: 'security',
  meta: { requiresAuth: true },
  beforeRouteEnter: (to, from, next) => routerGuard(to, from, next),
  component: () => import('../../Layout/Components/Pages/security.vue')
}]