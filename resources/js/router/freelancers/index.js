export default [{
  path: '/freelancers/stats',
  name: 'freelancers',
  meta: { requiresAuth: true },
  component: () => import('../../components/Freelancers')
}, {
  path: '/freelancers/search-by-skills',
  name: 'search-by-skills',
  meta: { requiresAuth: true  },
  component: () => import('../../components/Freelancers/search-by-skills')
}
// {
//   path: '/freelancers/add',
//   name: 'add-freelancers',
//   meta: { requiresAuth: true },
//   component: () => import('../../components/Freelancers/add.vue')
// }, {
//   path: '/freelancers/edit/:id',
//   name: 'edit-freelancer',
//   meta: { requiresAuth: true },
//   component: () => import('../../components/Freelancers/edit.vue')
// }
]