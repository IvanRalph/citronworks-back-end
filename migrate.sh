#!/bin/bash

echo 'Migrate Auth'
php artisan migrate --path=database/migrations/auth

echo 'Migrate freelancer'
php artisan migrate --path=database/migrations/freelancer

echo 'Migrate Employer'
php artisan migrate --path=database/migrations/employer

echo 'Migrate billing'
php artisan migrate --path=database/migrations/billing

echo 'Migrate GDPR'
php artisan migrate --path=database/migrations/gdpr

echo 'Migrate Jobs'
php artisan migrate --path=database/migrations/jobs

echo 'Migrate Bookmar'
php artisan migrate --path=database/migrations/bookmark

echo 'Migrate invitation'
php artisan migrate --path=database/migrations/invitation

echo 'Migrate category'
php artisan migrate --path=database/migrations/category

echo 'Migrate Skill'
php artisan migrate --path=database/migrations/skill

echo 'Migrate Global'
php artisan migrate --path=database/migrations/global

echo 'Migrate messaging'
php artisan migrate --path=database/migrations/messaging

echo 'Migrate hires'
php artisan migrate --path=database/migrations/hires

echo 'Migrate updated schema'
php artisan migrate --path=database/migrations/alter

echo 'FINISHED'

