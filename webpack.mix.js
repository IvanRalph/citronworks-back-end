const mix = require('laravel-mix')
const path = require('path')
const webpack = require('webpack')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    devServer: {
        historyApiFallback: true
    },
    plugins: [
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
    ],
    module: {
        rules: [{
            enforce: 'pre',
            test: /\.(js|vue)$/,
            exclude: /node_modules/,
            loader: 'eslint-loader'
            // options: {
            // 	formatter: require('eslint-friendly-formatter')
            // }
        }]
    },
    resolve: {
      alias: {
        "@store": path.resolve(__dirname, "resources/js/store"),
        "@assets": path.resolve(__dirname, "resources/js/assets"),
        "@router": path.resolve(__dirname, "resources/js/router"),
        "@layout": path.resolve(__dirname, "resources/js/Layout"),
        "@components": path.resolve(__dirname, "resources/js/components"),
        "@modules": path.resolve(__dirname, "resources/js/modules"),
        "@charts": path.resolve(__dirname, "resources/js/components/Charts")
      }
    },
    output: {
        chunkFilename: 'js/bundlers/[name].bundle.js',
        publicPath: '/'
    }
})
mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');
