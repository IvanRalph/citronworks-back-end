<?php

return [
    'hub_base_form_url' => env('HUB_BASE_URL', 'https://api.hsforms.com/submissions/v3/integration'),
    'portal_id' => env('HUB_PORTAL_ID', '7527095'),
    'contact_form_guid' => env('HUB_CONTACT_GUID', '04321b49-87a5-4c9c-ba77-ab792b6527bd'),
];

//{
//    "fields": [
//    {
//        "name": "firstname",
//      "value": "Apollo",
//      "fieldType": "text"
//    },
//    {
//        "name": "lastname",
//      "value": "Malapote",
//      "fieldType": "text"
//    },
//    {
//        "name": "email",
//      "value": "kofeje7832@toracw.com",
//      "fieldType": "text"
//    },
//    {
//        "name": "subject",
//      "value": "Test Subject",
//      "fieldType": "text"
//    },
//    {
//        "name": "message",
//      "value": "Test Message",
//      "fieldType": "text"
//    }
//  ],
//  "context": {
//    "pageUri": "https://citronworks.com/contact-form",
//    "pageName": "Contact Us Page"
//  }
//}
//
