<?php

return [
    'merchant' => [
        'domain' => env('PAP_MERCHANT_DOMAIN'),
        'username' => env('PAP_MERCHANT_USERNAME'),
        'password' => env('PAP_MERCHANT_PASSWORD'),
    ],
    'login' => [
        'url' => env('PAP_LOGIN_URL'),
    ],
];
