<?php

return [
    'personal' => [
        'client_id' => env('PASSPORT_PERSONAL_ACCESS_CLIENT_ID'),
    ],
];
