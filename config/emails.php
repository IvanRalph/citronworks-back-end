<?php

return [
    'urls' => [
        'website' => 'https://www.citronworks.com',
        'support' => 'mailto:team@citronworks.com',
        'unsubscribe' => 'mailto:team@citronworks.com',
        'logo' => 'https://www.citronworks.com/images/citronworkslogo.png',
    ],
];
