<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Defaults
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard" and password
    | reset options for your application. You may change these defaults
    | as required, but they're a perfect start for most applications.
    |
    */

    'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ],

    /*
    |--------------------------------------------------------------------------
    | Authentication Guards
    |--------------------------------------------------------------------------
    |
    | Next, you may define every authentication guard for your application.
    | Of course, a great default configuration has been defined for you
    | here which uses session storage and the Eloquent user provider.
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | Supported: "session", "token"
    |
    */

    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'passport',
            'provider' => 'users',
            'hash' => false,
        ],
        'employer' => [
            'driver' => 'passport',
            'provider' => 'employers',
            'hash' => false,
        ],
        'freelancer' => [
            'driver' => 'passport',
            'provider' => 'freelancers',
            'hash' => false,
        ],
        'company' => [
            'driver' => 'passport',
            'provider' => 'companies',
            'hash' => false,
        ],
        'message' => [
            'driver' => 'passport',
            'provider' => 'messages',
            'hash' => false,
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | User Providers
    |--------------------------------------------------------------------------
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | If you have multiple user tables or models you may configure multiple
    | sources which represent each model / table. These sources may then
    | be assigned to any extra authentication guards you have defined.
    |
    | Supported: "database", "eloquent"
    |
    */

    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\Model\Admin\User::class,
        ],
        'employers' => [
            'driver' => 'eloquent',
            'model' => App\Model\Employer\Employer::class,
        ],
        'freelancers' => [
            'driver' => 'eloquent',
            'model' => App\Model\Freelancer\Freelancer::class,
        ],
        'companies' => [
            'driver' => 'eloquent',
            'model' => App\Model\FreelancerCompany\FreelancerCompany::class,
        ],
        'messages' => [
            'driver' => 'eloquent',
            'model' => App\Model\Messaging\Message::class,
        ],

        // 'users' => [
        //     'driver' => 'database',
        //     'table' => 'users',
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Resetting Passwords
    |--------------------------------------------------------------------------
    |
    | You may specify multiple password reset configurations if you have more
    | than one user table or model in the application and you want to have
    | separate password reset settings based on the specific user types.
    |
    | The expire time is the number of minutes that the reset token should be
    | considered valid. This security feature keeps tokens short-lived so
    | they have less time to be guessed. You may change this as needed.
    |
    */

    'passwords' => [
        'freelancer' => [
            'provider' => 'freelancers',
            'table'    => 'password_resets',
            'expire'   => 60,
        ],
        'company' => [
            'provider' => 'companies',
            'table'    => 'password_resets',
            'expire'   => 60,
        ],
        'employer' => [
            'provider' => 'employers',
            'table'    => 'password_resets',
            'expire'   => 60,
        ],
        'users' => [
            'provider' => 'users',
            'table' => 'password_resets',
            'expire' => 60,
            'throttle' => 60,
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Password Confirmation Timeout
    |--------------------------------------------------------------------------
    |
    | Here you may define the amount of seconds before a password confirmation
    | times out and the user is prompted to re-enter their password via the
    | confirmation screen. By default, the timeout lasts for three hours.
    |
    */

    'password_timeout' => 10800,
    'employer_master_email' => env('EMPLOYER_MASTER_EMAIL', null),
    'employer_master_password' => env('EMPLOYER_MASTER_PASSWORD', null),
    'freelancer_master_email' => env('FREELANCER_MASTER_EMAIL', null),
    'freelancer_master_password' => env('FREELANCER_MASTER_PASSWORD', null),


    'passport' =>[
        'base_url' => env('PASSPORT_BASE_URL', null),
        'freelancer' => [
            'client_id' => env('FREELANCER_CLIENT_ID', null),
            'client_secret' => env('FREELANCER_CLIENT_SECRET', null),
            'grant_type' => env('FREELANCER_GRANT_TYPE', 'password'),
        ],
        'employer' => [
            'client_id' => env('EMPLOYER_CLIENT_ID', null),
            'client_secret' => env('EMPLOYER_CLIENT_SECRET', null),
            'grant_type' => env('EMPLOYER_GRANT_TYPE', 'password'),
        ],
        'company' => [
            'client_id' => env('COMPANY_CLIENT_ID', null),
            'client_secret' => env('COMPANY_CLIENT_SECRET', null),
            'grant_type' => env('COMPANY_GRANT_TYPE', 'password'),
        ],
        'message' => [
            'client_id' => env('MESSAGE_CLIENT_ID', null),
            'client_secret' => env('MESSAGE_CLIENT_SECRET', null),
            'grant_type' => env('MESSAGE_GRANT_TYPE', 'password'),
        ]
    ]
];
