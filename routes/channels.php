<?php

use App\Container\Messaging\MessageableInterface;
use App\Model\Messaging\Conversation;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('conversation.{conversation}', function (MessageableInterface $user, Conversation $conversation) {
    if (! empty($conversation->currentParticipant)) {
        return [
            'id' => $conversation->currentParticipant->participant_id,
            'name' => $user->full_name,
        ];
    }
});

Broadcast::channel('notifications.{memberType}.{memberId}', function (MessageableInterface $user, string $memberType, string $memberId) {
    if ($user->member_type === $memberType && $user->id === $memberId) {
        return [
            'id' => $user->id,
            'name' => $user->full_name,
        ];
    }
});
