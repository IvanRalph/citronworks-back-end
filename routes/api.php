<?php

use App\Http\Controllers\Api\v1\Billing\BillingController;
use App\Http\Controllers\Api\v1\Employer\ApplicationController;
use App\Http\Controllers\Api\v1\Employer\BillingController as EmployerBillingController;
use App\Http\Controllers\Api\v1\Employer\EmployerController;
use App\Http\Controllers\Api\v1\Employer\FreelancerController;
use App\Http\Controllers\Api\v1\Employer\JobController as EmployerJobController;
use App\Http\Controllers\Api\v1\Employer\JobInviteController;
use App\Http\Controllers\Api\v1\Freelancer\JobController as FreelancerJobController;
use App\Http\Controllers\Api\v1\Freelancer\MyProfileController;
use App\Http\Controllers\SignUp\EmployerController as EmployerSignUpController;
use App\Http\Controllers\SignUp\FreelancerController as FreelancerSignUpController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the 'api' middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function () {
    Route::namespace('v1')->prefix('v1')->group(function () {
        Route::prefix('signup')->group(function () {
            Route::prefix('freelancer')->group(function () {
                Route::post('/', [FreelancerSignUpController::class, 'store'])->middleware('guest');
                Route::get('location', [FreelancerSignUpController::class, 'location']);
                Route::middleware('auth:freelancer')->group(function () {
                    Route::put('my', [FreelancerSignUpController::class, 'update']);
                    Route::get('my', [FreelancerSignUpController::class, 'show']);
                });
            });

            /**
             * api/v1/employer.
             */
            Route::prefix('employer')->group(function () {
                Route::post('/', [EmployerSignUpController::class, 'store'])
                    ->middleware('guest');
                Route::middleware('auth:employer')->group(function () {
                    Route::put('my', [EmployerSignUpController::class, 'update']);
                    Route::get('my', [EmployerController::class, 'show']);
                });
            });
        });
    });
});

Route::group([
    'middleware' => 'guest',
    'namespace' => 'Api',
], function () {
    /* API -> v1 */
    Route::group([
        'namespace' => 'v1',
        'as' => '.v1',
        'prefix' => 'v1',
    ], function () {
        /* API -> v1 -> Referrer */
        Route::group([
            'namespace' => 'Referrer',
        ], function () {
            Route::post('/referrer', 'ReferrerController@index');
            Route::post('/return-visit', 'ReferrerController@return');
        });

        /* API -> v1  -> Braintree Webhook*/
        Route::group([
            'namespace' => 'Billing',
            'prefix' => 'braintree',
            'as' => '.braintree',
        ], function () {
            Route::post('/webhook', [
                'as' => '.webhook',
                'uses' => 'BillingWebhookController',
            ]);
        });

        /* API -> v1  -> Login */
        Route::group([
            'namespace' => 'Auth',
            'prefix' => 'oauth',
            'as' => '.auth',
        ], function () {
            /* API -> v1  -> Auth */
            Route::post('', [
                'as' => '.login',
                'uses' => 'LoginController@index',
            ]);

            Route::post('/change-password/{provider}/{id}', [
                'as' => '.change-password',
                'uses' => 'ChangePasswordController@index',
            ]);

            Route::post('password/email', [
                'as' => '.password.email',
                'uses' => 'ForgotPasswordController@sendResetLinkEmail',
            ]);

            Route::post('password/reset', [
                'as' => '.password.reset',
                'uses' => 'ResetPasswordController@reset',
            ]);

            Route::post('verify/{token}', [
                'as' => '.verify-email',
                'uses' => 'LoginController@verifyEmail',
            ]);
        });

        /* API -> v1  -> Contact */
        Route::group([
            'namespace' => 'Contact',
            'prefix' => 'contact',
            'as' => '.contact',
        ], function () {
            Route::post('', [
                'as' => '.store',
                'uses' => 'ContactController@store',
            ]);
        });

        /* API -> v1  -> Employer */
        Route::group([
            'namespace' => 'Employer',
            'prefix' => 'employer',
            'as' => '.employer',
        ], function () {
            Route::get('get/free-subscriptions', [EmployerBillingController::class, 'freeSubscriptions']);

            /* API -> v1  -> Employer -> register*/
            Route::group([
                'prefix' => 'register',
                'as' => '.register',
            ], function () {
                Route::post('/next/invite', [
                    'as' => '.invite',
                    'uses' => 'EmployerController@nextStepRegistrationInvitation',
                ]);
                Route::post('/next/upload-logo', [
                    'as' => '.invite',
                    'uses' => 'EmployerController@nextStepRegistrationUploadLogo',
                ]);
            });

            /* API -> v1  -> Employer -> update*/
            Route::group([
                'prefix' => 'update',
                'as' => '.update',
            ], function () {
                Route::post('/next/upload-logo/{employerId}', [
                    'as' => '.logo',
                    'uses' => 'EmployerController@nextStepRegistrationUploadLogo',
                ]);
                Route::post('/profile', [
                    'as' => '.update.profile',
                    'uses' => 'EmployerController@updateEmployerProfile',
                ]);
                Route::post('/company-logo', [
                    'as' => '.update.company-logo',
                    'uses' => 'EmployerController@updateCompanyLogo',
                ]);
                Route::post('/company-profile', [
                    'as' => '.update.company-logo',
                    'uses' => 'EmployerController@updateCompanyProfile',
                ]);
            });

            /* API -> v1  -> Employer -> concierge */
            Route::group([
                'prefix' => 'concierge',
            ], function () {
                Route::post('/request', [
                    'as' => '.concierge.request',
                    'uses' => 'ConciergeRequestController@store',
                ]);
            });

            Route::group([
                'prefix' => 'citronworkers',
            ], function () {
                Route::post('/request', [
                    'as' => '.citronworkers.request',
                    'uses' => 'CitronworkersController@store',
                ]);
            });
        });

        Route::group([
            'namespace' => 'Billing',
            'prefix' => 'payment',
            'as' => '.billing',
        ], function () {
            Route::get('token', [
                'as' => '.token',
                'uses' => 'BillingController@token',
            ]);

            Route::get('/test/{employerid}', [
                'as' => '.test',
                'uses' => 'BillingController@test',
            ]);
        });

        /* Api -> v1  -> freelancer company  */
        Route::group([
            'namespace' => 'FreelancerCompany',
            'prefix' => 'freelancer-company',
            'as' => '.freelancer.company',
        ], function () {
            /* Api -> v1  -> freelancer -> request */

            Route::post('/{freelancerCoId}', [
                'as' => '.fco-update',
                'uses' => 'FreelancerCompanyController@updateProfile',
            ]);

            Route::group([
                'prefix' => 'register',
            ], function () {
                Route::post('', [
                    'as' => '.register',
                    'uses' => 'FreelancerCompanyController@registration',
                ]);
                Route::post('/next', [
                    'as' => '.registration-next',
                    'uses' => 'FreelancerCompanyController@nextRegistration',
                ]);
            });
        });

        /* Api -> v1  -> Invitation */
        Route::group([
            'namespace' => 'Presentation',
            'prefix' => 'presentation',
            'as' => '.presentation',
        ], function () {
            Route::post('/invite', [
                'as' => '.apply',
                'uses' => 'InviteController@store',
            ]);
        });
    });
});

// make a separate route group for job CRUD functions
// because we should only allow Employers to create and update jobs
Route::middleware('auth:employer')->namespace('Api')->group(function () {
    Route::prefix('v1')->namespace('v1')->group(function () {
        // protect subscriptions with auth:employer middleware
        Route::post('payment/subscribe', [BillingController::class, 'subscribe']);

        /**
         * api/v1/employer.
         */
        Route::prefix('employer')->namespace('Employer')->group(function () {
            Route::get('my-profile', [EmployerController::class, 'show']);
            Route::get('name-and-email', [EmployerController::class, 'showNameAndEmail']);
            Route::put('name-and-email', [EmployerController::class, 'updateNameAndEmail']);
            Route::get('my-company', [EmployerController::class, 'showCompanyInfo']);
            Route::put('my-company', [EmployerController::class, 'updateCompanyInfo']);
            Route::post('my-company/logo', [EmployerController::class, 'updateCompanyLogo']);
            Route::apiResource('jobs', 'JobController');
            Route::delete('jobs/{job}/deactivate', [EmployerJobController::class, 'deactivate']);
            Route::delete('jobs/{job}/reactivate', [EmployerJobController::class, 'reactivate']);
            Route::get('applications/{application}', [ApplicationController::class, 'show']);
            Route::get('attachment-info/{attachmentid}', [ApplicationController::class, 'info']);
            Route::get('attachment', [ApplicationController::class, 'attachment']);
            Route::get('bookmarked-freelancers', [EmployerController::class, 'bookmarkedFreelancers']);
            Route::post('bookmark-freelancers', [EmployerController::class, 'storeBookmarkedFreelancers']);
            Route::post('bookmark-freelancers/{id}/delete', [EmployerController::class, 'destroyBookmarkedFreelancers']);
        });
    });
});

Route::group([
    'middleware' => 'auth:employer', 'namespace' => 'Api',
], function () {
    /* API -> v1 */
    Route::group([
        'namespace' => 'v1',
        'as' => '.v1',
        'prefix' => 'v1',
    ], function () {
        /*Broadcast Api*/

        /* Api -> v1  -> Employer */
        Route::group([
            'namespace' => 'Employer',
            'prefix' => 'employer',
            'as' => '.employer',
        ], function () {
            /* Api -> v1  -> Employer -> Invoices */
            Route::get('invoices', [
                'as' => '.invoices',
                'uses' => 'BillingController@invoices',
            ]);
            Route::get('invoices/{id}', [
                'as' => '.invoices',
                'uses' => 'BillingController@findInvoice',
            ]);
            Route::get('invoices/download/{id}', [
                'as' => '.invoices',
                'uses' => 'BillingController@downloadInvoice',
            ]);

            Route::get('get/subscription', [
                'as' => '.subscription.get',
                'uses' => 'BillingController@getSubscription',
            ]);

            Route::get('subscription-name', [EmployerBillingController::class, 'getSubscriptionName']);

            Route::post('upgrade/subscription/{id}', [
                'as' => '.subscription.update',
                'uses' => 'BillingController@upgradeSubscription',
            ]);

            Route::post('card/card-update', [
                'as' => '.update.card',
                'uses' => 'BillingController@updateCard',
            ]);

            Route::post('pause/subscription', [
                'as' => '.pause.subscription',
                'uses' => 'BillingController@pauseSubscription',
            ]);

            Route::post('resume/subscription', [
                'as' => '.resume.subscription',
                'uses' => 'BillingController@resumeSubscription',
            ]);

            /* Api -> v1  -> employer -> Collection */
            Route::get('', [
                'as' => '.index',
                'uses' => 'EmployerController@index',
            ]);
        });
    });
});

/* Api -> v1  -> skills */
Route::group([
    /* 'middleware' =>
        'auth:freelancer', */
    'namespace' => 'Api',
], function () {
    /* API -> v1 */
    Route::group([
        'namespace' => 'v1',
        'as' => '.v1',
        'prefix' => 'v1',
    ], function () {
        Route::group([
            'namespace' => 'Skill',
            'prefix' => 'skill-proposals',
            'as' => '.skill-proposals',
        ], function () {
            /*      SKILL PROPOSALS GET      */
            Route::get('', [
                'as' => '.all',
                'uses' => 'SkillController@proposals',
            ]);

            Route::post('', [
                'as' => '.create',
                'uses' => 'SkillController@createProposal',
            ]);

            Route::get('{id}', [
                'as' => '.find',
                'uses' => 'SkillController@findProposal',
            ]);

            Route::put('{id}', [
                'as' => '.update',
                'uses' => 'SkillController@updateProposal',
            ]);

            Route::delete('{id}', [
                'as' => '.delete',
                'uses' => 'SkillController@deleteProposal',
            ]);

            Route::post('{id}/approve', [
                'as' => '.approve',
                'uses' => 'SkillController@approveProposal',
            ]);
        });

        Route::group([
            'namespace' => 'Skill',
            'prefix' => 'skills',
            'as' => '.skills',
        ], function () {
            Route::group([
                'prefix' => 'categories',
                'as' => '.categories',
            ], function () {
                Route::get('/', [
                    'as' => '.categories-index',
                    'uses' => 'CategoryController@index',
                ]);
                Route::get('/popular', [
                    'as' => '.categories-popular',
                    'uses' => 'CategoryController@popular',
                ]);
                Route::post('/', [
                    'as' => '.categories-store',
                    'uses' => 'CategoryController@store',
                ]);

                Route::post('/{id}', [
                    'as' => '.categories-update',
                    'uses' => 'CategoryController@update',
                ]);

                Route::delete('/{id}', [
                    'as' => '.categories-delete',
                    'uses' => 'CategoryController@destroy',
                ]);

                Route::get('/search', [
                    'as' => '.categories-search',
                    'uses' => 'CategoryController@search',
                ]);

                Route::get('/{id}', [
                    'as' => '.categories-show',
                    'uses' => 'CategoryController@show',
                ]);
            });

            Route::get('/', [
                'as' => '.skills-index',
                'uses' => 'SkillController@index',
            ]);

            Route::post('/', [
                'as' => '.skills-store',
                'uses' => 'SkillController@store',
            ]);

            Route::post('/related', [
                'as' => '.skills-related',
                'uses' => 'SkillController@storeRelatedSkills',
            ]);

            Route::post('/{id}', [
                'as' => '.skills-update',
                'uses' => 'SkillController@update',
            ]);

            Route::delete('/{id}', [
                'as' => '.skills-delete',
                'uses' => 'SkillController@destroy',
            ]);

            Route::get('/search', [
                'as' => '.skills-search',
                'uses' => 'SkillController@search',
            ]);

            Route::get('/{id}', [
                'as' => '.skills-show',
                'uses' => 'SkillController@show',
            ]);
        });
    });
});

Route::group([
    'middleware' => 'auth:company', 'namespace' => 'Api',
], function () {
    /* API -> v1 */
    Route::group([
        'namespace' => 'v1',
        'as' => '.v1',
        'prefix' => 'v1',
    ], function () {
        /* Api -> v1  -> Freelancer Company */
        Route::group([
            'namespace' => 'FreelancerCompany',
            'prefix' => 'freelancer-company',
            'as' => '.freelancer-company',
        ], function () {
            Route::post('/{freelancerCoId}', [
                'as' => '.fco-update',
                'uses' => 'FreelancerCompanyController@updateProfile',
            ]);

            // Route::get('/{email}', [
            //     'as' => '.show',
            //     'uses' => 'FreelancerCompanyController@show',
            // ]);

            /* Api -> v1  -> Freelancer Company -> invoices  */
            Route::get('invoices', [
                'as' => '.invoices',
                'uses' => 'BillingController@invoices',
            ]);
            Route::get('invoices/{id}', [
                'as' => '.invoices',
                'uses' => 'BillingController@findInvoice',
            ]);
            Route::get('invoices/download/{id}', [
                'as' => '.invoices',
                'uses' => 'BillingController@downloadInvoice',
            ]);

            Route::get('get/subscription', [
                'as' => '.subscription.get',
                'uses' => 'BillingController@getSubscription',
            ]);

            Route::post('upgrade/subscription/{id}', [
                'as' => '.subscription.update',
                'uses' => 'BillingController@upgradeSubscription',
            ]);

            Route::post('card/card-update', [
                'as' => '.update.card',
                'uses' => 'BillingController@updateCard',
            ]);

            Route::post('pause/subscription', [
                'as' => '.pause.subscription',
                'uses' => 'BillingController@pauseSubscription',
            ]);

            Route::post('resume/subscription', [
                'as' => '.resume.subscription',
                'uses' => 'BillingController@resumeSubscription',
            ]);

            Route::get('/{email}', [
                'as' => '.show',
                'uses' => 'FreelancerCompanyController@show',
            ]);
        });
    });
});

// make a separate route group for job search and freelancer view
// because we should only allow Freelancers to search jobs across employers
Route::middleware('auth:freelancer')->namespace('Api')->group(function () {
    Route::prefix('v1')->namespace('v1')->group(function () {
        Route::get('freelancers/my-profile', [MyProfileController::class, 'show']);

        /**
         * api/v1/freelancer.
         */
        Route::prefix('freelancer')->namespace('Freelancer')->group(function () {
            /**
             * api/v1/freelancer/my-profile.
             */
            Route::prefix('my-profile')->group(function () {
                Route::get('latest-hired-notice', [MyProfileController::class, 'latestHiredNotice']);
                Route::delete('latest-hired-notice', [MyProfileController::class, 'closeHiredNotice']);
                Route::get('bio', [MyProfileController::class, 'showBio']);
                Route::put('bio', [MyProfileController::class, 'updateBio']);
                Route::get('profile-photo', [MyProfileController::class, 'showProfilePhoto']);
                Route::post('profile-photo', [MyProfileController::class, 'updateProfilePhoto']);
                Route::get('address', [MyProfileController::class, 'showAddress']);
                Route::put('address', [MyProfileController::class, 'updateAddress']);
                Route::get('name-and-email', [MyProfileController::class, 'showNameAndEmail']);
                Route::put('name-and-email', [MyProfileController::class, 'updateNameAndEmail']);
            });
            Route::apiResource('my-skills', 'MySkillsController')->except(['store', 'show']);
            Route::apiResource('my-skill-proposals', 'MySkillProposalsController')->except('show');

            /**
             * api/v1/freelancer/jobs.
             */
            Route::prefix('jobs')->group(function () {
                Route::get('applied', [FreelancerJobController::class, 'applied']);
                Route::get('bookmarked', [FreelancerJobController::class, 'bookmarked']);
                Route::get('{job}', [FreelancerJobController::class, 'show']);
                Route::post('search', [FreelancerJobController::class, 'search']);
                Route::post('apply/{job}', [FreelancerJobController::class, 'apply']);
                Route::post('apply/{job}/upload', [FreelancerJobController::class, 'uploadFile']);
                Route::post('bookmark/{job}', [FreelancerJobController::class, 'bookmark']);
                Route::delete('bookmark/{job}', [FreelancerJobController::class, 'unBookmark']);
            });
        });
    });
});

Route::group(['middleware' => 'auth:freelancer', 'namespace' => 'Api'], function () {
    /* API -> v1 */
    Route::group(['namespace' => 'v1', 'as' => '.v1', 'prefix' => 'v1'], function () {
        /* Api -> v1  -> freelancer */
        Route::group([
            'namespace' => 'Freelancer',
            'prefix' => 'freelancer',
            'as' => '.freelancer',
        ], function () {
            Route::get('/', [
                'as' => '.freelancer-index',
                'uses' => 'FreelancerController@index',
            ]);

            Route::get('invoices', [
                'as' => '.invoices',
                'uses' => 'BillingController@invoices',
            ]);
            Route::get('invoices/{id}', [
                'as' => '.invoices',
                'uses' => 'BillingController@findInvoice',
            ]);
            Route::get('invoices/download/{id}', [
                'as' => '.invoices',
                'uses' => 'BillingController@downloadInvoice',
            ]);
            // Route::post('/update/profile/address/{freeLancerId}', [
            //     'as' => '.update.profile.address',
            //     'uses' => 'FreelancerController@updateAddress',
            // ]);
            Route::post('/update/profile/name/{freeLancerId}', [
                'as' => '.update-profile.name',
                'uses' => 'FreelancerController@updateNameAndEmail',
            ]);

            // Put before route get with parameter email
            Route::get('/{id}', [
                'as' => '.freelancer-get',
                'uses' => 'FreelancerController@find',
            ])->where(['id' => '[0-9]+']);

            Route::delete('/{id}', [
                'as' => '.freelancer-delete',
                'uses' => 'FreelancerController@destroy',
            ]);
        });
    });
});

// Messenger routes, DRY for both employer and freelancer
Route::namespace('Api\v1\Messenger')->prefix('v1/messenger')->group(function () {
    Route::middleware('auth:employer')->prefix('employer')->group(function () {
        require base_path('routes/api/messenger.php');
    });

    Route::middleware('auth:freelancer')->prefix('freelancer')->group(function () {
        require base_path('routes/api/messenger.php');
    });
});

Route::middleware('auth:employer')->namespace('Api')->group(function () {
    Route::namespace('v1\Employer')->prefix('v1')->group(function () {
        Route::get('freelancers/{freelancer}', [FreelancerController::class, 'show']);
        Route::post('freelancers/search', [FreelancerController::class, 'search']);
        Route::post('freelancers/{freelancer}/hire', [FreelancerController::class, 'hire']);
        Route::post('job_invites', [JobInviteController::class, 'store']);
    });
});
