<?php

use App\Http\Controllers\Api\V1\Messenger\ConversationsController;
use App\Http\Controllers\Api\V1\Messenger\MessagesController;

Route::apiResource('conversations', 'ConversationsController');
Route::apiResource('conversations.messages', 'MessagesController');
Route::post('conversations/auth', 'BroadcastController@auth');
Route::post('conversations/search', [ConversationsController::class, 'search']);
Route::post('messages/search', [MessagesController::class, 'search']);
