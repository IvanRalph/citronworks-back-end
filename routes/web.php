<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return App::version();
//});

// Local test controller
//Route::get('/test/{method}', 'Test\TestController@index');
// Route::get('/test/{method}', 'Test\TestController@index');
require_once base_path() . '/routes/admin/index.php';
