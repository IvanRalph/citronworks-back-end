<?php

Route::prefix('get')->group(function () {
    Route::prefix('stats')->group(function () {
        Route::get('freelancer-signup', 'Dashboard\DashboardController@_stats_freelancer_signups');
        Route::get('freelancer-signup-total-count', 'Dashboard\DashboardController@_stats_freelancer_total_signup');
        Route::get('freelancer-utms', 'Dashboard\DashboardController@_stats_freelancer_utms');

        Route::get('freelancer-countries', 'Dashboard\DashboardController@_stats_countries');
    });
});
