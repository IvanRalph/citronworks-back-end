<?php

Route::prefix('get')->group(function () {
    Route::get('freelancers', 'Freelancer\FreelancerController@showFreelancers');
    Route::get('freelancers/{name}', 'Freelancer\FreelancerController@showFreelancers');
    Route::get('freelancer-company/{name}', 'Freelancer\FreelancerController@freelancerCompany');
    Route::get('freelancer/{id}', 'Freelancer\FreelancerController@getInfo');
});
Route::prefix('delete')->group(function () {
    Route::delete('freelancer/{id}', 'Freelancer\FreelancerController@delete');
});
Route::prefix('patch')->group(function () {
    Route::patch('freelancer', 'Freelancer\FreelancerController@freelancer');
    Route::patch('freelancer-profile/{freelancer_id}', 'Freelancer\FreelancerController@updateProfile');
    Route::patch('freelancer-company', 'Freelancer\FreelancerController@patchFreelancerCompany');
});
Route::prefix('post')->group(function () {
    Route::post('freelancer', 'Freelancer\FreelancerController@create');
    Route::post('freelancer/photo', 'Freelancer\FreelancerController@createPhoto');
    Route::get('freelancer/get-freelancer-searched-by-skills', 'Freelancer\FreelancerController@downloadBySkills');
});
