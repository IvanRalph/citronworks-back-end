<?php

Route::prefix('get')->group(function () {
    Route::get('jobs/{name}', 'Jobs\JobsController@findByName');

    Route::get('jobs', 'Jobs\JobsController@show');
});

Route::prefix('patch')->group(function () {
    Route::patch('jobs', 'Jobs\JobsController@edit');
});
