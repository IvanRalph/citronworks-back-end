<?php

Route::prefix('admin')->group(function () {
    Route::prefix('post')->group(function () {
        Route::post('profile-photo', 'Auth\AuthController@createPhoto');
    });

    Route::prefix('patch')->group(function () {
        Route::patch('profile', 'Auth\AuthController@updateProfile');
        Route::patch('password', 'Auth\AuthController@updatePw');
    });
});
