<?php

Route::prefix('get')->group(function () {
    Route::get('skills', 'Skills\SkillsController@show');
    Route::get('skills/{name}', 'Skills\SkillsController@findByName');
    Route::get('category', 'Skills\SkillsController@showCategory');
    Route::get('categories', 'Skills\CategoryController@show');
    Route::get('all-categories', 'Skills\CategoryController@showAll');
   
    Route::get('skill-proposals', 'Skills\SkillsProposalController@index');
});
Route::prefix('patch')->group(function () {
    Route::patch('skill', 'Skills\SkillsController@edit');
    Route::patch('category', 'Skills\CategoryController@update');
});
Route::prefix('delete')->group(function () {
    Route::delete('skill/{skillid}', 'Skills\SkillsController@delete');
    Route::delete('category/{categoryId}', 'Skills\CategoryController@destroy');
    Route::delete('suggested-skill/{id}', 'Skills\SkillsProposalController@delete');
});
Route::prefix('post')->group(function () {
    Route::post('skill', 'Skills\SkillsController@post');
    Route::post('category', 'Skills\CategoryController@post');
    Route::post('suggested-skill', 'Skills\SkillsProposalController@suggestedSkill');
    Route::post('revert-suggested-skill', 'Skills\SkillsProposalController@revertSuggestedSkill');

    Route::post('get-all-skills', 'Skills\SkillsController@all');
});
