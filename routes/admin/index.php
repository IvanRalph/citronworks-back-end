<?php

// Auth::routes();
Route::get('cwadmin', 'Web\v1\Auth\AuthController@index');
Route::get('cwadmin/{vueRoutes}', 'Web\v1\IndexController@app')->where('vueRoutes', '^((?!web|api).)*$');

Route::group(['namespace' => 'Web\v1', 'prefix' => 'web/v1'], function () {
    require_once base_path() . '/routes/admin/auth.php';
    Route::group(['middleware' => 'auth'], function () {
        require_once base_path() . '/routes/admin/dashboard.php';
        require_once base_path() . '/routes/admin/admin.php';
        require_once base_path() . '/routes/admin/freelancers.php';
        require_once base_path() . '/routes/admin/skills.php';
        require_once base_path() . '/routes/admin/jobs.php';
        require_once base_path() . '/routes/admin/employers.php';
    });
});
