<?php

Route::post('login', 'Auth\AuthController@authenticate');
Route::get('verify-auth', 'Auth\AuthController@verify');
Route::get('logout', 'Auth\AuthController@logout');
