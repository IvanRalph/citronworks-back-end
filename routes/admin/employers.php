<?php
Route::prefix('get')->group(function () {
    Route::resource('employers', 'Employer\EmployerController');
});
